# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_editstylelinedialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_editStyleLineDialog(object):
    def setupUi(self, editStyleLineDialog):
        editStyleLineDialog.setObjectName(_fromUtf8("editStyleLineDialog"))
        editStyleLineDialog.resize(502, 479)
        editStyleLineDialog.setWindowTitle(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        editStyleLineDialog.setWindowIcon(icon)
        editStyleLineDialog.setStyleSheet(_fromUtf8("\n"
"/*\n"
"* GLOBAL font and background SETTINGS.\n"
"*/\n"
"\n"
"* {\n"
"    font-size: 11px;\n"
"    font-family: Tahoma, Verdana, Arial, sans-serif;\n"
"    background-color: white;\n"
"}\n"
"\n"
"/* \n"
"* COMBOBOXES - sets border shape, dimensions and colours.\n"
"*/\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
" \n"
"    min-width: 6em;\n"
"    background-color: #ffffff;\n"
"    color:black;\n"
"    \n"
"     border-color: silver;\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    padding: 1px 0px 1px 3px; /*This makes text colour work*/\n"
"}\n"
"\n"
"QComboBox:editable {\n"
"    /*This changes colour behind expand icon*/\n"
"    background-color: #f2f2f2; \n"
"}\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox::drop-down { \n"
"    /*This sets up style of combobox list */\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; \n"
"    border-top-right-radius: 2px; \n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QComboBox::down-arrow {\n"
"    /* Custom expand icon source and dimensions, width forces proportional height resize*/\n"
"    image: url(:/menuEditShape/resources/moveDown.png); \n"
"    width: 12px;\n"
"}\n"
"\n"
"QComboBox::down-arrow:on { \n"
"    /* This moves activated arrow by 1px */\n"
"    top: 1px;\n"
"    left: 1px;\n"
"}\n"
"\n"
"/* TABLE OF CONTENTS */\n"
"\n"
"QDockWidget {\n"
"    border:none;\n"
"}\n"
"\n"
"QDockWidget::title {\n"
"    text-align: left;\n"
"    border: 1px solid silver;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"    padding-left: 35px;\n"
"}\n"
"\n"
"/* GROUPBOXES */\n"
"\n"
"QGroupBox {\n"
"    background-color: white;\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
"    margin-top: 1px; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"   \n"
"}\n"
"\n"
"/* HEADERS */\n"
"\n"
"QHeaderView::section {\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                                  stop:0 #616161, stop: 0.5 #505050,\n"
"                                  stop: 0.6 #434343, stop:1 #656565);\n"
"    color: black;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QHeaderView::section:checked\n"
"{\n"
"    background-color: red;\n"
"}\n"
"\n"
"QHeaderView::down-arrow {\n"
"    image: url(down_arrow.png);\n"
"}\n"
"\n"
"QHeaderView::up-arrow {\n"
"    image: url(up_arrow.png);\n"
"}\n"
"\n"
"/* MENU */\n"
"\n"
"QMenuBar {\n"
"    /* These sets background colour of menus */\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                      stop:0 silver, stop:1 lightgray); \n"
"}\n"
"\n"
"QMenu {\n"
"    background-color: darkgray;\n"
"}\n"
"\n"
"QMenuBar::item {\n"
"    spacing: 3px; /* spacing between menu bar items */\n"
"    padding: 1px 4px;\n"
"    border-radius: 2px;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:selected { /* when selected using mouse or keyboard */\n"
"    background: darkgray;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"    background: #888888;\n"
"    color: black;\n"
"}\n"
"\n"
"\n"
"/* PUSHBUTTONS */\n"
"\n"
"QPushButton {\n"
"    border: 1px solid darkgray;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    min-height: 24px;\n"
"    max-height: 24px;\n"
"    min-width: 100px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:flat {\n"
"    border: none; /* no border for a flat push button */\n"
"}\n"
"\n"
"QPushButton:default {\n"
"    border-color: navy; /* make the default button prominent */\n"
"}\n"
"\n"
"/* RADIOBUTTONS */\n"
"\n"
"QRadioButton::indicator {\n"
"    width: 13px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"/* SCROLLBAR\n"
"* I can\'t see you, but i know you\'re there.\n"
"*/\n"
"\n"
"QScrollBar{\n"
"    width: 1px;\n"
"}\n"
"\n"
"\n"
"/* TABWIDGET */\n"
"\n"
"\n"
"QTabWidget::pane {\n"
"    /* Border settings for TabWidget */\n"
"    border-top: 2px solid #C2C7CB;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    /* This moves tab bar left by 5px */\n"
"    left: 5px; \n"
"}\n"
"\n"
"/* Styles single tab using tab sub-control, uses QTabBar instead of QTabWidget */\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 1px solid lightgrey; \n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"    min-width: 120px;\n"
"    padding: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"        background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                    stop: 0 #fafafa, stop: 0.4 #f4f4f4,\n"
"                    stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    border-color: darkgray;\n"
"    border-bottom-color: lightgray; \n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    /* Unselected tab seems to be smaller */\n"
"    margin-top: 2px; \n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    /* Push selected tabs 4px in both directions */\n"
"    margin-left: -4px;\n"
"    margin-right: -4px;\n"
"}\n"
"\n"
"QTabBar::tab:first:selected {\n"
"    /* But do not push the first one left since it would fall out of the window :( */\n"
"    margin-left: 0; \n"
"}\n"
"\n"
"QTabBar::tab:last:selected {\n"
"    /* Same for the last one just to make it feel safe */\n"
"    margin-right: 0;\n"
"}\n"
"\n"
"QTabBar::tab:only-one {\n"
"    /* If there\'s only one tab, it doesn\'t have to do anything */\n"
"    margin: 0; \n"
"}\n"
"/* TABLES */\n"
"QTableView {\n"
"    selection-background-color: qlineargradient(x1: 0, y1: 0, x2: 0.5, y2: 0.5,\n"
"                                                 stop: 0 silver, stop: 1 lightgray);\n"
"}\n"
"\n"
"QTableView QTableCornerButton::section {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 darkgray);\n"
"    border: 1px outset darkgray;\n"
"}\n"
"\n"
"\n"
"/* TOOLBAR WITH BASIC ICONS */\n"
"\n"
"QToolBar {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 silver);\n"
"    border: 0;\n"
"}\n"
"\n"
"QToolButton { \n"
"    /* All types of tool button */\n"
"    border: none; \n"
"       border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"}\n"
"\n"
"QToolButton[popupMode=\"1\"] { \n"
"    /* Only for MenuButtonPopup \n"
"    Make way for the popup button */\n"
"    padding-right: 20px; \n"
"}\n"
"\n"
"QToolButton:selected {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QToolButton:on {\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"    width: 28px;\n"
"    height: 28px;\n"
"}\n"
"\n"
"/* the subcontrols below are used only in the MenuButtonPopup mode */\n"
"QToolButton::menu-button {\n"
"    border: 1px solid lightgray;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    /*16px width + 4px for border = 20px allocated above */\n"
"    width: 16px;\n"
"}\n"
"\n"
"QToolButton::menu-arrow {\n"
"    image: url(resources/moveDown.png);\n"
"}\n"
"\n"
"QToolButton::menu-arrow:open {\n"
"   top: 1px; left: 1px; /* Shift it a bit */\n"
"}\n"
"\n"
"/* TOOLBOX */\n"
"\n"
"QToolBox::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                         stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                         stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border-radius: 2px;\n"
"    color: lightgray;\n"
"}\n"
"\n"
"/* TOOLTIP MESSAGE STYLE */\n"
"\n"
"QToolTip {\n"
"    padding: 5px;\n"
"}\n"
"\n"
"/* FRAME SETTINGS */\n"
"\n"
"QFrame{\n"
"    border: none;\n"
"    background-color: qlineargradient(x1: 1, y1: 0, x2: 0, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"}\n"
"\n"
"/* LABEL SETTINGS */\n"
"\n"
"QLabel {\n"
"    border:none;\n"
"}\n"
"\n"
"QLabel#label {\n"
"    padding-left:30px;\n"
"}\n"
"\n"
"QLabel#zoomLabel {\n"
"    padding-right:30px;\n"
"}\n"
"\n"
"/* SLIDER (PARAMETERS), USED MOSTLY FOR OPACITY */\n"
"QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 2px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 18px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"    position: absolute; /* absolutely position 4px from the left and right of the widget. setting margins on the widget should work too... */\n"
"    left: 4px; right: 4px;\n"
"}\n"
"\n"
"QSlider::handle:vertical {\n"
"    height: 10px;\n"
"    background: green;\n"
"    margin: 0 -4px; /* expand outside the groove */\n"
"}\n"
"\n"
"QSlider::add-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"\n"
"QSlider::sub-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"\n"
"  "))
        editStyleLineDialog.setSizeGripEnabled(True)
        editStyleLineDialog.setModal(True)
        self.gridLayout_2 = QtGui.QGridLayout(editStyleLineDialog)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setContentsMargins(-1, 20, -1, 20)
        self.gridLayout.setHorizontalSpacing(6)
        self.gridLayout.setVerticalSpacing(40)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.widthLabel = QtGui.QLabel(editStyleLineDialog)
        self.widthLabel.setObjectName(_fromUtf8("widthLabel"))
        self.gridLayout.addWidget(self.widthLabel, 0, 0, 1, 1)
        self.widthSpinBox = QtGui.QDoubleSpinBox(editStyleLineDialog)
        self.widthSpinBox.setMinimumSize(QtCore.QSize(0, 22))
        self.widthSpinBox.setObjectName(_fromUtf8("widthSpinBox"))
        self.gridLayout.addWidget(self.widthSpinBox, 0, 1, 1, 1)
        self.colorLabel = QtGui.QLabel(editStyleLineDialog)
        self.colorLabel.setObjectName(_fromUtf8("colorLabel"))
        self.gridLayout.addWidget(self.colorLabel, 1, 0, 1, 1)
        self.linePatternLabel = QtGui.QLabel(editStyleLineDialog)
        self.linePatternLabel.setObjectName(_fromUtf8("linePatternLabel"))
        self.gridLayout.addWidget(self.linePatternLabel, 2, 0, 1, 1)
        self.colorButton = QtGui.QPushButton(editStyleLineDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.colorButton.sizePolicy().hasHeightForWidth())
        self.colorButton.setSizePolicy(sizePolicy)
        self.colorButton.setMinimumSize(QtCore.QSize(102, 26))
        self.colorButton.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/neue/resources/rgb.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.colorButton.setIcon(icon1)
        self.colorButton.setObjectName(_fromUtf8("colorButton"))
        self.gridLayout.addWidget(self.colorButton, 1, 1, 1, 1)
        self.lineJoinLabel = QtGui.QLabel(editStyleLineDialog)
        self.lineJoinLabel.setObjectName(_fromUtf8("lineJoinLabel"))
        self.gridLayout.addWidget(self.lineJoinLabel, 4, 0, 1, 1)
        self.lineJoinComboBox = QtGui.QComboBox(editStyleLineDialog)
        self.lineJoinComboBox.setMinimumSize(QtCore.QSize(83, 22))
        self.lineJoinComboBox.setObjectName(_fromUtf8("lineJoinComboBox"))
        self.gridLayout.addWidget(self.lineJoinComboBox, 4, 1, 1, 1)
        self.lineCapComboBox = QtGui.QComboBox(editStyleLineDialog)
        self.lineCapComboBox.setMinimumSize(QtCore.QSize(83, 22))
        self.lineCapComboBox.setObjectName(_fromUtf8("lineCapComboBox"))
        self.gridLayout.addWidget(self.lineCapComboBox, 5, 1, 1, 1)
        self.lineCapLabel = QtGui.QLabel(editStyleLineDialog)
        self.lineCapLabel.setObjectName(_fromUtf8("lineCapLabel"))
        self.gridLayout.addWidget(self.lineCapLabel, 5, 0, 1, 1)
        self.opacityLabel = QtGui.QLabel(editStyleLineDialog)
        self.opacityLabel.setObjectName(_fromUtf8("opacityLabel"))
        self.gridLayout.addWidget(self.opacityLabel, 6, 0, 1, 1)
        self.opacitySpinBox = QtGui.QDoubleSpinBox(editStyleLineDialog)
        self.opacitySpinBox.setMinimumSize(QtCore.QSize(0, 22))
        self.opacitySpinBox.setObjectName(_fromUtf8("opacitySpinBox"))
        self.gridLayout.addWidget(self.opacitySpinBox, 6, 1, 1, 1)
        self.linePatternComboBox = QtGui.QComboBox(editStyleLineDialog)
        self.linePatternComboBox.setMinimumSize(QtCore.QSize(83, 22))
        self.linePatternComboBox.setObjectName(_fromUtf8("linePatternComboBox"))
        self.gridLayout.addWidget(self.linePatternComboBox, 2, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 1, 0, 1, 1)
        self.gridLayout_3 = QtGui.QGridLayout()
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem, 0, 1, 1, 1)
        self.cancelButton = QtGui.QPushButton(editStyleLineDialog)
        self.cancelButton.setMinimumSize(QtCore.QSize(102, 26))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFile/resources/cancel.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.cancelButton.setIcon(icon2)
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.gridLayout_3.addWidget(self.cancelButton, 0, 3, 1, 1)
        self.applyButton = QtGui.QPushButton(editStyleLineDialog)
        self.applyButton.setMinimumSize(QtCore.QSize(102, 26))
        self.applyButton.setObjectName(_fromUtf8("applyButton"))
        self.gridLayout_3.addWidget(self.applyButton, 0, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout_3, 2, 0, 1, 1)

        self.retranslateUi(editStyleLineDialog)
        QtCore.QMetaObject.connectSlotsByName(editStyleLineDialog)

    def retranslateUi(self, editStyleLineDialog):
        self.widthLabel.setText(_translate("editStyleLineDialog", "Width", None))
        self.colorLabel.setText(_translate("editStyleLineDialog", "Color", None))
        self.linePatternLabel.setText(_translate("editStyleLineDialog", "Outline pattern", None))
        self.lineJoinLabel.setText(_translate("editStyleLineDialog", "Line Join", None))
        self.lineCapLabel.setText(_translate("editStyleLineDialog", "Line Cap", None))
        self.opacityLabel.setText(_translate("editStyleLineDialog", "Opacity", None))
        self.cancelButton.setText(_translate("editStyleLineDialog", "Cancel", None))
        self.applyButton.setText(_translate("editStyleLineDialog", "Apply", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    editStyleLineDialog = QtGui.QDialog()
    ui = Ui_editStyleLineDialog()
    ui.setupUi(editStyleLineDialog)
    editStyleLineDialog.show()
    sys.exit(app.exec_())

