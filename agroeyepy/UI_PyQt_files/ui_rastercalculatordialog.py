# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_rastercalculatordialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_rasterCalculatorDialog(object):
    def setupUi(self, rasterCalculatorDialog):
        rasterCalculatorDialog.setObjectName(_fromUtf8("rasterCalculatorDialog"))
        rasterCalculatorDialog.resize(869, 525)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        rasterCalculatorDialog.setWindowIcon(icon)
        rasterCalculatorDialog.setStyleSheet(_fromUtf8("\n"
"*/*\n"
"* GLOBAL font and background SETTINGS.\n"
"*/\n"
"\n"
"* {\n"
"    font-size: 11px;\n"
"    font-family: Tahoma, Verdana, Arial, sans-serif;\n"
"    background-color: white;\n"
"}\n"
"\n"
"/* \n"
"* COMBOBOXES - sets border shape, dimensions and colours.\n"
"*/\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
" \n"
"    min-width: 6em;\n"
"    background-color: #ffffff;\n"
"    color:black;\n"
"    \n"
"     border-color: silver;\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    padding: 1px 0px 1px 3px; /*This makes text colour work*/\n"
"}\n"
"\n"
"QComboBox:editable {\n"
"    /*This changes colour behind expand icon*/\n"
"    background-color: #f2f2f2; \n"
"}\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox::drop-down { \n"
"    /*This sets up style of combobox list */\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; \n"
"    border-top-right-radius: 2px; \n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QComboBox::down-arrow {\n"
"    /* Custom expand icon source and dimensions, width forces proportional height resize*/\n"
"    image: url(:/menuEditShape/resources/moveDown.png); \n"
"    width: 12px;\n"
"}\n"
"\n"
"QComboBox::down-arrow:on { \n"
"    /* This moves activated arrow by 1px */\n"
"    top: 1px;\n"
"    left: 1px;\n"
"}\n"
"\n"
"/* TABLE OF CONTENTS */\n"
"\n"
"QDockWidget {\n"
"    border:none;\n"
"}\n"
"\n"
"QDockWidget::title {\n"
"    text-align: left;\n"
"    border: 1px solid silver;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"    padding-left: 35px;\n"
"}\n"
"\n"
"/* GROUPBOXES */\n"
"\n"
"QGroupBox {\n"
"    background-color: white;\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
"    margin-top: 1px; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"   \n"
"}\n"
"\n"
"/* HEADERS */\n"
"\n"
"QHeaderView::section {\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                                  stop:0 #616161, stop: 0.5 #505050,\n"
"                                  stop: 0.6 #434343, stop:1 #656565);\n"
"    color: black;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QHeaderView::section:checked\n"
"{\n"
"    background-color: red;\n"
"}\n"
"\n"
"QHeaderView::down-arrow {\n"
"    image: url(down_arrow.png);\n"
"}\n"
"\n"
"QHeaderView::up-arrow {\n"
"    image: url(up_arrow.png);\n"
"}\n"
"\n"
"/* MENU */\n"
"\n"
"QMenuBar {\n"
"    /* These sets background colour of menus */\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                      stop:0 silver, stop:1 lightgray); \n"
"}\n"
"\n"
"QMenu {\n"
"    background-color: darkgray;\n"
"}\n"
"\n"
"QMenuBar::item {\n"
"    spacing: 3px; /* spacing between menu bar items */\n"
"    padding: 1px 4px;\n"
"    border-radius: 2px;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:selected { /* when selected using mouse or keyboard */\n"
"    background: darkgray;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"    background: #888888;\n"
"    color: black;\n"
"}\n"
"\n"
"\n"
"/* PUSHBUTTONS */\n"
"\n"
"QPushButton {\n"
"    border: 1px solid darkgray;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    min-width: 24px;\n"
"    max-width: 24px;\n"
"    min-height: 24px;\n"
"    max-height: 24px;\n"
" \n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: black;\n"
"    left: 1px;\n"
"    top: 1px;\n"
"}\n"
"\n"
"QPushButton:flat {\n"
"    border: none; /* no border for a flat push button */\n"
"}\n"
"\n"
"QPushButton:default {\n"
"    border-color: navy; /* make the default button prominent */\n"
"}\n"
"\n"
"/* RADIOBUTTONS */\n"
"\n"
"QRadioButton::indicator {\n"
"    width: 13px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"/* SCROLLBAR\n"
"* I can\'t see you, but i know you\'re there.\n"
"*/\n"
"\n"
"QScrollBar{\n"
"    width: 1px;\n"
"}\n"
"\n"
"\n"
"/* TABWIDGET */\n"
"\n"
"\n"
"QTabWidget::pane {\n"
"    /* Border settings for TabWidget */\n"
"    border-top: 2px solid #C2C7CB;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    /* This moves tab bar left by 5px */\n"
"    left: 5px; \n"
"}\n"
"\n"
"/* Styles single tab using tab sub-control, uses QTabBar instead of QTabWidget */\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 1px solid lightgrey; \n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"    min-width: 120px;\n"
"    padding: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"        background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                    stop: 0 #fafafa, stop: 0.4 #f4f4f4,\n"
"                    stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    border-color: darkgray;\n"
"    border-bottom-color: lightgray; \n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    /* Unselected tab seems to be smaller */\n"
"    margin-top: 2px; \n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    /* Push selected tabs 4px in both directions */\n"
"    margin-left: -4px;\n"
"    margin-right: -4px;\n"
"}\n"
"\n"
"QTabBar::tab:first:selected {\n"
"    /* But do not push the first one left since it would fall out of the window :( */\n"
"    margin-left: 0; \n"
"}\n"
"\n"
"QTabBar::tab:last:selected {\n"
"    /* Same for the last one just to make it feel safe */\n"
"    margin-right: 0;\n"
"}\n"
"\n"
"QTabBar::tab:only-one {\n"
"    /* If there\'s only one tab, it doesn\'t have to do anything */\n"
"    margin: 0; \n"
"}\n"
"/* TABLES */\n"
"QTableView {\n"
"    selection-background-color: qlineargradient(x1: 0, y1: 0, x2: 0.5, y2: 0.5,\n"
"                                                 stop: 0 silver, stop: 1 lightgray);\n"
"}\n"
"\n"
"QTableView QTableCornerButton::section {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 darkgray);\n"
"    border: 1px outset darkgray;\n"
"}\n"
"\n"
"\n"
"/* TOOLBAR WITH BASIC ICONS */\n"
"\n"
"QToolBar {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 silver);\n"
"    border: 0;\n"
"}\n"
"\n"
"QToolButton { \n"
"    /* All types of tool button */\n"
"    border: none; \n"
"       border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"}\n"
"\n"
"QToolButton[popupMode=\"1\"] { \n"
"    /* Only for MenuButtonPopup \n"
"    Make way for the popup button */\n"
"    padding-right: 20px; \n"
"}\n"
"\n"
"QToolButton:selected {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QToolButton:on {\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"    width: 28px;\n"
"    height: 28px;\n"
"}\n"
"\n"
"/* the subcontrols below are used only in the MenuButtonPopup mode */\n"
"QToolButton::menu-button {\n"
"    border: 1px solid lightgray;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    /*16px width + 4px for border = 20px allocated above */\n"
"    width: 16px;\n"
"}\n"
"\n"
"QToolButton::menu-arrow {\n"
"    image: url(resources/moveDown.png);\n"
"}\n"
"\n"
"QToolButton::menu-arrow:open {\n"
"   top: 1px; left: 1px; /* Shift it a bit */\n"
"}\n"
"\n"
"/* TOOLBOX */\n"
"\n"
"QToolBox::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                         stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                         stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border-radius: 2px;\n"
"    color: lightgray;\n"
"}\n"
"\n"
"/* TOOLTIP MESSAGE STYLE */\n"
"\n"
"QToolTip {\n"
"    padding: 5px;\n"
"}\n"
"\n"
"/* FRAME SETTINGS */\n"
"\n"
"QFrame{\n"
"    border: none;\n"
"    background-color: qlineargradient(x1: 1, y1: 0, x2: 0, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"}\n"
"\n"
"/* LABEL SETTINGS */\n"
"\n"
"QLabel {\n"
"    border:none;\n"
"    background-color: transparent;\n"
"}\n"
"\n"
"QLabel#label {\n"
"    padding-left:30px;\n"
"}\n"
"\n"
"QLabel#zoomLabel {\n"
"    padding-right:30px;\n"
"}\n"
"\n"
"/* SLIDER (PARAMETERS), USED MOSTLY FOR OPACITY */\n"
"QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 2px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 18px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"    position: absolute; /* absolutely position 4px from the left and right of the widget. setting margins on the widget should work too... */\n"
"    left: 4px; right: 4px;\n"
"}\n"
"\n"
"QSlider::handle:vertical {\n"
"    height: 10px;\n"
"    background: green;\n"
"    margin: 0 -4px; /* expand outside the groove */\n"
"}\n"
"\n"
"QSlider::add-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"\n"
"QSlider::sub-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"QPlainTextEdit {\n"
"    background-color: transparent;\n"
"}\n"
"QListView {\n"
"    background-color: transparent;\n"
"}\n"
"  "))
        rasterCalculatorDialog.setSizeGripEnabled(False)
        self.verticalLayout = QtGui.QVBoxLayout(rasterCalculatorDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.groupBox = QtGui.QGroupBox(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setMinimumSize(QtCore.QSize(300, 0))
        self.groupBox.setStyleSheet(_fromUtf8("QGroupBox::title {\n"
"padding-top: -9px;\n"
"}"))
        self.groupBox.setTitle(_fromUtf8(""))
        self.groupBox.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.rasterBandListView = QtGui.QListView(self.groupBox)
        self.rasterBandListView.setObjectName(_fromUtf8("rasterBandListView"))
        self.verticalLayout_3.addWidget(self.rasterBandListView)
        self.gridLayout_3.addLayout(self.verticalLayout_3, 1, 0, 1, 1)
        self.label_5 = QtGui.QLabel(self.groupBox)
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout_3.addWidget(self.label_5, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox, 4, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 1, 0, 1, 1)
        self.calculatorLayout = QtGui.QGridLayout()
        self.calculatorLayout.setContentsMargins(6, -1, -1, -1)
        self.calculatorLayout.setHorizontalSpacing(12)
        self.calculatorLayout.setVerticalSpacing(0)
        self.calculatorLayout.setObjectName(_fromUtf8("calculatorLayout"))
        self.greatButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.greatButton.sizePolicy().hasHeightForWidth())
        self.greatButton.setSizePolicy(sizePolicy)
        self.greatButton.setMinimumSize(QtCore.QSize(26, 26))
        self.greatButton.setMaximumSize(QtCore.QSize(26, 26))
        self.greatButton.setAccessibleName(_fromUtf8(">"))
        self.greatButton.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/more.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.greatButton.setIcon(icon1)
        self.greatButton.setIconSize(QtCore.QSize(30, 30))
        self.greatButton.setObjectName(_fromUtf8("greatButton"))
        self.calculatorLayout.addWidget(self.greatButton, 2, 4, 1, 1)
        self.addButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.addButton.sizePolicy().hasHeightForWidth())
        self.addButton.setSizePolicy(sizePolicy)
        self.addButton.setMinimumSize(QtCore.QSize(26, 26))
        self.addButton.setMaximumSize(QtCore.QSize(26, 26))
        self.addButton.setAccessibleName(_fromUtf8("+"))
        self.addButton.setText(_fromUtf8(""))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/plus (2).png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addButton.setIcon(icon2)
        self.addButton.setIconSize(QtCore.QSize(30, 30))
        self.addButton.setObjectName(_fromUtf8("addButton"))
        self.calculatorLayout.addWidget(self.addButton, 1, 0, 1, 1)
        self.equalButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.equalButton.sizePolicy().hasHeightForWidth())
        self.equalButton.setSizePolicy(sizePolicy)
        self.equalButton.setMinimumSize(QtCore.QSize(26, 26))
        self.equalButton.setMaximumSize(QtCore.QSize(26, 26))
        self.equalButton.setAccessibleName(_fromUtf8("="))
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/equal.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.equalButton.setIcon(icon3)
        self.equalButton.setIconSize(QtCore.QSize(30, 30))
        self.equalButton.setObjectName(_fromUtf8("equalButton"))
        self.calculatorLayout.addWidget(self.equalButton, 2, 2, 1, 1)
        self.subButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.subButton.sizePolicy().hasHeightForWidth())
        self.subButton.setSizePolicy(sizePolicy)
        self.subButton.setMinimumSize(QtCore.QSize(26, 26))
        self.subButton.setMaximumSize(QtCore.QSize(26, 26))
        self.subButton.setAccessibleName(_fromUtf8("-"))
        self.subButton.setText(_fromUtf8(""))
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/minus (2).png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.subButton.setIcon(icon4)
        self.subButton.setIconSize(QtCore.QSize(30, 30))
        self.subButton.setObjectName(_fromUtf8("subButton"))
        self.calculatorLayout.addWidget(self.subButton, 1, 1, 1, 1)
        self.lessequalButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lessequalButton.sizePolicy().hasHeightForWidth())
        self.lessequalButton.setSizePolicy(sizePolicy)
        self.lessequalButton.setMinimumSize(QtCore.QSize(26, 26))
        self.lessequalButton.setMaximumSize(QtCore.QSize(26, 26))
        self.lessequalButton.setAccessibleName(_fromUtf8("<="))
        self.lessequalButton.setText(_fromUtf8(""))
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/lessequal.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lessequalButton.setIcon(icon5)
        self.lessequalButton.setIconSize(QtCore.QSize(30, 30))
        self.lessequalButton.setObjectName(_fromUtf8("lessequalButton"))
        self.calculatorLayout.addWidget(self.lessequalButton, 2, 1, 1, 1)
        self.divButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.divButton.sizePolicy().hasHeightForWidth())
        self.divButton.setSizePolicy(sizePolicy)
        self.divButton.setMinimumSize(QtCore.QSize(26, 26))
        self.divButton.setMaximumSize(QtCore.QSize(26, 26))
        self.divButton.setAccessibleName(_fromUtf8("/"))
        self.divButton.setText(_fromUtf8(""))
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/divide.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.divButton.setIcon(icon6)
        self.divButton.setIconSize(QtCore.QSize(30, 30))
        self.divButton.setObjectName(_fromUtf8("divButton"))
        self.calculatorLayout.addWidget(self.divButton, 1, 3, 1, 1)
        self.lessButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lessButton.sizePolicy().hasHeightForWidth())
        self.lessButton.setSizePolicy(sizePolicy)
        self.lessButton.setMinimumSize(QtCore.QSize(26, 26))
        self.lessButton.setMaximumSize(QtCore.QSize(26, 26))
        self.lessButton.setAccessibleName(_fromUtf8("<"))
        self.lessButton.setText(_fromUtf8(""))
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/less.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lessButton.setIcon(icon7)
        self.lessButton.setIconSize(QtCore.QSize(30, 30))
        self.lessButton.setObjectName(_fromUtf8("lessButton"))
        self.calculatorLayout.addWidget(self.lessButton, 2, 0, 1, 1)
        self.multButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.multButton.sizePolicy().hasHeightForWidth())
        self.multButton.setSizePolicy(sizePolicy)
        self.multButton.setMinimumSize(QtCore.QSize(26, 26))
        self.multButton.setMaximumSize(QtCore.QSize(26, 26))
        self.multButton.setAccessibleName(_fromUtf8("*"))
        self.multButton.setText(_fromUtf8(""))
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/multiply.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.multButton.setIcon(icon8)
        self.multButton.setIconSize(QtCore.QSize(30, 30))
        self.multButton.setObjectName(_fromUtf8("multButton"))
        self.calculatorLayout.addWidget(self.multButton, 1, 2, 1, 1)
        self.greatequalButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.greatequalButton.sizePolicy().hasHeightForWidth())
        self.greatequalButton.setSizePolicy(sizePolicy)
        self.greatequalButton.setMinimumSize(QtCore.QSize(26, 26))
        self.greatequalButton.setMaximumSize(QtCore.QSize(26, 26))
        self.greatequalButton.setAccessibleName(_fromUtf8(">="))
        self.greatequalButton.setText(_fromUtf8(""))
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/moreequal.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.greatequalButton.setIcon(icon9)
        self.greatequalButton.setIconSize(QtCore.QSize(30, 30))
        self.greatequalButton.setObjectName(_fromUtf8("greatequalButton"))
        self.calculatorLayout.addWidget(self.greatequalButton, 2, 3, 1, 1)
        self.openbracketButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.openbracketButton.sizePolicy().hasHeightForWidth())
        self.openbracketButton.setSizePolicy(sizePolicy)
        self.openbracketButton.setMinimumSize(QtCore.QSize(26, 26))
        self.openbracketButton.setMaximumSize(QtCore.QSize(26, 26))
        self.openbracketButton.setAccessibleName(_fromUtf8("("))
        self.openbracketButton.setText(_fromUtf8(""))
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/leftbracket.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.openbracketButton.setIcon(icon10)
        self.openbracketButton.setIconSize(QtCore.QSize(30, 30))
        self.openbracketButton.setObjectName(_fromUtf8("openbracketButton"))
        self.calculatorLayout.addWidget(self.openbracketButton, 4, 0, 1, 1)
        self.orButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.orButton.sizePolicy().hasHeightForWidth())
        self.orButton.setSizePolicy(sizePolicy)
        self.orButton.setMinimumSize(QtCore.QSize(26, 26))
        self.orButton.setMaximumSize(QtCore.QSize(26, 26))
        self.orButton.setAccessibleName(_fromUtf8("OR"))
        self.orButton.setText(_fromUtf8(""))
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/or.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.orButton.setIcon(icon11)
        self.orButton.setIconSize(QtCore.QSize(30, 30))
        self.orButton.setObjectName(_fromUtf8("orButton"))
        self.calculatorLayout.addWidget(self.orButton, 4, 4, 1, 1)
        self.sqrtButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sqrtButton.sizePolicy().hasHeightForWidth())
        self.sqrtButton.setSizePolicy(sizePolicy)
        self.sqrtButton.setMinimumSize(QtCore.QSize(26, 26))
        self.sqrtButton.setMaximumSize(QtCore.QSize(26, 26))
        self.sqrtButton.setAccessibleName(_fromUtf8("sqrt"))
        self.sqrtButton.setText(_fromUtf8(""))
        icon12 = QtGui.QIcon()
        icon12.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/squareroot.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sqrtButton.setIcon(icon12)
        self.sqrtButton.setIconSize(QtCore.QSize(30, 30))
        self.sqrtButton.setObjectName(_fromUtf8("sqrtButton"))
        self.calculatorLayout.addWidget(self.sqrtButton, 4, 2, 1, 1)
        self.closebracketButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.closebracketButton.sizePolicy().hasHeightForWidth())
        self.closebracketButton.setSizePolicy(sizePolicy)
        self.closebracketButton.setMinimumSize(QtCore.QSize(26, 26))
        self.closebracketButton.setMaximumSize(QtCore.QSize(26, 26))
        self.closebracketButton.setAccessibleName(_fromUtf8(")"))
        self.closebracketButton.setText(_fromUtf8(""))
        icon13 = QtGui.QIcon()
        icon13.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/rightbracket.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.closebracketButton.setIcon(icon13)
        self.closebracketButton.setIconSize(QtCore.QSize(30, 30))
        self.closebracketButton.setObjectName(_fromUtf8("closebracketButton"))
        self.calculatorLayout.addWidget(self.closebracketButton, 4, 1, 1, 1)
        self.andButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.andButton.sizePolicy().hasHeightForWidth())
        self.andButton.setSizePolicy(sizePolicy)
        self.andButton.setMinimumSize(QtCore.QSize(26, 26))
        self.andButton.setMaximumSize(QtCore.QSize(26, 26))
        self.andButton.setAccessibleName(_fromUtf8("AND"))
        self.andButton.setText(_fromUtf8(""))
        icon14 = QtGui.QIcon()
        icon14.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/and.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.andButton.setIcon(icon14)
        self.andButton.setIconSize(QtCore.QSize(30, 30))
        self.andButton.setObjectName(_fromUtf8("andButton"))
        self.calculatorLayout.addWidget(self.andButton, 4, 3, 1, 1)
        self.sinButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sinButton.sizePolicy().hasHeightForWidth())
        self.sinButton.setSizePolicy(sizePolicy)
        self.sinButton.setMinimumSize(QtCore.QSize(26, 26))
        self.sinButton.setMaximumSize(QtCore.QSize(26, 26))
        self.sinButton.setAccessibleName(_fromUtf8("sin"))
        self.sinButton.setText(_fromUtf8(""))
        icon15 = QtGui.QIcon()
        icon15.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/sine.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sinButton.setIcon(icon15)
        self.sinButton.setIconSize(QtCore.QSize(30, 30))
        self.sinButton.setShortcut(_fromUtf8(""))
        self.sinButton.setObjectName(_fromUtf8("sinButton"))
        self.calculatorLayout.addWidget(self.sinButton, 0, 1, 1, 1)
        self.cosButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cosButton.sizePolicy().hasHeightForWidth())
        self.cosButton.setSizePolicy(sizePolicy)
        self.cosButton.setMinimumSize(QtCore.QSize(26, 26))
        self.cosButton.setMaximumSize(QtCore.QSize(26, 26))
        self.cosButton.setAccessibleName(_fromUtf8("cos"))
        self.cosButton.setText(_fromUtf8(""))
        icon16 = QtGui.QIcon()
        icon16.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/cosine.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.cosButton.setIcon(icon16)
        self.cosButton.setIconSize(QtCore.QSize(30, 30))
        self.cosButton.setObjectName(_fromUtf8("cosButton"))
        self.calculatorLayout.addWidget(self.cosButton, 0, 2, 1, 1)
        self.tanButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tanButton.sizePolicy().hasHeightForWidth())
        self.tanButton.setSizePolicy(sizePolicy)
        self.tanButton.setMinimumSize(QtCore.QSize(26, 26))
        self.tanButton.setMaximumSize(QtCore.QSize(26, 26))
        self.tanButton.setAccessibleName(_fromUtf8("tan"))
        self.tanButton.setText(_fromUtf8(""))
        icon17 = QtGui.QIcon()
        icon17.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/tangent.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tanButton.setIcon(icon17)
        self.tanButton.setIconSize(QtCore.QSize(30, 30))
        self.tanButton.setObjectName(_fromUtf8("tanButton"))
        self.calculatorLayout.addWidget(self.tanButton, 0, 3, 1, 1)
        self.atanButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.atanButton.sizePolicy().hasHeightForWidth())
        self.atanButton.setSizePolicy(sizePolicy)
        self.atanButton.setMinimumSize(QtCore.QSize(26, 26))
        self.atanButton.setMaximumSize(QtCore.QSize(26, 26))
        self.atanButton.setAccessibleName(_fromUtf8("atan"))
        self.atanButton.setText(_fromUtf8(""))
        icon18 = QtGui.QIcon()
        icon18.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/artangent.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.atanButton.setIcon(icon18)
        self.atanButton.setIconSize(QtCore.QSize(30, 30))
        self.atanButton.setObjectName(_fromUtf8("atanButton"))
        self.calculatorLayout.addWidget(self.atanButton, 5, 3, 1, 1)
        self.acosButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.acosButton.sizePolicy().hasHeightForWidth())
        self.acosButton.setSizePolicy(sizePolicy)
        self.acosButton.setMinimumSize(QtCore.QSize(26, 26))
        self.acosButton.setMaximumSize(QtCore.QSize(26, 26))
        self.acosButton.setAccessibleName(_fromUtf8("acos"))
        self.acosButton.setText(_fromUtf8(""))
        icon19 = QtGui.QIcon()
        icon19.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/arcosine.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.acosButton.setIcon(icon19)
        self.acosButton.setIconSize(QtCore.QSize(30, 30))
        self.acosButton.setAutoDefault(True)
        self.acosButton.setObjectName(_fromUtf8("acosButton"))
        self.calculatorLayout.addWidget(self.acosButton, 5, 2, 1, 1)
        self.asinButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.asinButton.sizePolicy().hasHeightForWidth())
        self.asinButton.setSizePolicy(sizePolicy)
        self.asinButton.setMinimumSize(QtCore.QSize(26, 26))
        self.asinButton.setMaximumSize(QtCore.QSize(26, 26))
        self.asinButton.setAccessibleName(_fromUtf8("asin"))
        self.asinButton.setText(_fromUtf8(""))
        icon20 = QtGui.QIcon()
        icon20.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/arsine.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.asinButton.setIcon(icon20)
        self.asinButton.setIconSize(QtCore.QSize(30, 30))
        self.asinButton.setObjectName(_fromUtf8("asinButton"))
        self.calculatorLayout.addWidget(self.asinButton, 5, 1, 1, 1)
        self.powButton = QtGui.QPushButton(rasterCalculatorDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.powButton.sizePolicy().hasHeightForWidth())
        self.powButton.setSizePolicy(sizePolicy)
        self.powButton.setMinimumSize(QtCore.QSize(26, 26))
        self.powButton.setMaximumSize(QtCore.QSize(26, 26))
        self.powButton.setAccessibleName(_fromUtf8("!="))
        self.powButton.setText(_fromUtf8(""))
        icon21 = QtGui.QIcon()
        icon21.addPixmap(QtGui.QPixmap(_fromUtf8(":/mathIcons/resources/nonequal.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.powButton.setIcon(icon21)
        self.powButton.setIconSize(QtCore.QSize(30, 30))
        self.powButton.setObjectName(_fromUtf8("powButton"))
        self.calculatorLayout.addWidget(self.powButton, 1, 4, 1, 1)
        self.gridLayout.addLayout(self.calculatorLayout, 1, 1, 2, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.label = QtGui.QLabel(rasterCalculatorDialog)
        self.label.setMinimumSize(QtCore.QSize(0, 22))
        self.label.setStyleSheet(_fromUtf8("padding-left:1px;"))
        self.label.setScaledContents(False)
        self.label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label.setIndent(0)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.calculatorTextEdit = QtGui.QPlainTextEdit(rasterCalculatorDialog)
        self.calculatorTextEdit.setObjectName(_fromUtf8("calculatorTextEdit"))
        self.verticalLayout.addWidget(self.calculatorTextEdit)
        self.label_4 = QtGui.QLabel(rasterCalculatorDialog)
        self.label_4.setMinimumSize(QtCore.QSize(0, 22))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout.addWidget(self.label_4)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_2 = QtGui.QLabel(rasterCalculatorDialog)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_4.addWidget(self.label_2)
        self.maxLineEdit = QtGui.QLineEdit(rasterCalculatorDialog)
        self.maxLineEdit.setMinimumSize(QtCore.QSize(0, 22))
        self.maxLineEdit.setObjectName(_fromUtf8("maxLineEdit"))
        self.horizontalLayout_4.addWidget(self.maxLineEdit)
        self.label_3 = QtGui.QLabel(rasterCalculatorDialog)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_4.addWidget(self.label_3)
        self.minLineEdit = QtGui.QLineEdit(rasterCalculatorDialog)
        self.minLineEdit.setMinimumSize(QtCore.QSize(0, 22))
        self.minLineEdit.setObjectName(_fromUtf8("minLineEdit"))
        self.horizontalLayout_4.addWidget(self.minLineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.statusBar = QtGui.QLabel(rasterCalculatorDialog)
        self.statusBar.setText(_fromUtf8(""))
        self.statusBar.setObjectName(_fromUtf8("statusBar"))
        self.horizontalLayout_2.addWidget(self.statusBar)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.okButton = QtGui.QPushButton(rasterCalculatorDialog)
        self.okButton.setMinimumSize(QtCore.QSize(92, 26))
        self.okButton.setMaximumSize(QtCore.QSize(92, 26))
        self.okButton.setStyleSheet(_fromUtf8("min-width: 90px;\n"
"max-width: 90px;\n"
"min-height: 24px;\n"
"max-height: 24px;"))
        self.okButton.setObjectName(_fromUtf8("okButton"))
        self.horizontalLayout_2.addWidget(self.okButton)
        self.cancelButton = QtGui.QPushButton(rasterCalculatorDialog)
        self.cancelButton.setMinimumSize(QtCore.QSize(92, 26))
        self.cancelButton.setMaximumSize(QtCore.QSize(92, 26))
        self.cancelButton.setStyleSheet(_fromUtf8("min-width: 90px;\n"
"max-width: 90px;\n"
"min-height: 24px;\n"
"max-height: 24px;"))
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.horizontalLayout_2.addWidget(self.cancelButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(rasterCalculatorDialog)
        QtCore.QMetaObject.connectSlotsByName(rasterCalculatorDialog)

    def retranslateUi(self, rasterCalculatorDialog):
        rasterCalculatorDialog.setWindowTitle(_translate("rasterCalculatorDialog", "Raster calculator", None))
        self.label_5.setText(_translate("rasterCalculatorDialog", "Raster Bands:", None))
        self.label.setText(_translate("rasterCalculatorDialog", "Raster calculator expression: ", None))
        self.label_4.setText(_translate("rasterCalculatorDialog", "Display range:", None))
        self.label_2.setText(_translate("rasterCalculatorDialog", "Max:", None))
        self.label_3.setText(_translate("rasterCalculatorDialog", "Min:", None))
        self.okButton.setText(_translate("rasterCalculatorDialog", "OK", None))
        self.cancelButton.setText(_translate("rasterCalculatorDialog", "Cancel", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    rasterCalculatorDialog = QtGui.QDialog()
    ui = Ui_rasterCalculatorDialog()
    ui.setupUi(rasterCalculatorDialog)
    rasterCalculatorDialog.show()
    sys.exit(app.exec_())

