<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="pl_PL" sourcelanguage="">
<context>
    <name>AE_Window</name>
    <message>
        <location filename="MainWindow.py" line="945"/>
        <source>AgroEye</source>
        <translation>AgroEye</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="946"/>
        <source>Table of Contents</source>
        <translation>Tabela zawartości</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="947"/>
        <source>File</source>
        <translation>Plik</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="948"/>
        <source>View</source>
        <translation>Widok</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="949"/>
        <source>Zoom</source>
        <translation>Powiększenie</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="950"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="951"/>
        <source>Extensions</source>
        <translation>Dodatki</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="952"/>
        <source>Edit</source>
        <translation>Edycja</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="954"/>
        <source>Tools</source>
        <translation>Narzędzia</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="955"/>
        <source>GAEC</source>
        <translation>DKR</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="956"/>
        <source>Coordinates:</source>
        <translation>Współrzędne:</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="957"/>
        <source>System:</source>
        <translation>Układ:</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="958"/>
        <source>Zoom:</source>
        <translation>Skala:</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="959"/>
        <source>Enter EPSG code ...</source>
        <translation>Wprowadź kod EPSG...</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="981"/>
        <source>Open project</source>
        <translation>Otwórz projekt</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="962"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="963"/>
        <source>Save as</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="964"/>
        <source>Quit</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="966"/>
        <source>Zoom In (10%)</source>
        <translation>Powiększ (10%)</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="967"/>
        <source>Zoom Out (10%)</source>
        <translation>Pomniejsz (10%)</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="968"/>
        <source>Normal Size (100%)</source>
        <translation>Cały zasięg (100%)</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="969"/>
        <source>About AgroEye</source>
        <translation>O AgroEye</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="970"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="971"/>
        <source>Preferences</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="972"/>
        <source>Show Layers</source>
        <translation>Pokaż warstwy</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="974"/>
        <source>Show &amp;Layers</source>
        <translation>Pokaż &amp;warstwy</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="976"/>
        <source>Show Coordinates</source>
        <translation>Pokaż pasek współrzędnych</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="977"/>
        <source>Measure distance</source>
        <translation>Pomiar odległośći</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="978"/>
        <source>Add file</source>
        <translation>Dodaj plik</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="979"/>
        <source>Measure area</source>
        <translation>Pomiar powierzchni</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="980"/>
        <source>Pan Action</source>
        <translation>Przesuń widok</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="983"/>
        <source>Choose rules</source>
        <translation>Wybierz normę</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="984"/>
        <source>Raster Calculator</source>
        <translation>Kalkulator Rastra</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="982"/>
        <source>Create new Shapefile</source>
        <translation>Utwórz nową warstwę Shapefile</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="965"/>
        <source>Quit application</source>
        <translation>Wyjdź z Agroeye</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="953"/>
        <source>Change Language</source>
        <translation>Zmień język</translation>
    </message>
</context>
<context>
    <name>addColumnDialog</name>
    <message>
        <location filename="ui_addcolumndialog.py" line="502"/>
        <source>Position:</source>
        <translation>Pozycja wstawiania:</translation>
    </message>
    <message>
        <location filename="ui_addcolumndialog.py" line="503"/>
        <source>Name:</source>
        <translation>Nazwa kolumny:</translation>
    </message>
    <message>
        <location filename="ui_addcolumndialog.py" line="504"/>
        <source>Width:</source>
        <translation>Szerokość pola:</translation>
    </message>
    <message>
        <location filename="ui_addcolumndialog.py" line="505"/>
        <source>Type:</source>
        <translation>Typ danych:</translation>
    </message>
    <message>
        <location filename="ui_addcolumndialog.py" line="506"/>
        <source>Precision:</source>
        <translation>Dokładność:</translation>
    </message>
    <message>
        <location filename="ui_addcolumndialog.py" line="507"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_addcolumndialog.py" line="508"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="ui_addcolumndialog.py" line="501"/>
        <source>Add new field</source>
        <translation>Wstaw kolumnę</translation>
    </message>
</context>
<context>
    <name>addLayersDialog</name>
    <message>
        <location filename="ui_addlayersdialog.py" line="461"/>
        <source>Select layers to add</source>
        <translation>Wybierz warstwy wektorowe do dodania</translation>
    </message>
    <message>
        <location filename="ui_addlayersdialog.py" line="462"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="ui_addlayersdialog.py" line="463"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>addNewObjectToLayerDialog</name>
    <message>
        <location filename="ui_addnewobjecttolayer.py" line="478"/>
        <source>Add new object to layer</source>
        <translation>Dodaj nowy obiekt do warstwy</translation>
    </message>
    <message>
        <location filename="ui_addnewobjecttolayer.py" line="479"/>
        <source>Coordinates</source>
        <translation>Współrzędne</translation>
    </message>
    <message>
        <location filename="ui_addnewobjecttolayer.py" line="480"/>
        <source>Attribute </source>
        <translation>Pola</translation>
    </message>
</context>
<context>
    <name>addNewVectorLayer</name>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="521"/>
        <source>New Shapefile layer</source>
        <translation>Nowa warstwa Shapefile</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="522"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="523"/>
        <source>Point</source>
        <translation>Punkt</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="524"/>
        <source>Line</source>
        <translation>Linia</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="525"/>
        <source>Polygon</source>
        <translation>Poligon</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="526"/>
        <source>Coordinate System</source>
        <translation>Układ współrzędnych</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="527"/>
        <source>Attributes</source>
        <translation>Pola</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="528"/>
        <source>New</source>
        <translation>Nowe pole</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="529"/>
        <source>Delete</source>
        <translation>Usuń pole</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="530"/>
        <source>Rename</source>
        <translation>Zmień nazwę</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="531"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_addnewvectorlayerdialog.py" line="532"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>attributeTableDialog</name>
    <message>
        <location filename="ui_attributetabledialog.py" line="536"/>
        <source>Preview table</source>
        <translation>Podgląd tabeli</translation>
    </message>
    <message>
        <location filename="ui_attributetabledialog.py" line="537"/>
        <source>Fields</source>
        <translation>Kolumny</translation>
    </message>
    <message>
        <location filename="ui_attributetabledialog.py" line="538"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="ui_attributetabledialog.py" line="539"/>
        <source>Save as</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <location filename="ui_attributetabledialog.py" line="540"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>compositionRGBDialog</name>
    <message>
        <location filename="ui_compositiondialog.py" line="596"/>
        <source>RGB Composition</source>
        <translation>Kompozycja RGB</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="597"/>
        <source>Grayscale Composition</source>
        <translation>Kompozycja w skali szarości</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="598"/>
        <source>Channel Red</source>
        <translation>Kanał czerwony</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="599"/>
        <source>Channel Green</source>
        <translation>Kanał zielony</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="600"/>
        <source>Channel Blue</source>
        <translation>Kanał niebieski</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="601"/>
        <source>Grayscale Channel</source>
        <translation>Kanał szary</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="602"/>
        <source>Color Palette</source>
        <translation>Paleta kolorów</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="603"/>
        <source>Invert</source>
        <translation>Odwróć</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="604"/>
        <source>Opacity</source>
        <translation>Przezroczystość</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="605"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="ui_compositiondialog.py" line="606"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
<context>
    <name>delColumnDialog</name>
    <message>
        <location filename="ui_delcolumndialog.py" line="451"/>
        <source>Delete field</source>
        <translation>Usuń kolumnę</translation>
    </message>
    <message>
        <location filename="ui_delcolumndialog.py" line="452"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_delcolumndialog.py" line="453"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>editStyleLineDialog</name>
    <message>
        <location filename="ui_editstylelinedialog.py" line="510"/>
        <source>Width</source>
        <translation>Szerokość</translation>
    </message>
    <message>
        <location filename="ui_editstylelinedialog.py" line="511"/>
        <source>Color</source>
        <translation>Kolor</translation>
    </message>
    <message>
        <location filename="ui_editstylelinedialog.py" line="512"/>
        <source>Outline pattern</source>
        <translation>Styl obrysu</translation>
    </message>
    <message>
        <location filename="ui_editstylelinedialog.py" line="513"/>
        <source>Line Join</source>
        <translation>Styl połączenia</translation>
    </message>
    <message>
        <location filename="ui_editstylelinedialog.py" line="514"/>
        <source>Line Cap</source>
        <translation>Styl zakończenia</translation>
    </message>
    <message>
        <location filename="ui_editstylelinedialog.py" line="515"/>
        <source>Opacity</source>
        <translation>Przezroczystość</translation>
    </message>
    <message>
        <location filename="ui_editstylelinedialog.py" line="516"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="ui_editstylelinedialog.py" line="517"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
</context>
<context>
    <name>editStylePointDialog</name>
    <message>
        <location filename="ui_editstylepointdialog.py" line="513"/>
        <source>Outline Width</source>
        <translation>Szerokość obrysu</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="514"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="515"/>
        <source>Fill Color</source>
        <translation>Kolor wypełnienia</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="516"/>
        <source>Outline Color</source>
        <translation>Kolor obrysu</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="517"/>
        <source>Opacity</source>
        <translation>Przezroczystość</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="518"/>
        <source>Symbol</source>
        <translation>Symbol</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="519"/>
        <source>Angle</source>
        <translation>Obrót</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="520"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="ui_editstylepointdialog.py" line="521"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>editStylePolygonDialog</name>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="507"/>
        <source>Outline Width</source>
        <translation>Szerokość obrysu</translation>
    </message>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="508"/>
        <source>Fill Color</source>
        <translation>Kolor wypełnienia</translation>
    </message>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="509"/>
        <source>Outline Color</source>
        <translation>Kolor obrysu</translation>
    </message>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="510"/>
        <source>Fill pattern</source>
        <translation>Styl wypełnienia</translation>
    </message>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="511"/>
        <source>Outline pattern</source>
        <translation>Styl obrysu</translation>
    </message>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="512"/>
        <source>Opacity</source>
        <translation>Przezroczystość</translation>
    </message>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="513"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="ui_editstylepolygondialog.py" line="514"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>histogramDialog</name>
    <message>
        <location filename="ui_histogramdialog.py" line="733"/>
        <source>Histogram</source>
        <translation>Histogram</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="720"/>
        <source>Area of statistics</source>
        <translation>Obszar statystyk</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="724"/>
        <source>Entire Raster</source>
        <translation>Cały raster</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="725"/>
        <source>Current Extent</source>
        <translation>Wybrane zaznaczenie</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="723"/>
        <source>Base Area of Stretch</source>
        <translation>Powierzchnia rozciągania</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="726"/>
        <source>Standard Deviation</source>
        <translation>Odchylenie Standardowe</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="728"/>
        <source>Value</source>
        <translation>Wartość</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="729"/>
        <source>Percentage Clip</source>
        <translation>Przycięcie procentowe</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="730"/>
        <source>None</source>
        <translation>Brak</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="731"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="732"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="ui_histogramdialog.py" line="734"/>
        <source>Look-Up Table</source>
        <translation>Tablica podglądu</translation>
    </message>
</context>
<context>
    <name>identifyDialog</name>
    <message>
        <location filename="ui_identifydialog.py" line="464"/>
        <source>Identify</source>
        <translation>Informacja o obiekcie</translation>
    </message>
    <message>
        <location filename="ui_identifydialog.py" line="465"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
<context>
    <name>measureDistanceDialog</name>
    <message>
        <location filename="ui_measuredistancetool.py" line="569"/>
        <source>Measure distance</source>
        <translation>Pomiar odległośći</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="570"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="571"/>
        <source>Measuring type</source>
        <translation>Rodzaj pomiaru</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="572"/>
        <source>Tracking mouse</source>
        <translation>Śledzenie myszki</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="573"/>
        <source>By point</source>
        <translation>Przez punkty</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="574"/>
        <source>Add new point by</source>
        <translation>Dodawanie nowego punktu</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="575"/>
        <source>Double click</source>
        <translation>Podwójne kliknięcie</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="576"/>
        <source>Single click</source>
        <translation>Pojedyncze kliknięcie</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="577"/>
        <source>Units:</source>
        <translation>Jednostki:</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="580"/>
        <source>Clear</source>
        <translation>Wyczyść</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="578"/>
        <source>Distance:</source>
        <translation>Odległość:</translation>
    </message>
    <message>
        <location filename="ui_measuredistancetool.py" line="579"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
<context>
    <name>modelInputRasterDialog</name>
    <message>
        <location filename="ui_modelinputrasterdialog.py" line="466"/>
        <source>Input raster</source>
        <translation>Wejściowy raster</translation>
    </message>
    <message>
        <location filename="ui_modelinputrasterdialog.py" line="467"/>
        <source>Raster </source>
        <translation>Raster</translation>
    </message>
    <message>
        <location filename="ui_modelinputrasterdialog.py" line="468"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_modelinputrasterdialog.py" line="469"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>modelOutputDbDialog</name>
    <message>
        <location filename="ui_modeloutputdbdialog.py" line="471"/>
        <source>Save results as...</source>
        <translation>Zapisz wyniki jako...</translation>
    </message>
    <message>
        <location filename="ui_modeloutputdbdialog.py" line="472"/>
        <source>Save as</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <location filename="ui_modeloutputdbdialog.py" line="473"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_modeloutputdbdialog.py" line="474"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>modelSegmentationDialog</name>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="517"/>
        <source>Segmentation preferences</source>
        <translation>Ustawienia segmentacji</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="518"/>
        <source>File settings</source>
        <translation>Ustawienia plików</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="519"/>
        <source>Raster input:</source>
        <translation>Raster wejściowy:</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="520"/>
        <source>Segmentation result: </source>
        <translation>Wynik segmentacji:</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="521"/>
        <source>Segmentation settings</source>
        <translation>Ustawienia segmentacji</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="522"/>
        <source>Segmentation type:</source>
        <translation>Typ segmentacji:</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="523"/>
        <source>Object Size</source>
        <translation>Rozmiar obiektu</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="524"/>
        <source>Run</source>
        <translation>Uruchom</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="525"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_modelsegmentationdialog.py" line="526"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>modelStatisticsDialog</name>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="518"/>
        <source>Statistics preferences</source>
        <translation>Ustawienia statystyk</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="519"/>
        <source>File settings</source>
        <translation>Ustawienia plików</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="520"/>
        <source>Raster to statistics:</source>
        <translation>Pliki rastrowe do statystyk:</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="521"/>
        <source>Statistics settings</source>
        <translation>Ustawienia statystyk</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="522"/>
        <source>Mean</source>
        <translation>Średnia</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="523"/>
        <source>StdDev</source>
        <translation>Odch. Standardowe</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="524"/>
        <source>NDVI</source>
        <translation>NDVI</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="525"/>
        <source>Load from file</source>
        <translation>Wczytaj z pliku</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="526"/>
        <source>Compute new statistics</source>
        <translation>Oblicz nowe statystyki</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="527"/>
        <source>Run</source>
        <translation>Uruchom</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="528"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_modelstatisticsdialog.py" line="529"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>modelWindowDialog</name>
    <message>
        <location filename="ui_modeldialog.py" line="873"/>
        <source>GAEC rules</source>
        <translation>Normy Dobrej Kultury Rolnej</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="874"/>
        <source>Choose GEAC rules:</source>
        <translation>Wybierz normę DKR:</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="875"/>
        <source>GAEC N.01</source>
        <translation>Norma N.01</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="876"/>
        <source>GAEC N.03</source>
        <translation>Norma N.03</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="877"/>
        <source>GAEC N.04</source>
        <translation>Norma N.04</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="878"/>
        <source>GAEC N.05</source>
        <translation>Norma N.05</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="879"/>
        <source>GAEC N.06</source>
        <translation>Norma N.06</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="880"/>
        <source>GAEC N.07</source>
        <translation>Norma N.07</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="881"/>
        <source>GAEC N.12</source>
        <translation>Norma N.12</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="882"/>
        <source>GAEC N.13</source>
        <translation>Norma N.13</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="883"/>
        <source>GAEC N.14</source>
        <translation>Norma N.14</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="884"/>
        <source>GAEC N.15</source>
        <translation>Norma N.15</translation>
    </message>
    <message>
        <location filename="ui_modeldialog.py" line="885"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>preferencesDialog</name>
    <message>
        <location filename="ui_preferencesdialog.py" line="383"/>
        <source>Preferences</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="387"/>
        <source>View options</source>
        <translation>Ustawienia obszaru wyświetlania</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="384"/>
        <source>Default projection for loading layers (only if nothing has been loaded)</source>
        <translation>Domyślny układ współrzędnych dla nowych warstw (tylko gdy brak wczytanych warstw)</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="385"/>
        <source>Default from list</source>
        <translation>Użyj domyślnego układu współrzędnych</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="386"/>
        <source>Coordinate system as from first loaded layer</source>
        <translation>Użyj układu współrzędnych projektu</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="392"/>
        <source>Raster options</source>
        <translation>Ustawienia warstw rastrowych</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="388"/>
        <source>None</source>
        <translation>Brak</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="389"/>
        <source>Percentage Clip</source>
        <translation>Przycięcie procentowe</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="390"/>
        <source>Standard Deviation</source>
        <translation>Odchylenie Standardowe</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="391"/>
        <source>Default stretch for rasters: </source>
        <translation>Domyślne rozciąganie histogramu:</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="401"/>
        <source>Vector options</source>
        <translation>Ustawienia warstw wektorowych</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="393"/>
        <source>Symbol</source>
        <translation>Symbol</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="394"/>
        <source>Default size for vector</source>
        <translation>Domyślny rozmiar</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="395"/>
        <source>Fill</source>
        <translation>Wypełnienie</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="396"/>
        <source>Line</source>
        <translation>Styl linii</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="397"/>
        <source>Opacity</source>
        <translation>Przezroczystość</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="398"/>
        <source>Set default color</source>
        <translation>Użyj domyślnego koloru</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="399"/>
        <source>Set random color</source>
        <translation>Użyj losowego koloru</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="400"/>
        <source>Color settings</source>
        <translation>Ustawienia koloru</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="402"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="ui_preferencesdialog.py" line="403"/>
        <source>Close</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>progressDialog</name>
    <message>
        <location filename="ui_progressdialog.py" line="69"/>
        <source>Processing progress</source>
        <translation>Proces przetwarzania</translation>
    </message>
    <message>
        <location filename="ui_progressdialog.py" line="70"/>
        <source>Save Callback information</source>
        <translation>Zapisz informacje zwrotne</translation>
    </message>
    <message>
        <location filename="ui_progressdialog.py" line="71"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
<context>
    <name>propertiesDialog</name>
    <message>
        <location filename="ui_propertiesdialog.py" line="438"/>
        <source>Properties</source>
        <translation>Właściwości</translation>
    </message>
    <message>
        <location filename="ui_propertiesdialog.py" line="439"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
<context>
    <name>rasterCalculatorDialog</name>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="880"/>
        <source>Raster calculator</source>
        <translation>Kalkulator rastra</translation>
    </message>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="881"/>
        <source>Raster Bands:</source>
        <translation>Kanały rastrowe:</translation>
    </message>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="882"/>
        <source>Raster calculator expression: </source>
        <translation>Wyrażenie kalkulatora rastrrowego:</translation>
    </message>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="883"/>
        <source>Display range:</source>
        <translation>Zasięg wyświetlania:</translation>
    </message>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="884"/>
        <source>Max:</source>
        <translation>Maks:</translation>
    </message>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="885"/>
        <source>Min:</source>
        <translation>Min:</translation>
    </message>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="886"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_rastercalculatordialog.py" line="887"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>renameColumnDialog</name>
    <message>
        <location filename="ui_renamecolumndialog.py" line="461"/>
        <source>New name of column:</source>
        <translation>Nowa nazwa kolumny:</translation>
    </message>
    <message>
        <location filename="ui_renamecolumndialog.py" line="462"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_renamecolumndialog.py" line="463"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>segmentTableDialog</name>
    <message>
        <location filename="ui_segmentationtabledialog.py" line="488"/>
        <source>Segmentation results</source>
        <translation>Wynik segmentacji</translation>
    </message>
    <message>
        <location filename="ui_segmentationtabledialog.py" line="489"/>
        <source>Min: </source>
        <translation>Min:</translation>
    </message>
    <message>
        <location filename="ui_segmentationtabledialog.py" line="490"/>
        <source>Max:</source>
        <translation>Maks:</translation>
    </message>
    <message>
        <location filename="ui_segmentationtabledialog.py" line="491"/>
        <source>Run</source>
        <translation>Uruchom</translation>
    </message>
    <message>
        <location filename="ui_segmentationtabledialog.py" line="492"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
</TS>
