# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_attributetabledialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_attributeTableDialog(object):
    def setupUi(self, attributeTableDialog):
        attributeTableDialog.setObjectName(_fromUtf8("attributeTableDialog"))
        attributeTableDialog.resize(647, 418)
        attributeTableDialog.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        attributeTableDialog.setWindowTitle(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        attributeTableDialog.setWindowIcon(icon)
        attributeTableDialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        attributeTableDialog.setAutoFillBackground(False)
        attributeTableDialog.setStyleSheet(_fromUtf8("/*\n"
"* GLOBAL font and background SETTINGS.\n"
"*/\n"
"\n"
"* {\n"
"    font-size: 11px;\n"
"    font-family: Tahoma, Verdana, Arial, sans-serif;\n"
"    background-color: white;\n"
"}\n"
"\n"
"/* \n"
"* COMBOBOXES - sets border shape, dimensions and colours.\n"
"*/\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
" \n"
"    min-width: 6em;\n"
"    background-color: #ffffff;\n"
"    color:black;\n"
"    \n"
"     border-color: silver;\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    padding: 1px 0px 1px 3px; /*This makes text colour work*/\n"
"}\n"
"\n"
"QComboBox:editable {\n"
"    /*This changes colour behind expand icon*/\n"
"    background-color: #f2f2f2; \n"
"}\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox::drop-down { \n"
"    /*This sets up style of combobox list */\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; \n"
"    border-top-right-radius: 2px; \n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QComboBox::down-arrow {\n"
"    /* Custom expand icon source and dimensions, width forces proportional height resize*/\n"
"    image: url(:/menuEditShape/resources/moveDown.png); \n"
"    width: 12px;\n"
"}\n"
"\n"
"QComboBox::down-arrow:on { \n"
"    /* This moves activated arrow by 1px */\n"
"    top: 1px;\n"
"    left: 1px;\n"
"}\n"
"\n"
"/* TABLE OF CONTENTS */\n"
"\n"
"QDockWidget {\n"
"    border:none;\n"
"}\n"
"\n"
"QDockWidget::title {\n"
"    text-align: left;\n"
"    border: 1px solid silver;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"    padding-left: 35px;\n"
"}\n"
"\n"
"/* FRAME */\n"
"\n"
"QFrame {\n"
"     background-color: transparent;\n"
"}\n"
"\n"
"/* GROUPBOXES */\n"
"\n"
"QGroupBox {\n"
"    background-color: white;\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
"    margin-top: 1px; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"   \n"
"}\n"
"\n"
"/* HEADERS */\n"
"\n"
"QHeaderView::section {\n"
"    background-color: #43a047;\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QHeaderView::section:checked\n"
"{\n"
"    background-color: #43a047;\n"
"    color: white;\n"
"}\n"
"\n"
"QHeaderView::down-arrow {\n"
"    image: url(down_arrow.png);\n"
"}\n"
"\n"
"QHeaderView::up-arrow {\n"
"    image: url(up_arrow.png);\n"
"}\n"
"\n"
"/* MENU */\n"
"\n"
"QMenuBar {\n"
"    /* These sets background colour of menus */\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                      stop:0 silver, stop:1 lightgray); \n"
"}\n"
"\n"
"QMenu {\n"
"    background-color: darkgray;\n"
"}\n"
"\n"
"QMenuBar::item {\n"
"    spacing: 3px; /* spacing between menu bar items */\n"
"    padding: 1px 4px;\n"
"    border-radius: 2px;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:selected { /* when selected using mouse or keyboard */\n"
"    background: darkgray;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"    background: #888888;\n"
"    color: black;\n"
"}\n"
"\n"
"\n"
"/* PUSHBUTTONS */\n"
"\n"
"QPushButton {\n"
"    border: 1px solid darkgray;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    min-height: 24px;\n"
"    max-height: 24px;\n"
"    min-width: 100px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:flat {\n"
"    border: none; /* no border for a flat push button */\n"
"}\n"
"\n"
"QPushButton:default {\n"
"    border-color: navy; /* make the default button prominent */\n"
"}\n"
"\n"
"/* RADIOBUTTONS */\n"
"\n"
"QRadioButton::indicator {\n"
"    width: 13px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"/* SCROLLBAR\n"
"* I can\'t see you, but i know you\'re there.\n"
"*/\n"
"\n"
"QScrollBar{\n"
"    width: 12px;\n"
"}\n"
"\n"
"\n"
"/* TABWIDGET */\n"
"\n"
"\n"
"QTabWidget::pane {\n"
"    /* Border settings for TabWidget */\n"
"    border-top: 2px solid #C2C7CB;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    /* This moves tab bar left by 5px */\n"
"    left: 5px; \n"
"}\n"
"\n"
"/* Styles single tab using tab sub-control, uses QTabBar instead of QTabWidget */\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 1px solid lightgrey; \n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"    min-width: 120px;\n"
"    padding: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"        background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                    stop: 0 #fafafa, stop: 0.4 #f4f4f4,\n"
"                    stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    border-color: darkgray;\n"
"    border-bottom-color: lightgray; \n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    /* Unselected tab seems to be smaller */\n"
"    margin-top: 2px; \n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    /* Push selected tabs 4px in both directions */\n"
"    margin-left: -4px;\n"
"    margin-right: -4px;\n"
"}\n"
"\n"
"QTabBar::tab:first:selected {\n"
"    /* But do not push the first one left since it would fall out of the window :( */\n"
"    margin-left: 0; \n"
"}\n"
"\n"
"QTabBar::tab:last:selected {\n"
"    /* Same for the last one just to make it feel safe */\n"
"    margin-right: 0;\n"
"}\n"
"\n"
"QTabBar::tab:only-one {\n"
"    /* If there\'s only one tab, it doesn\'t have to do anything */\n"
"    margin: 0; \n"
"}\n"
"/* TABLES */\n"
"QTableView {\n"
"    selection-background-color: qlineargradient(x1: 0, y1: 0, x2: 0.5, y2: 0.5,\n"
"                                                 stop: 0 silver, stop: 1 lightgray);\n"
"}\n"
"\n"
"QTableView QTableCornerButton::section {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 darkgray);\n"
"    border: 1px outset darkgray;\n"
"}\n"
"\n"
"\n"
"/* TOOLBAR WITH BASIC ICONS */\n"
"\n"
"QToolBar {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 silver);\n"
"    border: 0;\n"
"}\n"
"\n"
"QToolButton { \n"
"    /* All types of tool button */\n"
"    border: 1px solid darkgray;\n"
"       border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    min-height: 24px;\n"
"    max-height: 24px;\n"
"    min-width: 100px;\n"
"}\n"
"QToolButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
" QToolButton:flat {\n"
"    border: none;\n"
"}\n"
" QToolButton:default {\n"
"    border-color: navy;\n"
" }\n"
"\n"
"QToolButton[popupMode=\"1\"] { \n"
"    /* Only for MenuButtonPopup \n"
"    Make way for the popup button */\n"
"    padding-right: 20px; \n"
"}\n"
"\n"
"QToolButton:selected {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QToolButton:on {\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"    width: 28px;\n"
"    height: 28px;\n"
"}\n"
"\n"
"/* the subcontrols below are used only in the MenuButtonPopup mode */\n"
"QToolButton::menu-button {\n"
"    border: 1px solid lightgray;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    /*16px width + 4px for border = 20px allocated above */\n"
"    width: 16px;\n"
"}\n"
"\n"
"QToolButton::menu-arrow {\n"
"    image: url(resources/moveDown.png);\n"
"}\n"
"\n"
"QToolButton::menu-arrow:open {\n"
"   top: 1px; left: 1px; /* Shift it a bit */\n"
"}\n"
"\n"
"/* TOOLBOX */\n"
"\n"
"QToolBox::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                         stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                         stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border-radius: 2px;\n"
"    color: lightgray;\n"
"}\n"
"\n"
"/* TOOLTIP MESSAGE STYLE */\n"
"\n"
"QToolTip {\n"
"    background-color: #fafafa;\n"
"    color: black;\n"
"    border: darkgray solid 1px;\n"
" }\n"
"\n"
"/* FRAME SETTINGS */\n"
"\n"
"QFrame{\n"
"    border: none;\n"
"    background-color: qlineargradient(x1: 1, y1: 0, x2: 0, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"}\n"
"\n"
"/* LABEL SETTINGS */\n"
"\n"
"QLabel {\n"
"    background-color: transparent;\n"
"}\n"
"\n"
"QLabel#label {\n"
"    padding-left:30px;\n"
"}\n"
"\n"
"QLabel#zoomLabel {\n"
"    padding-right:30px;\n"
"}\n"
"\n"
"/* SLIDER (PARAMETERS), USED MOSTLY FOR OPACITY */\n"
"QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 2px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 18px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"    position: absolute; /* absolutely position 4px from the left and right of the widget. setting margins on the widget should work too... */\n"
"    left: 4px; right: 4px;\n"
"}\n"
"\n"
"QSlider::handle:vertical {\n"
"    height: 10px;\n"
"    background: green;\n"
"    margin: 0 -4px; /* expand outside the groove */\n"
"}\n"
"\n"
"QSlider::add-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"\n"
"QSlider::sub-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"\n"
""))
        self.gridLayout = QtGui.QGridLayout(attributeTableDialog)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.attrTabWidget = QtGui.QTabWidget(attributeTableDialog)
        self.attrTabWidget.setFocusPolicy(QtCore.Qt.TabFocus)
        self.attrTabWidget.setObjectName(_fromUtf8("attrTabWidget"))
        self.attrWidget = QtGui.QWidget()
        self.attrWidget.setObjectName(_fromUtf8("attrWidget"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.attrWidget)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.attrGridLayout = QtGui.QGridLayout()
        self.attrGridLayout.setObjectName(_fromUtf8("attrGridLayout"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.attrTableWidget = QtGui.QTableWidget(self.attrWidget)
        self.attrTableWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.attrTableWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.attrTableWidget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.attrTableWidget.setObjectName(_fromUtf8("attrTableWidget"))
        self.attrTableWidget.setColumnCount(0)
        self.attrTableWidget.setRowCount(0)
        self.horizontalLayout_3.addWidget(self.attrTableWidget)
        self.attrGridLayout.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)
        self.horizontalLayout_4.addLayout(self.attrGridLayout)
        self.attrVerticalLayout = QtGui.QVBoxLayout()
        self.attrVerticalLayout.setObjectName(_fromUtf8("attrVerticalLayout"))
        self.verticalLayout_8 = QtGui.QVBoxLayout()
        self.verticalLayout_8.setSpacing(10)
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.buttonEditRow = QtGui.QToolButton(self.attrWidget)
        self.buttonEditRow.setText(_fromUtf8(""))
        self.buttonEditRow.setObjectName(_fromUtf8("buttonEditRow"))
        self.verticalLayout_8.addWidget(self.buttonEditRow)
        self.buttonDelRow = QtGui.QToolButton(self.attrWidget)
        self.buttonDelRow.setText(_fromUtf8(""))
        self.buttonDelRow.setObjectName(_fromUtf8("buttonDelRow"))
        self.verticalLayout_8.addWidget(self.buttonDelRow)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem)
        self.attrVerticalLayout.addLayout(self.verticalLayout_8)
        self.horizontalLayout_4.addLayout(self.attrVerticalLayout)
        self.attrTabWidget.addTab(self.attrWidget, _fromUtf8(""))
        self.fieldsWidget = QtGui.QWidget()
        self.fieldsWidget.setObjectName(_fromUtf8("fieldsWidget"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout(self.fieldsWidget)
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.fieldsHorizontalLayout = QtGui.QHBoxLayout()
        self.fieldsHorizontalLayout.setObjectName(_fromUtf8("fieldsHorizontalLayout"))
        self.fieldsTableWidget = QtGui.QTableWidget(self.fieldsWidget)
        self.fieldsTableWidget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.fieldsTableWidget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.fieldsTableWidget.setObjectName(_fromUtf8("fieldsTableWidget"))
        self.fieldsTableWidget.setColumnCount(0)
        self.fieldsTableWidget.setRowCount(0)
        self.fieldsHorizontalLayout.addWidget(self.fieldsTableWidget)
        self.horizontalLayout_6.addLayout(self.fieldsHorizontalLayout)
        self.fieldsVerticalLayout = QtGui.QVBoxLayout()
        self.fieldsVerticalLayout.setSpacing(10)
        self.fieldsVerticalLayout.setObjectName(_fromUtf8("fieldsVerticalLayout"))
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.verticalLayout_5.setContentsMargins(-1, -1, -1, 50)
        self.verticalLayout_5.setSpacing(9)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.buttonMoveUp = QtGui.QToolButton(self.fieldsWidget)
        self.buttonMoveUp.setText(_fromUtf8(""))
        self.buttonMoveUp.setObjectName(_fromUtf8("buttonMoveUp"))
        self.verticalLayout_5.addWidget(self.buttonMoveUp)
        self.buttonMoveDown = QtGui.QToolButton(self.fieldsWidget)
        self.buttonMoveDown.setText(_fromUtf8(""))
        self.buttonMoveDown.setObjectName(_fromUtf8("buttonMoveDown"))
        self.verticalLayout_5.addWidget(self.buttonMoveDown)
        self.buttonMoveTop = QtGui.QToolButton(self.fieldsWidget)
        self.buttonMoveTop.setText(_fromUtf8(""))
        self.buttonMoveTop.setObjectName(_fromUtf8("buttonMoveTop"))
        self.verticalLayout_5.addWidget(self.buttonMoveTop)
        self.buttonAddCol = QtGui.QToolButton(self.fieldsWidget)
        self.buttonAddCol.setText(_fromUtf8(""))
        self.buttonAddCol.setObjectName(_fromUtf8("buttonAddCol"))
        self.verticalLayout_5.addWidget(self.buttonAddCol)
        self.buttonDelCol = QtGui.QToolButton(self.fieldsWidget)
        self.buttonDelCol.setText(_fromUtf8(""))
        self.buttonDelCol.setObjectName(_fromUtf8("buttonDelCol"))
        self.verticalLayout_5.addWidget(self.buttonDelCol)
        self.buttonChangeNameCol = QtGui.QToolButton(self.fieldsWidget)
        self.buttonChangeNameCol.setText(_fromUtf8(""))
        self.buttonChangeNameCol.setObjectName(_fromUtf8("buttonChangeNameCol"))
        self.verticalLayout_5.addWidget(self.buttonChangeNameCol)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_5.addItem(spacerItem1)
        self.fieldsVerticalLayout.addLayout(self.verticalLayout_5)
        self.horizontalLayout_6.addLayout(self.fieldsVerticalLayout)
        self.attrTabWidget.addTab(self.fieldsWidget, _fromUtf8(""))
        self.gridLayout.addWidget(self.attrTabWidget, 1, 9, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.buttonSave = QtGui.QPushButton(attributeTableDialog)
        self.buttonSave.setMinimumSize(QtCore.QSize(102, 26))
        self.buttonSave.setObjectName(_fromUtf8("buttonSave"))
        self.horizontalLayout.addWidget(self.buttonSave)
        self.buttonSaveAs = QtGui.QPushButton(attributeTableDialog)
        self.buttonSaveAs.setMinimumSize(QtCore.QSize(102, 26))
        self.buttonSaveAs.setObjectName(_fromUtf8("buttonSaveAs"))
        self.horizontalLayout.addWidget(self.buttonSaveAs)
        self.buttonCancel = QtGui.QPushButton(attributeTableDialog)
        self.buttonCancel.setMinimumSize(QtCore.QSize(102, 26))
        self.buttonCancel.setObjectName(_fromUtf8("buttonCancel"))
        self.horizontalLayout.addWidget(self.buttonCancel)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 9, 4, 1)

        self.retranslateUi(attributeTableDialog)
        self.attrTabWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(attributeTableDialog)

    def retranslateUi(self, attributeTableDialog):
        self.attrTableWidget.setSortingEnabled(False)
        self.attrTabWidget.setTabText(self.attrTabWidget.indexOf(self.attrWidget), _translate("attributeTableDialog", "Preview table", None))
        self.attrTabWidget.setTabText(self.attrTabWidget.indexOf(self.fieldsWidget), _translate("attributeTableDialog", "Fields", None))
        self.buttonSave.setText(_translate("attributeTableDialog", "Save", None))
        self.buttonSaveAs.setText(_translate("attributeTableDialog", "Save as", None))
        self.buttonCancel.setText(_translate("attributeTableDialog", "Cancel", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    attributeTableDialog = QtGui.QDialog()
    ui = Ui_attributeTableDialog()
    ui.setupUi(attributeTableDialog)
    attributeTableDialog.show()
    sys.exit(app.exec_())

