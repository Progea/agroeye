# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import os
import re
from gdal import osr
from collections import namedtuple
from PyQt4 import QtCore
_fromUtf8=QtCore.QString.fromUtf8

class RasterAttributes(object):
    """
    Class containing attributes of a single raster dataset:
    - histograms
    - current LUT (Look Up Table)
    """

    def __init__(self, sourceRaster):
        """!
        @param sourceRaster: Initializing with reference to the calling raster
        """
        self.raster = sourceRaster

        self.showingBands = ChangedList(self.raster.bandsCount)
        self.stretchClass = self.raster.stretchClass
        self.histograms = []
        ## @var self.histograms
        #  list containing calculated histograms
        self.localHistograms = []
        self.defaultHistogram = [None] * self.raster.bandsCount
        for i in range(self.raster.bandsCount):
            self.localHistograms.append([None, None])
        ## @var self.localHistograms
        #  list containing histogram of currently showed area,
        self.showLocalHistograms = False
        ## @var self.showLocalHistograms
        #  Determines whether histograms of current view extent are shown or not
        self.localAreaOfStretch = False
        ## @var self.localAreaOfStretch
        #  Determines whether histogram stretch is based on current view or not
        self.LUTs = [None] * self.raster.bandsCount
        ## @var self.LUTs
        #  currently applied mask to the raster
        self.stretchLUTs = [None] * self.raster.bandsCount
        ## @var self.stretchLUTs
        #  stretching Look-Up Table for whole raster
        self.localStretchLUTs = [None] * self.raster.bandsCount
        ## @var self.localStretchLUTs
        #  stretching Look-Up Table for current view
        self.dataTypeStored = None
        self.minMaxVals = np.zeros([self.raster.bandsCount, 2])
        self.statistics = [None] * self.raster.bandsCount
        self.bandName = dict()

        self.opacity = 0
        self.channelColors = dict()

        for band in range(self.raster.bandsCount):
            self.bandName[band+1] = _("Layer %i") % (band+1)

        if not self.raster.isInt:
            self.lowTopBandValues = []
            self.floatToIntCoefs = [None] * self.raster.bandsCount
            for i in range(1, self.raster.bandsCount+1):
                low, top = self.raster.Dataset.GetRasterBand(i).ComputeRasterMinMax(1)
                self.lowTopBandValues.append([low, top])

        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

        proj4 = osr.SpatialReference(self.raster.Dataset.GetProjection()).ExportToProj4()
        definition = proj4.split('units')[0]

        self.EPSG = 0
        with open(os.path.join(__location__, 'epsg'), 'r') as epsg_codes:
            for line in epsg_codes:
                if definition in line:
                    a = re.match(r'<\d+>', line)
                    self.EPSG = int(line[a.start()+1:a.end()-1])


        self.redPalette = np.arange(256)
        self.greenPalette = np.arange(256)
        self.bluePalette = np.arange(256)
        self.palette = 0
        self.paletteInverted = False

    def setDefaultLUT(self, tempRaster=None):
        """!
        @brief Checks
        @var self.dataTypeStored and applies default LUT to show image
        """
        if not tempRaster:
            if self.dataTypeStored is np.uint32:
                self.setLUT32bitTo8Bit()
            elif self.dataTypeStored is np.uint16:
                self.setLUT16bitTo8Bit()
            elif self.dataTypeStored is np.uint8:
                self.setLUT8BitTo8Bit()
            elif self.dataTypeStored is np.float32:
                self.setLUTfloat32To8Bit()
        else:
            if self.dataTypeStored is np.uint32:
                return self.setLUT32bitTo8Bit(tempRaster)
            elif self.dataTypeStored is np.uint16:
                return self.setLUT16bitTo8Bit(tempRaster)
            elif self.dataTypeStored is np.uint8:
                return self.setLUT8BitTo8Bit(tempRaster)

    def setLUTfloat32To8Bit(self, tempRaster=None):
        """!
        @brief Applies different default LUT depending on range of float values.
        """
        if not tempRaster:
            for i in range(self.raster.bandsCount):
                valuesRange = self.lowTopBandValues[i][1] - self.lowTopBandValues[i][0]
                self.floatToIntCoefs[i] = 256/valuesRange
                # self.stretchLUTs[i] = np.linspace(self.lowTopBandValues[i][0], self.lowTopBandValues[i][1], valuesRange,
                #                                   dtype=np.uint16)
                # LUT = np.linspace(self.lowTopBandValues[i][0], self.lowTopBandValues[i][1], valuesRange,
                #                   dtype=np.uint16)
                self.stretchLUTs[i] = np.linspace(0, 255, 256, dtype=np.uint8)
                LUT = np.linspace(0, 255, 256, dtype=np.uint8)

                self.LUTs[i] = LUT

    def setLUT32bitTo8Bit(self, tempRaster=None):
        """!
        @brief Applies to the raster LUT mask converting image from 32 bits into 8 bit.
        Lower bit depth is required in order to display raster on the screen.
        """
        if not tempRaster:
            for i in range(self.raster.bandsCount):
                LUT = np.linspace(0, 2**32 - 1, 2**32, dtype=np.uint32)/2**16
                self.LUTs[i] = LUT
                self.stretchLUTs[i] = np.linspace(0, 2**32 - 1, 2**32, dtype=np.uint32)
        else:
            return np.linspace(0, 2**32 - 1, 2**32, dtype=np.uint32)

    def setLUT16bitTo8Bit(self, tempRaster=None):
        """!
        @brief Applies to the raster LUT mask converting image from 16 bits into 8 bit.
        Lower bit depth is required in order to display raster on the screen.
        """
        if not tempRaster:
            for i in range(self.raster.bandsCount):
                LUT = np.linspace(0, 2**16 - 1, 2**16, dtype=np.uint16)/2**8
                self.LUTs[i] = LUT
                self.stretchLUTs[i] = np.linspace(0, 2**16 - 1, 2**16, dtype=np.uint16)
        else:
            return np.linspace(0, 2**16 - 1, 2**16, dtype=np.uint16)

    def setLUT8BitTo8Bit(self, tempRaster=None):
        """!
        @brief Applies to the raster LUT mask no-depth-conversion
        """
        if not tempRaster:
            for i in range(self.raster.bandsCount):
                LUT = np.linspace(0, 2**8 - 1, 2**8, dtype=np.uint8)
                self.LUTs[i] = LUT
                self.stretchLUTs[i] = np.linspace(0, 2**8 - 1, 2**8, dtype=np.uint8)
        else:
            return np.linspace(0, 2**8 - 1, 2**8, dtype=np.uint8)

    def setStretchType(self, index):

        if index == self.stretchClass.none.index:
            self.setDefaultHistogram()
        elif index == self.stretchClass.percentClip.index and self.raster.isInt:
            self.setPercetangeClipHistogram()
        elif index == self.stretchClass.percentClip.index and not self.raster.isInt:
            self.setPercentageClipHistogram_Float()
        elif index == self.stretchClass.standardDev.index and self.raster.isInt:
            self.setStandardDeviationClipHistogram()
        elif index == self.stretchClass.standardDev.index and not self.raster.isInt:
            self.setStandardDeviationClipHistogram_Float()

    def createDefaultHistogram(self, tempRaster=None):
        """
        @brief Computes Statistics, tries to use overviews to speed-up work
        """
        if not tempRaster:
            del self.histograms[:]
            self.setDefaultLUT()
            rasters = self.raster.bands
        else:
            rasters = [tempRaster.GetRasterBand(1)]
        for i, singleBand in enumerate(rasters):
            if self.raster.isInt:
                minValue, maxValue, mean, stdDev = singleBand.ComputeStatistics(1)
                hist = singleBand.GetHistogram(minValue, maxValue, int(maxValue - minValue))
                preHist = np.zeros((minValue,), dtype=self.dataTypeStored)
                postHist = np.zeros((self.raster.dataRange - maxValue,), dtype=self.dataTypeStored)
                hist = np.append(preHist, hist)
                hist = np.append(hist, postHist)
                if not tempRaster:
                    self.statistics[i] = [minValue, maxValue, mean, stdDev, hist]
                    self.histograms.append(hist)
                else:
                    statistics = [minValue, maxValue, mean, stdDev, hist]
                    return statistics

            else:
                minValue, maxValue, mean, stdDev = singleBand.ComputeStatistics(1)
                hist = singleBand.GetHistogram(minValue, maxValue, 256)
                preHist = np.zeros((minValue,), dtype=self.dataTypeStored)
                postHist = np.zeros((self.raster.dataTypeMaxValue[i] - self.raster.dataTypeMinValue[i],), dtype=self.dataTypeStored)
                if not tempRaster:
                    self.statistics[i] = [minValue, maxValue, mean, stdDev, hist]
                    self.histograms.append(hist)
                else:
                    statistics = [minValue, maxValue, mean, stdDev, hist]
                    return statistics


    def setDefaultHistogram(self, tempRaster=None):
        """
        @brief Set defaults histogram
        """
        if not tempRaster and self.raster.isInt:
            del self.histograms[:]
            for i, singleBand in enumerate(self.raster.bands):
                hist = self.statistics[i][4]
                xValues = np.linspace(self.raster.dataTypeMinValue, self.raster.dataTypeMaxUsed,
                                      num=self.raster.dataTypeMaxUsed - self.raster.dataTypeMinValue)
                self.histograms.append((xValues, hist))

            self.setDefaultLUT()

        elif not self.raster.isInt:
            del self.histograms[:]
            for i, singleBand in enumerate(self.raster.bands):
                hist = self.statistics[i][4]
                xValues = np.linspace(self.raster.dataTypeMinValue[i], self.raster.dataTypeMaxUsed[i],
                                      num=self.raster.dataTypeMaxUsed[i] - self.raster.dataTypeMinValue[i])
                self.histograms.append((xValues, hist))

            self.setDefaultLUT()

        else:
            tempStatistics = self.createDefaultHistogram(tempRaster)
            xValues = np.linspace(self.raster.dataTypeMinValue, self.raster.dataTypeMaxUsed,
                                  num=self.raster.dataTypeMaxUsed - self.raster.dataTypeMinValue)
            return [xValues, tempStatistics]

    def setPercetangeClipHistogram(self, tempRaster=None, statistics=None, float=False):

        """
        @brief Clips histogram and stretches with given percentile and sets up new stretch LUTs.
        """
        def calculateLut(x):
            return (self.raster.dataRange/(newMax-newMin))*(x-newMin)

        calculateLut = np.vectorize(calculateLut)

        if not self.raster.isInt:
            return

        if not tempRaster:
            rasters = self.raster.bands
        else:
            rasters = [tempRaster.GetRasterBand(1)]

        print 'percent', self.stretchClass.stretchType.param[0], type(self.stretchClass.stretchType.param[0])

        for i, singleBand in enumerate(self.raster.bands):
            minPercentile = self.stretchClass.stretchType.param[0]/100.0
            maxPercentile = (100 - self.stretchClass.stretchType.param[1])/100.0

            if not tempRaster:
                originalHistogram = self.statistics[i][4]
            else:
                originalHistogram = statistics[1][4]

            if self.raster.isInt:
                coef1 = np.linspace(self.raster.dataTypeMinValue, int(self.raster.dataRange), len(originalHistogram))
            else:
                coef1 = np.linspace(self.raster.dataTypeMinValue[i], int(self.raster.dataRange), len(originalHistogram))
                
            coef2 = coef1*originalHistogram
            coef3 = np.array(coef2).cumsum()
            minPercentileValue = minPercentile*coef3[-1]
            maxPercentileValue = maxPercentile*coef3[-1]
            newMin = np.where(coef3 > minPercentileValue)[0][0] - 1
            newMax = np.where(coef3 > maxPercentileValue)[0][0] + 1
            tempLut = np.linspace(newMin, newMax, newMax - newMin + 1)
            tempLut = calculateLut(tempLut).astype(dtype=self.dataTypeStored)
            preLUT = np.zeros([1, newMin])
            if self.raster.isInt:
                postLUT = np.zeros([1, self.raster.dataTypeMaxUsed - newMax])
            else:
                postLUT = np.zeros([1, self.raster.dataTypeMaxValue[i] - newMax])
            postLUT.fill(self.raster.dataRange)
            tempLut = np.append(preLUT, tempLut)
            tempLut = np.append(tempLut, postLUT)

            if not tempRaster:
                self.stretchLUTs[i] = tempLut.astype(dtype=np.uint16)
                # self.stretchLUTs[i] = tempLut.astype(dtype=self.dataTypeStored)

            else:
                localStretchLUT = tempLut.astype(dtype=self.dataTypeStored)
                return localStretchLUT

            if self.raster.isInt:

                slicedHistogram = originalHistogram[preLUT.size:-postLUT.size]
                xValuesSlicedHistogram = np.linspace(self.raster.dataTypeMinValue, self.raster.dataRange,
                                                     newMax - newMin + 1, dtype=self.dataTypeStored)
                newXValuesSlicedHistogram = np.linspace(xValuesSlicedHistogram[0],
                                                        xValuesSlicedHistogram[-1],
                                                        256) #Number of bins in histogram dialog window
                newSlicedHistogram = np.interp(newXValuesSlicedHistogram, xValuesSlicedHistogram[:-1], slicedHistogram)
                newSlicedHistogram = newSlicedHistogram.astype(np.uint32)

                self.histograms[i] = newXValuesSlicedHistogram, newSlicedHistogram
            else:
                slicedHistogram = originalHistogram[preLUT.size:-postLUT.size]
                # xValuesSlicedHistogram = np.linspace(self.raster.dataTypeMinValue[i], self.raster.dataTypeMaxValue[i]-self.raster.dataTypeMinValue[i],
                #                                      newMax - newMin + 1, dtype=self.dataTypeStored)
                xValuesSlicedHistogram = np.linspace(self.raster.dataTypeMinValue[i], self.raster.dataTypeMaxValue[i], 256)
                newXValuesSlicedHistogram = np.linspace(xValuesSlicedHistogram[0],
                                                        xValuesSlicedHistogram[-1],
                                                        256) #Number of bins in histogram dialog window
                newSlicedHistogram = np.interp(newXValuesSlicedHistogram, xValuesSlicedHistogram, originalHistogram)
                newSlicedHistogram = newSlicedHistogram.astype(np.uint32)


    def setStandardDeviationClipHistogram(self, tempRaster=None, statistics=None, float=False):

        """
        @brief Calculates histogram and stretches it for standard deviation
        and sets up new stretch LUTs.
        """
        print 'standard', self.stretchClass.stretchType.param[0], type(self.stretchClass.stretchType.param[0])
        def calculateLut(x):
            return (self.raster.dataRange/(newMax-newMin))*(x-newMin)

        calculateLut = np.vectorize(calculateLut)

        if not self.raster.isInt:
            return

        if not tempRaster:
            rasters = self.raster.bands
        else:
            rasters = tempRaster.GetRasterBand(1)

        for i, singleBand in enumerate(self.raster.bands):
            if not tempRaster:
                newMin = self.statistics[i][2] - self.stretchClass.stretchType.param[0]*self.statistics[i][3]
            else:
                newMin = statistics[1][0] - self.stretchClass.stretchType.param[0]*statistics[1][3]
            if newMin < 1:
                newMin = 1
                preLUT = np.zeros([1, newMin+1])
            else:
                preLUT = np.zeros([1, newMin+1])

            if not tempRaster:
                newMax = self.statistics[i][2] + self.stretchClass.stretchType.param[0]*self.statistics[i][3]
            else:
                newMax = statistics[1][2] + self.stretchClass.stretchType.param[0]*statistics[1][3]

            if newMax > self.raster.dataTypeMaxUsed:
                newMax = self.raster.dataTypeMaxUsed

            if not tempRaster:
                originalHistogram = self.statistics[i][4]
            else:
                originalHistogram = statistics[1][4]

            tempLut = np.linspace(newMin, newMax, newMax - newMin)
            tempLut = calculateLut(tempLut).astype(dtype=self.dataTypeStored)
            if self.raster.isInt:
                postLUT = np.zeros([1, self.raster.dataTypeMaxUsed - newMax + 1])
                postLUT.fill(self.raster.dataTypeMaxValue)
            else:
                postLUT = np.zeros([1, self.raster.dataTypeMaxValue[i] - newMax + 1])
                postLUT.fill(255)

            tempLut = np.append(preLUT, tempLut)
            tempLut = np.append(tempLut, postLUT)
            if not tempRaster:
                self.stretchLUTs[i] = tempLut.astype(dtype=np.uint16)
            else:
                localStretchLut = tempLut.astype(dtype=self.dataTypeStored)
                return localStretchLut


            if self.raster.isInt:
                slicedHistogram = originalHistogram[preLUT.size:-postLUT.size]
                xValuesSlicedHistogram = np.linspace(self.raster.dataTypeMinValue, self.raster.dataRange,
                                                     slicedHistogram.size, dtype=self.dataTypeStored)
                newXValuesSlicedHistogram = np.linspace(xValuesSlicedHistogram[0], xValuesSlicedHistogram[-1], 256) #Number of bins in histogram dialog window
                newSlicedHistogram = np.interp(newXValuesSlicedHistogram, xValuesSlicedHistogram, slicedHistogram)
                newSlicedHistogram = newSlicedHistogram.astype(np.uint32)

                self.histograms[i] = newXValuesSlicedHistogram, newSlicedHistogram
            else:
                slicedHistogram = originalHistogram[preLUT.size:-postLUT.size]
                # xValuesSlicedHistogram = np.linspace(self.raster.dataTypeMinValue[i], self.raster.dataTypeMaxValue[i]-self.raster.dataTypeMinValue[i],
                #                                      newMax - newMin + 1, dtype=self.dataTypeStored)
                xValuesSlicedHistogram = np.linspace(self.raster.dataTypeMinValue[i], self.raster.dataTypeMaxValue[i], 256)
                newXValuesSlicedHistogram = np.linspace(xValuesSlicedHistogram[0],
                                                        xValuesSlicedHistogram[-1],
                                                        256) #Number of bins in histogram dialog window
                newSlicedHistogram = np.interp(newXValuesSlicedHistogram, xValuesSlicedHistogram, originalHistogram)
                newSlicedHistogram = newSlicedHistogram.astype(np.uint32)

    def setLocalStretch(self, tempRaster, counter):
        self.localStretchLUTs[counter] = None
        localStatistics = self.setDefaultHistogram(tempRaster)

        if self.stretchClass.stretchType == self.stretchClass.none:
            localStretchLUT = self.setDefaultLUT(tempRaster)
            self.localStretchLUTs[counter] = localStretchLUT

        elif self.stretchClass.stretchType == self.stretchClass.percentClip:
            localStretchLUT = self.setPercetangeClipHistogram(tempRaster, localStatistics)
            self.localStretchLUTs[counter] = localStretchLUT
        elif self.stretchClass.stretchType == self.stretchClass.standardDev:
            localStretchLUT = self.setStandardDeviationClipHistogram(tempRaster, localStatistics)
            self.localStretchLUTs[counter] = localStretchLUT

    def setPercentageClipHistogram_Float(self):
        self.stretchLUTs, self.histograms = self.rasterAuxiliary.percentageClip(self.raster.Dataset, self.stretchClass.stretchType.param, self.histograms)

    def setStandardDeviationClipHistogram_Float(self):
        self.stretchLUTs, self.histograms = self.rasterAuxiliary.standardDeviationClip(self.raster.Dataset, self.stretchClass.stretchType.param, self.histograms)

    def setNoStretchHistogram_Float(self):
        self.stretchLUTs, self.histograms = self.rasterAuxiliary.noStretchFloat(self.raster.Dataset, self.histograms)

    def setLocalHistogramClip_Float(self, bandData, band, counter):
        if self.stretchClass.stretchType == self.stretchClass.percentClip:
            return self.rasterAuxiliary.percentageClip_local(bandData, self.stretchClass.stretchType.param, band, self.localHistograms[counter])
        elif self.stretchClass.stretchType == self.stretchClass.standardDev:
            return self.rasterAuxiliary.standardDeviation_local(bandData, self.stretchClass.stretchType.param, band, self.localHistograms[counter])
        elif self.stretchClass.stretchType == self.stretchClass.none:
            return self.stretchLUTs[counter]

class ChangedList(list):

    class Dict2Change(dict):

        def __getitem__(self, key):
            try:
                return dict.__getitem__(self, key)
            except KeyError:
                return None

    dict2Change = Dict2Change()

    def __init__(self, bands):
        super(ChangedList, self).__init__()

        if bands >= 5:
            self.append(1)
            self.append(3)
            self.append(4)
        elif bands >= 4:
            self.append(1)
            self.append(2)
            self.append(3)
        elif bands >= 3:
            self.append(0)
            self.append(1)
            self.append(2)
        else:
            self.append(0)

        self.dict2Change[0] = 'red'
        self.dict2Change[1] = 'green'
        self.dict2Change[2] = 'blue'
        self.dict2Change[3] = 'tomato'
        self.dict2Change[4] = 'aquamarine'
        self.dict2Change[5] = 'darkorchid'
        self.dict2Change[6] = 'darkkhaki'
        self.dict2Change[7] = 'goldenrod'
        self.dict2Change[8] = 'saddlebrown'
        self.dict2Change[9] = 'forestgreen'
        self.dict2Change[10] = 'darksage'
        self.dict2Change[11] = 'dodgerblue'
        self.dict2Change[12] = 'olive'
        self.dict2Change[13] = 'crimson'

class DefaultRasterStretch(object):

    def __init__(self):

        self.StretchType = namedtuple('StretchType', ['text', 'index', 'name', 'param'])
        self.none = self.StretchType(_fromUtf8(_('None')), 0, 'None', None)
        self.percentClip = self.StretchType(_fromUtf8(_('Percentage Clip')), 1, 'Percentage Clip', [2, 2])
        self.standardDev = self.StretchType(_fromUtf8(_('Standard Deviation')), 2, 'Standard Deviation', [2.5, 0])
        self.stretchType = self.percentClip

    def saveStretchType(self, index, newValue):
        if index == self.none.index:
            self.stretchType = self.none
        elif index == self.percentClip.index:
            self.percentClip = self.percentClip._replace(param=newValue)
            self.stretchType = self.percentClip
        elif index == self.standardDev.index:
            self.standardDev = self.standardDev._replace(param=newValue)
            self.stretchType = self.standardDev
