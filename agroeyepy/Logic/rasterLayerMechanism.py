# coding: utf-8

from __future__ import division
import gc
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import numpy as np
import os
import re
from osgeo import gdal
from osgeo import gdalconst
from osgeo import ogr
from osgeo import osr
from rasterattributes import RasterAttributes
from conversions import GdalToNumpy
from Control import exceptionModel
_fromUtf8 = QString.fromUtf8

class RasterFile(QModelIndex, QObject, QStandardItem):
    """Docu for a class.

    More details lower.
    """


    def __init__(self, fname, viewArea):
        """!
        @brief Open file and read basic information
        @param fname: is the input from Dialog Window
        """

        super(RasterFile, self).__init__()

        try:
            self.Dataset = gdal.Open(unicode(fname), gdal.GA_ReadOnly)
            if self.Dataset is None:
               raise exceptionModel.OpenFileError(fname)
        except exceptionModel.OpenFileError as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
            return None

        self.path = fname
        ## @var self.path
        # The path of file
        head, tail = os.path.split(fname)
        self.name = tail
        ## @var self.name
        #  Filename of the raster

        overviews = self.Dataset.GetRasterBand(1).GetOverviewCount()
        if overviews == 0:
            self.askForOverviews()

        self.viewArea = viewArea
        self.cols = self.Dataset.RasterXSize
        self.rows = self.Dataset.RasterYSize
        self.bandsCount = self.Dataset.RasterCount
        self.noDataValue = self.Dataset.GetRasterBand(1).GetNoDataValue()
        self.dataTypeInt = self.Dataset.GetRasterBand(1).DataType
        self.dataTypeName = gdal.GetDataTypeName(self.dataTypeInt)
        self.fileFormat = self.Dataset.GetDriver().GetDescription()
        self.isInt = False
        if 'INT' in self.dataTypeName.upper() or 'BYTE' in self.dataTypeName.upper():
            self.dataTypeMinValue = np.iinfo(GdalToNumpy.ConvertDataTypeNameToDataTypeName(self.dataTypeName)).min
            self.dataTypeMaxValue = np.iinfo(GdalToNumpy.ConvertDataTypeNameToDataTypeName(self.dataTypeName)).max
            self.isInt = True
        elif 'FLOAT' in self.dataTypeName.upper():
            self.dataTypeMinValue = self.bandsCount * [None]
            self.dataTypeMaxValue = self.bandsCount * [None]
            self.dataTypeMinValue, self.dataTypeMaxValue = GdalToNumpy.ConvertFloatTypeNameToDataTypeName(self.Dataset)
            self.isInt = False
        ## @var String
        #  The initialized type of stretch is Percentage
        self.stretchClass = self.viewArea.stretchClass

        self.bands = []
        self.processedBands = []
        self.isVector = False
        self.nameStyleFullPath = False
        # Checks what is actual range of raster data
        # Sometimes there are unused values between no data value or max value for data type
        self.rasterAttributes = RasterAttributes(self)
        if 'INT' in self.dataTypeName.upper() or 'BYTE' in self.dataTypeName.upper():
            if self.dataTypeMinValue < self.noDataValue < self.dataTypeMaxValue:
                self.dataRange = self.noDataValue - 1
            else:
                self.dataRange = self.dataTypeMaxValue

            if 2**16 <= self.dataRange < 2**32:
                self.rasterAttributes.dataTypeStored = np.uint32
            elif 2**8 <= self.dataRange < 2**16:
                self.rasterAttributes.dataTypeStored = np.uint16
            elif 2**4 <= self.dataRange < 2**8:
                self.rasterAttributes.dataTypeStored = np.uint8

            self.dataTypeMaxUsed = np.iinfo(self.rasterAttributes.dataTypeStored).max

        elif 'FLOAT' in self.dataTypeName.upper():
            self.dataRange = max(self.dataTypeMaxValue) - min(self.dataTypeMinValue)
            gdalDataTypeName = gdal.GetDataTypeName(self.Dataset.GetRasterBand(1).DataType)
            if '32' in gdalDataTypeName:
                self.rasterAttributes.dataTypeStored = np.float32
            elif '64' in gdalDataTypeName:
                self.rasterAttributes.dataTypeStored = np.float64
            self.dataTypeMaxUsed = self.dataTypeMaxValue

        try:
            self.geoMatrix = self.Dataset.GetGeoTransform()
            self.originX = self.geoMatrix[0] # top left x
            self.originY = self.geoMatrix[3] # top left y

            self.pixelSizeX = self.geoMatrix[1] # x-axis pixel resolution
            self.pixelSizeY = self.geoMatrix[5] # y-axis pixel resolution
            if self.geoMatrix[2] != 0 or self.geoMatrix[4] != 0:
                raise exceptionModel.InvalidRasterOrientated
        except exceptionModel.InvalidRasterOrientated as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)

        self.centerX = self.originX + self.pixelSizeX*self.cols/2
        self.centerY = self.originY + self.pixelSizeY*self.rows/2

        if self.isInt:
            self.coefficientToQImage = GdalToNumpy.CoefficientGDALtoQImage(self.dataTypeName)
        else:
            self.coefficientToQImage = GdalToNumpy.CoefficientGDALtoQImageFloat(self.bands)
        ## @var coefficientToQImage
        #  Something

        self.EPSG = self.rasterAttributes.EPSG
        self.EPSGbase = self.rasterAttributes.EPSG

        for counter in range(self.bandsCount):
            self.bands.append(self.Dataset.GetRasterBand(counter+1))
        ## @var self.bands
        #  Obtain pointers to each layer

        self.pathLayer = fname

        self.visible = True
        ## @var self.visible
        #  By default visibility is set ON
        self.icon = QIcon(_fromUtf8(":/MainWindow/resources/raster.png"))

        self.srs = osr.SpatialReference(self.Dataset.GetProjection())
        self.proj4 = self.srs.ExportToProj4()
        self.rasterSpatialReference = osr.SpatialReference()
        self.rasterSpatialReference.ImportFromProj4(self.proj4)
        self.spatialReferencePrettyWkt = self.rasterSpatialReference.ExportToPrettyWkt()

        self.ratio = None
        ## @var self.ratio:
        #  Conversion ratio from raster pixel to viewport pixel

        self.offsetX = self.pixelSizeX/2
        self.offsetY = self.pixelSizeY/2

        rasterBoundary = ogr.Geometry(ogr.wkbLinearRing)
        rasterBoundary.AddPoint(self.originX, self.originY)
        rasterBoundary.AddPoint(self.originX + self.pixelSizeX*self.cols, self.originY)
        rasterBoundary.AddPoint(self.originX + self.pixelSizeX*self.cols,
                                self.originY + self.pixelSizeY*self.rows)
        rasterBoundary.AddPoint(self.originX, self.originY + self.pixelSizeY*self.rows)
        rasterBoundary.AddPoint(self.originX, self.originY)
        self.boundary = ogr.Geometry(ogr.wkbPolygon)
        self.boundary.AddGeometry(rasterBoundary)


        self.rasterAttributes.setDefaultLUT()
        self.rasterAttributes.createDefaultHistogram()

        if not self.isVector and self.isInt:
            if self.stretchClass.stretchType == self.stretchClass.percentClip:
                self.rasterAttributes.setPercetangeClipHistogram()
            elif self.stretchClass.stretchType == self.stretchClass.standardDev:
                self.rasterAttributes.setStandardDeviationClipHistogram()
            elif self.stretchClass.stretchType == self.stretchClass.none:
                self.rasterAttributes.setDefaultHistogram()



            # if self.viewArea.stretchClass.typeOfStretch == "Percentage Clip":
            #     self.rasterAttributes.setPercetangeClipHistogram()
            # elif self.viewArea.stretchClass.typeOfStretch == "Standard Deviation":
            #     self.rasterAttributes.setStandardDeviationClipHistogram()
            # elif self.viewArea.stretchClass.typeOfStretch == "None":
            #     self.rasterAttributes.setDefaultHistogram()
        # else:
        #     if self.viewArea.stretchClass.typeOfStretch == "Percentage Clip":
        #         self.rasterAttributes.setPercetangeClipHistogram(float=True)
        #     elif self.viewArea.stretchClass.typeOfStretch == "Standard Deviation":
        #         self.rasterAttributes.setStandardDeviationClipHistogram(float=True)
        #     else:
        #         self.rasterAttributes.setDefaultHistogram()
        else:
            if self.stretchClass.stretchType == self.stretchClass.percentClip:
                self.rasterAttributes.setPercetangeClipHistogram_Float()
            elif self.stretchClass.stretchType == self.stretchClass.standardDev:
                self.rasterAttributes.setStandardDeviationClipHistogram_Float()
            elif self.stretchClass.stretchType == self.stretchClass.none:
                self.rasterAttributes.setDefaultHistogram_Float()


            # if self.viewArea.stretchClass.typeOfStretch == "Percentage Clip":
            #     self.rasterAttributes.setPercentageClipHistogram_Float()
            # elif self.viewArea.stretchClass.typeOfStretch == "Standard Deviation":
            #     self.rasterAttributes.setStandardDeviationClipHistogram_Float()
            # elif self.viewArea.stretchClass.typeOfStretch == "None":
            #     self.rasterAttributes.setNoStretchHistogram_Float()



    def getQimageData(self, viewSize, viewBoundary, pixSize,
                  viewAreaProj4, viewAreaSpatialReference, viewAreaAsRaster):
        """!
        @brief Returns image in of the desired parameters.
        Scale of the image, upper-left point and size of the view area
        must be provided. Using these parameters method returns
        an image fitting to the appropriate area.

        @param ratio: ratio to produce right size of the image with sides aspect ratio
        @param centralPoint: Center  of the view area
        @param viewSize: Size of the area where QImage is about to be displayed.
        """


        spatialRefViewArea = osr.SpatialReference()
        spatialRefViewArea.ImportFromProj4(viewAreaProj4)

        transformation = osr.CoordinateTransformation(spatialRefViewArea, self.rasterSpatialReference)
        tempViewArea = ogr.CreateGeometryFromWkb(viewBoundary.ExportToWkb())
        tempViewArea.Transform(transformation)
        if tempViewArea.Intersection(self.boundary).IsEmpty():
            return [0, 0, 0, 0]
        else:
            inter = tempViewArea.Intersection(self.boundary)


        rasterEnvelope = inter.GetEnvelope()


        self.minXY = self.fromCoordinatesToPixels([rasterEnvelope[0], rasterEnvelope[3]])
        self.maxXY = self.fromCoordinatesToPixels([rasterEnvelope[1], rasterEnvelope[2]])


        if viewAreaSpatialReference.GetAttrValue('UNIT') == 'degree' and \
                self.rasterSpatialReference.GetAttrValue('UNIT') == 'metre':
            pixSize *= 111196.672
        elif viewAreaSpatialReference.GetAttrValue('UNIT') == 'metre' and\
                self.rasterSpatialReference.GetAttrValue('UNIT') == 'degree':
            pixSize /= 111196.672
        # in case of computing from meters into degrees
        # revert


        ratio = self.pixelSizeX/pixSize


        self.outColumns = int((self.maxXY.x() - self.minXY.x())*ratio)
        if self.outColumns < 1: self.outColumns = 1
        self.outRows = int((self.maxXY.y() - self.minXY.y())*ratio)
        if self.outRows < 1: self.outRows = 1



        numpyArray = np.zeros((self.outRows, self.outColumns, self.bandsCount+1), dtype=np.uint8)
        opacity = 255 - int(self.rasterAttributes.opacity*(255.0/100.0) + 0.5)
        numpyArray[..., -1].fill(opacity)


        for i, counter in enumerate(range(self.bandsCount), start=0):
            band = self.bands[counter].ReadAsArray(self.minXY.x(),
                                                   self.minXY.y(),
                                                   self.maxXY.x() - self.minXY.x(),
                                                   self.maxXY.y() - self.minXY.y(),
                                                   self.outColumns, self.outRows).astype(self.rasterAttributes.dataTypeStored)


            if not self.rasterAttributes.localAreaOfStretch and self.isInt:

                numpyArray[..., counter] = \
                        self.rasterAttributes.LUTs[counter][self.rasterAttributes.stretchLUTs[counter][band]]
            elif not self.rasterAttributes.localAreaOfStretch and not self.isInt:
                try:
                    numpyArray[..., counter] = self.rasterAttributes.rasterAuxiliary.stretchBandFloat(band, self.rasterAttributes.stretchLUTs[counter])
                except Exception as e:
                    print e
            elif self.rasterAttributes.localAreaOfStretch and not self.isInt:
                temp_lut = self.rasterAttributes.setLocalHistogramClip_Float(band, self.bands[counter], counter)
                numpyArray[..., counter] = self.rasterAttributes.rasterAuxiliary.stretchBandFloat_local(band, temp_lut, self.rasterAttributes.localHistograms[counter])
            elif self.rasterAttributes.localAreaOfStretch:
                tempRaster = gdal.GetDriverByName("MEM").Create('', self.outColumns, self.outRows, 1,
                                                                self.bands[counter].DataType)
                tempBand = tempRaster.GetRasterBand(1)
                tempBand.WriteArray(band)
                self.rasterAttributes.setLocalStretch(tempRaster, counter, self.stretchClass)
                numpyArray[..., counter] = \
                        self.rasterAttributes.LUTs[counter][self.rasterAttributes.localStretchLUTs[counter][band]]


        selectedRaster = gdal.GetDriverByName("MEM").Create('', self.outColumns, self.outRows, self.bandsCount, gdalconst.GDT_Byte)
        selectedRasterGeoTransform = [0] * 6
        selectedRasterGeoTransform[0] = rasterEnvelope[0]
        selectedRasterGeoTransform[1] = pixSize
        selectedRasterGeoTransform[3] = rasterEnvelope[3]
        selectedRasterGeoTransform[5] = -pixSize
        selectedRaster.SetGeoTransform(selectedRasterGeoTransform)
        selectedRaster.SetProjection(self.Dataset.GetProjection())

        for numberOfBand in range(1, self.bandsCount+1):
            col, row = np.where(numpyArray[..., numberOfBand-1] == 0)
            numpyArray[col, row, numberOfBand-1] += 1
            selectedRaster.GetRasterBand(numberOfBand).WriteArray(numpyArray[..., numberOfBand-1])
        dst = gdal.GetDriverByName("MEM").Create('', viewSize[0], viewSize[1], self.bandsCount, gdalconst.GDT_Byte)
        dst.SetGeoTransform(viewAreaAsRaster.GetGeoTransform())
        dst.SetProjection(viewAreaAsRaster.GetProjection())

        gdal.ReprojectImage(selectedRaster, dst, self.Dataset.GetProjection(), dst.GetProjection(),
                            gdalconst.GRA_Bilinear)
        del selectedRaster
        gc.collect()

        numpyArray = np.zeros((viewSize[1], viewSize[0], 3+1), dtype=np.uint8)
        numpyArray[..., -1].fill(opacity)

        numpyArray[..., 0] = dst.GetRasterBand(self.rasterAttributes.showingBands[0]+1).ReadAsArray()
        if len(self.rasterAttributes.showingBands) < 3:
            col, row = np.where(numpyArray[..., 0] == 0)
            numpyArray[..., 2] = self.rasterAttributes.redPalette[numpyArray[..., 0]]
            numpyArray[..., 1] = self.rasterAttributes.greenPalette[numpyArray[..., 0]]
            numpyArray[..., 0] = self.rasterAttributes.bluePalette[numpyArray[..., 0]]
        else:
            numpyArray[..., 1] = dst.GetRasterBand(self.rasterAttributes.showingBands[1]+1).ReadAsArray()
            numpyArray[..., 2] = dst.GetRasterBand(self.rasterAttributes.showingBands[2]+1).ReadAsArray()
            col, row = np.where(numpyArray[..., 0] == 0) and np.where(numpyArray[..., 1] == 0) and \
                                np.where(numpyArray[..., 2] == 0)

        numpyArray[col, row, -1] = 0


        qImageData = [numpyArray.data, viewSize[0], viewSize[1], QImage.Format_ARGB32]

        return qImageData

    def intersectionToViewAreaCoordinates(self, bound, transformation):

        points = bound.GetGeometryRef(0).GetPoints()
        outPoints = []
        for point in points:
            tempPoint = ogr.Geometry(ogr.wkbPoint)
            tempPoint.AddPoint(point[0], point[1])
            tempPoint.Transform(transformation)
            outPoints.append(tempPoint)

        return outPoints

    def IsPointInViewRange(self, inputPoint, viewBoundary):
        point = inputPoint.Clone()
        transformation = osr.CoordinateTransformation(viewBoundary.GetSpatialReference(), self.rasterSpatialReference)
        tempViewBoundary = viewBoundary.Clone()
        tempViewBoundary.Transform(transformation)
        tempViewBoundary = tempViewBoundary.Intersection(self.boundary)
        point.Transform(transformation)
        if tempViewBoundary.Intersects(point):
            return True
        else:
            return False


    def getCoor(self, area):
        """
        @brief From given polygon area gives the physical coordinations
        of each corner provided area
        @param area: Area which corners are desirable
        """
        x_min = area.Centroid().GetX()
        y_min = area.Centroid().GetY()
        x_max = x_min
        y_max = y_min

        upLeft = None
        upRight = None
        downLeft = None
        downRight = None

        for point in area.GetGeometryRef(0).GetPoints()[:4]:
            if point[0] < x_min:
                x_min = point[0]
            elif point[0] > x_max:
                x_max = point[0]
            if point[1] < y_min:
                y_min = point[1]
            elif point[1] > y_max:
                y_max = point[1]

        for point in area.GetGeometryRef(0).GetPoints()[:4]:
            if point[0] >= x_max:
                if point[1] >= y_max:
                    upRight = point
                elif point[1] <= y_min:
                    downRight = point
            elif point[0] <= x_min:
                if point[1] >= y_max:
                    upLeft = point
                elif point[1] >= y_min:
                    downLeft = point

        return upLeft, upRight, downLeft, downRight

    def fromCoordinatesToPixels(self, point):


        x = point[0]
        y = point[1]

        xPix = int((x - self.originX) / self.pixelSizeX)
        yPix = int((y - self.originY) / self.pixelSizeY)


        return QPoint(xPix, yPix)

    def getEnvelope(self, envelopePoints):

        env = ogr.Geometry(ogr.wkbPolygon)
        bounds = ogr.Geometry(ogr.wkbLinearRing)
        bounds.AddPoint(envelopePoints[0], envelopePoints[2])
        bounds.AddPoint(envelopePoints[0], envelopePoints[3])
        bounds.AddPoint(envelopePoints[1], envelopePoints[3])
        bounds.AddPoint(envelopePoints[1], envelopePoints[2])
        bounds.AddPoint(envelopePoints[0], envelopePoints[2])
        env.AddGeometry(bounds)
        return env

    def getHistogram(self):

        if not self.rasterAttributes.histograms:
            self.rasterAttributes.createDefaultHistogram()

        return self.rasterAttributes.histograms


    def bandsValuesInPoint(self, qPoint):

        x = qPoint.x()
        y = qPoint.y()
        values = []

        try:
            for i, band in enumerate(self.bands):
                valueOriginal = band.ReadAsArray(x, y, 1, 1,)[0, 0]
                layerName = self.rasterAttributes.bandName[i+1]
                values.append([layerName, valueOriginal])
        except TypeError:
            self.emit(SIGNAL("pixelOutOfBand"))

        return values


    def askForOverviews(self):

        answer = QMessageBox.question(None, _fromUtf8(_('No overviews detected')), _fromUtf8(_('In provided raster file no overviews were detected\n'
                                                            'It could really slow down the browsing through raster\n'
                                                            'Would you like to create them?\n\n'
                                                            'In case of yes:\n'
                                                            '- You must have write access in the source folder\n'
                                                            '- You will have to wait (Progress bar will be displayed\n'
                                                            'In case of no:\n'
                                                            '- AgroEye can run slower\n')),
                                    QMessageBox.Yes | QMessageBox.No)

        if answer == QMessageBox.Yes:
            overviewList = range(2, 14, 2)
            progressDialog = ProgressDialog(name=self.name)
            progressDialog.show()
            self.Dataset.BuildOverviews("AVERAGE", overviewList, callback=progressDialog.setProgress, callback_data="Overviews ")

class ProgressDialog(QDialog):

    def __init__(self, parent=None, name=None):
        super(ProgressDialog, self).__init__(parent)
        self.layout = QVBoxLayout(self)
        if name:
            description = QLabel(_(u'Building overviews for raster <b>%s</b>') % name)
            self.layout.addWidget(description)
        self.progressBar = QProgressBar()
        self.progressBar.setBaseSize(200, 200)
        self.layout.addWidget(self.progressBar)
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        self.show()

    def setProgress(self, progress, message, data):
        self.progressBar.setValue(int(progress*100))
        QApplication.processEvents()

