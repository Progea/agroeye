# coding: utf-8

from __future__ import division
from PyQt4.QtGui import QStandardItem, QMessageBox, QColor, QIcon
from PyQt4.QtCore import SIGNAL, QObject, QString, QModelIndex, QDate, QPoint
from osgeo import ogr, osr
import os
import re
import random
from View import addlayersdialog
from Control import exceptionModel
import vectorSVGRenderer

_fromUtf8 =QString.fromUtf8






class VectorFile(QModelIndex, QObject, QStandardItem):

   def __init__(self, parent=None, vectorFile=None, selectLayer = 0, pathTemp = None):
        """!
        @brief Open file and read basic information
        @param vectorFile: is the Object of vectorCheckFile.
        @param selectLayer: Number of selected layer.
        """
        super(VectorFile, self).__init__(parent)
        self.parent = parent
        self.viewArea = parent.viewArea
        self.vectorFile = vectorFile
        self.pathTemp = pathTemp
        self.addVectorFile(selectLayer)


   def addVectorFile(self, layer):
       """!
        @brief Add vector to loadfiles model.
        @param layer: the number of layer.
        """

       self.isVector = True
       self.nameStyleFullPath = False
       self.geomType = 0
       self.selectLayer = layer

       self.driverName = self.vectorFile.driverName
       ## @var self.driverName
       # The name of vector driver

       self.driver = self.vectorFile.driver
       ## @var self.driver
       # Vector driver
       self.vectorDataSource = self.vectorFile.vectorDataSource


       self.fileFormat = self.driver.GetName()
       ## @var self.fileFormat
       # The format of vector file

       self.numLayer = self.vectorDataSource.GetLayerCount()
       ## @var self.numLayer
       # The number of layers

       self.pathLayer = self.vectorDataSource.GetName()
       ## @var self.pathLayer
       # The absolute path of the shapefile


       self.vectorLayer = self.vectorDataSource.GetLayer(self.selectLayer)
       ## @var self.vectorLayer
       # Object of layer

       name = os.path.basename(self.pathLayer)
       self.name, ext = os.path.splitext(name)

       if self.numLayer > 1:
            self.name = self.vectorLayer.GetName()


       self.layerDefinition = self.vectorLayer.GetLayerDefn()
       ## @var self.vectorLayer
       # Schema information for layer

       self.fieldName = []
       ## @var self.fieldName
       # Name of columns in atrribute table

       self.fieldWidth = []
       ## @var self.fieldLength
       # length of all field

       self.fieldPrec = []
       ## @var self.fieldPrec
       # precision of float field

       self.fieldTypeCol = []
       ## @var self.fieldTypeCol
       # Type of columns in atrribute table


       self.columnCount = self.layerDefinition.GetFieldCount()
       ## @var self.columnCount
       # The number of columns in atrribute table

       self.fieldVals = []
       ## @var self.fieldVals
       # List of value of each field

       for i in range(self.columnCount):
           fieldDefn = self.layerDefinition.GetFieldDefn(i)
           typeName = fieldDefn.GetFieldTypeName(fieldDefn.GetType())
           self.fieldName.append(QString(fieldDefn.GetName()))
           ## @var self.fieldName
           # List of column name
           self.fieldTypeCol.append(QString(typeName))
           ## @var self.fieldTypeCol
           # List of type of column
           self.fieldPrec.append(fieldDefn.GetPrecision())
           ## @var self.fieldPrec
           # List of precision of column
           self.fieldWidth.append(fieldDefn.GetWidth() - fieldDefn.GetPrecision())
           ## @var self.fieldWidth
           # List of width of column

           self.rowCount = self.vectorLayer.GetFeatureCount()
           ## @var self.rowCount
           # The number of features (rows in table)

           tempVals = []
           for row in range(self.rowCount):
                if self.fileFormat in ('SQLite', 'spatialite'):
                    row += 1
                feature = self.vectorLayer.GetFeature(row)
                if feature:
                    value = self.getDataWithType(feature, typeName, i)
                    feature.Destroy()
                else:
                    value = _('NULL')
                tempVals.append(value)
           self.fieldVals.append(tempVals)



       if self.driverName in ('KML', 'GML', 'GPX'):
           self.geomType = self.vectorFile.checkGeomType(self.vectorLayer, True)
       else:
        self.geomType = self.vectorFile.checkGeomType(self.vectorLayer)
       ## @var self.geomType
       # Type of vector geometry


       self.icon = self.setStyle()
       ## @var self.icon
       # Icon of file

       self.visible = True
       ## @var self.visible
       # By default visibility is set ON

       self.srs = self.vectorLayer.GetSpatialRef()
       ## @var self.srs
       # Spatial reference system

       self.setCoordSystem()
       self.boundary = self.setBoundary()
       ## @var self.boundary
       # Boundary of vector


   def setCoordSystem(self):
       """!
       @brief Set spatial reference and Coordinates System.
       """
       try:
           if self.srs is None:
               raise exceptionModel.SpatialReferenceSystemError()
       except exceptionModel.SpatialReferenceSystemError as e:
           QMessageBox.critical(self.parent, '%s'%e.title, '%s'%e.msg, QMessageBox.Ok)
           self.srs = osr.SpatialReference()
           self.srs.ImportFromProj4('+proj=utm +zone=34 +ellps=WGS84 +datum=WGS84 +units=m +no_defs')

       self.proj4 = self.srs.ExportToProj4()
       self.vectorSpatialReference = osr.SpatialReference()
       self.vectorSpatialReference.ImportFromProj4(self.proj4)
       self.vectorSpatialReferenceBase = osr.SpatialReference()
       self.vectorSpatialReferenceBase.ImportFromProj4(self.proj4)
       if self.viewArea.viewAreaProj4 or (self.driverName in ('KML', 'GML', 'GPX')):
            if self.viewArea.viewAreaProj4 == self.proj4:
                self.coordTransform = osr.CoordinateTransformation(self.vectorSpatialReferenceBase, self.vectorSpatialReference)
            else:
               QMessageBox.information(self.parent, _("Spatial Reference System"), _("Spatial Reference System is other than current. The layer will be transformed."), QMessageBox.Ok)
               indexCombo = self.viewArea.parent.coordinateSystemComboBox.currentIndex()
               epsg = self.viewArea.comboIndexEpsg[indexCombo]

               self.vectorSpatialReference.ImportFromEPSG(epsg)
               self.coordTransform = osr.CoordinateTransformation(self.vectorSpatialReferenceBase, self.vectorSpatialReference)
               self.srs = self.vectorSpatialReference
               self.spatialReferencePrettyWkt = self.vectorSpatialReference.ExportToPrettyWkt()
               self.proj4 = self.vectorSpatialReference.ExportToProj4()

       else:
           self.coordTransform = osr.CoordinateTransformation(self.vectorSpatialReferenceBase, self.vectorSpatialReference)

       extent = self.vectorLayer.GetExtent()
       self.extent1 = self.coordTransform.TransformPoint(extent[0], extent[2])
       self.extent2 = self.coordTransform.TransformPoint(extent[1], extent[3])
       self.xSize = self.extent2[0] - self.extent1[0]
       self.ySize = self.extent2[1] - self.extent1[1]


       definition = self.proj4.split('units')[0]
       __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
       self.EPSG = 0
       with open(os.path.join(__location__, 'epsg'), 'r') as epsg_codes:
        for line in epsg_codes:
            if definition in line:
                a = re.match(r'<\d+>', line)
                self.EPSG = int(line[a.start()+1:a.end()-1])
                self.EPSGbase = int(line[a.start()+1:a.end()-1])

   def getDataWithType(self, feature, typeName, pos):
        """!
        @brief Read data with appropriate type of loaded data.
        @param feature: The feature of vector.
        @param typeName: The type of data.
        @param pos: Number of field in feature.
        @return Returns value from field.
        """

        if typeName in ('Date', 'DateTime'):
            data = feature.GetFieldAsDateTime(pos)
            value = QDate(data[0], data[1], data[2])
        elif typeName == 'Real':
            value = feature.GetFieldAsDouble(pos)
        elif typeName == 'String':
            value = feature.GetFieldAsString(pos)
            if value == 'True':
                value = bool(1)
            elif value == 'False':
                value = bool(0)
        elif typeName in ('Integer', 'Integer64'):
            value = feature.GetFieldAsInteger(pos)
        elif typeName == 'Bool':
            value = feature.GetFieldAsBool(pos)
        else:
            value = 0
        return value

   def getPixSize(self, oldUnit, newUnit):
        """!
        @brief Returns new value of pixel size.
        @param oldUnit: Old spatial unit.
        @param newUnit: New spatial unit.
        """
        pixelSize = 500
        if oldUnit in ('Meter', 'metre') and newUnit in ('Meter', 'metre', 'degree'):
           rate = 1
        elif oldUnit in ('degree') and newUnit in ('Meter', 'metre', 'degree'):
           rate = 111196.672
           pixelSize = pixelSize  / 111196.672
        return (pixelSize,rate)

   def setBoundary(self):
        """!
        @brief Returns polygon which is the boundary of vector.
        """

        self.pixelSize, self.rate = self.getPixSize(self.vectorSpatialReferenceBase.GetAttrValue('UNIT'), self.vectorSpatialReference.GetAttrValue('UNIT'))
        self.extent = self.vectorLayer.GetExtent()
        self.firstPoint = QPoint(self.extent[0]*self.rate, self.extent[3]*self.rate)
        self.cols = int((self.extent[1] - self.extent[0]) / self.pixelSize)
        self.rows = int((self.extent[3] - self.extent[2]) / self.pixelSize)

        vectorBoundary = ogr.Geometry(ogr.wkbPolygon)
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(self.extent[0], self.extent[2])
        ring.AddPoint(self.extent[0], self.extent[3])
        ring.AddPoint(self.extent[1], self.extent[3])
        ring.AddPoint(self.extent[1], self.extent[2])

        ring.CloseRings()
        vectorBoundary.AddGeometry(ring)

        self.centerX = self.extent[0] + ((self.extent[1] - self.extent[0])/2)
        self.centerY = self.extent[2] + ((self.extent[3] - self.extent[2])/2)

        return vectorBoundary


   def getQPixmap(self, viewBoundary, filesInTop, viewAreaProj4, viewAreaSpatialReference, pixSize):
       """!
        @brief Returns name of created *svg files.
        @param viewBoundary: Boundary of view.
        @param filesInTop: List of files which are before displayed vector.
        @param viewAreaProj4: Proj4 for view area.
        @param viewAreaSpatialReference: Spatial Reference system for view area.
        @param pixSize: Pixel size of vector.
        Scale of the image, upper-left point and size of the view area must be provided.
        @param Returns lists of name of created *svg file.
        """
       self.coords = []
       self.vectorRender = vectorSVGRenderer.VectorCreate(self)

       (dataName, size) = self.vectorRender.render(self.vectorLayer, viewBoundary, viewAreaSpatialReference, self.pixelSize, pixSize, self.geomType, self.extent, self.boundary, filesInTop, self.coordTransform,
                                                   self.pointStyle, self.lineStyle, self.fillStyle)

       return  (dataName, size, self.firstPoint)


   def setStyle(self):
      """!
      @brief Create icon for vector.
      @return Returns QIcon
      """
      if not self.parent.viewArea.vectorColorIn:
           r = lambda: random.randint(0,255)
           colorIn = QColor(r(),r(),r())
      else:
           colorIn = self.parent.viewArea.vectorColorIn

      if self.geomType in (1, 4):
        self.pointStyle = PointStyle(self.parent.viewArea.vectorPixSize, 0, self.parent.viewArea.vectorSymbolStyle)
        self.lineStyle = LineStyle(1, QColor(1,1,1))
        self.fillStyle = FillStyle(colorIn, self.parent.viewArea.vectorOpacity)
        return QIcon(_fromUtf8(":/MainWindow/resources/point.png"))
      elif self.geomType in (2, 5):
          self.pointStyle = PointStyle(self.parent.viewArea.vectorPixSize)
          self.lineStyle = LineStyle(1, colorIn, self.parent.viewArea.vectorLineStyle, 1, 1)
          self.fillStyle = FillStyle(QColor(0,0,0), self.parent.viewArea.vectorOpacity)
          return QIcon(_fromUtf8(":/MainWindow/resources/polyline.png"))
      elif self.geomType in (3, 6):
          self.pointStyle = PointStyle(self.parent.viewArea.vectorPixSize)
          self.fillStyle = FillStyle(colorIn, self.parent.viewArea.vectorOpacity, self.parent.viewArea.vectorFillStyle)
          self.lineStyle = LineStyle(1, QColor(1, 1, 1), 1)
          return QIcon(_fromUtf8(":/MainWindow/resources/polygon.png"))


   def IsPointInViewRange(self, inputPoint, viewBoundary):
        """!
        @brief Check if point intersects view range.
        @param inputPoint: Check point.
        @param viewBoundary: Boundary of vector.
        """
        point = inputPoint.Clone()
        transformation = osr.CoordinateTransformation(viewBoundary.GetSpatialReference(), self.vectorSpatialReference)
        tempViewBoundary = viewBoundary.Clone()
        tempViewBoundary.Transform(transformation)
        tempViewBoundary = tempViewBoundary.Intersection(self.boundary)
        point.Transform(transformation)
        if tempViewBoundary.Intersects(point):
            return True
        else:
            return False




class VectorCheckLayer(QModelIndex, QObject, QStandardItem):


    def __init__(self,parent, fname=None):
        """!
        @brief Open file and check the file is valid and number of layers.
        @param fname: is the input from Dialog Window.
        """
        super(VectorCheckLayer, self).__init__()
        self.parent = parent

        self.path = fname
        self.row = []
        self.checkLayer(self.path)

    def defineDriverName(self, ext):
        """!
        @brief Define driver name, to open vector file.
        @param ext: is the extension of loaded file.
        @return Returns driver name and the access permissions to file (0- read, 1 - read/write).
        """
        if ext in('.shp', '.SHP', '.shx', '.SHX', '.dbf', '.DBF'):
            driverName = 'ESRI Shapefile'
            rule = 1
        elif ext in ('.kml', '.KML'):
            driverName = 'KML'
            rule = 0
        elif ext in('.gml', '.GML'):
            driverName = 'GML'
            rule = 0
        elif ext in('.gpx', '.GPX'):
            driverName = 'GPX'
            rule = 0
        elif ext in('.spatialite', '.sqlite'):
            driverName = 'SQLite'
            rule = 0
        # elif ext == '.gdb' or ext == '.GDB':
        #     driverName = 'OpenFileGDB'
        #     rule = 0
        # elif ext == '.csv' or ext == '.CSV':
        #     driverName = 'CSV'
        #     rule = 0

        return driverName, rule

    def checkLayer(self, path):
        """!
        @brief Check number of layer. If is more than one, users have possibility to choose layers to load.
        @param path: path of loaded files.
        @return Returns the list of selected layer.
        """
        try:
           if not os.path.exists(path):
               raise exceptionModel.PathError(path)
           root, ext = os.path.splitext(path)
           self.driverName, rule = self.defineDriverName(ext)
           self.driver = ogr.GetDriverByName(self.driverName)

           if self.driver is None:
                raise exceptionModel.DriverError(self.driverName)

           self.vectorDataSource = self.driver.Open(str(path), rule)
           if self.vectorDataSource is None:
               raise exceptionModel.OpenFileError(path)
           self.numLayer = self.vectorDataSource.GetLayerCount()


           if self.numLayer == 0:
               raise exceptionModel.LayerError(self.numLayer)
           elif self.numLayer == 1:
               layer = self.vectorDataSource.GetLayer()
               feature = layer.GetFeatureCount()
               if feature <= 0:
                   raise exceptionModel.FeatureError()
               else:
                self.row.append(0)
           else:
               self.addLayers = addlayersdialog.AddLayersDialog(self)
               self.addLayers.show()
               self.connect(self.addLayers, SIGNAL("layerAccept()"), self.addLayer)
               if not self.addLayers.exec_():
                   self.disconnect(self.addLayers, SIGNAL("layerAccept()"), self.addLayer)
        except exceptionModel.PathError as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
        except exceptionModel.DriverError as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
        except exceptionModel.OpenFileError as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
        except exceptionModel.LayerError as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
        except exceptionModel.FeatureError as e:
           print e
           QMessageBox.critical(None, e.title, '%s'%e.msg, QMessageBox.Ok)


    def addLayer(self):
        """!
        @brief Save selected layer.
        """
        self.row = self.addLayers.getRow()

    def checkGeomType(self, vectorLayer, isOtherSHP = None):
       """!
       @brief Check geometry type of loaded vector.
       @return Returns geometry type as Integer
       (1 - POINT, 2 - LINESTRING, 3 - POLYGON, 5 - MULTIPOINT, 6 - MULTILINE, 7 - MULTIPOLYGON,
       -2147483647 - POINT25D, -2147483646 - LINESTRING25D, -2147483645 - POLYGON25D, -2147483644 - MULTIPOINT25D).
       """
       geomType = 0

       try:
               if isOtherSHP:
                   feat = vectorLayer.GetFeature(0)
                   geometry = feat.GetGeometryRef()
                   geomType = geometry.GetGeometryType()
                   geomType = self.change25DTo2D(geomType)
               else:
                   for feat in vectorLayer:
                        geometry = feat.GetGeometryRef()
                        geomType = geometry.GetGeometryType()
                        geomType = self.change25DTo2D(geomType)
       except exceptionModel.GeometryTypeException as e:
           QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)

       return geomType

    def change25DTo2D(self, geomType):
         """!
         @brief Check the geometry type is 2.5D and change to 2D.
         @return Returns geometry type converted to 2D.
         """

         if geomType == -2147483647:
           geomType = 1
         elif geomType == -2147483646:
           geomType = 2
         elif geomType == -2147483645:
           geomType = 3
         elif geomType == -2147483644:
          geomType = 4
         elif geomType == -2147483643:
           geomType = 5
         elif geomType == -2147483642:
           geomType = 6
         else:
             geomType = geomType

         return geomType


class PointStyle(object):
    def __init__(self, pointSize, angle = None, symbol = None):
        self.size = pointSize
        ## @var self.size
        # The size of point
        self.angle = angle
        ## @var self.angle
        # The angle of symbol
        self.symbol = symbol
        ## @var self.symbol
        # The symbol of point

class FillStyle(object):
    def __init__(self, colour, opacity, pattern = None):
        self.colour = colour
        ## @var self.colour
        # The colour of filling
        self.opacity = opacity
        ## @var self.opacity
        # The opacity of filling
        self.pattern = pattern
        ## @var self.pattern
        # The pattern of filling

class LineStyle(object):
    def __init__(self, width, colour, patternLine = None, joinLine = None, capLine = None):
        self.width = width
        ## @var self.width
        # The width of line
        self.colour = colour
        ## @var self.colourOut
        # The colour of outline
        self.patternLine = patternLine
        ## @var self.patternLine
        # The pattern of line
        self.joinLine = joinLine
        ## @var self.joinLine
        # The join style of line
        self.capLine = capLine
        ## @var self.capLine
        # The cap style of line