# coding: utf-8

from __future__ import division
from PyQt4.QtGui import QStandardItem, QMessageBox, QColor, QIcon, QImage
from PyQt4.QtCore import SIGNAL, QObject, QString, QModelIndex, QDate, QPoint, QSize
from osgeo import ogr
import os
from xml.etree import ElementTree as et
import numpy as np
from Control import exceptionModel

_fromUtf8 =QString.fromUtf8




class VectorCreate(QModelIndex, QObject, QStandardItem):

    def __init__(self, parent=None):
        """!
        @brief Create SVG for display vector.
        """
        super(VectorCreate, self).__init__(parent)
        self.parent = parent
        self.pathTemp = self.parent.pathTemp


    def render(self, vectorLayer, viewBoundary, viewAreaSpatialReference, vectorPixSize, viewPixSize,  typeGeom, extent, vectorBoundary, filesInTop, coordTransform,
               pointStyle, lineStyle, fillStyle):
       """!
       @brief Returns name of created svg file.
       @param vectorLayer: Layer of vector.
       @param viewBoundary: Boundary of view.
       @param viewAreaSpatialReference: Spatial Reference system for view area.
       @param vectorPixSize: Pixel size of vector.
       @param viewPixSize: Pixel size of view area.
       @param typeGeom: Geometry type of vector.
       @param extent: Extent of vector.
       @param vectorBoundary: Boundary of vector file
       @param filesInTop: List of files which are before displayed vector.
       @param coordTransform: Transform coordinates.
       @param pointStyle: Style of point.
       @param lineStyle: Style of line.
       @param fillStyle: Style of filling.
       @param Returns lists of name of created *svg file.
       """

       self.layer = vectorLayer
       self.cooordTransform = coordTransform
       self.filesInTop = filesInTop


        # intersection file boundary with view boundary
       tempViewBoundary = ogr.CreateGeometryFromWkb(viewBoundary.ExportToWkb())
       if tempViewBoundary.Intersection(vectorBoundary).IsEmpty():
            viewIntersection = [0, 0, 0, 0]
       else:
            viewIntersection = tempViewBoundary.Intersection(vectorBoundary)
       self.vectorEnvelope = viewIntersection.GetEnvelope()

       self.minXY = self.fromCoordinatesToPixels([self.vectorEnvelope[0], self.vectorEnvelope[3]],viewPixSize, extent)
       self.maxXY = self.fromCoordinatesToPixels([self.vectorEnvelope[1], self.vectorEnvelope[2]],viewPixSize, extent)

       #print 'pixSize', vectorPixSize, viewPixSize
       ratio = vectorPixSize/viewPixSize
       #print 'ratio', ratio
       self.outColumns = int(((self.maxXY.x() - self.minXY.x()) * ratio))
       if self.outColumns < 1: self.outColumns = 1
       if self.outColumns > 20000: self.outColumns = 20000

       self.outRows = int(((self.maxXY.y() - self.minXY.y()) * ratio))
       if self.outRows < 1: self.outRows = 1
       if self.outRows > 20000: self.outRows = 20000


       self.outRows = self.outRows
       self.outColumns = self.outColumns


       count = self.layer.GetFeatureCount()
       self.countPointInView(count, vectorBoundary)

       if count:
           vector = np.zeros(count, dtype=object)
           vector.fill(255)

           self.vertex = self.createVertex(vector, extent, typeGeom, self.parent.name, pointStyle, lineStyle, fillStyle)
           index = 0

           for i in range(0, self.layer.GetFeatureCount()):
               try:
                   feat = self.layer.GetFeature(i)
                   if not feat:
                       raise exceptionModel.FeatureError()
                   geom = feat.GetGeometryRef()
                   wkb = geom.ExportToWkb()
                   vectorTemp = self.processWKB(self.vertex, wkb)

                   if vectorTemp is not None:
                        vector[index] = vectorTemp
                        index += 1
               except exceptionModel.FeatureError as e:
                   QMessageBox.critical(self.parent, '%s'%e.title, '%s'%e.msg, QMessageBox.Ok)

           svg = SvgRenderer(self, self.outColumns, self.outRows)
           svgName = svg.writeToSvg(self.vertex, viewPixSize, extent)


           return (svgName, QSize(self.outColumns, self.outRows))

    # def renderSpatialite(self, geom, typeGeom, name):
    #
    #   self.geom = geom
    #   self.typeGeom = typeGeom
    #   self.name = name
    #
    #   vector = np.zeros(len(self.geom), dtype=object)
    #   vector.fill(255)
    #
    #   # vertex = self.createVertex(vector, extent, typeGeom, self.parent.name, pointStyle, lineStyle, fillStyle)
    #   # index = 0
    #   # for obj in geom:
    #   #  for row in geom:
    #   #      for elem in row:
    #   #          vector[index] = self.processPolygon(elem.)


    def createVertex(self, vector, extent, typeGeom, name, pointStyle, lineStyle, fillStyle):
       """
       @brief Returns vertex object.
       @param vector: Cooridnates array.
       @param extent: Extent of vector.
       @param typeGeom: Geometry type.
       @param name: Name of vector.
       @param pointStyle: Style of point.
       @param lineStyle: Style of line.
       @param fillStyle: Style of filling.
       """
       vertex = Vertex(vector, extent, typeGeom, name,pointStyle, lineStyle, fillStyle)
       return vertex

    def processWKB(self, vertex, wkb):
        """
        @brief Returns vector coordinates.
        @param vertex: Vertex object.
        @param wkb: Well-known binary.
        """

        if vertex.typeGeom == 1:
            return self.processPoint(wkb)
        elif vertex.typeGeom == 2:
            return self.processLine(wkb)
        elif vertex.typeGeom == 3:
            return self.processPolygon(wkb)


    def processPoint(self, wkb):
        """
        @brief Returns coordinates of points.
        @param wkb: Well-known binary.
        """
        geom = ogr.CreateGeometryFromWkb(wkb)
        points = geom.GetPointCount()

        if points:
            tempVector = []
            for p in xrange(0, points):
                point = geom.GetPoint(p)
                if not self.singlePointInView(point, points):
                    tempVector.append([point[0], point[1]])

            vector = np.array(tempVector)
            return vector


    def processLine(self, wkb):
        """
        @brief Returns coordinates of lines.
        @param wkb: Well-known binary.
        """

        geom = ogr.CreateGeometryFromWkb(wkb)
        points = geom.GetPointCount()
        tempVectorAll = []
        if points:
            for p in xrange(0, points-1):
                p1 = geom.GetPoint(p)
                p2 = geom.GetPoint(p+1)
                coord = self.singleLineInView(p1, p2, points)
                tempVectorAll.append(coord)
            vector = np.array(tempVectorAll)
            return vector

    def processPolygon(self, wkb):
       """
       @brief Returns coordinates of polygons.
       @param wkb: Well-known binary.
       """

       geom = ogr.CreateGeometryFromWkb(wkb)
       geomCount = geom.GetGeometryCount()

       tempVectorAll = []
       for g in range(geomCount):
           tempVector = []
           geomIn = geom.GetGeometryRef(g)
           diff = self.singlePolygonInView(geomIn)
           geomName = diff.GetGeometryName()

           if diff:
               if geomName == 'LINEARRING':
                    points = diff.GetPointCount()
                    for p in range(points):
                        point = diff.GetPoint(p)
                        tempVector.append([point[0], point[1]])
               elif geomName == 'POLYGON':
                   for ring in diff:
                       pointCount = ring.GetPointCount()
                       for p in range(pointCount):
                           point = ring.GetPoint(p)
                           tempVector.append([point[0], point[1]])


           tempVectorAll.append(tempVector)

       vector = np.array(tempVectorAll)

       return vector


    def singlePointInView(self, point, points):
        """
        @brief Check if point is in view.
        @param point: Verified point.
        @param points: All visible points.
        """

        for file in self.filesInTop:
            ogrPoint = ogr.Geometry(ogr.wkbPoint)
            ogrPoint.AddPoint(point[0], point[1])
            if file.boundary.Contains(ogrPoint):
                points -= 1
                return True
        return False

    def singleLineInView(self, point1, point2, points):
        """
        @brief Check if line is in view.
        @param point1: First point of verified line.
        @param point2: Second point of verified line.
        @param points: All visible lines.
        """

        pointLineAll = []
        if not self.filesInTop:
             pointLine = []
             pointLine.append([point1[0], point1[1]])
             pointLine.append([point2[0], point2[1]])
             pointLineAll.append(pointLine)


        for file in self.filesInTop:
            pointLine = []
            line = ogr.Geometry(ogr.wkbLineString)
            line.AddPoint(point1[0], point1[1])
            line.AddPoint(point2[0], point2[1])

            boundaryCheck = self.checkLineBoundary(file, line)
            if not boundaryCheck[0].IsEmpty():
                pointLine.append([boundaryCheck[0].GetX(), boundaryCheck[0].GetY()])
                if point1[0] < point2[0]:
                    pointLine.append([point1[0], point1[1]])
                else:
                    pointLine.append([point2[0], point2[1]])
                pointLineAll.append(pointLine)
                pointLine = []

            if not boundaryCheck[1].IsEmpty():
                pointLine.append([boundaryCheck[1].GetX(), boundaryCheck[1].GetY()])
                if point1[0] > point2[0]:
                    pointLine.append([point1[0], point1[1]])
                else:
                    pointLine.append([point2[0], point2[1]])
                pointLineAll.append(pointLine)
                pointLine = []


            if not boundaryCheck[2].IsEmpty():
                pointLine.append([boundaryCheck[2].GetX(), boundaryCheck[2].GetY()])
                if point1[1] < point2[1]:
                    pointLine.append([point1[0], point1[1]])
                else:
                    pointLine.append([point2[0], point2[1]])
                pointLineAll.append(pointLine)
                pointLine = []

            if not boundaryCheck[3].IsEmpty():
                pointLine.append([boundaryCheck[3].GetX(), boundaryCheck[3].GetY()])
                if point1[0] > point2[0]:
                    pointLine.append([point1[0], point1[1]])
                else:
                    pointLine.append([point2[0], point2[1]])
                pointLineAll.append(pointLine)

            if not file.boundary.Intersect(line):
                 pointLine.append([point1[0], point1[1]])
                 pointLine.append([point2[0], point2[1]])
                 pointLineAll.append(pointLine)

        return pointLineAll

    def singlePolygonInView(self, polygon):
        """
        @brief Check if polygon is in view.
        @param polygon: Verified polygon.
        """

        diff = polygon
        if self.filesInTop:
            if polygon.GetGeometryName() == 'LINEARRING':
                polygon.CloseRings()
                diff = ogr.Geometry(ogr.wkbPolygon)
                diff.AddGeometry(polygon)

        for file in self.filesInTop:
            if diff.Intersect(file.boundary):
                diff = diff.Difference(file.boundary)
        return diff

    def countPointInView(self, count, vectorBoundary):
        """
        @brief Count number of points which crosses with view.
        @param polygon: Verified polygon.
        """
        # jeszcze do poprawy jak zadziała wczytywanie kilku wektorów
        allInter = None

        for file in self.filesInTop:
            if file.boundary.Intersect(vectorBoundary):
                inter = vectorBoundary.Intersection(file.boundary)
                if not allInter:
                    allInter = inter
                else:
                    allInter = allInter.Union(inter)

                self.layer.SetSpatialFilter(inter)
                count -= self.layer.GetFeatureCount()
                self.layer.SetSpatialFilter(None)

    def checkLineBoundary(self, file, line):
       """!
       @brief Test if line intersects the raster.
       @param file: Raster file.
       @param line: Verified line.
       @brief Returns points of intersection (left, right, up, top).
       """
       x_min, x_max, y_min, y_max = file.boundary.GetEnvelope()
       left = ogr.Geometry(ogr.wkbLineString)
       left.AddPoint(x_min, y_min)
       left.AddPoint(x_min, y_max)
       right = ogr.Geometry(ogr.wkbLineString)
       right.AddPoint(x_max, y_min)
       right.AddPoint(x_max, y_max)
       up = ogr.Geometry(ogr.wkbLineString)
       up.AddPoint(x_min, y_min)
       up.AddPoint(x_max, y_min)
       top = ogr.Geometry(ogr.wkbLineString)
       top.AddPoint(x_min, y_max)
       top.AddPoint(x_max, y_max)

       l = line.Intersection(left)
       r = line.Intersection(right)
       u = line.Intersection(up)
       t = line.Intersection(top)

       return [l, r, u, t]

    def fromCoordinatesToPixels(self, point, pixSize, extent):
        """
        @brief Convert cooridnates to pixel.
        @param point: Coordinates of point.
        @param pixSize: Size of pixel.
        @param extent: Extent of vector.
        """
        x = point[0]
        y = point[1]

        xPix = int((x - extent[0]) / pixSize)
        yPix = int((extent[3] - y) / pixSize)

        return QPoint(xPix, yPix)

class Vertex(object):
    def __init__(self, vector, extent, typeGeom, name, pointStyle, lineStyle, fillStyle):
        self._vector = vector
        self._extent = extent
        self._typeGeom = typeGeom
        self._name = name
        self._pointStyle = pointStyle
        self._lineStyle = lineStyle
        self._fillStyle = fillStyle


    @property
    def vector(self):
        return self._vector

    @property
    def extent(self):
        return self._extent

    @property
    def typeGeom(self):
        return self._typeGeom

    @property
    def name(self):
        return self._name

    @property
    def pointStyle(self):
        return self._pointStyle

    @property
    def lineStyle(self):
        return self._lineStyle

    @property
    def fillStyle(self):
        return self._fillStyle


class SvgRenderer(object):
    def __init__(self, parent, column, row):
        """!
        @brief Open file and read basic information
        @param column: Size of column.
        @param row: Size of row.
        """
        self.parent = parent
        self.doc = et.Element('svg', width='%s'%column, height='%s'%row, xmlns='http://www.w3.org/2000/svg')
        self.svgLineStyle = {1: 'none', 2: '10,5', 3: '5,5', 4: '15,10,5,10', 5: '20,10,5,5,5,10'}
        self.svgLineCapStyle = {1: 'butt', 2: 'square', 3: 'round'}
        self.svgLineJoinStyle = {1: 'miter', 2: 'round', 3: 'bevel'}


    def writeToSvg(self, vertex, pixSize, extent):
        """!
        @brief Create file and write basic parameters. Returns name of created svg file.
        @param vertex: Vertex object.
        @param pixSize: Size of pixel.
        @param extent: Extent of vector.
        """
        if vertex._typeGeom == 1:
            self.addPoint(vertex, pixSize, extent)
        elif vertex._typeGeom == 2:
            self.addLine(vertex, pixSize, extent)
        elif vertex._typeGeom == 3:
            self.addPolygon(vertex, pixSize, extent)

        svgName = self.getTempPathToSvg(vertex._name)

        f = open('%s'%svgName, 'w')
        f.write('<?xml version=\"1.0\" standalone=\"no\"?>\n')
        f.write('<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n')
        f.write('\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n')
        f.write(et.tostring(self.doc))
        f.close()

        return svgName

    def getTempPathToSvg(self, name):
        """!
        @brief Create path to svg. Returns the path to created svg file.
        @param name: The name of created svg file.
        """
        tempPath = os.path.join(self.parent.pathTemp)

        svgName = os.path.join(tempPath, name + '.svg')
        return svgName


    def addPoint(self, vertex, pixSize, extent):
        """!
        @brief Add points to svg graphics.
        @param vertex: Vertex object.
        @param pixSize: Size of pixel.
        @param extent: Extent of vector.
        """
        for obj in vertex._vector:
            for elem in obj:
                point = self.parent.fromCoordinatesToPixels(elem, pixSize, extent)
                if vertex._fillStyle.opacity: opacity = 1 - (vertex._fillStyle.opacity / 100)
                else: opacity = 1
                et.SubElement(self.doc, 'circle',
                              cx = '%s'%(point.x() + vertex._pointStyle.size+vertex._lineStyle.width),
                              cy = '%s'%(point.y() + vertex._pointStyle.size+vertex._lineStyle.width),
                              r = '%s'%vertex._pointStyle.size,
                              stroke = 'rgb(%s,%s,%s)'%(vertex._lineStyle.colour.red(), vertex._lineStyle.colour.green(), vertex._lineStyle.colour.blue()),
                              style = 'stroke-opacity: %s; stroke-width: %s;'%(opacity, vertex._lineStyle.width),
                              fill = 'rgb(%s,%s,%s)'%(vertex._fillStyle.colour.red(), vertex._fillStyle.colour.green(), vertex._fillStyle.colour.blue()))

    def addLine(self, vertex, pixSize, extent):
        """!
        @brief Add lines to svg graphics.
        @param vertex: Vertex object.
        @param pixSize: Size of pixel.
        @param extent: Extent of vector.
        """
        for obj in vertex._vector:
            for elem in obj:
                for singlePoint in elem:
                    path = ''
                    for singleGeom in singlePoint:
                        point = self.parent.fromCoordinatesToPixels(singleGeom, pixSize, extent)
                        path += str(point.x()+vertex._pointStyle.size+vertex._lineStyle.width) + ',' + str(point.y()+vertex._pointStyle.size+vertex._lineStyle.width) + ' '
                        if vertex._fillStyle.opacity: opacity = 1 - (vertex._fillStyle.opacity / 100)
                        else: opacity = 1
                    et.SubElement(self.doc, 'polyline',
                          points = '%s'%path,
                          stroke = 'rgb(%s,%s,%s)'%(vertex._lineStyle.colour.red(), vertex._lineStyle.colour.green(), vertex._lineStyle.colour.blue()),
                          style = 'stroke-width: %s; stroke-linecap: %s; stroke-dasharray: %s; stroke-opacity: %s; stroke-linejoin: %s;'%
                                  (vertex._lineStyle.width, self.svgLineCapStyle[vertex._lineStyle.capLine], self.svgLineStyle[vertex._lineStyle.patternLine],
                                   opacity, self.svgLineJoinStyle[vertex._lineStyle.joinLine]),fill = 'none')


    def addPolygon(self, vertex, pixSize, extent):
        """!
        @brief Add polygons to svg graphics.
        @param vertex: Vertex object.
        @param pixSize: Size of pixel.
        @param extent: Extent of vector.
        """

        for obj in vertex._vector:
            for elem in obj:
                path = ''
                if vertex._fillStyle.opacity: opacity = 1 - (vertex._fillStyle.opacity / 100)
                else: opacity = 1

                for singleGeom in elem:
                    point = self.parent.fromCoordinatesToPixels(singleGeom, pixSize, extent)
                    path += str(point.x()+vertex._pointStyle.size+vertex._lineStyle.width) + ',' + str(point.y()+vertex._pointStyle.size+vertex._lineStyle.width) + ' '
                et.SubElement(self.doc, 'polyline',
                          points = '%s'%path,
                          stroke = 'rgb(%s,%s,%s)'%(vertex._lineStyle.colour.red(), vertex._lineStyle.colour.green(), vertex._lineStyle.colour.blue()),
                          fill = 'none',
                          style = 'stroke-width: %s; stroke-opacity: %s; stroke-dasharray: %s;'%(vertex._lineStyle.width, opacity, self.svgLineStyle[vertex._lineStyle.patternLine]))

