# -*- coding: utf-8 -*-

class GdalToNumpy(object):

    @staticmethod
    def ConvertDataTypeNameToDataTypeName(GdalFormatName):
        """!
        @brief Converts GDAL array type into Numpy array type.
        @param GdalFormatName String name of data type in GDAL convention
        @return String name of the same data type in NumPy convention.
        """
        if GdalFormatName == 'UInt16': return 'uint16'
        elif GdalFormatName == 'Byte': return 'uint8'
        elif GdalFormatName == 'UInt32': return 'uint32'

    @staticmethod
    def CoefficientGDALtoQImage(stringType):
        """!
        @brief CURRENTLY NOT USED !! REMAINS JUST IN CASE !!
        @brief !! NOW USES LUT (Look Up Tables) !!
        @brief Gets coefficient converting image into 24bit
        @param stringType Name of data type in NumPy convention
        @return Coefficient recquired to compute from broader data type into narrower.
        """
        result = GdalToNumpy.ConvertDataTypeNameToDataTypeName(stringType)
        if result == 'uint8': return 1
        elif result == 'uint16': return 256
        elif result == 'uint32': return 65536

    @staticmethod
    def ConvertFloatTypeNameToDataTypeName(gdalDataset):
        numberOfBands = gdalDataset.RasterCount
        minValue = numberOfBands * [None]
        maxValue = numberOfBands * [None]
        for i in range(1, numberOfBands+1):
            band = gdalDataset.GetRasterBand(i)
            minValue[i-1], maxValue[i-1] = band.ComputeRasterMinMax(1)
        return minValue, maxValue

    @staticmethod
    def CoefficientGDALtoQImageFloat(bands):
        coeffs = len(bands) * [None]
        for i, band in enumerate(bands):
            mi, ma = band.ComputeRasterMinMax(1)
            coeffs[i] = 255/(ma - mi)
        return coeffs

