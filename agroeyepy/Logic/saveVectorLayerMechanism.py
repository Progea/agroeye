# -*- coding: utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import QString
import re
import os
import ogr, osr
from Logic import vectorLayerMechanism
from Control import exceptionModel
_fromUtf8 = QString.fromUtf8

class NewVector(object):

    def __init__(self, parent):
        super(NewVector, self).__init__()
        self.parent = parent


    def checkFileName(self, filename):
        """
        @brief Check if file name is valid.
        When creating new file, it is important to follow these rules:
        - names must start with letter
        - names must contain only letters, numbers and underscore
        - spaces and other special characters are not supported
        - names is limited to 15 characters
        @param filename: The verified name.
        """
        pattern1 = '^[A-Za-z]'
        pattern2 = '^[A-Za-z][A-Za-z0-9_]*$'
        pattern3 = '^[A-Za-z][A-Za-z0-9_]{1,15}$'

        try:
            if re.match(pattern1, filename):
                if re.match(pattern2, filename):
                    if re.match(pattern3, filename):
                        return True
                    else:
                        raise exceptionModel.NumberCharWarning()
                else:
                    raise exceptionModel.SpecialCharWarning()
            else:
                raise exceptionModel.StartCharWarning()
        except exceptionModel.NumberCharWarning as e:
            QMessageBox.warning(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
            return False
        except exceptionModel.SpecialCharWarning as e:
            QMessageBox.warning(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
            return False
        except exceptionModel.StartCharWarning as e:
            QMessageBox.warning(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)
            return False

    def createNewShapefileWithoutField(self, filename, geomType, epsg, geomData):
        """!
        @brief Save geometries as own shapefile file and add it to loadfiles_model. Returns shapefile without field, only coordinates.
        @param filename: The name of created file.
        @param geomType: The geometry type of created file.
        @param epsg: The epsg code.
        @param geomData: The data contains coordinates.
        """
        driver = ogr.GetDriverByName('ESRI Shapefile')
        nameOutShape = str(filename)

        if os.path.exists(nameOutShape):
            driver.DeleteDataSource(nameOutShape)

        spatialReference = osr.SpatialReference()
        spatialReference.ImportFromEPSG(epsg)

        outDataSource = driver.CreateDataSource(nameOutShape)

        if geomType == 1:
            self.savePointWithoutField(outDataSource, spatialReference, geomData[0])
        elif geomType == 2:
            self.saveLineWithoutField(outDataSource, spatialReference, geomData[1])
        elif geomType == 3:
            self.savePolyWithoutField(outDataSource, spatialReference, geomData[2])

        outDataSource.Destroy()
        self.addToList(nameOutShape, self.parent)

        return True

    def deleteSource(self, vectorLayer):
        """!
        @brief Delete opened datasource.
        @param vectorLayer: Vector datasource to delete.
        """
        vectorLayer.vectorDataSource.Destroy()

    def createNewShapefileWithField(self, filename, tableLayer, viewArea):
        """!
        @brief Save geometries as new shapefile file and add it to loadfiles_model.
        @param filename: The name of created file.
        @param tableLayer: Object to Table Class.
        @param viewArea: View.
        """

        nameOutShape = str(filename)

        head, tail  = os.path.split(str(filename))
        root, ext = os.path.splitext(tail)

        if ext == '.shp':
            driver = ogr.GetDriverByName('ESRI Shapefile')
        elif ext == '.kml':
            driver = ogr.GetDriverByName('KML')
        elif ext == '.gml':
            driver = ogr.GetDriverByName('GML')


        try:
            if os.path.isfile(str(filename)):
                os.remove(str(filename))
        except exceptionModel.OverwriteFileError as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)

        spatialReference = tableLayer.srs
        outDataSource = driver.CreateDataSource(nameOutShape)
        if tableLayer.geomType == 1:
            self.savePointWithField(outDataSource, spatialReference, tableLayer)
        elif tableLayer.geomType == 2:
            self.saveLineWithField(outDataSource, spatialReference, tableLayer)
        elif tableLayer.geomType == 3:
            self.savePolyWithField(outDataSource, spatialReference, tableLayer)
        outDataSource.Destroy()
        self.addToList(nameOutShape, viewArea, tableLayer.pathTemp)

    def savePointWithField(self, outDataSource, spatialReference, tableLayer):
        """!
        @brief Save geometries as points with features.
        @param outDataSource: Datasource for created file.
        @param spatialReference: Spatial reference system for created file.
        @param tableLayer: Object of Table Class.
        """

        outLayer = outDataSource.CreateLayer('', spatialReference, geom_type=ogr.wkbPoint)
        outLayerDefinition = outLayer.GetLayerDefn()

        self.setNewField(tableLayer.fieldName, tableLayer, outLayer)

        if tableLayer.geomData.size:
            for row, object in enumerate(tableLayer.geomData):
                for point in object:
                    geom = ogr.Geometry(ogr.wkbPoint)
                    geom.AddPoint(point[0], point[1])
                    feature = ogr.Feature(outLayerDefinition)
                    feature.SetGeometry(geom)

                    for col in range(0, len(tableLayer.fieldName)):
                        if str(tableLayer.fieldTypeCol[col]) in ('Date', 'DateTime'):
                            date = tableLayer.fieldVals[col][row]
                            feature.SetField(str(tableLayer.fieldName[col]),int(date.year()), int(date.month()), int(date.day()), 0, 0, 0,0)
                        elif str(tableLayer.fieldTypeCol[col]) == 'Boolean':
                            if tableLayer.fieldVals[col][row]:
                                val = 'True'
                            else: val = 'False'
                            feature.SetField(str(tableLayer.fieldName[col]),val)
                        else:
                            feature.SetField(str(tableLayer.fieldName[col]),tableLayer.fieldVals[col][row])
                    outLayer.CreateFeature(feature)
                    geom.Destroy()
                    feature.Destroy()

    def saveLineWithField(self, outDataSource, spatialReference, tableLayer):
        """!
        @brief Save geometries as polyline with features.
        @param outDataSource: Datasource for created file.
        @param spatialReference: Spatial reference system for created file.
        @param tableLayer: Object of Table Class.
        """

        outLayer = outDataSource.CreateLayer('', spatialReference, geom_type=ogr.wkbLineString)
        outLayerDefinition = outLayer.GetLayerDefn()

        self.setNewField(tableLayer.fieldName, tableLayer, outLayer)


        if tableLayer.geomData.size:
            for row, line in enumerate(tableLayer.geomData):
                geom = ogr.Geometry(ogr.wkbLineString)
                for point in line:
                    geom.AddPoint(point[0], point[1])
                feature = ogr.Feature(outLayerDefinition)
                feature.SetGeometryDirectly(geom)

                for col in range(0, len(tableLayer.fieldName)):
                    if str(tableLayer.fieldTypeCol[col]) in ('Date',  'DateTime'):
                        date = tableLayer.fieldVals[col][row]
                        feature.SetField(str(tableLayer.fieldName[col]),int(date.year()), int(date.month()), int(date.day()), 0, 0, 0,0)
                    elif str(tableLayer.fieldTypeCol[col]) == 'Boolean':
                        if tableLayer.fieldVals[col][row]:
                            val = 'True'
                        else: val = 'False'
                        feature.SetField(str(tableLayer.fieldName[col]),val)
                    else:
                        feature.SetField(str(tableLayer.fieldName[col]),tableLayer.fieldVals[col][row])
                outLayer.CreateFeature(feature)
                geom.Destroy()
                feature.Destroy()




    def savePolyWithField(self, outDataSource, spatialReference, tableLayer):
        """!
        @brief Save geometries as polygon with features.
        @param outDataSource: Datasource for created file.
        @param spatialReference: Spatial reference system for created file.
        @param tableLayer: Object of Table Class.
        """
        outLayer = outDataSource.CreateLayer('', spatialReference, geom_type=ogr.wkbPolygon)
        outLayerDefinition = outLayer.GetLayerDefn()

        self.setNewField(tableLayer.fieldName, tableLayer, outLayer)

        if tableLayer.geomData.size:
            for row, line in enumerate(tableLayer.geomData):
                geom = ogr.Geometry(ogr.wkbLinearRing)
                for point in line:
                    geom.AddPoint(point[0], point[1])
                geom.CloseRings()
                poly = ogr.Geometry(ogr.wkbPolygon)
                poly.AddGeometry(geom)
                feature = ogr.Feature(outLayerDefinition)
                feature.SetGeometryDirectly(poly)
                for col in range(0, len(tableLayer.fieldName)):
                    if str(tableLayer.fieldTypeCol[col]) in ('Date',  'DateTime'):
                        date = tableLayer.fieldVals[col][row]
                        feature.SetField(str(tableLayer.fieldName[col]),int(date.year()), int(date.month()), int(date.day()), 0, 0, 0,0)
                    elif str(tableLayer.fieldTypeCol[col]) == 'Boolean':
                        if tableLayer.fieldVals[col][row]:
                            val = 'True'
                        else: val = 'False'
                        feature.SetField(str(tableLayer.fieldName[col]),val)
                    else:
                        print 'value', tableLayer.fieldVals[col][row]
                        feature.SetField(str(tableLayer.fieldName[col]),tableLayer.fieldVals[col][row])
                outLayer.CreateFeature(feature)
                geom.Destroy()
                feature.Destroy()

    def savePointWithoutField(self, outDataSource, spatialReference, geomData):
        """!
        @brief Save geometries as points without field.
        @param outDataSource: Datasource for created file.
        @param spatialReference: Spatial reference system for created file.
        @param geomData: Coordinates of points.
        """

        outLayer = outDataSource.CreateLayer('', spatialReference, geom_type=ogr.wkbPoint)
        outLayerDefinition = outLayer.GetLayerDefn()

        for i, point in enumerate(geomData):
            geom = ogr.Geometry(ogr.wkbPoint)
            geom.AddPoint(point[0], point[1])
            feature = ogr.Feature(outLayerDefinition)
            feature.SetGeometry(geom)
            feature.SetFID(i)
            outLayer.CreateFeature(feature)
            geom.Destroy()
            feature.Destroy()

    def saveLineWithoutField(self, outDataSource, spatialReference, geomData):
        """!
        @brief Save geometries as lines without field.
        @param outDataSource: Datasource for created file.
        @param spatialReference: Spatial reference system for created file.
        @param geomData: Coordinates of lines.
        """
        outLayer = outDataSource.CreateLayer('', spatialReference, geom_type=ogr.wkbLineString)
        outLayerDefinition = outLayer.GetLayerDefn()

        geom = ogr.Geometry(ogr.wkbLineString)
        for point in geomData:
            geom.AddPoint(point[0], point[1])
        feature = ogr.Feature(outLayerDefinition)
        feature.SetGeometryDirectly(geom)
        feature.SetFID(1)
        outLayer.CreateFeature(feature)

    def savePolyWithoutField(self, outDataSource, spatialReference, geomData):
        """!
        @brief Save geometries as polygon without field.
        @param outDataSource: Datasource for created file.
        @param spatialReference: Spatial reference system for created file.
        @param geomData: Coordinates of polygons.
        """
        outLayer = outDataSource.CreateLayer('', spatialReference, geom_type=ogr.wkbPolygon)
        outLayerDefinition = outLayer.GetLayerDefn()

        geom = ogr.Geometry(ogr.wkbLinearRing)
        for point in geomData:
            geom.AddPoint(point[0], point[1])

        poly = ogr.Geometry(ogr.wkbPolygon)
        poly.AddGeometry(geom)
        feature = ogr.Feature(outLayerDefinition)
        feature.SetGeometryDirectly(poly)
        feature.SetFID(1)
        outLayer.CreateFeature(feature)

    def diffList(self, oldList, newList):
        """!
        @brief Returns difference between two list.
        """
        return list(set(newList).difference(set(oldList)))

    def checkRenameList(self, renameField, currField):
         """!
         @brief Returns the rename column name.
         @param renameField: List of rename column
         @param currField: List of name edited column (field)
         """
         tempList = [t[1] for t in renameField]
         return self.diffList(tempList, currField)

    def setNewField(self, newField, tableLayer, vectorLayer):
        """!
        @brief Set new Field in vector file (column in attribute table).
        @param newField: The new field.
        @param tableLayer: The object of Table Class.
        @param vectorLayer: Object of selected vector.
        """
        if newField:
            for name in newField:
                index = tableLayer.fieldName.index(name)
                typeOgr = self.transformType(tableLayer.fieldTypeCol[index])
                newFld = ogr.FieldDefn(str(name), typeOgr)
                newFld.SetWidth(tableLayer.fieldWidth[index])
                newFld.SetPrecision(tableLayer.fieldPrec[index]+1)
                vectorLayer.CreateField(newFld)


    def transformType(self, type):
        """!
        @brief Returns the string type to ogr type.
        @param type: Type of geometry read from file.
        """
        if type in ('Integer', 'Integer64') :
            type = ogr.OFTInteger
        elif type == 'Real':
            type = ogr.OFTReal
        elif type == 'String':
            type = ogr.OFTString
        elif type == 'Date':
            type = ogr.OFTDate
        elif type == 'Boolean':
            type = ogr.OFTString
        return type

    def delField(self, delField, vectorLayer):
        """!
        @brief Delete Field in vector file (column in attribute table).
        @param delField: List of name column to delete (field).
        @param vectorLayer: Object of selected vector.
        """
        if delField:
            for col in delField:
                index = vectorLayer.fieldName.index(col)
                vectorLayer.vectorLayer.DeleteField(index)

    def renameField(self, vectorLayer, editColumnName):
        """!
        @brief Change name of Field in vector file (column in attribute table).
        @param vectorLayer: Object of selected vector.
        @param editColumnName: List of name edited column (field).
        """

        for col in editColumnName:
            index = vectorLayer.fieldName.index(col[0])
            fieldDefn = vectorLayer.layerDefinition.GetFieldDefn(index)
            fieldDefn.SetName(str(col[1]))
            vectorLayer.vectorLayer.CreateField(fieldDefn)
            vectorLayer.vectorLayer.DeleteField(index)

    def setFeature(self, vectorLayer, editRow):
        """!
        @brief Set Features in vector file (rows in attribute table).
        @param vectorLayer: Object of selected vector.
        @param editRow: List of values of edited row (features).
        """

        for row in range(0, vectorLayer.vectorLayer.GetFeatureCount()):
            inFeature = vectorLayer.vectorLayer.GetFeature(row)
            inLayerDefn = vectorLayer.vectorLayer.GetLayerDefn()
            for col in range(0, inLayerDefn.GetFieldCount()):
                if inFeature:
                    type = inLayerDefn.GetFieldDefn(col).GetType()
                    nameType = inLayerDefn.GetFieldDefn(col).GetFieldTypeName(type)
                    if str(nameType) in ('Date', 'DateTime'):
                       date = vectorLayer.fieldVals[col][row]
                       inFeature.SetField(str(vectorLayer.fieldName[col]),int(date.year()), int(date.month()), int(date.day()), 0, 0, 0,0)
                    else:
                        if isinstance(editRow[col][row], bool):
                            if editRow[col][row]:
                                val = 'True'
                            else: val = 'False'
                            inFeature.SetField(inLayerDefn.GetFieldDefn(col).GetNameRef(),val)
                        else:
                            inFeature.SetField(inLayerDefn.GetFieldDefn(col).GetNameRef(),editRow[col][row])
                    vectorLayer.vectorLayer.SetFeature(inFeature)

    def delFeature(self, vectorLayer, editRow):
        """!
        @brief Delete Features in vector file (rows in attribute table).
        @param vectorLayer: Object of selected vector.
        @param editRow: List of values of edited row (features).
        """

        if editRow:
            for col in editRow:
                featID = vectorLayer.vectorLayer.GetFeature(col).GetFID()
                vectorLayer.vectorLayer.DeleteFeature(featID)
                vectorLayer.vectorDataSource.ExecuteSQL('REPACK ' + vectorLayer.vectorLayer.GetName())
                vectorLayer.vectorLayer.GetNextFeature()

    def updateFile(self, vectorLayer):
        """!
        @brief Updates the file after save.
        @param vectorLayer: Object of selected vector.
        """
        vectorLayer.vectorDataSource.Destroy()
        vectorLayer.vectorFile.checkLayer(vectorLayer.vectorFile.path)
        vectorLayer.addVectorFile(0)

    def addToList(self, filename, viewArea, pathTemp):
        """!
        @brief Add new file to loadfiles model.
        @param filename: The name of created file.
        @param viewArea: The view.
        """
        vectorFile = vectorLayerMechanism.VectorCheckLayer(self, filename)
        vectorDataset = vectorLayerMechanism.VectorFile(viewArea, vectorFile, 0, pathTemp)

        try:
            if vectorDataset:
                viewArea.loadedfiles_model.insertRows(vectorDataset, 0)
                QMessageBox.information(self.parent, _fromUtf8(_('Save status')), _fromUtf8(_('All changes have been saved to:\n%s'))%filename, QMessageBox.Ok)
                viewArea.update_ui()
            else:
                raise exceptionModel.SaveFileError()
        except exceptionModel.SaveFileError as e:
            QMessageBox.critical(self.parent, e.title, '%s'%e.msg, QMessageBox.Ok)

