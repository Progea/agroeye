# -*- coding: utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import datetime
from View import ui_addcolumndialog
from View import ui_delcolumndialog
from View import ui_renamecolumndialog
from Control import exceptionModel
_fromUtf8 = QString.fromUtf8


class ColumnAddDialog(QDialog, ui_addcolumndialog.Ui_addColumnDialog):
    def __init__(self, parent, tableWidget):
        """!
        @brief Open dialog to add new column
        @brief Check if file name is valid.
        When creating new column, it is important to follow these rules:
        - names must start with letter
        - names must contain only letters, numbers and underscore
        - spaces and other special characters are not supported
        - names is limited to 15 characters
        """

        super(ColumnAddDialog, self).__init__(parent, Qt.WindowSystemMenuHint|Qt.WindowTitleHint)
        self.parent = parent
        self.validatorClass = ValidateClass(self.parent)
        self.tabletWidget = tableWidget
        self.setupUi(self)
        self.okButton.setEnabled(False)
        self.connect(self.okButton, SIGNAL("clicked()"), self.accept)
        self.connect(self.cancelButton, SIGNAL("clicked()"), self.close)
        self.connect(self.nameLineEdit, SIGNAL("textChanged(QString)"), self.changeName)
        self.connect(self.nameLineEdit, SIGNAL("keyPressEvent(QKeyEvent *e)"), self.keyEvent)
        self.connect(self.lenLineEdit, SIGNAL("keyPressEvent(QKeyEvent *e)"), self.keyEvent)
        self.connect(self.precLineEdit, SIGNAL("keyPressEvent(QKeyEvent *e)"), self.keyEvent)
        self.connect(self.typeComboBox, SIGNAL("currentIndexChanged(int)"), self.changeType)


        regexpNameEdit = QRegExp(r'^[A-Za-z][A-Za-z0-9_]{1,15}$')
        validator = QRegExpValidator(regexpNameEdit, self.nameLineEdit)
        self.nameLineEdit.setValidator(validator)

        regexpNumberEdit = QRegExp(r'^[1-9]{0,1}$')
        validator = QRegExpValidator(regexpNumberEdit, self.lenLineEdit)
        self.lenLineEdit.setValidator(validator)
        self.precLineEdit.setValidator(validator)

        self.addType()
        self.addPosition()


    def changeName(self):
        """!
        @brief Action after write the name.
        """
        value = self.nameLineEdit.text().isEmpty()
        self.okButton.setEnabled(not value)


    def changeType(self, index):
        """!
        @brief Action after changed field in typeComboBox.
        @param index: Index of new selected row.
        """
        self.nameLineEdit.setToolTip(_fromUtf8(_('Name contains only letters, numbers and underscores.\nIt is limited to 15 characters.\nIt starts with letter')))
        if index == 0:
            regexpNumberEdit = QRegExp(r'^[1-9]{0,1}$')
            validator = QRegExpValidator(regexpNumberEdit, self.lenLineEdit)
            self.lenLineEdit.setValidator(validator)
            self.lenLineEdit.setEnabled(True)
            self.precLineEdit.setEnabled(False)
            self.lenLineEdit.setToolTip(_fromUtf8('Number of digits'))
            self.lenLineEdit.setText('10')
            self.precLineEdit.setToolTip('')
            self.precLineEdit.setText('')
            self.typeComboBox.setToolTip('')
        elif index == 1:
            regexpLengthEdit = QRegExp(r'^(?:[1-9]|[12]\d|3[0-2])')
            validator = QRegExpValidator(regexpLengthEdit, self.lenLineEdit)
            self.lenLineEdit.setValidator(validator)
            regexpPrecEdit = QRegExp(r'^[1-9]{0,1}$')
            validator = QRegExpValidator(regexpPrecEdit, self.precLineEdit)
            self.precLineEdit.setValidator(validator)
            self.precLineEdit.setEnabled(True)
            self.lenLineEdit.setEnabled(True)
            self.lenLineEdit.setToolTip(_fromUtf8(_('Number of digits before the decimal point')))
            self.precLineEdit.setToolTip(_fromUtf8(_('Number of digits after the decimal point')))
            self.lenLineEdit.setText('12')
            self.precLineEdit.setText('10')
            self.typeComboBox.setToolTip('')
        elif index == 2:
            regexpNumberEdit = QRegExp(r'[0-9]|[0-7][0-9]|80')
            validator = QRegExpValidator(regexpNumberEdit, self.lenLineEdit)
            self.lenLineEdit.setValidator(validator)
            self.lenLineEdit.setEnabled(True)
            self.precLineEdit.setEnabled(False)
            self.lenLineEdit.setToolTip(_fromUtf8(_('Number of characters')))
            self.precLineEdit.setToolTip('')
            self.lenLineEdit.setText('80')
            self.precLineEdit.setText('')
            self.typeComboBox.setToolTip('')
        elif index == 3:
            self.lenLineEdit.setEnabled(False)
            self.precLineEdit.setEnabled(False)
            self.precLineEdit.setToolTip('')
            self.lenLineEdit.setToolTip('')
            self.lenLineEdit.setText('')
            self.precLineEdit.setText('')
            self.typeComboBox.setToolTip(_fromUtf8(_('Date format is: yyyy-mm-dd')))
        elif index == 4:
            self.lenLineEdit.setEnabled(False)
            self.precLineEdit.setEnabled(False)
            self.precLineEdit.setToolTip('')
            self.lenLineEdit.setToolTip('')
            self.lenLineEdit.setText('')
            self.precLineEdit.setText('')
            self.typeComboBox.setToolTip('')

    def addType(self):
        """!
        @brief Create type for typeComboBox.
        """
        typeList = ['Integer', 'Real', 'String', 'Date' ]
        self.typeComboBox.addItem('Integer')
        self.typeComboBox.addItem('Real')
        self.typeComboBox.addItem('String')
        self.typeComboBox.addItem('Date')
        self.typeComboBox.addItem('Boolean')

    def addPosition(self):
        """!
        @brief Create type for positionComboBox.
        """
        self.posComboBox.addItem(_fromUtf8(_('to the top')))
        for row in xrange(self.tabletWidget.rowCount()):
            item = self.tabletWidget.item(row, 0)
            self.posComboBox.addItem(_fromUtf8(_('after column ')) + item.text())
        self.posComboBox.setCurrentIndex(1)


    def accept(self):
        """!
        @brief Action after clicked OK button. Creates new column with given name and type.
        """
        nameCol = self.nameLineEdit.text()
        try:
            if self.validatorClass.checkNameIsUnique(nameCol):
                 typeColName = self.typeComboBox.currentText()
                 typeCol = self.typeComboBox.currentIndex()
                 posCol = self.posComboBox.currentIndex()

                 if self.lenLineEdit.isEnabled():
                    if not self.lenLineEdit.text().isEmpty():
                        lenCol = int(self.lenLineEdit.text())
                    else:
                        lenCol = self.validatorClass.setDefaultLength(typeCol)
                 else:
                    lenCol = 0
                 if self.precLineEdit.isEnabled():
                    if not self.precLineEdit.text().isEmpty():
                        preCol = int(self.precLineEdit.text())
                    else:
                        preCol = 3
                 else:
                     preCol = 0

                 self.addNewColumn(nameCol,typeColName, posCol, lenCol, preCol)
            else:
                raise exceptionModel.UniqueNameError
        except exceptionModel.UniqueNameError as e:
            QMessageBox.warning(self, '%s'%e.title, '%s'%e.msg, QMessageBox.Ok)


    def addNewColumn(self, nameCol, typeColName, posCol, lenCol, preCol):
        """!
        @brief Adds new column and updates all values
        @param nameCol: Name of new field.
        @param typeColName: Type of field.
        @param posCol: Position of field.
        @param lenCol: Length of field.
        @param preCol: Precision of field.
        """

        tempVals = []
        for i in xrange(self.parent.rowCount):
            tempVals.append(self.validatorClass.setValueFromType(typeColName, 0))

        if tempVals:
            self.parent.fieldVals.insert(posCol, tempVals)

        self.parent.fieldWidth.insert(posCol,lenCol)
        self.parent.fieldPrec.insert(posCol, preCol)
        self.parent.fieldTypeCol.insert(posCol, typeColName)

        self.tabletWidget.insertRow(posCol)
        tempItem1 = QTableWidgetItem()
        tempItem1.setData(0, nameCol)
        tempItem2 = QTableWidgetItem()
        text = self.validatorClass.setWidthAndPrecText(typeColName, posCol)
        tempItem2.setData(0, text)
        self.tabletWidget.setItem(posCol, 0, tempItem1)
        self.tabletWidget.setItem(posCol, 1, tempItem2)
        self.parent.updateColumn()

        modelIndex = self.tabletWidget.model().index(posCol, 0)
        self.tabletWidget.setCurrentIndex(modelIndex)
        self.tabletWidget.scrollTo(modelIndex)
        try:
            self.parent.buttonSave.setEnabled(True)
            self.parent.buttonSaveAs.setEnabled(True)
        except:
            self.parent.okButton.setEnabled(True)
        self.parent.addColumn = True
        self.close()

    def keyEvent(self, event):
        """!
        @brief Event for CTRl+C and CTRL+V.
        """
        if event.type() == QEvent.KeyPress:
            key = event.key()
            if key == QKeySequence.Copy or key == QKeySequence.Paste:
                key.accept()


class ColumnDelDialog(QDialog, ui_delcolumndialog.Ui_delColumnDialog):
    def __init__(self, parent, tableWidget):
        """!
        @brief Open dialog to delete column.
        """
        super(ColumnDelDialog, self).__init__(parent, Qt.WindowSystemMenuHint|Qt.WindowTitleHint)
        self.parent = parent
        self.tabletWidget = tableWidget
        self.setupUi(self)
        self.okButton.setEnabled(False)
        self.validateClass = ValidateClass(self.parent)
        self.listView.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.connect(self.okButton, SIGNAL("clicked()"), self.accept)
        self.connect(self.cancelButton, SIGNAL("clicked()"), self.close)
        self.connect(self.listView, SIGNAL("itemClicked(QListWidgetItem *)"), lambda: self.setButtonEnabled(True))
        self.connect(self.listView, SIGNAL("itemSelectionChanged()"), lambda: self.setButtonEnabled(True))
        self.addColumn()

    def addColumn(self):
        """!
        @brief Add names of column from attribute table to QListWidget.
        """
        for row in xrange(self.tabletWidget.rowCount()):
            rowHead = self.tabletWidget.item(row, 0)
            item = QListWidgetItem(rowHead.text())
            self.listView.addItem(item)

    def setButtonEnabled(self, bool):
        self.okButton.setEnabled(bool)

    def accept(self):
        """!
        @brief Action after clicked OK button. Delete the selected rows.
        """
        self.selectList = self.listView.selectedItems()
        for i in self.selectList:
            index = self.listView.row(i)
            self.tabletWidget.removeRow(index)
            item = self.listView.takeItem(index)
            item = None

            if self.listView.count() == 0:
                self.setButtonEnabled(False)
            if self.parent.fieldVals:
                self.parent.fieldVals.pop(index)
            self.parent.fieldTypeCol.pop(index)
            self.parent.fieldWidth.pop(index)
            self.parent.fieldPrec.pop(index)
            self.parent.updateColumn()
            modelIndex = self.tabletWidget.model().index(index, 0)
            self.tabletWidget.setCurrentIndex(modelIndex)
            self.tabletWidget.scrollTo(modelIndex)
        try:
            self.parent.buttonSave.setEnabled(True)
            self.parent.buttonSaveAs.setEnabled(True)
        except:
            self.setButtonEnabled(True)
        self.parent.delColumn = True
        self.close()



class RenameDialog(QDialog, ui_renamecolumndialog.Ui_renameColumnDialog):
    def __init__(self, parent, tableWidget, filename, selectedRow):
        """!
        @brief Open dialog to change name of column.
        """
        super(RenameDialog, self).__init__(parent, Qt.WindowSystemMenuHint|Qt.WindowTitleHint)
        self.parent = parent
        self.tableWidget = tableWidget
        self.setupUi(self)
        self.validateClass = ValidateClass(self.parent)
        self.filename = filename
        self.selectedRow = selectedRow
        self.setWindowTitle(_fromUtf8(_('Rename field: %s'%self.filename)))
        self.lineEdit.setText(self.filename)
        self.lineEdit.setFocus()
        regexpNameEdit = QRegExp(r'^[A-Za-z][A-Za-z0-9_]{1,15}$')
        validator = QRegExpValidator(regexpNameEdit, self.lineEdit)
        self.lineEdit.setValidator(validator)
        self.connect(self.cancelButton, SIGNAL("clicked()"), self.close)
        self.connect(self.okButton, SIGNAL("clicked()"), self.acceptNewName)
        self.connect(self.lineEdit, SIGNAL("keyPressEvent(QKeyEvent *e)"), self.keyEvent)
        self.okButton.setEnabled(True)


    def acceptNewName(self):
        """!
        @brief Action after clicked OK button. Change name of column.
        """
        newName = QString(self.lineEdit.text())
        try:
            if self.validateClass.checkNameIsUnique(newName, self.selectedRow):
                tempItem = QTableWidgetItem()
                tempItem.setData(0, newName)
                self.tableWidget.setItem(self.selectedRow, 0, tempItem)

                self.parent.updateColumn()
                modelIndex = self.tableWidget.model().index(self.selectedRow, 0)
                self.tableWidget.setCurrentIndex(modelIndex)
                self.tableWidget.scrollTo(modelIndex)
                self.parent.renameColumn.append([self.filename, newName])
                self.close()
                try:
                    self.parent.buttonSave.setEnabled(True)
                    self.parent.buttonSaveAs.setEnabled(True)
                except:
                    self.parent.okButton.setEnabled(True)
            else:
                raise exceptionModel.UniqueNameError
        except exceptionModel.UniqueNameError as e:
            QMessageBox.warning(self, '%s'%e.title, '%s'%e.msg, QMessageBox.Ok)


    def keyEvent(self, event):
        """!
        @brief Event for CTRl+C and CTRL+V.
        """
        if event.type() == QEvent.KeyPress:
            key = event.key()
            if key == QKeySequence.Copy or key == QKeySequence.Paste:
                key.accept()

class VariantDelegate(QItemDelegate):
    def __init__(self, parent=None):
        """!
        @brief Delegate for edit cell.
        """
        super(VariantDelegate, self).__init__(parent)
        self.parent = parent
        self.intExp = QRegExp()
        ## @var self.intExp
        # The regex expression for integer values.
        self.dbExp = QRegExp()
        ## @var self.dbExp
        # The regex expression for double values.
        self.strExp = QRegExp()
        ## @var self.strExp
        # The regex expression for text.
        self.dateExp = QRegExp()
        ## @var self.dateExp
        # The regex expression for date.
        self.boolExp = QRegExp()
        ## @var self.boolExp
        # The regex expression for bool values.



    def createEditor(self, parent, option, index):
        """!
        @brief Create editor.
        """
        ind = self.parent.attrTableWidget.currentColumn()
        type = self.parent.fieldTypeCol[ind]
        len = self.parent.fieldWidth[ind]
        prec = self.parent.fieldPrec[ind]

        variant = index.model().data(index)
        originalValue = variant.toPyObject()

        if not self.isSupportedType(originalValue):
            return None
        lineEdit = QLineEdit(parent)


        self.setRegularExpr(type, len, prec)

        if isinstance(originalValue, int):
            regExp = self.intExp
        elif isinstance(originalValue, float):
            regExp = self.dbExp
        elif isinstance(originalValue, QString):
            regExp = self.strExp
        elif isinstance(originalValue, QDate):
            regExp = self.dateExp
        elif isinstance(originalValue, bool):
            regExp = self.dateExp
        else:
            regExp = QRegExp()

        if not regExp.isEmpty():
            validator = QRegExpValidator(regExp, lineEdit)
            lineEdit.setValidator(validator)
        return lineEdit



    def setRegularExpr(self, type, length, prec):
        """!
        @brief Sets regular expression for edit column.
        @param type: Type of field.
        @param length: Length of field.
        @param prec: Precision of field
        """
        if type == 'Integer':
            self.intExp.setPattern(r'-?[0-9]{1,%s}'%length)
        elif type == 'Real':
            length = str(int(length) - 1)
            self.dbExp.setPattern(r'[-]?([1-9]{1}[0-9]{0,%s}(\.[0-9]{0,%s})?|0(\.[0-9]{0,%s})?|\.[0-9]{1,%s})$'%(length,prec, prec, prec))
        elif type == 'String':
            self.strExp.setPattern(r'.{1,%s}$'%length)
        elif type == 'Date':
            self.dateExp.setPattern(r'\d\d\d\d-\d\d-\d\d')
        elif type == 'Boolean':
            self.boolExp.setPattern(r'(i)^\w[tru]|[fals]e$]')


    def setEditorData(self, editor, index):
        """!
        @brief Sets data after accept.
        """
        variant = index.model().data(index)
        value = variant.toPyObject()
        if editor is not None:
            editor.setText(self.displayText(value))

    def setModelData(self, editor, model, index):
        """!
        @brief Sets model of data.
        """
        if not editor.isModified():
            return

        text = editor.text()
        validator = editor.validator()

        if validator is not None:
            state, temp = validator.validate(text, 0)

            if state != QValidator.Acceptable:
                return

        variant = index.model().data(index)
        originalValue = variant.toPyObject()

        if isinstance(originalValue,QDate):
            value = QDate.fromString(text, Qt.ISODate)
            if not value.isValid():
                return
        else:
            value = type(originalValue)(text)

        model.setData(index, value, Qt.DisplayRole)
        model.setData(index, value, Qt.UserRole)


    @staticmethod
    def isSupportedType(value):
        """!
        @brief Checks the type is supported.
        """
        return isinstance(value, (int, float, str, QString, QDate, bool))

    @staticmethod
    def displayText(value):
        """!
        @brief Displays texts.
        """
        if isinstance(value, int):
            return str(value)
        elif isinstance(value, float):
            return '%g' % value
        elif isinstance(value, (str, QString)):
            return value
        elif isinstance(value, QDate):
            return value.toString(Qt.ISODate)
        else:
            return '<Invalid>'

        return '<%s>' % value



class CoordinateDelegate(QItemDelegate):
    def createEditor(self, widget, option, index):
        if index.column() in (1,2):
            editor = QLineEdit(widget)
            validator = QRegExpValidator(QRegExp('[-]?([1-9]{1}[0-9]{0,10}(\.[0-9]{0,10})?|0(\.[0-9]{0,10})?|\.[0-9]{1,10})$'), editor)
            editor.setValidator(validator)
            return editor
        return super(CoordinateDelegate, self).createEditor(widget, option, index)


class ValidateClass(object):
    def __init__(self, parent):
        """!
        @brief class for validate text.
        """
        self.parent = parent


    def setWidthAndPrecText(self, type, n):
        """!
        @brief Set text for column view.
        @param type: Type of column.
        @param n: Index of row.
        """

        type = str(type)
        if type in ('int', 'Integer','QString','String', 'Integer64'):
            text = "%s(%d)" % (str(self.parent.fieldTypeCol[n]), self.parent.fieldWidth[n])
        elif type in ('float','double','Real'):
            text = "%s(%d,%d)" % (str(self.parent.fieldTypeCol[n]), self.parent.fieldWidth[n],self.parent.fieldPrec[n])
        elif type in ('QDate','QDateTime','Date' ,'Boolean','bool', 'DateTime'):
            text = str(self.parent.fieldTypeCol[n])
        return text


    def setValueFromType(self, type, value):
            """!
            @brief Returns the value, from string value.
            @param type: Type of value.
            @param value: Value.
            """
            type = str(type)
            if type in ('int', 'Integer'):
                try:
                    val = int(value)
                except:
                    val = 0
            elif type in ('float', 'double', 'Real'):
                try:
                    val = float(value)
                except:
                    val = 0.0
            elif type in ('QString', 'String'):
                try:
                    val = str(value)
                except:
                    val = 'NULL'
            elif type in ('QDate', 'QDateTime', 'Date'):
                 if not value:
                    now = datetime.datetime.now()
                    val = QDate(now.year, now.month, now.day)
                 else:
                    value.split('-')
                    val = QDate(int(value[0]), int(value[1]), int(value[2]))
            elif type in ('Boolean', 'bool'):
                    val = bool(value)
            else:
                val = None

            return val

    def checkNameIsUnique(self, newName, index = -1):
        """!
        @brief Checks the name of field is unique.
        @param newName: Name of field.
        @param index: Index of new field.
        """
        for i, name in enumerate(self.parent.fieldName):
            if newName == name:
                if not index == i:
                    return False

        return True

    def setDefaultLength(self, type):
        """!
        @brief Returns default length of field, if users set null.
        @param type: type of field
        """
        if type == 0:
            len = 10
        elif type == 1:
            len = 32
        elif type == 2:
            len = 80
        return len
