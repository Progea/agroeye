# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_progressdialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_progressDialog(object):
    def setupUi(self, progressDialog):
        progressDialog.setObjectName(_fromUtf8("progressDialog"))
        progressDialog.resize(640, 480)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        progressDialog.setWindowIcon(icon)
        progressDialog.setAccessibleName(_fromUtf8(""))
        self.gridLayout = QtGui.QGridLayout(progressDialog)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.progressBar = QtGui.QProgressBar(progressDialog)
        self.progressBar.setMinimumSize(QtCore.QSize(0, 30))
        self.progressBar.setBaseSize(QtCore.QSize(0, 0))
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(100)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setOrientation(QtCore.Qt.Horizontal)
        self.progressBar.setFormat(_fromUtf8("%p%"))
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.gridLayout.addWidget(self.progressBar, 1, 0, 1, 1)
        self.outputInfo = QtGui.QTextEdit(progressDialog)
        self.outputInfo.setEnabled(True)
        self.outputInfo.setAcceptDrops(False)
        self.outputInfo.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.outputInfo.setUndoRedoEnabled(False)
        self.outputInfo.setReadOnly(True)
        self.outputInfo.setObjectName(_fromUtf8("outputInfo"))
        self.gridLayout.addWidget(self.outputInfo, 0, 0, 1, 1)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.saveButton = QtGui.QPushButton(progressDialog)
        self.saveButton.setEnabled(False)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.horizontalLayout_5.addWidget(self.saveButton)
        self.closeButton = QtGui.QPushButton(progressDialog)
        self.closeButton.setEnabled(False)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout_5.addWidget(self.closeButton)
        self.gridLayout.addLayout(self.horizontalLayout_5, 2, 0, 1, 1)

        self.retranslateUi(progressDialog)
        QtCore.QMetaObject.connectSlotsByName(progressDialog)

    def retranslateUi(self, progressDialog):
        progressDialog.setWindowTitle(_translate("progressDialog", "Processing progress", None))
        self.saveButton.setText(_translate("progressDialog", "Save Callback information", None))
        self.closeButton.setText(_translate("progressDialog", "Close", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    progressDialog = QtGui.QDialog()
    ui = Ui_progressDialog()
    ui.setupUi(progressDialog)
    progressDialog.show()
    sys.exit(app.exec_())

