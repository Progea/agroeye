# -*- coding: utf-8 -*-

import os
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import ui_attributetabledialog
from Logic import saveVectorLayerMechanism
from Logic import vectorTableMechanism
_fromUtf8=QString.fromUtf8



class TableDialog(QDialog, ui_attributetabledialog.Ui_attributeTableDialog):

    def __init__(self, parent, rowSelected):
        """!
        @brief Open attribute table
        @param rowSelected: is the selected file
        """
        super(TableDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)
        self.validateClass = vectorTableMechanism.ValidateClass(self)

        self.file = self.parent.model().files[rowSelected]
        self.fileName, ext = os.path.splitext(self.file.name)
        self.setWindowTitle(_fromUtf8(_("Attribute table - ")) + self.fileName)
        self.fieldName = self.file.fieldName[:]
        ## @var self.fieldName
        # Name of field.
        self.rowCount = self.file.rowCount
        self.fieldVals = self.file.fieldVals[:]
        ## @var self.fieldType
        # Values of objects.
        self.fieldTypeCol = self.file.fieldTypeCol[:]
        ## @var self.fieldTypeCol
        # Type of field.
        self.fieldWidth = self.file.fieldWidth[:]
        ## @var self.fieldWidth
        # Width of field
        self.fieldPrec = self.file.fieldPrec[:]
        ## @var self.fieldPrec
        # Precision of field.
        self.geomType = self.file.geomType
        self.geomData = self.file.vectorRender.vertex._vector
        self.srs = self.file.vectorSpatialReferenceBase

        self.createActionsTablePreview()
        self.createActionsColumnView()
        self.addDataColumnView()
        self.addDataTablePreview()

        self.attrTableWidget.setItemDelegate(vectorTableMechanism.VariantDelegate(self))
        self.attrTabWidget.setCurrentIndex(0)
        self.connect(self.fieldsTableWidget, SIGNAL("itemClicked(QTableWidgetItem *)"), self.selectRow)
        self.connect(self.attrTabWidget, SIGNAL("currentChanged(int)"), self.refreshTablePreview)

        self.buttonCancel.clicked.connect(self.cancel)
        self.buttonSave.clicked.connect(self.save)
        self.buttonSaveAs.clicked.connect(self.saveAs)

        self.setButtonSaveEnabled(False, False)

        self.addColumn = False
        self.delColumn = False
        self.editColumn = False
        self.renameColumn = []
        self.delRow = []


    def createActionsColumnView(self):
        """!
        @brief Create action for QToolButton on Toolbar in Table Preview
        """
        self.moveUpAction = self.createAction(_fromUtf8(_('Move up')), self.moveUp, _fromUtf8(_('Move selected up')), ':/menuEditShape/resources/moveUp.png')
        self.moveDownAction = self.createAction(_fromUtf8(_('Move down')), self.moveDown, _fromUtf8(_('Move selected down')), ':/menuEditShape/resources/moveDown.png')
        self.moveTopAction = self.createAction(_fromUtf8(_('Move to top')), self.moveTop, _fromUtf8(_('Move selected to top')), ':/menuEditShape/resources/moveTop.png')
        self.addColAction = self.createAction(_fromUtf8(_('Insert')), self.addColumn, _fromUtf8(_('Add new field')), ':/menuEditShape/resources/addCol.png')
        self.delColAction = self.createAction(_fromUtf8(_('Delete')), self.deleteColumn, _fromUtf8(_('Delete field')), ':/menuEditShape/resources/delCol.png')
        self.changeNameAction = self.createAction(_fromUtf8(_('Rename')), self.renameColumn, _fromUtf8(_('Rename selected field')), ':/menuEditShape/resources/editName.png')

        buttonList = [self.buttonMoveUp, self.buttonMoveDown, self.buttonMoveTop, self.buttonAddCol, self.buttonDelCol, self.buttonChangeNameCol]
        actionList = [self.moveUpAction, self.moveDownAction, self.moveTopAction, self.addColAction, self.delColAction, self.changeNameAction]
        self.setButtonProperties(buttonList, actionList)
        self.setButtonsMoveEnabled(False, False, False)
        self.setButtonsEditEnabled(True, True, False)


    def createAction(self, text, trigger, tooltipText, icon):
        """!
        @brief Returns action for button.
        @param text: text for button.
        @param trigger: signal for button clicked.
        @param tooltipText: text for tooltip.
        @param icon: icon path.
        """
        action = QAction(text, self)
        action.triggered.connect(trigger)
        action.setToolTip(tooltipText)
        action.setIcon(QIcon(icon))
        return action

    def setButtonProperties(self, buttonList, actionList):
        """!
        @brief Set properties for button.
        @param buttonList: buttons list.
        @param actionList: actions list.
        """
        for i, button in enumerate(buttonList):
            button.setDefaultAction(actionList[i])
            button.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
            button.setFixedSize(120,25)


    def createActionsTablePreview(self):
        """!
        @brief Create action for QToolButton on Toolbar in Table Preview
        """
        self.editAction = self.createAction(_fromUtf8(_('Editing mode')), self.editRow, _fromUtf8(_('Start editing single values')),':/menuContext/resources/editfeat.png')
        self.delRowAction = self.createAction(_fromUtf8(_('Delete')), self.deleteRow, _fromUtf8(_('Delete selected row')), ':/menuEditShape/resources/delRow.png')

        value = False
        if self.file.driverName == 'ESRI Shapefile':
            value = True

        self.buttonEditRow.setEnabled(value)
        self.buttonDelRow.setEnabled(value)
        self.setButtonProperties([self.buttonEditRow, self.buttonDelRow], [self.editAction, self.delRowAction])

    def addDataTablePreview(self):
        """!
        @brief Create attribute table.
        """
        self.editAction.setCheckable(True)
        self.setTableWidgetProperties(self.attrTableWidget, len(self.fieldName), self.rowCount, self.fieldName)
        self.attrTableWidget.setSortingEnabled(False)
        self.attrTableWidget.horizontalHeader().setStyleSheet("QHeaderView::section{background-color: #43a047;color:white;}")

        self.comboList = []
        listHeader = QStringList()
        for n, item1 in enumerate(self.fieldVals):
            for m, item2 in enumerate(item1):
                tempItem = QTableWidgetItem()
                if isinstance(item2, bool):
                    comboBool = self.createBoolComboBox(item2)
                    self.attrTableWidget.setCellWidget(m, n, comboBool)
                    self.connect(comboBool, SIGNAL('currentIndexChanged(int)'), self.comboChanged)
                    self.comboList.append(comboBool)
                else:
                    tempItem.setData(0, item2)
                    self.attrTableWidget.setItem(m, n,tempItem)
                    listHeader.append(str(m))

        self.attrTableWidget.setVerticalHeaderLabels(listHeader)
        self.attrTableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.attrTableWidget.setSortingEnabled(True)

    def addDataColumnView(self):
        """!
        @brief Create preview of column.
        """
        self.setTableWidgetProperties(self.fieldsTableWidget, 2, len(self.fieldName), (_fromUtf8(_('Name')), _fromUtf8(_('Type'))))

        for n, item in enumerate(self.fieldName):
                tempItem1 = QTableWidgetItem()
                tempItem1.setData(0, item)
                tempItem2 = QTableWidgetItem()
                text = self.validateClass.setWidthAndPrecText(self.fieldTypeCol[n], n)
                tempItem2.setData(0, text)
                self.fieldsTableWidget.setItem(n, 0, tempItem1)
                self.fieldsTableWidget.setItem(n, 1, tempItem2)

    def createBoolComboBox(self, item):
        """!
        @brief Create comboBox with bool value.
        """
        comboBool = QComboBox()
        comboBool.addItem('False')
        comboBool.addItem('True')
        comboBool.setCurrentIndex(int(item))
        comboBool.setFixedSize(QSize(70, 20))
        comboBool.setEnabled(False)
        return comboBool

    def setTableWidgetProperties(self, tableWidget, numberCol, numberRow, fieldName):
        """!
        @brief Set tableWidget properties.
        @param tableWidget: Table widget object.
        @param numberCol: number of columns.
        @param numberRow: number of rows.
        @param fieldName: name of fields.
        """
        tableWidget.clear()
        tableWidget.setColumnCount(numberCol)
        tableWidget.setRowCount(numberRow)
        tableWidget.setHorizontalHeaderLabels(fieldName)

    def setButtonSaveEnabled(self, boolButtonSave, boolButtonSaveAs):
        """!
        @brief Sets button save and save as enabled.
        @param boolButtonSave: bool value of button save.
        @param boolButtonSaveAs: bool value of button save as.
        """
        self.buttonSave.setEnabled(boolButtonSave)
        self.buttonSaveAs.setEnabled(boolButtonSaveAs)

    def setButtonsMoveEnabled(self, boolButtonMoveUp, boolButtonMoveDown, boolButtonMoveTop):
        """!
        @brief Sets buttons move enabled.
        @param boolButtonMoveUp: bool value of button Move Up.
        @param boolButtonMoveDown: bool value of button Move Down.
        @param boolButtonMoveTop: bool value of button Move Top.
        """
        self.buttonMoveUp.setEnabled(boolButtonMoveUp)
        self.buttonMoveTop.setEnabled(boolButtonMoveTop)
        self.buttonMoveDown.setEnabled(boolButtonMoveDown)

    def setButtonsEditEnabled(self, boolButtonAddCol, boolButtonDelCol, boolButtonChangeNameCol):
        """!
        @brief Sets buttons edit enabled.
        @param boolButtonMoveUp: bool value of button Add Column.
        @param boolButtonMoveDown: bool value of button Delete Column.
        @param boolButtonMoveTop: bool value of button Rename.
        """
        self.buttonAddCol.setEnabled(boolButtonAddCol)
        self.buttonDelCol.setEnabled(boolButtonDelCol)
        self.buttonChangeNameCol.setEnabled(boolButtonChangeNameCol)

    def save(self):
        """!
        @brief Save the all changes to vector file. Edits opened file, not create new.
        """

        saveVector = saveVectorLayerMechanism.NewVector(self.parent)
        # if saveVector.copyShapefile(str(self.file.pathLayer), self):
        #     self.buttonSave.setEnabled(True)
        #     self.buttonSaveAs.setEnabled(True)


        if self.addColumn:
            newField = saveVector.diffList(self.file.fieldName, self.fieldName)
            if self.renameColumn:
                newField = saveVector.checkRenameList(self.renameColumn, newField)
            saveVector.setNewField(newField, self, self.file.vectorLayer)
        if self.delColumn:
            delField = saveVector.diffList(self.fieldName, self.file.fieldName)
            if self.renameColumn:
                delField = saveVector.checkRenameList(self.renameColumn, delField)
            saveVector.delField(delField, self.file)
        if self.renameColumn:
            saveVector.renameField(self.file, self.renameColumn)
        if self.delRow:
            saveVector.delFeature(self.file, self.delRow)

        saveVector.setFeature(self.file, self.fieldVals)
        saveVector.updateFile(self.file)
        QMessageBox.information(self, _fromUtf8(_('Save status')), _fromUtf8(_('All changes have been saved')), QMessageBox.Ok)
        self.buttonSave.setEnabled(False)

    def saveAs(self):
        """!
        @brief Open dialog to save vector to new shapefile. User choose new name.
        """
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFiles)
        dialog.setDirectory(self.parent.lastDirPath)

        filename, path = dialog.getSaveFileNameAndFilter(self, _fromUtf8(_('Save shapefile')), self.parent.lastDirPath, filter=self.tr('ESRI Shapefile (*.shp)'))
        head, tail  = os.path.split(str(filename))
        root, ext = os.path.splitext(tail)

        saveVector = saveVectorLayerMechanism.NewVector(self.parent)
        if not filename.isEmpty():
            if saveVector.checkFileName(root):
                self.parent.lastDirPath = head
                if saveVector.createNewShapefileWithField(filename, self, self.parent.parent):
                    self.buttonSave.setEnabled(True)
                    self.buttonSaveAs.setEnabled(True)

            else:
                self.saveShape()

    def editRow(self):
        """!
        @brief turn on/off edit button for table preview
        """
        if self.buttonEditRow.isChecked():

            self.attrTableWidget.setEditTriggers(QTableWidget.AllEditTriggers)
            self.attrTableWidget.setSelectionBehavior(QAbstractItemView.SelectItems)
            self.connect(self.attrTableWidget, SIGNAL("itemChanged(QTableWidgetItem *)"), self.changeRow)
            if self.attrTableWidget.item(0, 0).background().color() == Qt.darkGray:
                self.attrTableWidget.item(0, 0).setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            if self.attrTableWidget.item(0, 1).background().color() == Qt.darkGray:
                self.attrTableWidget.item(0, 1).setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            if self.comboList:
                for i in self.comboList:
                    i.setEnabled(True)
        else:
            self.attrTableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
            self.attrTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
            self.disconnect(self.attrTableWidget, SIGNAL("itemChanged(QTableWidgetItem *)"), self.changeRow)
            if self.comboList:
                for i in self.comboList:
                    i.setEnabled(False)

    def deleteRow(self):
        """!
        @brief Delete selected row.
        """
        rowSelected = self.attrTableWidget.currentIndex().row()
        msgRow = int(rowSelected)
        reply = QMessageBox.warning(self, _fromUtf8(_('Confirm delete row')), _fromUtf8(_('Are you sure you want to delete row number %d?'))%msgRow, QMessageBox.Yes|QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.attrTableWidget.removeRow(rowSelected)
            if self.comboList:
                self.comboList.pop(rowSelected)
            self.setButtonSaveEnabled(True, True)
            self.delRow.append(rowSelected)
            modelIndex = self.attrTableWidget.model().index(rowSelected, 0)
            self.attrTableWidget.setCurrentIndex(modelIndex)
            self.attrTableWidget.scrollTo(modelIndex)
        else:
            self.setButtonSaveEnabled(False, False)

    def setIndexInTableWidget(self, insertIndex, removeIndex, modelValue):
        """!
        @brief Change order in table widget.
        @param insertIndex: index of inserting objects.
        @param removeIndex: index of deleting objects.
        @param modelValue: index of model.
        """
        rowSelected = self.attrTableWidget.currentIndex().row()
        self.fieldsTableWidget.insertRow(insertIndex)
        for i in xrange(self.fieldsTableWidget.columnCount()):
            item = self.fieldsTableWidget.takeItem(removeIndex, i)
            self.fieldsTableWidget.setItem(insertIndex, i, item)

        self.fieldsTableWidget.removeRow(removeIndex)
        self.fieldVals.insert(insertIndex, self.fieldVals[rowSelected])
        self.fieldVals.pop(removeIndex)
        self.fieldTypeCol.insert(insertIndex, self.fieldTypeCol[rowSelected])
        self.fieldTypeCol.pop(removeIndex)
        self.fieldWidth.insert(insertIndex, self.fieldWidth[rowSelected])
        self.fieldWidth.pop(removeIndex)
        self.fieldPrec.insert(insertIndex, self.fieldPrec[rowSelected])
        self.fieldPrec.pop(removeIndex)
        modelIndex = self.fieldsTableWidget.model().index(modelValue, 0)
        self.fieldsTableWidget.setCurrentIndex(modelIndex)
        self.fieldsTableWidget.scrollTo(modelIndex)

    def moveUp(self):
        """!
        @brief Move up selected column.
        """
        self.setButtonSaveEnabled(True, True)
        rowSelected = self.fieldsTableWidget.currentRow()
        self.setIndexInTableWidget(rowSelected-1, rowSelected+1, rowSelected-1)
        if rowSelected-1 == 0:
            self.setButtonsMoveEnabled(False, True, False)
        else:
            self.setButtonsMoveEnabled(True, True, True)
        self.editColumn = True
        self.updateColumn()

    def moveDown(self):
        """!
        @brief Move down selected column.
        """
        self.setButtonSaveEnabled(True, True)

        rowSelected = self.fieldsTableWidget.currentRow()
        self.setIndexInTableWidget(rowSelected+2, rowSelected, rowSelected+1)
        if rowSelected+1 == self.fieldsTableWidget.rowCount()-1:
            self.setButtonsMoveEnabled(True, False, True)
        else:
            self.setButtonsMoveEnabled(True, True, True)

        self.editColumn = True
        self.updateColumn()

    def moveTop(self):
        """!
        @brief Move selected column at the top of table.
        """
        self.setButtonSaveEnabled(True, True)
        rowSelected = self.fieldsTableWidget.currentRow()
        self.setIndexInTableWidget(0, rowSelected+1, 0)
        self.setButtonsMoveEnabled(False, True, False)
        self.editColumn = True
        self.updateColumn()

    def addColumn(self):
        """!
        @brief Add one column at the end of table. Shows new dialog to set name and type of new column. Returns new column.
        """
        colAdd = vectorTableMechanism.ColumnAddDialog(self, self.fieldsTableWidget)
        colAdd.show()

    def deleteColumn(self):
        """!
        @brief Delete one or more columns. Shows new dialog to choose deleted column.
        """
        colRem = vectorTableMechanism.ColumnDelDialog(self, self.fieldsTableWidget)
        colRem.show()

    def renameColumn(self):
        """!
        @brief Edit column name
        """
        rowSelected = self.fieldsTableWidget.currentRow()
        item = self.fieldsTableWidget.item(rowSelected, 0)
        changeName = vectorTableMechanism.RenameDialog(self, self.fieldsTableWidget, item.text(), rowSelected)
        changeName.show()

    def changeRow(self, item):
        """!
        @brief Action after edit rows
        """
        self.setButtonSaveEnabled(True, True)
        self.updateRow()

    def selectRow(self, item):
        """!
        @brief Action after selected row
        """
        selectedRow = item.row()
        if self.file.driverName == 'ESRI Shapefile':
            self.setButtonsEditEnabled(True, True, True)
            if self.fieldsTableWidget.rowCount() > 1:
                if selectedRow == 0:
                    self.setButtonsMoveEnabled(False, True, False)
                elif selectedRow == self.fieldsTableWidget.rowCount() - 1:
                    self.setButtonsMoveEnabled(True, False, True)
                else:
                    self.setButtonsMoveEnabled(True, True, True)
        else:
            self.setButtonsMoveEnabled(False, False, False)
            self.setButtonsEditEnabled(False, False, False)

    def updateColumn(self):
        """!
        @brief Update column after edit
        """
        self.fieldName = []
        for i in xrange(self.fieldsTableWidget.rowCount()):
            item = self.fieldsTableWidget.item(i, 0)
            self.fieldName.append(item.text())

    def updateRow(self):
        """!
        @brief Updates rows after edit
        """
        self.fieldVals = []
        for n in xrange(self.attrTableWidget.columnCount()):
            tempVals = []
            for m in xrange(self.attrTableWidget.rowCount()):
                item = self.attrTableWidget.item(m, n)
                if item:
                    if item.background().color() != Qt.darkGray:
                        type = item.data(0).typeName()
                        if type == 'QDate':
                            text = item.text().split('-')
                            val = QDate(int(text[0]), int(text[1]), int(text[2]))
                        else:
                            val = self.validateClass.setValueFromType(type, item.text())
                        tempVals.append(val)
                else:
                    comboTemp = self.comboList[m]
                    val = bool(comboTemp.currentIndex())
                    tempVals.append(val)
            if tempVals:
                self.fieldVals.append(tempVals)

    def refreshTablePreview(self, index):
        """!
        @brief Action after switch tab
        @param index: index of switched table.
        """
        if index == 0:
            self.addDataTablePreview()
        else:
            self.editAction.setCheckable(False)
            self.disconnect(self.attrTableWidget, SIGNAL("itemChanged(QTableWidgetItem *)"), self.changeRow)

    def cancel(self):
        """!
        @brief Action after select cancel.
        """
        if self.buttonSave.isEnabled() or self.buttonSaveAs.isEnabled():
            reply = QMessageBox.question(self, _fromUtf8(_('Attribute table - ')) + self.fileName, _fromUtf8(_('Are you sure you want to quit without save?')), QMessageBox.Yes|QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.close()
        else:
            self.close()

    def comboChanged(self, index):
        """!
        @brief Action after changed field in typeComboBox.
        @param index: index of new selected row.
        """
        self.setButtonSaveEnabled(True, True)
        self.updateRow()





