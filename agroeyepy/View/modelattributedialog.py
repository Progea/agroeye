# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
_fromUtf8=QString.fromUtf8
from collections import namedtuple
import os



class ModelAttribute(object):
    def __init__(self, parent):
        self.parent = parent
        self.agroeye = self.parent.parent
        self.currentRaster = ''
        self.currentSaveRasterPath = ''
        self.dbPath = ''
        self.dbList = []
        self.additionalLayerPath = []

        SegmentationType = namedtuple('SegmentationType',['type', 'index', 'sizeName', 'sizeValue', 'minValue', 'maxValue', 'bands'])
        self.chessboardType = SegmentationType('Chessboard', 0, 'Object Size', 120, 1, 255, 0)
        self.quadtreeType = SegmentationType('Quadtree', 1, 'Threshold value', 200, 1, 2550, [])
        self.segmentationType = self.chessboardType


    def getPathToRaster(self):
        """!
        @brief Returns the path to the raster file.
        """
        for file in self.agroeye.loadedfiles_model.files:
            if file.name == self.currentRaster:
                return file.path

    def getPathToDatabase(self):
        """!
        @brief Returns the path to the database.
        """
        if self.dbPath:
            location = self.dbPath
        else:
            location = self.agroeye.lastDirPath
        return location

    def getRasterIndex(self):
        """!
        @brief Returns the index of file in table of contents.
        """
        for i, file in enumerate(self.agroeye.loadedfiles_model.files):
            if not file.isVector:
                if file.name == self.currentRaster:
                    return i

    def getBandList(self, rasterBandComboBox):
        """!
        @brief Returns if the bands are checked.
        @param rasterBandComboBox: combobox with raster bands.
        """
        band = []
        if rasterBandComboBox.isEnabled() == True:
            for i in range(rasterBandComboBox.count()-1):
                item = rasterBandComboBox.model().item(i + 1, 0)
                if item.checkState():
                 band.append(i+1)
        return band

    def getSegmentationResult(self):
        """!
        @brief Returns the results of segmentation.
        """
        return self.parent.segm.isFinish

    def getStatisticsResult(self):
        """!
        @brief Returns the results of statistics
        """
        return self.parent.stats.isFinish

    def checkPathToDb(self, path):
        """!
        @brief Returns the path is valid.
        @param path: path of database.
        """
        value = path.endsWith('.spatialite')
        return value

    def checkIsNdvi(self, name, value, checkStatus):
        """!
        @brief Returns the results of segmentation.
        @param name: name of statistics.
        @param value: band number.
        @param checkStatus: status of check.
        """
        if (name.contains('ndvi', Qt.CaseSensitive) and value in (4, 8)):
            check = Qt.Checked
        elif (name.contains('ndvi', Qt.CaseSensitive) and value not in (4, 8)):
            check = Qt.Unchecked
        else:
            check = not(checkStatus)
        return check

    def checkIsZabud(self, name, value, checkStatus):
        if (name.contains('zabud', Qt.CaseSensitive) and value in (2, 3, 4, 8)):
            check = Qt.Checked
        elif (name.contains('zabud', Qt.CaseSensitive) and value not in (2, 3, 4, 8)):
            check = Qt.Unchecked
        else:
            check = not(checkStatus)
        return check


    def checkBand(self, rasterBandComboBox):
        """!
        @brief Returns if the bands are checked.
        @param rasterBandComboBox: combobox with raster bands.
        """
        band = []
        if rasterBandComboBox.isEnabled() == True:
            for i in range(rasterBandComboBox.count()-1):
                item = rasterBandComboBox.model().item(i + 1, 0)
                if item.checkState():
                 band.append(i+1)
            if band: checkStatus = True
            else: checkStatus = False
        elif rasterBandComboBox.isEnabled() == False:
            checkStatus = True
        return checkStatus

    def saveSegmentationType(self, index, newValue, bands=0):
        """!
        @brief Save the results of segmentation.
        @param index: index of segmentation type.
        @param newValue: value of segmentation.
        @param band: list of bands.
        """
        if index == self.chessboardType.index:
            if newValue:
                self.chessboardType = self.chessboardType._replace(sizeValue=newValue)
            self.segmentationType = self.chessboardType
        elif index == self.quadtreeType.index:
            if newValue:
                self.quadtreeType = self.quadtreeType._replace(sizeValue=newValue)
                self.quadtreeType = self.quadtreeType._replace(bands=bands)
            self.segmentationType = self.quadtreeType

    def saveRasterInput(self, raster):
        """!
        @brief Save the new raster file.
        @param raster: raster name.
        """
        self.currentRaster = raster
        if self.currentRaster:
            getattr(self.parent, self.parent.inputRaster.name).setPen(QPen(self.parent.greenColor, 2))
            value = False
        else:
            getattr(self.parent, self.parent.inputRaster.name).setPen(QPen(self.parent.redColor, 2))
            value = True
        return value

    def savePathToDb(self, path):
        """!
        @brief Save the new raster file.
        @param path: real path to file.
        """
        self.dbPath = path
        if self.dbPath:
            self.dbList.append(path)
            getattr(self.parent, self.parent.outputDb.name).setPen(QPen(self.parent.greenColor, 2))
            value = False
        else:
            getattr(self.parent, self.parent.outputDb.name).setPen(QPen(self.parent.redColor, 2))
            value = True
        return value

    def saveAdditionalLayer(self, path):
        if path:
            self.additionalLayerPath.append(path)
        getattr(self.parent, self.parent.synth.name).setPen(QPen(self.parent.greenColor, 2))

    def saveSegmentation(self):
        """!
        @brief Save segmentation when is finished.
        """
        self.parent.segm = self.parent.segm._replace(isFinish=True)
        getattr(self.parent, self.parent.segm.name).setPen(QPen(self.parent.greenColor, 2))

    def saveStatistics(self):
        """!
        @brief Save the statistics which will be computed.
        """
        self.parent.stats = self.parent.stats._replace(isFinish=True)
        getattr(self.parent, self.parent.stats.name).setPen(QPen(self.parent.greenColor, 2))

    def saveDbToStatistics(self, db):
        """!
        @brief Save the name of new raster file which will contain statistics.
        @param raster: path to new file.
        """
        self.dbPath = db
        if self.dbPath:
            value = True
        else:
            value = False
        return value

    def addRasterFile(self, rasterComboBox, param):
        """!
        @brief Add name of raster file which are loaded into AgroEye.
        @param rasterComboBox: QComboBox to add name of raster file.
        """
        for file in self.agroeye.loadedfiles_model.files:
            if not file.isVector:
                rasterComboBox.addItem(file.name)
        index = rasterComboBox.findText(self.currentRaster, Qt.MatchExactly)
        rasterComboBox.setCurrentIndex(index)
        if param:
            for file in self.additionalLayerPath:
                rasterComboBox.addItem(file)

    def addDbFile(self, dbComboBox):
        for file in self.dbList:
            dbComboBox.addItem(file)
        dbComboBox.setCurrentIndex(-1)

    def addRasterBands(self, bandComboBox):
        """!
        @brief Add band name of selected raster file.
        @param bandComboBox: QComboBox to add band name.
        """

        index = self.getRasterIndex()
        if index >= 0:
            bandsCount = self.agroeye.loadedfiles_model.files[index].bandsCount
            bandComboBox.addItem(_fromUtf8(_('---- Select bands ----')))
            firstItem = bandComboBox.model().item(0, 0)
            firstItem.setBackground(QBrush(QColor(200,200,200)))
            firstItem.setSelectable(False)
            for i in range(bandsCount):
                bandComboBox.addItem(_fromUtf8(_('Band %s') % (i + 1)))
                item = bandComboBox.model().item(i+1, 0)

                if bandComboBox.objectName().contains('ndvi', Qt.CaseSensitive):
                    valueNdvi = self.checkIsNdvi(bandComboBox.objectName(), (i + 1), Qt.Checked)
                    item.setCheckState(valueNdvi)
                elif bandComboBox.objectName().contains('zabud', Qt.CaseSensitive):
                    valueZabud = self.checkIsZabud(bandComboBox.objectName(), (i + 1), Qt.Checked)
                    item.setCheckState(valueZabud)
                else:
                    item.setCheckState(False)
        bandComboBox.setCurrentIndex(0)


    def setButtonOkVisibility(self, button, bool):
        """!
        @brief Set true/false visibility of button.
        @param button: button for change visibility.
        @param bool: value of visibility.
        """
        button.setEnabled(bool)

    def setButtonRunVisibility(self, button, text, index, rasterComboBox):
        """!
        @brief Set true/false visibility of run buttons.
        @param button: run button for change visibility.
        @param text: path to file.
        @param index: index of combobox.
        """
        if self.checkPathToDb(text) and index >= 0 and self.checkBand(rasterComboBox):
            button.setEnabled(True)
        else:
            button.setEnabled(False)

    def setCheckedBands(self, rasterBandComboBox):

        for band in self.segmentationType.bands:
            item = rasterBandComboBox.model().item(band, 0)
            item.setCheckState(True)
            rasterBandComboBox.setCurrentIndex(band)

    def handleItemPressed(self, comboBox, modelIndex, list):

        self.clearSelected(comboBox, modelIndex)
        if list.selectedIndexes():
            index = list.selectedIndexes()[0]
            item = comboBox.model().itemFromIndex(index)
            item.setCheckState(not item.checkState())

    def clearSelected(self, combobox, modelIndex):
        for i in range(combobox.count()):
            if i > 0:
                item = modelIndex.item(i)
                item.setCheckState(False)

    def addPath(self, lineEdit, ext):
        """!
        @brief Open file dialog to add path to database.
        """
        location = self.getPathToDatabase()
        locationDir = os.path.dirname(str(location))
        path = QFileDialog.getSaveFileName(None, _fromUtf8(_('Save as')), locationDir, _fromUtf8(_(ext)))
        if path:
            lineEdit.setText(path)
            return True
        else:
            return False




