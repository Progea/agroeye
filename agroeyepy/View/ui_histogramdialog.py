# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_histogramdialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_histogramDialog(object):
    def setupUi(self, histogramDialog):
        histogramDialog.setObjectName(_fromUtf8("histogramDialog"))
        histogramDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        histogramDialog.resize(892, 618)
        histogramDialog.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        histogramDialog.setWindowTitle(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        histogramDialog.setWindowIcon(icon)
        histogramDialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        histogramDialog.setAutoFillBackground(False)
        histogramDialog.setStyleSheet(_fromUtf8("\n"
"/*\n"
"* GLOBAL font and background SETTINGS.\n"
"*/\n"
"\n"
"* {\n"
"    font-size: 11px;\n"
"    font-family: Tahoma, Verdana, Arial, sans-serif;\n"
"    background-color: white;\n"
"}\n"
"\n"
"/* \n"
"* COMBOBOXES - sets border shape, dimensions and colours.\n"
"*/\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
" \n"
"    min-width: 6em;\n"
"    background-color: #ffffff;\n"
"    color:black;\n"
"    \n"
"     border-color: silver;\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    padding: 1px 0px 1px 3px; /*This makes text colour work*/\n"
"}\n"
"\n"
"QComboBox:editable {\n"
"    /*This changes colour behind expand icon*/\n"
"    background-color: #f2f2f2; \n"
"}\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox::drop-down { \n"
"    /*This sets up style of combobox list */\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; \n"
"    border-top-right-radius: 2px; \n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QComboBox::down-arrow {\n"
"    /* Custom expand icon source and dimensions, width forces proportional height resize*/\n"
"    image: url(:/menuEditShape/resources/moveDown.png); \n"
"    width: 12px;\n"
"}\n"
"\n"
"QComboBox::down-arrow:on { \n"
"    /* This moves activated arrow by 1px */\n"
"    top: 1px;\n"
"    left: 1px;\n"
"}\n"
"\n"
"/* TABLE OF CONTENTS */\n"
"\n"
"QDockWidget {\n"
"    border:none;\n"
"}\n"
"\n"
"QDockWidget::title {\n"
"    text-align: left;\n"
"    border: 1px solid silver;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"    padding-left: 35px;\n"
"}\n"
"\n"
"/* GROUPBOXES */\n"
"\n"
"QGroupBox {\n"
"    background-color: white;\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
"    margin-top: 1px; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"   \n"
"}\n"
"\n"
"/* HEADERS */\n"
"\n"
"QHeaderView::section {\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                                  stop:0 #616161, stop: 0.5 #505050,\n"
"                                  stop: 0.6 #434343, stop:1 #656565);\n"
"    color: black;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QHeaderView::section:checked\n"
"{\n"
"    background-color: red;\n"
"}\n"
"\n"
"QHeaderView::down-arrow {\n"
"    image: url(down_arrow.png);\n"
"}\n"
"\n"
"QHeaderView::up-arrow {\n"
"    image: url(up_arrow.png);\n"
"}\n"
"\n"
"/* MENU */\n"
"\n"
"QMenuBar {\n"
"    /* These sets background colour of menus */\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                      stop:0 silver, stop:1 lightgray); \n"
"}\n"
"\n"
"QMenu {\n"
"    background-color: darkgray;\n"
"}\n"
"\n"
"QMenuBar::item {\n"
"    spacing: 3px; /* spacing between menu bar items */\n"
"    padding: 1px 4px;\n"
"    border-radius: 2px;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:selected { /* when selected using mouse or keyboard */\n"
"    background: darkgray;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"    background: #888888;\n"
"    color: black;\n"
"}\n"
"\n"
"\n"
"/* PUSHBUTTONS */\n"
"\n"
"QPushButton {\n"
"    border: 1px solid darkgray;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    min-height: 24px;\n"
"    max-height: 24px;\n"
"    min-width: 100px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:flat {\n"
"    border: none; /* no border for a flat push button */\n"
"}\n"
"\n"
"QPushButton:default {\n"
"    border-color: navy; /* make the default button prominent */\n"
"}\n"
"\n"
"/* RADIOBUTTONS */\n"
"\n"
"QRadioButton::indicator {\n"
"    width: 13px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"/* SCROLLBAR\n"
"* I can\'t see you, but i know you\'re there.\n"
"*/\n"
"\n"
"QScrollBar{\n"
"    width: 1px;\n"
"}\n"
"\n"
"\n"
"/* TABWIDGET */\n"
"\n"
"\n"
"QTabWidget::pane {\n"
"    /* Border settings for TabWidget */\n"
"    border-top: 2px solid #C2C7CB;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    /* This moves tab bar left by 5px */\n"
"    left: 5px; \n"
"}\n"
"\n"
"/* Styles single tab using tab sub-control, uses QTabBar instead of QTabWidget */\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 1px solid lightgrey; \n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"    min-width: 120px;\n"
"    padding: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"        background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                    stop: 0 #fafafa, stop: 0.4 #f4f4f4,\n"
"                    stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    border-color: darkgray;\n"
"    border-bottom-color: lightgray; \n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    /* Unselected tab seems to be smaller */\n"
"    margin-top: 2px; \n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    /* Push selected tabs 4px in both directions */\n"
"    margin-left: -4px;\n"
"    margin-right: -4px;\n"
"}\n"
"\n"
"QTabBar::tab:first:selected {\n"
"    /* But do not push the first one left since it would fall out of the window :( */\n"
"    margin-left: 0; \n"
"}\n"
"\n"
"QTabBar::tab:last:selected {\n"
"    /* Same for the last one just to make it feel safe */\n"
"    margin-right: 0;\n"
"}\n"
"\n"
"QTabBar::tab:only-one {\n"
"    /* If there\'s only one tab, it doesn\'t have to do anything */\n"
"    margin: 0; \n"
"}\n"
"/* TABLES */\n"
"QTableView {\n"
"    selection-background-color: qlineargradient(x1: 0, y1: 0, x2: 0.5, y2: 0.5,\n"
"                                                 stop: 0 silver, stop: 1 lightgray);\n"
"}\n"
"\n"
"QTableView QTableCornerButton::section {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 darkgray);\n"
"    border: 1px outset darkgray;\n"
"}\n"
"\n"
"\n"
"/* TOOLBAR WITH BASIC ICONS */\n"
"\n"
"QToolBar {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 silver);\n"
"    border: 0;\n"
"}\n"
"\n"
"QToolButton { \n"
"    /* All types of tool button */\n"
"    border: none; \n"
"       border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"}\n"
"\n"
"QToolButton[popupMode=\"1\"] { \n"
"    /* Only for MenuButtonPopup \n"
"    Make way for the popup button */\n"
"    padding-right: 20px; \n"
"}\n"
"\n"
"QToolButton:selected {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QToolButton:on {\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"    width: 28px;\n"
"    height: 28px;\n"
"}\n"
"\n"
"/* the subcontrols below are used only in the MenuButtonPopup mode */\n"
"QToolButton::menu-button {\n"
"    border: 1px solid lightgray;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    /*16px width + 4px for border = 20px allocated above */\n"
"    width: 16px;\n"
"}\n"
"\n"
"QToolButton::menu-arrow {\n"
"    image: url(resources/moveDown.png);\n"
"}\n"
"\n"
"QToolButton::menu-arrow:open {\n"
"   top: 1px; left: 1px; /* Shift it a bit */\n"
"}\n"
"\n"
"/* TOOLBOX */\n"
"\n"
"QToolBox::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                         stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                         stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border-radius: 2px;\n"
"    color: lightgray;\n"
"}\n"
"\n"
"/* TOOLTIP MESSAGE STYLE */\n"
"\n"
"QToolTip {\n"
"    padding: 5px;\n"
"}\n"
"\n"
"/* FRAME SETTINGS */\n"
"\n"
"QFrame{\n"
"    border: none;\n"
"    background-color: qlineargradient(x1: 1, y1: 0, x2: 0, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"}\n"
"\n"
"/*SPINNBOX SETTINGS*/\n"
"\n"
"QSpinBox, QDoubleSpinBox {\n"
"    border-width: 2px;\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    padding-top: 1.5px;\n"
"    padding-bottom: 1.5px;\n"
"}\n"
"\n"
"/* LABEL SETTINGS */\n"
"\n"
"QLabel {\n"
"    border:none;\n"
"}\n"
"\n"
"/* SLIDER (PARAMETERS), USED MOSTLY FOR OPACITY */\n"
"QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 2px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 18px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"    position: absolute; /* absolutely position 4px from the left and right of the widget. setting margins on the widget should work too... */\n"
"    left: 4px; right: 4px;\n"
"}\n"
"\n"
"QSlider::handle:vertical {\n"
"    height: 10px;\n"
"    background: green;\n"
"    margin: 0 -4px; /* expand outside the groove */\n"
"}\n"
"\n"
"QSlider::add-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"\n"
"QSlider::sub-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
" \n"
""))
        histogramDialog.setSizeGripEnabled(True)
        self.verticalLayout_3 = QtGui.QVBoxLayout(histogramDialog)
        self.verticalLayout_3.setContentsMargins(-1, -1, 9, -1)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.plotsTabWidget = QtGui.QTabWidget(histogramDialog)
        self.plotsTabWidget.setMinimumSize(QtCore.QSize(0, 0))
        self.plotsTabWidget.setObjectName(_fromUtf8("plotsTabWidget"))
        self.histogramPlotTab = QtGui.QWidget()
        self.histogramPlotTab.setObjectName(_fromUtf8("histogramPlotTab"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.histogramPlotTab)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.histogramPlotLayout = QtGui.QVBoxLayout()
        self.histogramPlotLayout.setObjectName(_fromUtf8("histogramPlotLayout"))
        self.histogramTabWidget = QtGui.QTabWidget(self.histogramPlotTab)
        self.histogramTabWidget.setStyleSheet(_fromUtf8("QTabWidget {\n"
"    qproperty-geometry:rect(0 0 872 350);\n"
"   }\n"
"  "))
        self.histogramTabWidget.setObjectName(_fromUtf8("histogramTabWidget"))
        self.histogramPlotLayout.addWidget(self.histogramTabWidget)
        self.verticalLayout_6.addLayout(self.histogramPlotLayout)
        self.frame = QtGui.QFrame(self.histogramPlotTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.frame)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_6.addWidget(self.label_3)
        self.statisticsComboBox = QtGui.QComboBox(self.frame)
        self.statisticsComboBox.setEnabled(True)
        self.statisticsComboBox.setMinimumSize(QtCore.QSize(83, 0))
        self.statisticsComboBox.setMaximumSize(QtCore.QSize(200, 16777215))
        self.statisticsComboBox.setBaseSize(QtCore.QSize(0, 0))
        self.statisticsComboBox.setObjectName(_fromUtf8("statisticsComboBox"))
        self.statisticsComboBox.addItem(_fromUtf8(""))
        self.statisticsComboBox.addItem(_fromUtf8(""))
        self.horizontalLayout_6.addWidget(self.statisticsComboBox)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_6.addWidget(self.label_4)
        self.baseAreaStretchComboBox = QtGui.QComboBox(self.frame)
        self.baseAreaStretchComboBox.setMinimumSize(QtCore.QSize(83, 0))
        self.baseAreaStretchComboBox.setStyleSheet(_fromUtf8("/* \n"
"*\n"
"* CBBOX, RADIUS RAMKI, WYMIARY, KOLOR\n"
"*\n"
"*/\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
" \n"
"    min-width: 6em;\n"
"    background-color: #ffffff;\n"
"    color:black;\n"
"    \n"
"     border-color: silver;\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    padding: 1px 0px 1px 3px; /*This makes text colour work*/\n"
"}\n"
"\n"
"/* \n"
"*\n"
"*ENABLED CBBOX - KOLOR TŁA NA WSZELKI WYPADEK \n"
"*przy wykończeniu sprawdzić czy potrzebne\n"
"*unikamy redundancji w kodzie, elo\n"
"*\n"
"*/\n"
"\n"
"QComboBox:editable {\n"
"    background-color: #f2f2f2; /*zmienia kolor za strzałkobuttonem*/\n"
"}\n"
"\n"
"/*\n"
"*\n"
"* ROZWINIĘTY COMBOBOX\n"
"*jw\n"
"*/\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"/* \n"
"*\n"
"*ROZWINIĘTY COMBOBOX\n"
"*jw\n"
"*/\n"
"\n"
"QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
" \n"
"/*\n"
"*\n"
"* ROZWINIĘTY COMBOBOX\n"
"* styl rozwiniętej listy\n"
"*\n"
"*/\n"
"\n"
"QComboBox::drop-down {\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; \n"
"    border-top-right-radius: 2px; \n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"/*\n"
"*\n"
"* KUSTOMOWA STRZAŁKA W COMBOBOXACH\n"
"* source & size\n"
"*\n"
"*/\n"
"\n"
"QComboBox::down-arrow {\n"
"    image: url(:/menuEditShape/resources/moveDown.png);\n"
"    width: 12px;\n"
"}\n"
"\n"
"/*\n"
"*\n"
"* PRZESUNIĘCIE SZCZAŁKI PO OTWARCIU LISTY \n"
"*\n"
"*/\n"
"\n"
"QComboBox::down-arrow:on { \n"
"    top: 1px;\n"
"    left: 1px;\n"
"}"))
        self.baseAreaStretchComboBox.setObjectName(_fromUtf8("baseAreaStretchComboBox"))
        self.baseAreaStretchComboBox.addItem(_fromUtf8(""))
        self.baseAreaStretchComboBox.addItem(_fromUtf8(""))
        self.horizontalLayout_6.addWidget(self.baseAreaStretchComboBox)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.groupBox = QtGui.QGroupBox(self.frame)
        self.groupBox.setTitle(_fromUtf8(""))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.standardDeviationStrectchRadioButton = QtGui.QRadioButton(self.groupBox)
        self.standardDeviationStrectchRadioButton.setEnabled(True)
        self.standardDeviationStrectchRadioButton.setMinimumSize(QtCore.QSize(0, 35))
        self.standardDeviationStrectchRadioButton.setMaximumSize(QtCore.QSize(16777215, 35))
        self.standardDeviationStrectchRadioButton.setObjectName(_fromUtf8("standardDeviationStrectchRadioButton"))
        self.gridLayout_3.addWidget(self.standardDeviationStrectchRadioButton, 5, 0, 1, 1)
        self.standardDeviationLabel = QtGui.QLabel(self.groupBox)
        self.standardDeviationLabel.setEnabled(False)
        self.standardDeviationLabel.setMinimumSize(QtCore.QSize(40, 22))
        self.standardDeviationLabel.setMaximumSize(QtCore.QSize(32, 35))
        self.standardDeviationLabel.setAcceptDrops(False)
        self.standardDeviationLabel.setStyleSheet(_fromUtf8("max-width: 32px;\n"
"\n"
""))
        self.standardDeviationLabel.setLineWidth(1)
        self.standardDeviationLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.standardDeviationLabel.setObjectName(_fromUtf8("standardDeviationLabel"))
        self.gridLayout_3.addWidget(self.standardDeviationLabel, 5, 1, 1, 1)
        self.standardDeviationClipSpinBox = QtGui.QDoubleSpinBox(self.groupBox)
        self.standardDeviationClipSpinBox.setEnabled(False)
        self.standardDeviationClipSpinBox.setMinimumSize(QtCore.QSize(0, 22))
        self.standardDeviationClipSpinBox.setMaximumSize(QtCore.QSize(75, 16777215))
        self.standardDeviationClipSpinBox.setSingleStep(0.1)
        self.standardDeviationClipSpinBox.setProperty("value", 2.0)
        self.standardDeviationClipSpinBox.setObjectName(_fromUtf8("standardDeviationClipSpinBox"))
        self.gridLayout_3.addWidget(self.standardDeviationClipSpinBox, 5, 2, 1, 1)
        self.percentageClipLabel = QtGui.QLabel(self.groupBox)
        self.percentageClipLabel.setEnabled(False)
        self.percentageClipLabel.setMinimumSize(QtCore.QSize(40, 22))
        self.percentageClipLabel.setMaximumSize(QtCore.QSize(32, 22))
        self.percentageClipLabel.setBaseSize(QtCore.QSize(0, 0))
        self.percentageClipLabel.setStyleSheet(_fromUtf8("max-width: 32px;\n"
" "))
        self.percentageClipLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.percentageClipLabel.setObjectName(_fromUtf8("percentageClipLabel"))
        self.gridLayout_3.addWidget(self.percentageClipLabel, 4, 1, 1, 1)
        self.percentageClipSpinBox = QtGui.QDoubleSpinBox(self.groupBox)
        self.percentageClipSpinBox.setEnabled(False)
        self.percentageClipSpinBox.setMaximumSize(QtCore.QSize(75, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Tahoma,Verdana,Arial,sans-serif"))
        font.setPointSize(-1)
        self.percentageClipSpinBox.setFont(font)
        self.percentageClipSpinBox.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.percentageClipSpinBox.setSuffix(_fromUtf8(""))
        self.percentageClipSpinBox.setDecimals(1)
        self.percentageClipSpinBox.setMaximum(100.0)
        self.percentageClipSpinBox.setSingleStep(0.1)
        self.percentageClipSpinBox.setProperty("value", 2.0)
        self.percentageClipSpinBox.setObjectName(_fromUtf8("percentageClipSpinBox"))
        self.gridLayout_3.addWidget(self.percentageClipSpinBox, 4, 2, 1, 1)
        self.percentageClipRadioButton = QtGui.QRadioButton(self.groupBox)
        self.percentageClipRadioButton.setEnabled(True)
        self.percentageClipRadioButton.setObjectName(_fromUtf8("percentageClipRadioButton"))
        self.gridLayout_3.addWidget(self.percentageClipRadioButton, 4, 0, 1, 1)
        self.noStretchRadioButton = QtGui.QRadioButton(self.groupBox)
        self.noStretchRadioButton.setChecked(True)
        self.noStretchRadioButton.setObjectName(_fromUtf8("noStretchRadioButton"))
        self.gridLayout_3.addWidget(self.noStretchRadioButton, 2, 0, 1, 1)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.verticalLayout_6.addWidget(self.frame)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.applyButton = QtGui.QPushButton(self.histogramPlotTab)
        self.applyButton.setMinimumSize(QtCore.QSize(102, 22))
        self.applyButton.setMaximumSize(QtCore.QSize(100, 22))
        self.applyButton.setStyleSheet(_fromUtf8("QPushButton {\n"
"    border: 1px solid darkgray;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"min-height: 20px;\n"
"max-height: 20px;\n"
"min-width: 100px;\n"
"\n"
"}"))
        self.applyButton.setObjectName(_fromUtf8("applyButton"))
        self.horizontalLayout.addWidget(self.applyButton)
        self.closeButton = QtGui.QPushButton(self.histogramPlotTab)
        self.closeButton.setMinimumSize(QtCore.QSize(112, 22))
        self.closeButton.setMaximumSize(QtCore.QSize(100, 22))
        self.closeButton.setStyleSheet(_fromUtf8("\n"
"    border: 1px solid darkgray;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"min-height: 20px;\n"
"max-height: 20px;\n"
"min-width: 100px;\n"
"\n"
"\n"
"margin-right: 10px;"))
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout.addWidget(self.closeButton)
        self.verticalLayout_6.addLayout(self.horizontalLayout)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.verticalLayout_6.addLayout(self.horizontalLayout_7)
        self.plotsTabWidget.addTab(self.histogramPlotTab, _fromUtf8(""))
        self.LUTPlot = QtGui.QWidget()
        self.LUTPlot.setObjectName(_fromUtf8("LUTPlot"))
        self.gridLayout_2 = QtGui.QGridLayout(self.LUTPlot)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.LUTPlotLayout = QtGui.QGridLayout()
        self.LUTPlotLayout.setObjectName(_fromUtf8("LUTPlotLayout"))
        self.LUTLayersTabWidget = QtGui.QTabWidget(self.LUTPlot)
        self.LUTLayersTabWidget.setMinimumSize(QtCore.QSize(848, 0))
        self.LUTLayersTabWidget.setStyleSheet(_fromUtf8("font-size: 9px;"))
        self.LUTLayersTabWidget.setTabShape(QtGui.QTabWidget.Rounded)
        self.LUTLayersTabWidget.setObjectName(_fromUtf8("LUTLayersTabWidget"))
        self.LUTPlotLayout.addWidget(self.LUTLayersTabWidget, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.LUTPlotLayout, 1, 0, 1, 1)
        self.plotsTabWidget.addTab(self.LUTPlot, _fromUtf8(""))
        self.verticalLayout_3.addWidget(self.plotsTabWidget)
        self.histogramDialogLayout = QtGui.QVBoxLayout()
        self.histogramDialogLayout.setSpacing(6)
        self.histogramDialogLayout.setObjectName(_fromUtf8("histogramDialogLayout"))
        self.verticalLayout_3.addLayout(self.histogramDialogLayout)

        self.retranslateUi(histogramDialog)
        self.plotsTabWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), histogramDialog.close)
        QtCore.QMetaObject.connectSlotsByName(histogramDialog)

    def retranslateUi(self, histogramDialog):
        self.label_3.setText(_translate("histogramDialog", "Area of statistics", None))
        self.statisticsComboBox.setItemText(0, _translate("histogramDialog", "Entire Raster", None))
        self.statisticsComboBox.setItemText(1, _translate("histogramDialog", "Current Extent", None))
        self.label_4.setText(_translate("histogramDialog", "Base Area of Stretch", None))
        self.baseAreaStretchComboBox.setItemText(0, _translate("histogramDialog", "Entire Raster", None))
        self.baseAreaStretchComboBox.setItemText(1, _translate("histogramDialog", "Current Extent", None))
        self.standardDeviationStrectchRadioButton.setText(_translate("histogramDialog", "Standard Deviation", None))
        self.standardDeviationLabel.setText(_translate("histogramDialog", "Value", None))
        self.standardDeviationClipSpinBox.setAccessibleName(_translate("histogramDialog", "standard", None))
        self.percentageClipLabel.setText(_translate("histogramDialog", "Value", None))
        self.percentageClipSpinBox.setAccessibleName(_translate("histogramDialog", "percent", None))
        self.percentageClipRadioButton.setText(_translate("histogramDialog", "Percentage Clip", None))
        self.noStretchRadioButton.setText(_translate("histogramDialog", "None", None))
        self.applyButton.setText(_translate("histogramDialog", "Apply", None))
        self.closeButton.setText(_translate("histogramDialog", "Close", None))
        self.plotsTabWidget.setTabText(self.plotsTabWidget.indexOf(self.histogramPlotTab), _translate("histogramDialog", "Histogram", None))
        self.plotsTabWidget.setTabText(self.plotsTabWidget.indexOf(self.LUTPlot), _translate("histogramDialog", "Look-Up Table", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    histogramDialog = QtGui.QDialog()
    ui = Ui_histogramDialog()
    ui.setupUi(histogramDialog)
    histogramDialog.show()
    sys.exit(app.exec_())

