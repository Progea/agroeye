# -*- coding: utf-8 -*-

from PyQt4.QtGui import QDialog, QFileDialog, QMessageBox
from PyQt4.Qt import Qt, SIGNAL, QApplication, QString, QFile, QTextStream, QIODevice

from Modules import agroeye
from View import ui_progressdialog
from Control.exceptionModel import PathError

_fromUtf8=QString.fromUtf8

class AgroeyeCallback(agroeye.Callback):

    def __init__(self):
        super(AgroeyeCallback, self).__init__()
        self.widget = None

    def setupQWidget(self, widget=None):
        self.widget = widget

    def call(self):
        self.widget.processCallback()


class PyCallback(QDialog, ui_progressdialog.Ui_progressDialog):

    def __init__(self, parent=None, progressName=None):
        """!
        @brief Callback Progress dialog box
        """
        super(PyCallback, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)
        self.agroeyeCallback = None

        if progressName:
            self.progressName = progressName
        else:
            self.progressName = u'raport'

        ## Connect buttons
        self.closeButton.clicked.connect(self.closeEvent)
        self.saveButton.clicked.connect(self.saveOutput)

    def start(self):
        self.agroeyeCallback = AgroeyeCallback()
        self.agroeyeCallback.setupQWidget(self)
        self.show()

    def updateProgressBar(self):
        progress = self.agroeyeCallback.getProgress()
        self.progressBar.setValue(int(progress*100))

    def processCallback(self):
        messagge = self.agroeyeCallback.getMessage()
        callbacksList = self.agroeyeCallback.getCallbackCategories()

        # if agroeye.Callback_t.DETAIL in callbacksList:
        #     return
        if agroeye.Callback_t.PROGRESS in callbacksList:
            self.updateProgressBar()
        elif agroeye.Callback_t.FINISH in callbacksList:
            self.__finish()

        self.outputInfo.append(messagge)
        QApplication.processEvents()

    def closeEvent(self, event):
        self.close()
        self.emit(SIGNAL("segmentation_dialog_closed()"))

    def __finish(self):
        self.emit(SIGNAL("segmentation_finished()"))
        self.progressBar.setValue(100)
        self.saveButton.setEnabled(True)
        self.closeButton.setEnabled(True)

    def saveOutput(self):
        """!
        @brief Save messages from output info
        """
        outMessage = self.outputInfo.toPlainText()
        caption = QString(self.progressName)
        dialogTitle = QString(_fromUtf8(_(u"Save raport to the file")))
        filterOptions = QString(_fromUtf8(_(u"Text Files (*.txt);;")))
        fileName = QFileDialog.getSaveFileNameAndFilter(self, dialogTitle, caption, filter=filterOptions)

        try:
            if fileName[0].isEmpty():
                raise PathError(fileName[0])
            else:
                fh = QFile(fileName[0])
                fh.open(QIODevice.WriteOnly)
                stream = QTextStream(fh)
                stream.setCodec('UTF-8')
                stream << outMessage
                fh.close()
                self.close()

        except Exception as error:
            QMessageBox.critical(self, QString(_fromUtf8(_(u"Error!"))), error.msg)


