# -*- coding: utf-8 -*-


import random
import editstyledialog
from PyQt4 import QtCore
from PyQt4 import QtGui
from ui_preferencesdialog import Ui_preferencesDialog

_fromUtf8 = QtCore.QString.fromUtf8

class Preferences(QtGui.QDialog, Ui_preferencesDialog):

    def __init__(self, parent=None):
        super(Preferences, self).__init__(parent, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)
        self.dirty = False
        self.setParameterForViewAreaTab()
        self.setParameterForVectorTab()
        self.setParamterForRasterTab()

        self.closeButton.clicked.connect(self.close)
        self.applyButton.clicked.connect(self.apply)

    def setParameterForViewAreaTab(self):

        self.defaultProjectionCombobox.setEnabled(False)
        self.asFirstLayerRadio.setChecked(True)
        allItems = [self.parent.coordinateSystemComboBox.itemText(i) for i in range(self.parent.coordinateSystemComboBox.count())]
        allItems = QtCore.QStringList(allItems)
        self.defaultProjectionCombobox.addItems(allItems)
        self.asDeclaredHere.toggled.connect(lambda isActive: self.activateComboEPSG(isActive))
        self.defaultProjectionCombobox.activated.connect(lambda chooseindex:
                    self.parent.viewArea.chooseEPSG(chooseindex, self.defaultProjectionCombobox, True))
        self.defaultProjectionCombobox.currentIndexChanged.connect(lambda: self.applyButton.setEnabled(True))
       # QtCore.QObject.connect(self.parent.parent, QtCore.SIGNAL("fileLoaded()"), self.disableProjectionSetting)


    def setParamterForRasterTab(self):


        self.stretchParametersLabel = QtGui.QLabel(_fromUtf8(_(u'Stretch parameters')))
        self.minPercentageLabel = QtGui.QLabel(_fromUtf8(_(u'Min: ')))
        self.minPercentageSpinbox = QtGui.QDoubleSpinBox()
        self.minPercentageSpinbox.setRange(0.0, 50.0)
        self.minPercentageSpinbox.setDecimals(2)
        self.minPercentageSpinbox.setSingleStep(0.1)
        self.minPercentageSpinbox.setSuffix(QtCore.QString(u' %'))
        self.minPercentageSpinbox.setValue(self.parent.viewArea.stretchClass.percentClip.param[0])
        self.maxPercentageLabel = QtGui.QLabel(_fromUtf8(_(u'Max: ')))
        self.maxPercentageSpinbox = QtGui.QDoubleSpinBox()
        self.maxPercentageSpinbox.setRange(0.0, 50.0)
        self.maxPercentageSpinbox.setDecimals(2)
        self.maxPercentageSpinbox.setSingleStep(0.1)
        self.maxPercentageSpinbox.setSuffix(QtCore.QString(u' %'))
        self.maxPercentageSpinbox.setValue(self.parent.viewArea.stretchClass.percentClip.param[1])
        padlockIcon = QtGui.QIcon()
        padlockIcon.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuEdit/resources/padlock_open.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        padlockIcon.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuEdit/resources/padlock_close.png")), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.blockDataRangeButton = QtGui.QPushButton()
        self.blockDataRangeButton.setIcon(padlockIcon)
        self.blockDataRangeButton.setCheckable(True)
        self.standardDeviationLabel = QtGui.QLabel(_fromUtf8(_(u'Standard Deviation: ')))
        self.standardDeviationSpinbox = QtGui.QDoubleSpinBox()
        self.standardDeviationSpinbox.setDecimals(2)
        self.standardDeviationSpinbox.setSingleStep(0.1)
        self.standardDeviationSpinbox.setValue(self.parent.viewArea.stretchClass.standardDev.param[0])


        self.stretchComboBox.currentIndexChanged.connect(self.setStretchLayout)
        self.stretchComboBox.setCurrentIndex(self.parent.viewArea.stretchClass.stretchType.index)
        self.blockDataRangeButton.toggled.connect(self.blockMinMaxPercentage)
        self.minPercentageSpinbox.valueChanged.connect(lambda: self.applyButton.setEnabled(True))
        self.maxPercentageSpinbox.valueChanged.connect(lambda: self.applyButton.setEnabled(True))
        self.standardDeviationSpinbox.valueChanged.connect(lambda: self.applyButton.setEnabled(True))

    def setParameterForVectorTab(self):

        editStyle = editstyledialog.Edit(self)
        self.sizeSpinBox.setMinimum(1.0)
        self.sizeSpinBox.setValue(self.parent.viewArea.vectorPixSize)
        self.opacitySpinBox.setMinimum(0)
        self.opacitySpinBox.setMaximum(100)
        self.opacitySpinBox.setValue(self.parent.viewArea.vectorOpacity)
        self.opacitySlider.valueChanged.connect(self.opacitySpinBox.setValue)
        self.opacitySlider.setMaximum(100)
        self.opacitySpinBox.valueChanged.connect(self.opacitySlider.setValue)
        self.colourIn = self.parent.viewArea.vectorColorIn
        self.setColorRadioButton.toggled.connect(lambda isActive: self.activateColorButton(isActive))

        editStyle.setSymbol(self.parent.viewArea.vectorSymbolStyle, self.symbolComboBox)
        editStyle.setFillPattern(self.parent.viewArea.vectorFillStyle, self.fillComboBox)
        editStyle.setLinePattern(self.parent.viewArea.vectorLineStyle, self.lineComboBox)

        self.randomColorRadioButton.setChecked(True)
        self.colorFillButton.setEnabled(False)
        self.opacitySlider.valueChanged.connect(lambda: self.applyButton.setEnabled(True))
        self.sizeSpinBox.valueChanged.connect(lambda: self.applyButton.setEnabled(True))
        self.symbolComboBox.currentIndexChanged.connect(lambda: self.applyButton.setEnabled(True))
        self.lineComboBox.currentIndexChanged.connect(lambda: self.applyButton.setEnabled(True))
        self.fillComboBox.currentIndexChanged.connect(lambda: self.applyButton.setEnabled(True))


    def activateComboEPSG(self, isActivated):
        self.defaultProjectionCombobox.setEnabled(isActivated)
        self.applyButton.setEnabled(True)

    def activateColorButton(self, isActivated):

        if isActivated:
            if self.colourIn:
                rgb = self.getRGBFromColour(self.colourIn)
            else:
                rgb = self.getRandomColour()
            self.colorFillButton.setStyleSheet("QWidget { background-color: rgb(%d,%d,%d) }" % rgb)
            self.colourIn = QtGui.QColor(rgb[0], rgb[1], rgb[2])
            self.colorFillButton.clicked.connect(self.colorDialog)
        else:
            self.colourIn = 0
            self.colorFillButton.setStyleSheet("QWidget { color: gray }")
            self.colorFillButton.clicked.disconnect(self.colorDialog)

        self.colorFillButton.setEnabled(isActivated)
        self.applyButton.setEnabled(True)

    def colorDialog(self):

        colour = QtGui.QColorDialog.getColor(self.colourIn, self)
        self.colourIn = colour
        if colour.isValid():
                self.colorFillButton.setStyleSheet("QWidget {background-color: rgb(%d,%d,%d)}" %self.getRGBFromColour(colour))
                self.applyButton.setEnabled(True)

    def getRGBFromColour(self, colour):
        return (colour.red(), colour.green(),colour.blue())

    def getRandomColour(self):
        r = lambda: random.randint(0, 255)
        return (r(), r(), r())

    def setStretchLayout(self, index):
        self.applyButton.setEnabled(True)
        if index == self.parent.viewArea.stretchClass.percentClip.index:
            self.stretchParametersLabel.setEnabled(True)
            self.standardDeviationLabel.setParent(None)
            self.standardDeviationSpinbox.setParent(None)
            self.stretchParametersLayout.setVerticalSpacing(0)
            self.stretchParametersLayout.addWidget(self.minPercentageLabel, 3, 3, QtCore.Qt.AlignBottom)
            self.stretchParametersLayout.addWidget(self.minPercentageSpinbox, 3, 4, QtCore.Qt.AlignBottom)
            self.stretchParametersLayout.addWidget(self.blockDataRangeButton, 4, 4, 1, 1)
            self.stretchParametersLayout.addWidget(self.maxPercentageLabel, 5, 3, QtCore.Qt.AlignTop)
            self.stretchParametersLayout.addWidget(self.maxPercentageSpinbox, 5, 4, QtCore.Qt.AlignTop)

        elif index == self.parent.viewArea.stretchClass.standardDev.index:
            self.stretchParametersLabel.setEnabled(True)
            self.minPercentageLabel.setParent(None)
            self.minPercentageSpinbox.setParent(None)
            self.maxPercentageLabel.setParent(None)
            self.maxPercentageSpinbox.setParent(None)
            self.blockDataRangeButton.setParent(None)
            self.stretchParametersLayout.addWidget(self.standardDeviationLabel, 3, 3)
            self.stretchParametersLayout.addWidget(self.standardDeviationSpinbox, 3, 4, QtCore.Qt.AlignLeft)
        elif index == self.parent.viewArea.stretchClass.none.index:
            self.stretchParametersLabel.setEnabled(False)
            self.minPercentageLabel.setParent(None)
            self.minPercentageSpinbox.setParent(None)
            self.maxPercentageLabel.setParent(None)
            self.maxPercentageSpinbox.setParent(None)
            self.blockDataRangeButton.setParent(None)
            self.standardDeviationLabel.setParent(None)
            self.standardDeviationSpinbox.setParent(None)

    def blockMinMaxPercentage(self, block):
        if block:
            self.blockDataRangeButton.setAutoFillBackground(True)
            self.blockDataRangeButton.setStyleSheet("background-color: rgb(107,108,109); border: none;")
            self.minPercentageSpinbox.valueChanged.connect(self.maxPercentageSpinbox.setValue)
            self.maxPercentageSpinbox.valueChanged.connect(self.minPercentageSpinbox.setValue)
        else:
            self.blockDataRangeButton.setAutoFillBackground(True)
            self.blockDataRangeButton.setStyleSheet("background-color: rgb(218,219,222); border: none;")
            self.minPercentageSpinbox.valueChanged.disconnect(self.maxPercentageSpinbox.setValue)
            self.maxPercentageSpinbox.valueChanged.disconnect(self.minPercentageSpinbox.setValue)

    def apply(self):

        self.dirty = False
        if self.asDeclaredHere.isChecked():

            self.parent.viewArea.epsgChanged(self.defaultProjectionCombobox.currentIndex(), self.defaultProjectionCombobox, setupMain=True)
        else:
            self.parent.viewArea.projectionFromFirst()

        index = self.stretchComboBox.currentIndex()
        if index == 0:
            self.parent.viewArea.stretchClass.saveStretchType(index, None)
        elif index == 1:
            self.parent.viewArea.stretchClass.saveStretchType(index, [self.minPercentageSpinbox.value(),self.maxPercentageSpinbox.value()])
        elif index == 2:
            self.parent.viewArea.stretchClass.saveStretchType(index, [self.standardDeviationSpinbox.value(), 0])

        self.parent.viewArea.vectorPixSize = self.sizeSpinBox.value()
        self.parent.viewArea.vectorLineStyle = self.lineComboBox.currentIndex() + 1
        self.parent.viewArea.vectorFillStyle = self.fillComboBox.currentIndex()+ 1
        self.parent.viewArea.vectorOpacity = self.opacitySpinBox.value()
        self.parent.viewArea.vectorSymbolStyle = self.symbolComboBox.currentIndex() + 1
        if self.randomColorRadioButton.isChecked():
            self.parent.viewArea.vectorColorIn = None
        else:
            self.parent.viewArea.vectorColorIn = self.colourIn

        self.applyButton.setEnabled(False)