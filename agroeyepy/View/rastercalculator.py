# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from View import ui_rastercalculatordialog
import os
_fromUtf8 = QString.fromUtf8

class RasterCalculator(QDialog, ui_rastercalculatordialog.Ui_rasterCalculatorDialog):
    def __init__(self, parent=None, propertiesObj=None):
        super(RasterCalculator, self).__init__(parent, flags = Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parentClusterTableDialog = propertiesObj
        self.parentMain = parent
        self.setupUi(self)
        self.cancelButton.clicked.connect(self.cancelDialog)
        self.okButton.clicked.connect(self.okDialog)

        self.setRasterBandList()
        self.setCalculatorButton()
        self.setDisplayRange()

    def cancelDialog(self):
        """!
        @brief Cancel raster calculator window.
        """
        self.close()

    def okDialog(self):
        """!
        @brief Accept raster calculator window.
        """
        # do sth
        self.close()


    def setRasterBandList(self):
        self.rasterBandListView.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.rasterBandListView.doubleClicked.connect(self.addBandExpression)
        self.files =  self.parentMain.loadedfiles_model.files

        self.model = QStandardItemModel(self.rasterBandListView)


        for file in self.files:
            if not file.isVector:
                for band in range(1,file.bandsCount+1):
                    fileName, ext = os.path.splitext(file.name)
                    item = QStandardItem('\"%s@%s\"'%(fileName, band))
                    self.model.appendRow(item)
        self.rasterBandListView.setModel(self.model)


    def addBandExpression(self, signal):
        text =  self.model.item(signal.row(), 0).text()
        self.calculatorTextEdit.moveCursor(QTextCursor.End)
        self.calculatorTextEdit.insertPlainText(' ' + text)
        self.calculatorTextEdit.moveCursor(QTextCursor.End)

    def setCalculatorButton(self):
        for index in range(0, self.calculatorLayout.count()):
            item = self.calculatorLayout.itemAt(index).widget()
            item.setAutoDefault(False)
            item.clicked.connect(self.addMath)

    def addMath(self):
        text = self.sender().accessibleName()
        self.calculatorTextEdit.moveCursor(QTextCursor.End)
        if text.startsWith('a') or text.startsWith('c') or text.startsWith('s') or text.startsWith('t'):
            text = ' ' + text + ' ( '

        else:
            text = ' ' + text
        self.calculatorTextEdit.insertPlainText(text)
        self.calculatorTextEdit.moveCursor(QTextCursor.End)



    def setDisplayRange(self):
        validator = QDoubleValidator()
        self.minLineEdit.setValidator(validator)
        self.maxLineEdit.setValidator(validator)

    def checkState(self):
        sender = self.sender()
        print sender
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]
        if state == QValidator.Acceptable:
            color = '#c4df9b' # green
        elif state == QValidator.Intermediate:
            color = '#fff79a' # yellow
        else:
            color = '#f6989d' # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)