
# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from collections import namedtuple
from View import ui_modeldialog
from View import modelfilesdialog
from View import modelfunctiondialog
from View import modelattributedialog
_fromUtf8 = QString.fromUtf8

class ModelGAEC(QDialog, ui_modeldialog.Ui_modelWindowDialog):
    """!
    The class ModelGAEC is the base class for GAEC rules.
    """
    def __init__(self, parent=None):
        super(ModelGAEC, self).__init__(parent, flags = Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)
        self.setMinimumSize(800,400)
        self.modelAttr = modelattributedialog.ModelAttribute(self)
        self.redColor = QColor(255,0,0)
        self.greenColor = QColor(0, 153, 0)
        self.widthBlock = 100
        ## @var self.widthBlock
        # Width of block.
        self.heightBlock = 50
        ## @var self.heightBlock
        # Height of block.
        self.rulesNumber = {0: 1, 1: 3, 2: 4, 3: 5, 4: 6, 5: 7, 6: 12, 7: 13, 8: 14, 9: 15}
        self.defineRules()
        self.chooseModelComboBox.setCurrentIndex(-1)
        self.chooseModelComboBox.currentIndexChanged.connect(self.setRules)
        self.cancelButton.clicked.connect(self.close)

    def defineRules(self):
        """!
        @brief Define GAEC rules model.
        """
        self.rulesType = {}
        ## @var self.rulesType
        # List with all rules.
        self.initFilesType()
        self.initFunctionsType()

        self.rulesType[14] = [self.inputRaster, self.segm,  self.outputDb, self.synth, self.stats]

    def initFilesType(self):
        """!
        @brief Init blocks of files type in GAEC model.
        """
        self.FilesType = namedtuple('FilesType', ['text', 'index', 'name'])
        ## @var self.FilesType
        # Structure for files type of block.
        self.inputRaster = self.FilesType(_fromUtf8(_('Input Raster')), 1, 'rasterIn')
        self.outputDb = self.FilesType(_fromUtf8(_('Output database')), 2, 'dbOut')
        self.outputStats = self.FilesType(_fromUtf8(_('Output statistics')), 3, 'statOut')
        self.outputClass = self.FilesType(_fromUtf8(_('Output classification')), 4, 'classOut')

    def initFunctionsType(self):
        """!
        @brief Init blocks of functions type in GAEC model.
        """
        self.FunctionsType = namedtuple('FunctionType', ['text', 'index', 'name', 'isFinish'])
        ## @var self.FunctionsType
        # Structure for functions type of block.
        self.segm = self.FunctionsType(_fromUtf8(_('Segmentation')), 1, 'segmBlock', False)
        self.stats = self.FunctionsType(_fromUtf8(_('Statistics')), 2, 'statsBlock', False)
        self.synth = self.FunctionsType(_fromUtf8(_('Synthetic bands')), 3, 'synthBlock', False)
        self.classify = self.FunctionsType(_fromUtf8(_('Classification')), 4, 'classBlock', False)

    def setRules(self):
        """!
        @brief Set GAEC model for rule.
        """
        self.scene = QGraphicsScene(self)
        self.scene.clear()
        self.scene.setSceneRect(0,0, self.modelGraphicsView.width(), self.modelGraphicsView.height())
        self.modelGraphicsView.setScene(self.scene)
        index = self.rulesNumber[self.chooseModelComboBox.currentIndex()]
        if self.rulesType.has_key(index):
            self.drawWorkflow(self.rulesType[index])
        self.modelGraphicsView.setScene(self.scene)

    def drawWorkflow(self, rules):
        """!
        @brief Draw block of model in Scene.
        @param rules: ordered list with type of rules.
        """
        self.height = self.modelGraphicsView.height()/2
        self.width = self.modelGraphicsView.width()/2
        self.drawPoint = QPoint(0, self.height)

        for rule in rules:
            if type(rule) == self.FilesType:
                self.drawFilesBlock(rule)
                self.drawTextItem(rule.text)
                self.drawArrowItem()
            elif type(rule) == self.FunctionsType:
                self.drawFunctionBlock(rule)
                self.drawTextItem(rule.text)
                self.drawArrowItem()

    def drawFilesBlock(self, rule):
        """!
        @brief Draw ellipse for files type.
        @param rule: structure of rules type.
        """
        setattr(self, rule.name, FilesBlock(self, rule.index))
        getattr(self, rule.name).setPos(self.drawPoint)
        self.scene.addItem(getattr(self, rule.name))

    def drawFunctionBlock(self, rule):
        """!
        @brief Draw rectangle for function type.
        @param rule: structure of rules type.
        """
        setattr(self, rule.name, FunctionsBlock(self, rule.index))
        getattr(self, rule.name).setPos(self.drawPoint)
        self.scene.addItem(getattr(self, rule.name))

    def drawArrowItem(self):
        """!
        @brief Draw arrow to connect blocks.
        """
        arrow = ArrowItem(QPoint(self.drawPoint.x(), self.height+self.heightBlock/2), QPoint(self.drawPoint.x()+self.widthBlock/2, self.height+self.heightBlock/2))
        self.scene.addItem(arrow)
        self.updateDrawPoint(self.widthBlock/2)

    def drawTextItem(self, text):
        """!
        @brief Draw text into block.
        @param text: text which will be drawn.
        """
        text = TextItem(text)
        text.setPos(QPoint(self.drawPoint.x()+8, self.height+15))
        self.scene.addItem(text)
        self.updateDrawPoint(self.widthBlock)

    def updateDrawPoint(self, x):
        """!
        @brief Change x coordinate of drawn point.
        @param x: new value of x.
        """
        self.drawPoint = self.drawPoint.__add__(QPoint(x,0))

    def setGradientInBlock(self):
        """!
        @brief Returns color gradient.
        """
        gradient = QLinearGradient(0, 0, 0, 0)
        gradient.setColorAt(0, QColor(218, 219, 222))
        gradient.setColorAt(1, QColor(246, 247, 250))
        return gradient



class FilesBlock(QGraphicsEllipseItem):
    """!
    The class FilesBlock handles any type of files block. It handles input and output files for segmentation, statistics and classification.
    """
    def __init__(self, parent, index):
        super(FilesBlock, self).__init__()
        self.parent = parent
        self.index = index
        ## @var self.index
        # Number of block.
        self.setPen(QPen(self.parent.redColor, 2))
        self.setBrush(self.parent.setGradientInBlock())
        self.setRect(0,0,self.parent.widthBlock,self.parent.heightBlock)
        self.setFlag(QGraphicsEllipseItem.ItemIsSelectable)


    def mouseDoubleClickEvent(self, mouseEvent):
        """!
        @brief Action after mouse double click.
        @param mouseEvent: double click event.
        """
        if self.index == self.parent.inputRaster.index:
            self.addInputWindow()
        elif self.index == self.parent.outputDb.index:
            self.addOutputWindow()

    def addInputWindow(self):
        """!
        @brief Input files window.
        """
        inputRaster = modelfilesdialog.InputFiles(self.parent)
        inputRaster.exec_()

    def addOutputWindow(self):
        """!
        @brief Output files window.
        """
        outputDB = modelfilesdialog.OutputFiles(self.parent)
        outputDB.show()

class FunctionsBlock(QGraphicsRectItem):
    """!
    The class FunctionsBlock handles any type of functions block. It handles functions as segmentation, statistics and classification.
    """
    def __init__(self, parent, index):
        super(FunctionsBlock, self).__init__()
        self.parent = parent
        self.index = index
        ## @var self.index
        # Number of block.
        self.setPen(QPen(self.parent.redColor, 2))
        self.setBrush(self.parent.setGradientInBlock())
        self.setRect(0,0,self.parent.widthBlock,self.parent.heightBlock)


    def mouseDoubleClickEvent(self, mouseEvent):
        """!
        @brief Action after mouse double click.
        @param mouseEvent: double click event.
        """
        if self.index == self.parent.segm.index:
            self.segmentationWindow()
        elif self.index == self.parent.stats.index:
             self.statisticsWindow()
        elif self.index == self.parent.synth.index:
             self.syntheticWindow()

    def segmentationWindow(self):
        """!
        @brief Segmentation window.
        """
        segment = modelfunctiondialog.Segmentation(self.parent)
        segment.show()

    def statisticsWindow(self):
        """!
        @brief Statistics window.
        """
        stats = modelfunctiondialog.Statistics(self.parent)
        stats.show()

    def syntheticWindow(self):
        """!
        @brief Synthetic window.
        """
        synth = modelfunctiondialog.Synthetic(self.parent)
        synth.show()

class ArrowItem(QGraphicsLineItem):
    def __init__(self, point1, point2):
        """!
        The class ArrowItem draws arrow which connects the blocks.
        """
        super(ArrowItem, self).__init__()
        self.setPen(QPen(Qt.black,2))
        self.setLine(float(point1.x()), float(point1.y()), float(point2.x()), float(point2.y()))

class TextItem(QGraphicsTextItem):
    """!
    The class TextItem draws text in blocks.
    """
    def __init__(self, name):
        super(TextItem, self).__init__()
        self.setPlainText(name)


