# -*- coding: utf-8 -*-

from __future__ import division
import os
import re
import platform
import time
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4 import QtCore
from collections import namedtuple
from PyQt4 import QtSvg
import osgeo
import osr
import ogr
import gdal
import gdalconst
import viewscene
from Control import exceptionModel
from Logic import rasterattributes

_fromUtf8 =QString.fromUtf8

class ViewArea(QGraphicsView):

    def __init__(self, parent=None):
        super(ViewArea, self).__init__(parent)
        if platform.system() == "Windows":
            try:
                os.environ['GDAL_DATA']
            except:
                osgeo_source_folder = os.path.dirname(osgeo.__file__)
                gdal_data_folder = os.path.join(osgeo_source_folder, "gdal-data")
                gdal.SetConfigOption("GDAL_DATA", gdal_data_folder)


        self.scene = viewscene.ViewScene()
        self.parent = parent
        self.setRenderHint(QPainter.HighQualityAntialiasing)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.wheelScrollZoomFactor = 1.41
        self.setScene(self.scene)
        self.setMouseTracking(True)
        self.panActionActive = False
        self.setCursor(Qt.CrossCursor)

        self.stretchClass = rasterattributes.DefaultRasterStretch()
        self.vectorPixSize = 4
        self.vectorSymbolStyle = 1
        self.vectorLineStyle = 1
        self.vectorFillStyle = 1
        self.vectorOpacity = 0
        self.vectorColorIn = 0
        self.projectionFirst = True

        self.centralPaintPoint = ogr.Geometry(ogr.wkbPoint)
        self.centralPaintPoint.AddPoint(0, 0)
        ## @var self.paintPoint:
        #  This point describes coordinates the middle of
        #  the view area and the view scene. Movement will be based on the size of viewport.
        #  When you add a pixmap to the view area it is by default added
        #  at the upper left corner of graphics scene. At the beginning we have
        #  resize the scene to the graphics view element.
        self.paintSize = QSize()
        ## @var self.paintSize:
        #  Size of the displaying widget
        self.projectionEPSG = 4326
        ## @var self.projectionEPSG:
        #  EPGS of showed files
        self.viewAreaSpatialReference = None
        self.viewAreaProj4 = None


        self.mem_drv = gdal.GetDriverByName('MEM')
        self.viewAreaAsRaster = self.mem_drv.Create('', 1, 1, 1, gdalconst.GDT_Byte)
        self.viewAreaGeoMatrix = [0, 0, 0, 0, 0, 0]


        self.pixSize = 0
        ## @var self.pixSize:
        #  Current size of the graphics pix in meters
        self.scaleNominator = 1000
        self.ratio = 0
        self.ratioHeight = 0
        self.ratioWidth = 0
        ## @var self.ratio:
        #  Coefficient to convert file pix size into graphics scene size
        self.movePressed = False
        ## @var self.movePressed:
        #  flag checking whether shifting button is pressed or not
        self.startPosition = QPoint()
        ## @var self.startPosition:
        #  Cooridnates of the position of the beginning of the movement.
        self.offset = 0
        ## @var self.offset:
        #  Pixel coordinates indicate upper left corner
        #  whereas geographic coordinates indicates center position of center of pix.
        self.viewBoundary = ogr.Geometry(ogr.wkbPolygon)
        self.sceneHeight = 0
        self.sceneWidth = 0
        self.factor = 1.0
        self.pressed = False
        ## @var self.comboIndexEpsg:
        # Dictionary with current used EPSG code, show in viewerApplication.coordinateSystemComboBox
        self.comboIndexEpsg = {0: 4326, 1: 2180, 2: 2176, 3: 2177, 4: 2178, 5: 2179,
                               6: 32633, 7: 32634, 8: 32635, 9: 3034, 10: 3035,
                               }

        ## @var self.EPSG2Name:
        # Dictionary with all EPSG (by name)
        ## @var self.EPSG2Proj4:
        # Dictionary with all EPSG (by Proj4)
        self.EPSG2Name = dict()
        self.EPSG2Proj4 = dict()

        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__, os.path.join(os.pardir, 'Logic'), 'epsg'), 'r') as epsg_codes:
            tempDatumName = ''

            while 1:
                line = epsg_codes.readline()
                if not line:
                    break
                if '#' in line and len(line) > 2:
                    if re.match('# ', line):
                        tempDatumName = line.split('#')[1].strip()
                    line = epsg_codes.readline()
                    if re.match('^<\d{4,5}>', line):
                        datumName = tempDatumName
                        codeEPSG = int(re.findall('<\d{4,5}>', line)[0].strip("<>"))
                        proj4 = line.split('>')[1][1:-3]
                        try:
                            self.EPSG2Name[codeEPSG] = datumName
                            self.EPSG2Proj4[codeEPSG] = proj4
                        except UnboundLocalError:
                            pass


        self.getImageList = []
        # self.zoomThread = ZoomingThread()
        # self.connect(self.zoomThread, SIGNAL("areaZoomed()"), self.zoomSignal)
        self.thread = QThread()
        self.zoomObject = ZoomingObject(QThread.currentThread())
        self.zoomThread = QThread()

    def addFilesModel(self, model):
        self.files = model

    def convertLayersToData(self, fileListVisible, fileListAll):
        """!
        @brief Set parameters of views, run thread for all loaded files. Returns nothing.
        @param fileList: List of all loaded files
        """


        self.files = fileListVisible
        self.filesAll = fileListAll

        try:
            if self.viewAreaProj4 is None or self.viewBoundary.GetArea() == 0:
                self.setupViewAreaParameters(file = self.files[0])
                self.parent.coordinateSystemComboBox.setEnabled(True)
                self.parent.zoomComboBox.setEnabled(True)
                self.parent.coordinatesLineEdit.setEnabled(True)

        except IndexError:
            pass

        if not self.thread.isRunning():
            self.thread = QtCore.QThread(self)

        self.getDataThread = GetDataImagesThread(self.files, [[self.width(), self.height()], self.viewBoundary,
                         self.pixSize, self.viewAreaProj4, self.viewAreaSpatialReference, self.viewAreaAsRaster])
        self.getDataThread.moveToThread(self.thread)

        self.connect(self.getDataThread, SIGNAL("conversionFinished(PyQt_PyObject)"), self.addLayers)
        self.thread.started.connect(self.getDataThread.processData)

        self.thread.start()

    def rescale(self, scale):
        self.scale(scale, scale)

    def addLayers(self, params):
        """!
        @brief Add loaded files to view as QGraphicsPixmapItem.
        @param params: 0 - list of raster data;
                       1 - list of vector data;
        """

        self.updateUI()
        self.thread.quit()
        self.clearScene()


        dataImage = params[0]
        dataGeom = params[1]
        dataSize = params[2]
        dataPos = params[3]


        for data in reversed(dataImage):
            qImage = QImage(data[0], data[1], data[2], data[3])
            item = QGraphicsPixmapItem(QPixmap.fromImage(qImage))
            self.scene.addItem(item)


        for i, data in reversed(list(enumerate(dataGeom))):


            self.svgWidget = QtSvg.QSvgWidget('%s'%data)
            minXY = self.revertCoordinates(dataPos[i].x(), dataPos[i].y())
            self.svgWidget.setGeometry(minXY[0], minXY[1], dataSize[i].width(), dataSize[i].height())
            self.svgWidget.setWindowFlags(Qt.Window | Qt.FramelessWindowHint)
            palette = QPalette(self.svgWidget.palette())

            palette.setColor(QPalette.Window, Qt.transparent)

            # if self.files[i].isVector:
            #     if self.getFileName(data) == self.files[i].name:

            #         if self.files[i].geomType in (3,6):
            #             brush = QBrush()
            #             brush.setStyle(self.files[i].fillStyle.pattern)
            #             brush.setColor(self.files[i].fillStyle.colour)
            #             palette.setBrush(QPalette.Background, brush)


            self.svgWidget.setPalette(palette)
            self.scene.addWidget(self.svgWidget)


        self.resetTransform()
        self.factor = 1.0
        #self.emit(SIGNAL("pointAdd()"))
        #self.emit(SIGNAL("lineAdd()"))
        #self.emit(SIGNAL("polyAdd()"))
        self.emit(SIGNAL("measureDistanceTool()"))


    def getFileName(self, path):
        fileName, ext = os.path.splitext(path)
        return os.path.basename(fileName)


    def getVisibleLayers(self):

        tempList = []
        for file in self.files:
            if file.visible:
                tempList.append(file)
        return  tempList

    def setupViewAreaParameters(self, file=None, fullExtent=False):
        """!
        @brief Setup ViewArea parameters such as: projection EPSG, size of ViewScene,
        coordinates of center.
        @param file: Required parameters are set up from provided file, raster and vector are possible.
        """
        wholeArea = None
        if file is None:
            for file in self.files:
                tempBoundary = ogr.CreateGeometryFromWkb(file.boundary.ExportToWkb())
                if not file.isVector:
                    transformation = osr.CoordinateTransformation(file.rasterSpatialReference, self.viewAreaSpatialReference)
                else:
                    transformation = osr.CoordinateTransformation(file.vectorSpatialReference, self.viewAreaSpatialReference)
                tempBoundary.Transform(transformation)
                if not wholeArea:
                    wholeArea = tempBoundary
                wholeArea = wholeArea.Union(tempBoundary)

            wholeArea = wholeArea.ConvexHull()

            hzScrollHeight = self.horizontalScrollBar().height()
            vScrollWidth = self.verticalScrollBar().width()
            self.sceneWidth = self.width() - vScrollWidth - 2
            self.sceneHeight = self.height() - hzScrollHeight - 2
            self.setSceneRect(0, 0, self.sceneWidth, self.sceneHeight)
            widthPixs = self.width()
            heightPixs = self.height()
            self.ratioHeight = self.sceneHeight/heightPixs
            self.ratioWidth = self.sceneWidth/widthPixs

            if self.ratioWidth < self.ratioHeight:
                self.ratio = self.ratioWidth
            else:
                self.ratio = self.ratioHeight

            centralX = wholeArea.Centroid().GetX()
            centralY = wholeArea.Centroid().GetY()

            self.pixSize *= (wholeArea.Area()/self.viewBoundary.Area())

            self.centralPaintPoint.AddPoint(centralX, centralY)
            self.viewAreaGeoMatrix = [centralX - self.pixSize*self.sceneWidth/2,
                                  self.pixSize, 0,
                                  centralY + self.pixSize*self.sceneHeight/2,
                                  0, -self.pixSize]

            self.buildUpBoundary()

        elif file is not None:
            hzScrollHeight = self.horizontalScrollBar().height()
            vScrollWidth = self.verticalScrollBar().width()
            self.sceneWidth = self.width() - vScrollWidth - 2
            self.sceneHeight = self.height() - hzScrollHeight - 2
            self.setSceneRect(0, 0, self.sceneWidth, self.sceneHeight)
            try:
                widthPixs = file.cols
                heightPixs = file.rows
            except:
                widthPixs = self.width()
                heightPixs = self.height()


            self.ratioHeight = self.sceneHeight/heightPixs
            self.ratioWidth = self.sceneWidth/widthPixs

            if self.ratioWidth < self.ratioHeight:
                self.ratio = self.ratioWidth
            else:
                self.ratio = self.ratioHeight

            try:
                if file.isVector:
                    file.pixelSizeX = file.pixelSize
                self.pixSize = file.pixelSizeX / self.ratio
                centralX = file.centerX
                centralY = file.centerY
            except:
                self.pixSize = file.GetGeoTransform()[1]
                centralX = file.GetGeoTransform()[0] + (widthPixs/2)*self.pixSize
                centralY = file.GetGeoTransform()[3] - (heightPixs/2)*self.pixSize

            self.centralPaintPoint.AddPoint(centralX, centralY)
            self.viewAreaGeoMatrix = [centralX - self.pixSize*self.sceneWidth/2,
                                  self.pixSize, 0,
                                  centralY + self.pixSize*self.sceneHeight/2,
                                  0, -self.pixSize]
            try:
                self.viewAreaAsRaster = self.mem_drv.Create('', self.sceneWidth, self.sceneHeight, 1, gdal.GDT_Byte)
                self.viewAreaAsRaster.SetGeoTransform(self.viewAreaGeoMatrix)


                if not self.files[0].isVector:
                    self.viewAreaAsRaster.SetProjection(file.Dataset.GetProjection())
                    self.viewAreaSpatialReference = osr.SpatialReference()
                    self.viewAreaSpatialReference.ImportFromWkt(file.Dataset.GetProjection())
                    if self.projectionFirst:
                        srs = osr.SpatialReference(file.Dataset.GetProjection())
                        self.viewAreaProj4 = srs.ExportToProj4()
                        epsg_code = file.rasterAttributes.EPSG
                    else:
                        self.epsgChanged(self.parent.coordinateSystemComboBox.currentIndex())
                        epsg_code = self.projectionEPSG

                else:
                    if self.projectionFirst:
                        self.viewAreaAsRaster.SetProjection(file.srs.ExportToWkt())
                        self.viewAreaSpatialReference = osr.SpatialReference()
                        self.viewAreaSpatialReference.ImportFromWkt(file.srs.ExportToWkt())
                        self.viewAreaProj4 = file.srs.ExportToProj4()
                        epsg_code = file.EPSG
            except Exception as e:
                epsg_code = self.projectionEPSG
            self.setUpEPSG(self.parent.coordinateSystemComboBox, epsg_code)
            self.buildUpBoundary()


    def newSceneParameters(self, event):
        """!
        @brief Calculates new width and height of View Scene
        @param event: Event causes changes
        """

        deltaWidth = event.size().width() - event.oldSize().width()
        deltaHeight = event.size().height() - event.oldSize().height()

        self.sceneWidth += deltaWidth
        self.sceneHeight += deltaHeight

    def clearScene(self):
        """!
        @brief Clears the scene in order to create space for new elements after
        for example resizing, zooming, adding new layer or removing it.
        """
        self.scene = viewscene.ViewScene()
        self.setScene(self.scene)

    def buildUpBoundary(self):
        """!
        @brief Builds new boundary in the currently displayed window.
        """


        try:
            self.viewBoundary.Empty()
        except:
            pass
        bounds = ogr.Geometry(ogr.wkbLinearRing)
        bounds.AddPoint(self.centralPaintPoint.GetX() - self.sceneWidth*self.pixSize/2,
                        self.centralPaintPoint.GetY() - self.sceneHeight*self.pixSize/2)
        bounds.AddPoint(self.centralPaintPoint.GetX() - self.sceneWidth*self.pixSize/2,
                        self.centralPaintPoint.GetY() + self.sceneHeight*self.pixSize/2)
        bounds.AddPoint(self.centralPaintPoint.GetX() + self.sceneWidth*self.pixSize/2,
                        self.centralPaintPoint.GetY() + self.sceneHeight*self.pixSize/2)
        bounds.AddPoint(self.centralPaintPoint.GetX() + self.sceneWidth*self.pixSize/2,
                        self.centralPaintPoint.GetY() - self.sceneHeight*self.pixSize/2)
        bounds.AddPoint(self.centralPaintPoint.GetX() - self.sceneWidth*self.pixSize/2,
                        self.centralPaintPoint.GetY() - self.sceneHeight*self.pixSize/2)
        self.viewBoundary.AddGeometry(bounds)


        if self.files:
            self.viewAreaGeoMatrix[0] = self.centralPaintPoint.GetX() - self.sceneWidth*self.pixSize/2
            self.viewAreaGeoMatrix[1] = self.pixSize
            self.viewAreaGeoMatrix[3] = self.centralPaintPoint.GetY() + self.sceneHeight*self.pixSize/2
            self.viewAreaGeoMatrix[5] = -self.pixSize

            for file in self.filesAll:
                    tempProj = self.viewAreaAsRaster.GetProjection()
                    self.viewAreaAsRaster = self.mem_drv.Create('', self.sceneWidth, self.sceneHeight, 1, gdal.GDT_Byte)
                    self.viewAreaAsRaster.SetGeoTransform(self.viewAreaGeoMatrix)
                    self.viewAreaAsRaster.SetProjection(tempProj)


        onePixeSizeMM = float(self.parent.desktopWidget.widthMM())/float(self.parent.desktopWidget.width())
        onePixeSizeM = onePixeSizeMM/1000
        self.scaleNominator = self.pixSize/onePixeSizeM
        self.setZoomComboBoxValue(self.scaleNominator)

    def setZoomWheelScrollFactor(self, valueIn):
        """!
        @brief Changes the strength of the zoom.
        @param valueIn: Parameter for changes
        """
        self.wheelScrollZoomFactor = valueIn

    def wheelEvent(self, event):
        """!
        @brief Examines what type of mouse wheel event has just occured. Overrides the default method.
        @param event: Scrolling the mouse wheel triggers this event
        """
        # self.factor = self.wheelScrollZoomFactor ** (event.delta() / 240.0)
        if event.delta() > 0:
            self.zoomIn(event)
        elif event.delta() < 0:
            self.zoomOut(event)

    def contextMenuEvent(self, event):
        """!
        @brief Creates context menu if right mouse button clicked inside the view area.
        """

        xReal, yReal = self.getMouseCoordinates(event.pos())
        pointReal = ogr.Geometry(ogr.wkbPoint)
        pointReal.AddPoint(xReal, yReal)
        visibleLayers = self.getVisibleLayers()
        topLayer = None

        for layer in visibleLayers:
            if layer.isVector:
                topLayer = layer
            else:
                if layer.IsPointInViewRange(pointReal, self.viewBoundary):
                    topLayer = layer
                    break

        if len(self.items()) == 0:
            self.emit(SIGNAL("statusBarChanged"), QString(_fromUtf8(_("No files were loaded so far!"))))
        else:
            if topLayer is not None:
                menu = QMenu()

                zoomAction = QAction(_fromUtf8(_("Zoom to full extent")), None)
                menu.addAction(zoomAction)
                zoomAction.setIcon(QIcon(_fromUtf8(":/menuContext/resources/zoomfull.png")))
                self.connect(zoomAction, SIGNAL("triggered()"),
                             lambda: self.emit(SIGNAL("zoomFullExtent(PyQt_PyObject)"), topLayer))
                propertiesAction = QAction(_fromUtf8(_("Properties")), None)
                menu.addAction(propertiesAction)
                propertiesAction.setIcon(QIcon(_fromUtf8(":/neue/resources/rasterproperties.png")))
                self.connect(propertiesAction, SIGNAL("triggered()"),
                             lambda: self.emit(SIGNAL("properties(PyQt_PyObject)"), topLayer))
                refreshAction = QAction(_fromUtf8(_("Refresh (F5)")), None)
                menu.addAction(refreshAction)
                refreshAction.setIcon(QIcon(_fromUtf8(":/neue/Resources/refresh.png")))
                self.connect(refreshAction, SIGNAL("triggered()"), self.parent.update_ui)

                if not topLayer.isVector:
                    identifyAction = QAction(_fromUtf8(_("Identify")), None)
                    menu.addAction(identifyAction)
                    identifyAction.setIcon(QIcon(_fromUtf8(":/menuHelp/resources/info.png")))
                    self.connect(identifyAction, SIGNAL("triggered()"),
                                 lambda: self.emit(SIGNAL("identify(PyQt_PyObject)"), topLayer))

                menu.exec_(event.globalPos())
                self.viewport().update()



    def mouseMoveEvent(self, event):
        """!
        @brief Changes the coordinates in the main window if the mouse moves over the view area
        or moves layers if the middle button was pressed or proper tool was selected
        @param event: Mouse event
        """

        if self.movePressed:
            self.setCursor(Qt.ClosedHandCursor)
            vector = event.pos() - self.startPosition
            x = self.centralPaintPoint.GetX()
            y = self.centralPaintPoint.GetY()
            deltaX = -vector.x()*self.pixSize
            deltaY = vector.y()*self.pixSize
            self.centralPaintPoint.AddPoint(x + deltaX, y + deltaY)
            self.startPosition = event.pos()
            for item in self.scene.items():
                item.moveBy(vector.x(), vector.y())
        elif not self.movePressed:
            self.getMouseCoordinates(event.pos())
            self.emit(SIGNAL("mouseMoved(QEvent)"), event)

    def mouseReleaseEvent(self, event):
        """!
        @brief Finishes moving layers triggering shift
        @param event: Mouse event
        """

        if (event.button() == Qt.MiddleButton) or (event.button() == Qt.LeftButton & self.panActionActive):
            self.movePressed = False
            self.startPosition = QPoint()
            self.buildUpBoundary()
            self.emit(SIGNAL("viewAreaResized()"))
            if self.panActionActive:
                self.setCursor(Qt.OpenHandCursor)
            else:
                self.setCursor(Qt.CrossCursor)

    def getMouseCoordinates(self, qPoint):
        """!
        @brief Converts pixel coordinates into physical coordinates in the terrain.
        @param qPoint: Pair of coordinates of pixel under current position of mouse
        @return Emits signal with terrain coordinates of mouse
        """

        if self.ratio is not None:
            centralScene = QPoint(self.sceneWidth/2, self.sceneHeight/2)
            qPoint = self.mapToScene(qPoint)
            xScreen = qPoint.x()
            yScreen = qPoint.y()
            deltaX = centralScene.x() - xScreen
            deltaY = centralScene.y() - yScreen
            xReal = self.centralPaintPoint.GetX() - deltaX*self.pixSize
            yReal = self.centralPaintPoint.GetY() + deltaY*self.pixSize

            mouseCoordinates = QPointF(xReal, yReal)
            self.emit(SIGNAL("mouseCoordinates(QPointF)"), mouseCoordinates)
            return xReal, yReal

    def revertCoordinates(self, xReal, yReal):


        if self.ratio is not None:
            centralScene = QPoint(self.sceneWidth/2, self.sceneHeight/2)
            deltaX = -(xReal - self.centralPaintPoint.GetX())/self.pixSize
            deltaY = (yReal - self.centralPaintPoint.GetY())/self.pixSize
            xScreen = centralScene.x() - deltaX
            yScreen = centralScene.y() - deltaY

            return int(xScreen), int(yScreen)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton and not self.panActionActive:
            self.emit(SIGNAL("mouseClicked(QEvent)"), event)
        elif event.button() == Qt.MiddleButton or (event.button() == Qt.LeftButton & self.panActionActive):
            self.movePressed = True
            self.startPosition = QPoint(event.pos())
            self.setCursor(Qt.ClosedHandCursor)
        elif event.button() == Qt.RightButton:
            self.emit(SIGNAL("mouseRightClicked(QEvent)"), event)

    def mouseDoubleClickEvent(self, event):
        self.emit(SIGNAL("mouseDoubleClicked(QEvent)"), event)

    def panActionToggled(self, isToogled):

        if isToogled:
            self.panActionActive = True
            self.setCursor(Qt.OpenHandCursor)
        else:
            self.panActionActive = False
            self.setCursor(Qt.CrossCursor)

    def resizeEvent(self, event):
        """!
        @param event: Sets new value of view area after every resizing
        """
        self.paintSize = event.size()
        dWidth = event.oldSize().width()/event.size().width()
        dHeight = event.oldSize().height()/event.size().height()
        self.ratioWidth *= dWidth
        self.ratioHeight *= dHeight
        if self.ratioHeight < self.ratioWidth:
            self.ratio = self.ratioHeight
            self.pixSize *= dHeight
        else:
            self.ratio = self.ratioWidth
            self.pixSize *= dWidth

        deltaWidth = event.size().width() - event.oldSize().width()
        deltaHeight = event.size().height() - event.oldSize().height()
        self.sceneWidth += deltaWidth
        self.sceneHeight += deltaHeight
        self.setSceneRect(0, 0, self.sceneWidth, self.sceneHeight)
        self.buildUpBoundary()
        self.emit(SIGNAL("viewAreaResized()"))

    def properties(self):
        self.propertyDialog.show()

    def zoomIn(self, event=None, delta=120):
        """
        if event is not None:
            mousePos = self.mapFromGlobal(event.globalPos())
            self.centralPaintPoint = self.fromPixelsToCoordinates(mousePos)
        """
        # Commented - zoom in & out at central point of current view
        self.pixSize *= 1.41 ** (-delta/240)
        self.factor = self.wheelScrollZoomFactor ** (delta/240)




        self.buildUpBoundary()
        self.scale(self.factor, self.factor)
        if not self.zoomThread.isRunning():
            self.zoomThread = QThread()
            self.zoomObject = ZoomingObject()
            self.zoomObject.moveToThread(self.zoomThread)
            self.zoomThread.started.connect(self.zoomObject.process)
            self.connect(self.zoomObject, SIGNAL("areaZoomed()"), self.updateUI)
            self.zoomThread.start()

    def zoomOut(self, event=None, delta=-120):
        """
        if event is not None:
            mousePos = self.mapFromGlobal(event.globalPos())
            self.centralPaintPoint = self.fromPixelsToCoordinates(mousePos)
        """
        # Commented - zoom in & out at central point of current view
        self.pixSize *= 1.41 ** (-delta/240)
        self.factor = self.wheelScrollZoomFactor ** (delta/240)
        self.buildUpBoundary()
        self.scale(self.factor, self.factor)
        if not self.zoomThread.isRunning():
            self.zoomThread = QThread()
            self.zoomObject = ZoomingObject()
            self.zoomObject.moveToThread(self.zoomThread)
            self.zoomThread.started.connect(self.zoomObject.process)
            self.connect(self.zoomObject, SIGNAL("areaZoomed()"), self.updateUI)
            self.zoomThread.start()

    def updateUI(self):

        self.zoomThread.quit()
        self.parent.update_ui()


    def fromPixelsToCoordinates(self, qPoint):
        """!
        @brief Re-Calculates point coordinates from pixel into Geo-Coordinates
        @param qPoint: Given point as a instance of a QPoint class from PyQt
        @return Returns a point with geo-coordinates, instance of ogr.Point Class
        """
        x = qPoint.x()
        y = qPoint.y()

        centerSceneX = int(self.sceneWidth/2)
        centerSceneY = int(self.sceneHeight/2)

        deltaX = x - centerSceneX
        deltaY = y - centerSceneY


        deltaX *= self.pixSize
        deltaY *= self.pixSize

        returnX = self.centralPaintPoint.GetX() + deltaX
        returnY = self.centralPaintPoint.GetY() + deltaY

        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(returnX, returnY)

        return point

    def setUpEPSG(self, combobox, code):
        self.viewAreaSpatialReference = osr.SpatialReference()
        self.viewAreaSpatialReference.ImportFromEPSG(code)
        if not self.files[0].isVector:
            self.viewAreaAsRaster.SetProjection(self.viewAreaSpatialReference.ExportToWkt())
        item = combobox.findText(str(code), Qt.MatchContains)
        if item == -1:
            combobox.insertItem(len(combobox)-1, str(code))
            item = len(combobox)-2

        combobox.setCurrentIndex(item)
        self.projectionEPSG = code

    def chooseEPSG(self, newIndex, combobox, isTemp=False):
        """!
        @brief Choose EPSG code. User can choose EPSG code from list or enter new EPSG Code. If user EPSG Code is valid, new code is add to list.
        @param newIndex: Index of EPSG code list
        @param combobox: Given QComboBox widget as a instance of a QComboBox class from PyQt
        """
        if newIndex == len(combobox)-1:
            combobox.setEditable(True)
            combobox.clearEditText()
            combobox.setDuplicatesEnabled(False)
            combobox.setInsertPolicy(QComboBox.InsertBeforeCurrent)

            lineEdit = combobox.lineEdit()
            lineEdit.selectAll()
            regexp = QRegExp(r'\d{4,5}')
            validator = QRegExpValidator(regexp, lineEdit)
            lineEdit.setValidator(validator)
            lineEdit.textChanged.connect(self.checkComboBoxValues)
            lineEdit.textChanged.emit(lineEdit.text())
        else:
            combobox.setEditable(False)
            if not isTemp:
                self.epsgChanged(newIndex, combobox)

    def epsgChanged(self, newIndex, combobox=False, setupMain=False):

        """!
        @brief Set EPSG code to Raster
        @param newIndex: Index of chosen EPSG code
        @param combobox: Given QComboBox widget as a instance of a QComboBox class from PyQt
        """
        self.projectionFirst = False
        if setupMain:
            if newIndex <= 12:
                self.parent.coordinateSystemComboBox.setCurrentIndex(int(newIndex))

        try:
            self.projectionEPSG = self.comboIndexEpsg[newIndex]
        except KeyError:
            if combobox:
                self.comboIndexEpsg[newIndex] = int(combobox.currentText())
                self.projectionEPSG = int(combobox.currentText())
        self.projectionEPSG, self.viewAreaSpatialReference
        if self.EPSG2Name.has_key(self.projectionEPSG):
            newSpatialReference = osr.SpatialReference()
            newSpatialReference.ImportFromEPSG(self.projectionEPSG)
            oldSpatialReference = self.viewAreaSpatialReference
            self.viewAreaProj4 = newSpatialReference.ExportToProj4()
            oldArea = self.viewBoundary.Area()
            self.transformation = osr.CoordinateTransformation(oldSpatialReference, newSpatialReference)
            try:
                self.viewBoundary.Transform(self.transformation)
                newArea = self.viewBoundary.Area()

            except TypeError:
                self.viewAreaSpatialReference = newSpatialReference

            else:
                try:
                    lengthChange = (newArea/oldArea)**0.5
                except ZeroDivisionError:
                    lengthChange = 1
                self.viewAreaSpatialReference = newSpatialReference
                index = 0


                for file in reversed(self.filesAll):
                        oldGeoTransform = list(self.viewAreaAsRaster.GetGeoTransform())
                        upLeftPoint = ogr.Geometry(ogr.wkbPoint)
                        upLeftPoint.AddPoint(oldGeoTransform[0], oldGeoTransform[3])
                        upLeftPoint.Transform(self.transformation)
                        oldGeoTransform[0] = upLeftPoint.GetX()
                        oldGeoTransform[3] = upLeftPoint.GetY()
                        oldGeoTransform[1] *= lengthChange
                        oldGeoTransform[5] *= lengthChange
                        self.viewAreaAsRaster.SetGeoTransform(oldGeoTransform)
                        self.viewAreaAsRaster.SetProjection(newSpatialReference.ExportToWkt())
                        self.setupViewAreaParameters(self.viewAreaAsRaster)

                if combobox:
                    self.parent.update_ui()

        else:
                QMessageBox.critical(self, _fromUtf8(_("Coordinates System Error"),
                                     _("Code %s not found in EPSG support files.\n"
                                     "Enter valid EPSG code.")) % combobox.currentText(),
                                     QMessageBox.Ok)
                combobox.removeItem(newIndex)
                self.comboIndexEpsg.__delitem__(newIndex)
                self.chooseEPSG(newIndex, combobox)

    def checkComboBoxValues(self, *args, **kwargs):
        """!
        @brief Simply styling line with editing EPSG code.
        When background is yellow it means that more digits are required,
        When background is green the number of digits is satisfactory
        Red background never appears because right format of characters is imposed
        by Regular Expression Validator
        """
        sender = self.sender()
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]

        if state == QValidator.Acceptable:
            color = '#c4df9b' # green
        elif state == QValidator.Intermediate:
            color = '#fff79a' # yellow
        elif state == QValidator.Invalid:
            color = '#f6989d' # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def zoomTyped(self, textTyped, combobox):

        combobox.setEditable(True)
        lineEdit = combobox.lineEdit()
        regexp = QRegExp(r'\d*\s*:?\s*\d*\s*\d*\s*\d*')
        validator = QRegExpValidator(regexp)
        lineEdit.setValidator(validator)
        state = validator.validate(textTyped, 0)[0]
        if state == QValidator.Acceptable:
            color = '#c4df9b' # green

        elif state == QValidator.Intermediate:
            color = '#fff79a' # yellow

        else:
            color = '#f6989d' # red
        lineEdit.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def fullExtent(self, file):
        """!
        @brief Zooms to full extent of selecter @var file
        @param raster: Full extent zoom to that file
        """
        self.setupViewAreaParameters(file, fullExtent=True)
        self.parent.update_ui()

    def changeZoom(self, lineEdit=None, expression=None):
        """!
        @brief Does everything connected with change zoom
        @param lineEdit: Parameter send if rescale was caused by typing value on keyboard
        @param expression: Parameter send if rescale was caused by selecting value from QComboBox
        """
        if lineEdit:
            scale = lineEdit.text()
        elif expression:
            scale = expression

        try:
            if re.match(r'^\d*\s*:?\s*\d*\s*\d*\s*\d*$', scale):
                pattern = re.compile(r'\d*\s*:?\s*\d*\s*\d*\s*\d*')
                result = pattern.search(str(scale))
                scale = result.string[result.regs[0][0]:result.regs[0][1]]
                scale = scale.split(r":")[-1]
                scale = scale.replace(r" ", "")
                scale = int(scale)
                self.setZoomComboBoxValue(scale)
                onePixeSizeMM = float(self.parent.desktopWidget.widthMM())/float(self.parent.desktopWidget.width())
                onePixeSizeM = onePixeSizeMM/1000
                self.pixSize = onePixeSizeM * scale
                self.setZoomComboBoxValue(self.scaleNominator)
                self.buildUpBoundary()
                self.parent.update_ui()
                self.parent.zoomComboBox.lineEdit().setStyleSheet('QLineEdit { background-color: white }')
            else:
                raise exceptionModel.ScaleError()
        except exceptionModel.ScaleError as e:
            QMessageBox.critical(self, e.title, "%s"%e.msg,QMessageBox.Ok)

    def setZoomComboBoxValue(self, scaleNominator):

        numberOfItems = self.parent.zoomComboBox.count()
        lastItem = numberOfItems - 1
        self.parent.zoomComboBox.removeItem(lastItem)
        scaleText = r"1:" + "{:,}".format(int(scaleNominator)).replace(",", " ")
        self.parent.zoomComboBox.addItem(QString(scaleText))
        self.parent.zoomComboBox.setCurrentIndex(lastItem)
        self.parent.zoomComboBox.removeItem(15)
        self.viewBoundary.AssignSpatialReference(self.viewAreaSpatialReference)

    def setNoStyleZoomComboBox(self, index):

        self.parent.zoomComboBox.lineEdit().setStyleSheet('QLineEdit { background-color: white }')

    def projectionFromFirst(self):

        if not self.parent.loadedfiles_model.files:
            self.projectionFirst = True
            self.viewAreaProj4 = None
            self.viewAreaAsRaster = None
            self.parent.coordinateSystemComboBox.setCurrentIndex(0)


    def transformCoordinates(self, oldProj4, xCoord, yCooord):

        newSpatialReference = osr.SpatialReference()
        newSpatialReference.ImportFromProj4(self.viewAreaProj4)

        oldSpatialReference = osr.SpatialReference()
        oldSpatialReference.ImportFromProj4(oldProj4)
        coordTransform = osr.CoordinateTransformation(newSpatialReference, oldSpatialReference)
        point = coordTransform.TransformPoint(xCoord, yCooord)


        return (int(point[0]), int(point[1]))


class GetDataImagesThread(QObject):

    def __init__(self, files, params, parent=None):
        super(GetDataImagesThread, self).__init__(parent)
        self.files = files
        self.params = params
        self.threadRef = None

    def processData(self):
        imagesData = []
        nameData = []
        sizeData = []
        posData = []

        filesInTop = []

        for i, file in enumerate(self.files):
            if not file.isVector:
                imageData = file.getQimageData(self.params[0], self.params[1], self.params[2],
                                           self.params[3], self.params[4], self.params[5])
                imagesData.append(imageData)
            else:
                if i != 0:
                    for j, f in enumerate(self.files):
                        if j < i:
                            filesInTop.append(f)


                (fileName, size, pos) = file.getQPixmap(self.params[1], filesInTop, self.params[3], self.params[4], self.params[2])
                nameData.append(fileName)
                sizeData.append(size)
                posData.append(pos)


        self.emit(SIGNAL("conversionFinished(PyQt_PyObject)"), [imagesData, nameData, sizeData, posData])



class ZoomingThread(QThread):

    def run(self):

        self.startTime = time.time()
        while time.time() - self.startTime < 0.2:
            time.sleep(0.01)

        self.emit(SIGNAL("areaZoomed()"))
        return

class ZoomingObject(QObject):

    def __init__(self, parent=None):
        super(ZoomingObject, self).__init__(parent)

    def process(self):
        self.statTime = time.time()
        while time.time() - self.statTime < 0.2:
            time.sleep(0.05)

        self.emit(SIGNAL("areaZoomed()"))

#
# class DefaultRasterStretch(object):
#
#     def __init__(self):
#
#         self.stretchDict = dict()
#         self.stretchDict = {u'None':0, u'Percentage Clip':1, u'Standard Deviation':2}
#         self.stretchDictRev = {0:u'None', 1:u'Percentage Clip', 2:u'Standard Deviation'}
#
#         self.typeOfStretch = u'Percentage Clip'
#         self.stretchParameters = {0:None, 1:[2, 2], 2:[2.5, 0]}
#
#         self.StretchType = namedtuple('StretchType', ['text', 'index', 'name', 'param'])
#         self.none = self.StretchType(_fromUtf8(_('None')), 0, 'None', None)
#         self.percentClip = self.StretchType(_fromUtf8(_('Percentage Clip')), 1, 'Percentage Clip', [2, 2])
#         self.standardDev = self.StretchType(_fromUtf8(_('Standard Deviation')), 2, 'Standard Deviation', [2.5, 0])
#         self.stretchType = self.percentClip


