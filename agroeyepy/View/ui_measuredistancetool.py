# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_measuredistancetool.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_measureDistanceDialog(object):
    def setupUi(self, measureDistanceDialog):
        measureDistanceDialog.setObjectName(_fromUtf8("measureDistanceDialog"))
        measureDistanceDialog.resize(353, 325)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        measureDistanceDialog.setWindowIcon(icon)
        measureDistanceDialog.setStyleSheet(_fromUtf8("/*\n"
"* GLOBAL font and background SETTINGS.\n"
"*/\n"
"\n"
"* {\n"
"    font-size: 11px;\n"
"    font-family: Tahoma, Verdana, Arial, sans-serif;\n"
"    background-color: white;\n"
"}\n"
"\n"
"/* \n"
"* COMBOBOXES - sets border shape, dimensions and colours.\n"
"*/\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
" \n"
"    min-width: 6em;\n"
"    background-color: #ffffff;\n"
"    color:black;\n"
"    \n"
"     border-color: silver;\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    padding: 1px 0px 1px 3px; /*This makes text colour work*/\n"
"}\n"
"\n"
"QComboBox:editable {\n"
"    /*This changes colour behind expand icon*/\n"
"    background-color: #f2f2f2; \n"
"}\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"    background: #ffffff;\n"
"}\n"
"\n"
"QComboBox::drop-down { \n"
"    /*This sets up style of combobox list */\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; \n"
"    border-top-right-radius: 2px; \n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QComboBox::down-arrow {\n"
"    /* Custom expand icon source and dimensions, width forces proportional height resize*/\n"
"    image: url(:/menuEditShape/resources/moveDown.png); \n"
"    width: 12px;\n"
"}\n"
"\n"
"QComboBox::down-arrow:on { \n"
"    /* This moves activated arrow by 1px */\n"
"    top: 1px;\n"
"    left: 1px;\n"
"}\n"
"\n"
"/* TABLE OF CONTENTS */\n"
"\n"
"QDockWidget {\n"
"    border:none;\n"
"}\n"
"\n"
"QDockWidget::title {\n"
"    text-align: left;\n"
"    border: 1px solid silver;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"    padding-left: 35px;\n"
"}\n"
"\n"
"/* GROUPBOXES */\n"
"\n"
"QGroupBox {\n"
"    background-color: white;\n"
"    border: 1px solid gray;\n"
"    border-radius: 2px;\n"
"    margin-top: 1px; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"   \n"
"}\n"
"\n"
"/* HEADERS */\n"
"\n"
"QHeaderView::section {\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                                  stop:0 #616161, stop: 0.5 #505050,\n"
"                                  stop: 0.6 #434343, stop:1 #656565);\n"
"    color: black;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QHeaderView::section:checked\n"
"{\n"
"    background-color: red;\n"
"}\n"
"\n"
"QHeaderView::down-arrow {\n"
"    image: url(down_arrow.png);\n"
"}\n"
"\n"
"QHeaderView::up-arrow {\n"
"    image: url(up_arrow.png);\n"
"}\n"
"\n"
"/* MENU */\n"
"\n"
"QMenuBar {\n"
"    /* These sets background colour of menus */\n"
"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                      stop:0 silver, stop:1 lightgray); \n"
"}\n"
"\n"
"QMenu {\n"
"    background-color: darkgray;\n"
"}\n"
"\n"
"QMenuBar::item {\n"
"    spacing: 3px; /* spacing between menu bar items */\n"
"    padding: 1px 4px;\n"
"    border-radius: 2px;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:selected { /* when selected using mouse or keyboard */\n"
"    background: darkgray;\n"
"    color: black;\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"    background: #888888;\n"
"    color: black;\n"
"}\n"
"\n"
"\n"
"/* PUSHBUTTONS */\n"
"\n"
"QPushButton {\n"
"    border: 1px solid darkgray;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    min-height: 24px;\n"
"    max-height: 24px;\n"
"    min-width: 100px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:flat {\n"
"    border: none; /* no border for a flat push button */\n"
"}\n"
"\n"
"QPushButton:default {\n"
"    border-color: navy; /* make the default button prominent */\n"
"}\n"
"\n"
"/* RADIOBUTTONS */\n"
"\n"
"QRadioButton::indicator {\n"
"    width: 13px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"/* SCROLLBAR\n"
"* I can\'t see you, but i know you\'re there.\n"
"*/\n"
"\n"
"QScrollBar{\n"
"    width: 1px;\n"
"}\n"
"\n"
"\n"
"/* TABWIDGET */\n"
"\n"
"\n"
"QTabWidget::pane {\n"
"    /* Border settings for TabWidget */\n"
"    border-top: 2px solid #C2C7CB;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    /* This moves tab bar left by 5px */\n"
"    left: 5px; \n"
"}\n"
"\n"
"/* Styles single tab using tab sub-control, uses QTabBar instead of QTabWidget */\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 1px solid lightgrey; \n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"    min-width: 120px;\n"
"    padding: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"        background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                    stop: 0 #fafafa, stop: 0.4 #f4f4f4,\n"
"                    stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    border-color: darkgray;\n"
"    border-bottom-color: lightgray; \n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    /* Unselected tab seems to be smaller */\n"
"    margin-top: 2px; \n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    /* Push selected tabs 4px in both directions */\n"
"    margin-left: -4px;\n"
"    margin-right: -4px;\n"
"}\n"
"\n"
"QTabBar::tab:first:selected {\n"
"    /* But do not push the first one left since it would fall out of the window :( */\n"
"    margin-left: 0; \n"
"}\n"
"\n"
"QTabBar::tab:last:selected {\n"
"    /* Same for the last one just to make it feel safe */\n"
"    margin-right: 0;\n"
"}\n"
"\n"
"QTabBar::tab:only-one {\n"
"    /* If there\'s only one tab, it doesn\'t have to do anything */\n"
"    margin: 0; \n"
"}\n"
"/* TABLES */\n"
"QTableView {\n"
"    selection-background-color: qlineargradient(x1: 0, y1: 0, x2: 0.5, y2: 0.5,\n"
"                                                 stop: 0 silver, stop: 1 lightgray);\n"
"}\n"
"\n"
"QTableView QTableCornerButton::section {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 darkgray);\n"
"    border: 1px outset darkgray;\n"
"}\n"
"\n"
"\n"
"/* TOOLBAR WITH BASIC ICONS */\n"
"\n"
"QToolBar {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                         stop:0 lightgray, stop:1 silver);\n"
"    border: 0;\n"
"}\n"
"\n"
"QToolButton { \n"
"    /* All types of tool button */\n"
"    border: none; \n"
"       border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"}\n"
"\n"
"QToolButton[popupMode=\"1\"] { \n"
"    /* Only for MenuButtonPopup \n"
"    Make way for the popup button */\n"
"    padding-right: 20px; \n"
"}\n"
"\n"
"QToolButton:selected {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QToolButton:on {\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"    width: 28px;\n"
"    height: 28px;\n"
"}\n"
"\n"
"/* the subcontrols below are used only in the MenuButtonPopup mode */\n"
"QToolButton::menu-button {\n"
"    border: 1px solid lightgray;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    /*16px width + 4px for border = 20px allocated above */\n"
"    width: 16px;\n"
"}\n"
"\n"
"QToolButton::menu-arrow {\n"
"    image: url(resources/moveDown.png);\n"
"}\n"
"\n"
"QToolButton::menu-arrow:open {\n"
"   top: 1px; left: 1px; /* Shift it a bit */\n"
"}\n"
"\n"
"/* TOOLBOX */\n"
"\n"
"QToolBox::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                         stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                         stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border-radius: 2px;\n"
"    color: lightgray;\n"
"}\n"
"\n"
"/* TOOLTIP MESSAGE STYLE */\n"
"\n"
"QToolTip {\n"
"    padding: 5px;\n"
"}\n"
"\n"
"/* FRAME SETTINGS */\n"
"\n"
"QFrame{\n"
"    border: none;\n"
"    background-color: qlineargradient(x1: 1, y1: 0, x2: 0, y2: 0,\n"
"                      stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
"}\n"
"\n"
"/* LABEL SETTINGS */\n"
"\n"
"QLabel {\n"
"    border:none;\n"
"} \n"
"\n"
"/* SLIDER (PARAMETERS), USED MOSTLY FOR OPACITY */\n"
"QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 2px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 18px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"    position: absolute; /* absolutely position 4px from the left and right of the widget. setting margins on the widget should work too... */\n"
"    left: 4px; right: 4px;\n"
"}\n"
"\n"
"QSlider::handle:vertical {\n"
"    height: 10px;\n"
"    background: green;\n"
"    margin: 0 -4px; /* expand outside the groove */\n"
"}\n"
"\n"
"QSlider::add-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
"\n"
"QSlider::sub-page:vertical {\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 lightgray, stop:1 darkgray);\n"
"}\n"
" \n"
"  "))
        measureDistanceDialog.setSizeGripEnabled(False)
        self.gridLayout_2 = QtGui.QGridLayout(measureDistanceDialog)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.measureDistanceDialogGridLayout = QtGui.QGridLayout()
        self.measureDistanceDialogGridLayout.setVerticalSpacing(10)
        self.measureDistanceDialogGridLayout.setObjectName(_fromUtf8("measureDistanceDialogGridLayout"))
        self.groupBox = QtGui.QGroupBox(measureDistanceDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setAccessibleName(_fromUtf8(""))
        self.groupBox.setStyleSheet(_fromUtf8("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 5px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}"))
        self.groupBox.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox)
        self.gridLayout_3.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.gridLayout_3.setContentsMargins(5, 15, 5, 5)
        self.gridLayout_3.setHorizontalSpacing(5)
        self.gridLayout_3.setVerticalSpacing(7)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.groupBox_3 = QtGui.QGroupBox(self.groupBox)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
        self.groupBox_3.setSizePolicy(sizePolicy)
        self.groupBox_3.setMinimumSize(QtCore.QSize(0, 50))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Tahoma,Verdana,Arial,sans-serif"))
        font.setPointSize(-1)
        self.groupBox_3.setFont(font)
        self.groupBox_3.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.groupBox_3.setAccessibleDescription(_fromUtf8(""))
        self.groupBox_3.setAutoFillBackground(False)
        self.groupBox_3.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.groupBox_3.setCheckable(False)
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout(self.groupBox_3)
        self.horizontalLayout_5.setContentsMargins(-1, 15, -1, -1)
        self.horizontalLayout_5.setSpacing(2)
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.trackByMouseButton = QtGui.QRadioButton(self.groupBox_3)
        self.trackByMouseButton.setMinimumSize(QtCore.QSize(0, 22))
        self.trackByMouseButton.setChecked(True)
        self.trackByMouseButton.setObjectName(_fromUtf8("trackByMouseButton"))
        self.horizontalLayout_5.addWidget(self.trackByMouseButton)
        self.trackByPointsButton = QtGui.QRadioButton(self.groupBox_3)
        self.trackByPointsButton.setMinimumSize(QtCore.QSize(0, 22))
        self.trackByPointsButton.setObjectName(_fromUtf8("trackByPointsButton"))
        self.horizontalLayout_5.addWidget(self.trackByPointsButton)
        self.gridLayout_3.addWidget(self.groupBox_3, 0, 0, 1, 1)
        self.groupBox_2 = QtGui.QGroupBox(self.groupBox)
        self.groupBox_2.setMinimumSize(QtCore.QSize(0, 22))
        self.groupBox_2.setFlat(False)
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_2.setMargin(3)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.doubleClickRadioButton = QtGui.QRadioButton(self.groupBox_2)
        self.doubleClickRadioButton.setMinimumSize(QtCore.QSize(0, 22))
        self.doubleClickRadioButton.setObjectName(_fromUtf8("doubleClickRadioButton"))
        self.horizontalLayout_2.addWidget(self.doubleClickRadioButton)
        self.singleClickRadioButton = QtGui.QRadioButton(self.groupBox_2)
        self.singleClickRadioButton.setMinimumSize(QtCore.QSize(0, 22))
        self.singleClickRadioButton.setChecked(True)
        self.singleClickRadioButton.setObjectName(_fromUtf8("singleClickRadioButton"))
        self.horizontalLayout_2.addWidget(self.singleClickRadioButton)
        self.gridLayout_3.addWidget(self.groupBox_2, 1, 0, 1, 1)
        self.measureDistanceDialogGridLayout.addWidget(self.groupBox, 2, 0, 1, 2)
        self.label_2 = QtGui.QLabel(measureDistanceDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setMinimumSize(QtCore.QSize(0, 22))
        self.label_2.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.measureDistanceDialogGridLayout.addWidget(self.label_2, 0, 1, 1, 1)
        self.unitsComboBox = QtGui.QComboBox(measureDistanceDialog)
        self.unitsComboBox.setMinimumSize(QtCore.QSize(83, 22))
        self.unitsComboBox.setEditable(False)
        self.unitsComboBox.setObjectName(_fromUtf8("unitsComboBox"))
        self.unitsComboBox.addItem(_fromUtf8(""))
        self.unitsComboBox.setItemText(0, _fromUtf8("mm"))
        self.unitsComboBox.addItem(_fromUtf8(""))
        self.unitsComboBox.setItemText(1, _fromUtf8("cm"))
        self.unitsComboBox.addItem(_fromUtf8(""))
        self.unitsComboBox.setItemText(2, _fromUtf8("dm"))
        self.unitsComboBox.addItem(_fromUtf8(""))
        self.unitsComboBox.setItemText(3, _fromUtf8("m"))
        self.unitsComboBox.addItem(_fromUtf8(""))
        self.unitsComboBox.setItemText(4, _fromUtf8("km"))
        self.measureDistanceDialogGridLayout.addWidget(self.unitsComboBox, 1, 1, 1, 1)
        self.calculatedDistanceField = QtGui.QLineEdit(measureDistanceDialog)
        self.calculatedDistanceField.setMinimumSize(QtCore.QSize(0, 22))
        self.calculatedDistanceField.setFrame(True)
        self.calculatedDistanceField.setEchoMode(QtGui.QLineEdit.Normal)
        self.calculatedDistanceField.setReadOnly(True)
        self.calculatedDistanceField.setObjectName(_fromUtf8("calculatedDistanceField"))
        self.measureDistanceDialogGridLayout.addWidget(self.calculatedDistanceField, 1, 0, 1, 1)
        self.label = QtGui.QLabel(measureDistanceDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QtCore.QSize(0, 22))
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.label.setObjectName(_fromUtf8("label"))
        self.measureDistanceDialogGridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.measureDistanceDialogGridLayout, 1, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.closeButton = QtGui.QPushButton(measureDistanceDialog)
        self.closeButton.setMinimumSize(QtCore.QSize(102, 26))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFile/resources/cancel.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.closeButton.setIcon(icon1)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout.addWidget(self.closeButton)
        self.clearButton = QtGui.QPushButton(measureDistanceDialog)
        self.clearButton.setMinimumSize(QtCore.QSize(102, 26))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuTools/resources/rubbish.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.clearButton.setIcon(icon2)
        self.clearButton.setFlat(False)
        self.clearButton.setObjectName(_fromUtf8("clearButton"))
        self.horizontalLayout.addWidget(self.clearButton)
        self.gridLayout_2.addLayout(self.horizontalLayout, 2, 0, 1, 1)

        self.retranslateUi(measureDistanceDialog)
        self.unitsComboBox.setCurrentIndex(0)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), measureDistanceDialog.close)
        QtCore.QMetaObject.connectSlotsByName(measureDistanceDialog)

    def retranslateUi(self, measureDistanceDialog):
        measureDistanceDialog.setWindowTitle(_translate("measureDistanceDialog", "Measure distance", None))
        self.groupBox.setTitle(_translate("measureDistanceDialog", "Options", None))
        self.groupBox_3.setTitle(_translate("measureDistanceDialog", "Measuring type", None))
        self.trackByMouseButton.setText(_translate("measureDistanceDialog", "Tracking mouse", None))
        self.trackByPointsButton.setText(_translate("measureDistanceDialog", "By point", None))
        self.groupBox_2.setTitle(_translate("measureDistanceDialog", "Add new point by", None))
        self.doubleClickRadioButton.setText(_translate("measureDistanceDialog", "Double click", None))
        self.singleClickRadioButton.setText(_translate("measureDistanceDialog", "Single click", None))
        self.label_2.setText(_translate("measureDistanceDialog", "Units:", None))
        self.label.setText(_translate("measureDistanceDialog", "Distance:", None))
        self.closeButton.setText(_translate("measureDistanceDialog", "Close", None))
        self.clearButton.setText(_translate("measureDistanceDialog", "Clear", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    measureDistanceDialog = QtGui.QDialog()
    ui = Ui_measureDistanceDialog()
    ui.setupUi(measureDistanceDialog)
    measureDistanceDialog.show()
    sys.exit(app.exec_())

