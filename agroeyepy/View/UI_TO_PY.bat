@echo off

call pyuic4 -x MainWindow.ui -o MainWindow.py
call pyuic4 -x ui_addcolumndialog.ui -o ui_addcolumndialog.py
call pyuic4 -x ui_addlayersdialog.ui -o ui_addlayersdialog.py
call pyuic4 -x ui_addnewobjecttolayer.ui -o ui_addnewobjecttolayer.py
call pyuic4 -x ui_addnewvectorlayerdialog.ui -o ui_addnewvectorlayerdialog.py
call pyuic4 -x ui_attributetabledialog.ui -o ui_attributetabledialog.py
call pyuic4 -x ui_compositiondialog.ui -o ui_compositiondialog.py
call pyuic4 -x ui_delcolumndialog.ui -o ui_delcolumndialog.py
call pyuic4 -x ui_editstylelinedialog.ui -o ui_editstylelinedialog.py
call pyuic4 -x ui_editstylepointdialog.ui -o ui_editstylepointdialog.py
call pyuic4 -x ui_editstylepolygondialog.ui -o ui_editstylepolygondialog.py
call pyuic4 -x ui_histogramdialog.ui -o ui_histogramdialog.py
call pyuic4 -x ui_identifydialog.ui  -o ui_identifydialog.py
call pyuic4 -x ui_measuredistancetool.ui -o ui_measuredistancetool.py
call pyuic4 -x ui_modeldialog.ui -o ui_modeldialog.py
call pyuic4 -x ui_modelinputrasterdialog.ui -o ui_modelinputrasterdialog.py
call pyuic4 -x ui_modeloutputdbdialog.ui -o ui_modeloutputdbdialog.py
call pyuic4 -x ui_modelsegmentationdialog.ui  -o ui_modelsegmentationdialog.py
call pyuic4 -x ui_modelstatisticsdialog.ui -o ui_modelstatisticsdialog.py
call pyuic4 -x ui_modelsyntheticdialog.ui  -o ui_modelsyntheticdialog.py
call pyuic4 -x ui_preferencesdialog.ui -o ui_preferencesdialog.py
call pyuic4 -x ui_progressdialog.ui -o ui_progressdialog.py
call pyuic4 -x ui_propertiesdialog.ui -o ui_propertiesdialog.py
call pyuic4 -x ui_rastercalculatordialog.ui  -o ui_rastercalculatordialog.py
call pyuic4 -x ui_renamecolumndialog.ui -o ui_renamecolumndialog.py
call pyuic4 -x ui_segmentationtabledialog.ui -o ui_segmentationtabledialog.py
taskkill

REM pause