# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_preferencesdialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_preferencesDialog(object):
    def setupUi(self, preferencesDialog):
        preferencesDialog.setObjectName(_fromUtf8("preferencesDialog"))
        preferencesDialog.resize(522, 393)
        preferencesDialog.setMaximumSize(QtCore.QSize(522, 16777215))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        preferencesDialog.setWindowIcon(icon)
        preferencesDialog.setStyleSheet(_fromUtf8("* {\n"
"font-family: Arial;\n"
"font-size: 9;\n"
"}\n"
"\n"
"QPushButton {\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 4px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    min-width: 20px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"}\n"
"\n"
"QPushButton:flat {\n"
"    border: none; /* no border for a flat push button */\n"
"}\n"
"\n"
"QPushButton:default {\n"
"    border-color: navy; /* make the default button prominent */\n"
"}\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 1px;\n"
"    padding: 1px 18px 1px 1px;\n"
"    min-width: 10em;\n"
"}\n"
"\n"
"QComboBox:editable {\n"
"    background: white;\n"
"}\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                 stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                 stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"}\n"
"\n"
"/* QComboBox gets the \"on\" state when the popup is open */\n"
"QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
"                                stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
"}\n"
"\n"
"QComboBox:on { /* shift the text when the popup opens */\n"
"    padding-top: 3px;\n"
"    padding-left: 2px;\n"
"}\n"
"\n"
"QComboBox::drop-down {\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 20px;\n"
"    \n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; /* just a single line */\n"
"    border-top-right-radius: 2px; /* same radius as the QComboBox */\n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QToolButton { /* all types of tool button */\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 4px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"}\n"
"\n"
"QToolButton[popupMode=\"1\"] { /* only for MenuButtonPopup */\n"
"    padding-right: 20px; /* make way for the popup button */\n"
"}\n"
"\n"
"QToolButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"/* the subcontrols below are used only in the MenuButtonPopup mode */\n"
"QToolButton::menu-button {\n"
"    border: 1px solid gray;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    /* 16px width + 4px for border = 20px allocated above */\n"
"    width: 16px;\n"
"}\n"
"\n"
"QToolButton::menu-arrow:open {\n"
"    top: 1px; left: 1px; /* shift it a bit */\n"
"}\n"
"\n"
"QTabWidget::pane { /* The tab widget frame */\n"
"    border-top: 2px solid #C2C7CB;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    left: 5px; /* move to the left by 5px */\n"
"    right: 5px; /* move to the right by 5px */\n"
"}\n"
"\n"
"/* Style the tab using the tab sub-control. Note that\n"
"    it reads QTabBar _not_ QTabWidget */\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 1px solid #C4C4C3;\n"
"    border-bottom-color: #C2C7CB; /* same as the pane color */\n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"    min-width:30ex;\n"
"    padding: 5px;\n"
"}\n"
"\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #fafafa, stop: 0.4 #f4f4f4,\n"
"                                stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    border-color: #9B9B9B;\n"
"    border-bottom-color: #C2C7CB; /* same as pane color */\n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    margin-top: 1px; /* make non-selected tabs look smaller */\n"
"}\n"
"\n"
"QSpinBox, QDoubleSpinBox {\n"
"    border-width: 2px;\n"
"    border: 1px solid #8f8f91;\n"
"    border-radius: 2px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #f6f7fa, stop: 1 #dadbde);\n"
"    padding-top: 1.5px;\n"
"    padding-bottom: 1.5px;\n"
"}\n"
"\n"
"QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #c1c1c1, stop:1 #f6f5f3);\n"
"    margin: 2px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #f6f4fc, stop:1 #eaebeb);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 18px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}\n"
"\n"
"QGroupBox {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #EEEEEE, stop: 1 #FFFFFF);\n"
"    border: 1px solid gray;\n"
"    border-radius: 5px;\n"
"    margin-top: 1ex; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"    padding: 0 3px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #FFOECE, stop: 1 #FFFFFF);\n"
"}"))
        self.verticalLayout = QtGui.QVBoxLayout(preferencesDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.tabWidget = QtGui.QTabWidget(preferencesDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setMinimumSize(QtCore.QSize(471, 0))
        self.tabWidget.setIconSize(QtCore.QSize(16, 16))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.ViewAreaTab = QtGui.QWidget()
        self.ViewAreaTab.setObjectName(_fromUtf8("ViewAreaTab"))
        self.gridLayout_2 = QtGui.QGridLayout(self.ViewAreaTab)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.viewAreGridLayout = QtGui.QGridLayout()
        self.viewAreGridLayout.setObjectName(_fromUtf8("viewAreGridLayout"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.viewAreGridLayout.addItem(spacerItem, 14, 1, 1, 1)
        self.defaultProjectionLabel = QtGui.QLabel(self.ViewAreaTab)
        self.defaultProjectionLabel.setMinimumSize(QtCore.QSize(0, 22))
        self.defaultProjectionLabel.setObjectName(_fromUtf8("defaultProjectionLabel"))
        self.viewAreGridLayout.addWidget(self.defaultProjectionLabel, 0, 0, 1, 1)
        self.asDeclaredHere = QtGui.QRadioButton(self.ViewAreaTab)
        self.asDeclaredHere.setObjectName(_fromUtf8("asDeclaredHere"))
        self.viewAreGridLayout.addWidget(self.asDeclaredHere, 10, 0, 1, 1)
        self.asFirstLayerRadio = QtGui.QRadioButton(self.ViewAreaTab)
        self.asFirstLayerRadio.setObjectName(_fromUtf8("asFirstLayerRadio"))
        self.viewAreGridLayout.addWidget(self.asFirstLayerRadio, 9, 0, 1, 1)
        self.defaultProjectionCombobox = QtGui.QComboBox(self.ViewAreaTab)
        self.defaultProjectionCombobox.setMinimumSize(QtCore.QSize(161, 22))
        self.defaultProjectionCombobox.setStyleSheet(_fromUtf8(""))
        self.defaultProjectionCombobox.setObjectName(_fromUtf8("defaultProjectionCombobox"))
        self.viewAreGridLayout.addWidget(self.defaultProjectionCombobox, 11, 0, 1, 1)
        self.gridLayout_2.addLayout(self.viewAreGridLayout, 0, 0, 1, 1)
        self.tabWidget.addTab(self.ViewAreaTab, _fromUtf8(""))
        self.LoadedRastersTab = QtGui.QWidget()
        self.LoadedRastersTab.setObjectName(_fromUtf8("LoadedRastersTab"))
        self.gridLayout_6 = QtGui.QGridLayout(self.LoadedRastersTab)
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.rasterGridLayout = QtGui.QGridLayout()
        self.rasterGridLayout.setObjectName(_fromUtf8("rasterGridLayout"))
        self.stretchParametersLayout = QtGui.QGridLayout()
        self.stretchParametersLayout.setContentsMargins(-1, -1, -1, 0)
        self.stretchParametersLayout.setHorizontalSpacing(0)
        self.stretchParametersLayout.setVerticalSpacing(2)
        self.stretchParametersLayout.setObjectName(_fromUtf8("stretchParametersLayout"))
        spacerItem1 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.stretchParametersLayout.addItem(spacerItem1, 4, 0, 1, 1)
        self.stretchComboBox = QtGui.QComboBox(self.LoadedRastersTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.stretchComboBox.sizePolicy().hasHeightForWidth())
        self.stretchComboBox.setSizePolicy(sizePolicy)
        self.stretchComboBox.setMinimumSize(QtCore.QSize(161, 22))
        self.stretchComboBox.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.stretchComboBox.setAcceptDrops(False)
        self.stretchComboBox.setEditable(True)
        self.stretchComboBox.setFrame(True)
        self.stretchComboBox.setObjectName(_fromUtf8("stretchComboBox"))
        self.stretchComboBox.addItem(_fromUtf8(""))
        self.stretchComboBox.addItem(_fromUtf8(""))
        self.stretchComboBox.addItem(_fromUtf8(""))
        self.stretchParametersLayout.addWidget(self.stretchComboBox, 2, 3, 1, 1)
        self.stretchLabel = QtGui.QLabel(self.LoadedRastersTab)
        self.stretchLabel.setMinimumSize(QtCore.QSize(0, 30))
        self.stretchLabel.setMaximumSize(QtCore.QSize(180, 16777215))
        self.stretchLabel.setObjectName(_fromUtf8("stretchLabel"))
        self.stretchParametersLayout.addWidget(self.stretchLabel, 2, 2, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.stretchParametersLayout.addItem(spacerItem2, 4, 4, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.stretchParametersLayout.addItem(spacerItem3, 5, 2, 1, 1)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.stretchParametersLayout.addItem(spacerItem4, 5, 3, 1, 1)
        self.rasterGridLayout.addLayout(self.stretchParametersLayout, 0, 1, 1, 1)
        self.gridLayout_6.addLayout(self.rasterGridLayout, 0, 0, 1, 1)
        self.tabWidget.addTab(self.LoadedRastersTab, _fromUtf8(""))
        self.LoadedVectorsTab = QtGui.QWidget()
        self.LoadedVectorsTab.setMinimumSize(QtCore.QSize(0, 0))
        self.LoadedVectorsTab.setObjectName(_fromUtf8("LoadedVectorsTab"))
        self.gridLayout_4 = QtGui.QGridLayout(self.LoadedVectorsTab)
        self.gridLayout_4.setSpacing(11)
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.gridLayout_3 = QtGui.QGridLayout()
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.symbolComboBox = QtGui.QComboBox(self.LoadedVectorsTab)
        self.symbolComboBox.setObjectName(_fromUtf8("symbolComboBox"))
        self.gridLayout_3.addWidget(self.symbolComboBox, 11, 3, 1, 1)
        self.symbolLabel = QtGui.QLabel(self.LoadedVectorsTab)
        self.symbolLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.symbolLabel.setObjectName(_fromUtf8("symbolLabel"))
        self.gridLayout_3.addWidget(self.symbolLabel, 11, 1, 1, 1)
        spacerItem5 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem5, 14, 1, 1, 1)
        self.opacitySlider = QtGui.QSlider(self.LoadedVectorsTab)
        self.opacitySlider.setOrientation(QtCore.Qt.Horizontal)
        self.opacitySlider.setObjectName(_fromUtf8("opacitySlider"))
        self.gridLayout_3.addWidget(self.opacitySlider, 0, 5, 1, 1)
        self.sizeLabel = QtGui.QLabel(self.LoadedVectorsTab)
        self.sizeLabel.setMinimumSize(QtCore.QSize(21, 21))
        self.sizeLabel.setMaximumSize(QtCore.QSize(16777215, 21))
        self.sizeLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.sizeLabel.setObjectName(_fromUtf8("sizeLabel"))
        self.gridLayout_3.addWidget(self.sizeLabel, 1, 1, 1, 1)
        self.fillLabel = QtGui.QLabel(self.LoadedVectorsTab)
        self.fillLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.fillLabel.setObjectName(_fromUtf8("fillLabel"))
        self.gridLayout_3.addWidget(self.fillLabel, 13, 1, 1, 1)
        self.lineLabel = QtGui.QLabel(self.LoadedVectorsTab)
        self.lineLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lineLabel.setObjectName(_fromUtf8("lineLabel"))
        self.gridLayout_3.addWidget(self.lineLabel, 12, 1, 1, 1)
        self.fillComboBox = QtGui.QComboBox(self.LoadedVectorsTab)
        self.fillComboBox.setObjectName(_fromUtf8("fillComboBox"))
        self.gridLayout_3.addWidget(self.fillComboBox, 13, 3, 1, 1)
        spacerItem6 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem6, 14, 5, 1, 1)
        self.opacityLabel = QtGui.QLabel(self.LoadedVectorsTab)
        self.opacityLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.opacityLabel.setObjectName(_fromUtf8("opacityLabel"))
        self.gridLayout_3.addWidget(self.opacityLabel, 0, 1, 1, 1)
        spacerItem7 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem7, 14, 3, 1, 1)
        self.lineComboBox = QtGui.QComboBox(self.LoadedVectorsTab)
        self.lineComboBox.setObjectName(_fromUtf8("lineComboBox"))
        self.gridLayout_3.addWidget(self.lineComboBox, 12, 3, 1, 1)
        self.sizeSpinBox = QtGui.QSpinBox(self.LoadedVectorsTab)
        self.sizeSpinBox.setObjectName(_fromUtf8("sizeSpinBox"))
        self.gridLayout_3.addWidget(self.sizeSpinBox, 1, 3, 1, 1)
        self.opacitySpinBox = QtGui.QSpinBox(self.LoadedVectorsTab)
        self.opacitySpinBox.setObjectName(_fromUtf8("opacitySpinBox"))
        self.gridLayout_3.addWidget(self.opacitySpinBox, 0, 3, 1, 1)
        self.setColorRadioButton = QtGui.QRadioButton(self.LoadedVectorsTab)
        self.setColorRadioButton.setObjectName(_fromUtf8("setColorRadioButton"))
        self.gridLayout_3.addWidget(self.setColorRadioButton, 2, 3, 1, 1)
        self.randomColorRadioButton = QtGui.QRadioButton(self.LoadedVectorsTab)
        self.randomColorRadioButton.setObjectName(_fromUtf8("randomColorRadioButton"))
        self.gridLayout_3.addWidget(self.randomColorRadioButton, 2, 5, 1, 1)
        self.colorLabel = QtGui.QLabel(self.LoadedVectorsTab)
        self.colorLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.colorLabel.setObjectName(_fromUtf8("colorLabel"))
        self.gridLayout_3.addWidget(self.colorLabel, 2, 1, 1, 1)
        self.colorFillButton = QtGui.QPushButton(self.LoadedVectorsTab)
        self.colorFillButton.setText(_fromUtf8(""))
        self.colorFillButton.setObjectName(_fromUtf8("colorFillButton"))
        self.gridLayout_3.addWidget(self.colorFillButton, 4, 3, 1, 1)
        self.gridLayout_4.addLayout(self.gridLayout_3, 0, 0, 1, 1)
        self.tabWidget.addTab(self.LoadedVectorsTab, _fromUtf8(""))
        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(-1, -1, 12, -1)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.applyButton = QtGui.QPushButton(preferencesDialog)
        self.applyButton.setMinimumSize(QtCore.QSize(22, 26))
        self.applyButton.setMaximumSize(QtCore.QSize(100, 26))
        self.applyButton.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.applyButton.setObjectName(_fromUtf8("applyButton"))
        self.horizontalLayout.addWidget(self.applyButton)
        self.closeButton = QtGui.QPushButton(preferencesDialog)
        self.closeButton.setMinimumSize(QtCore.QSize(22, 26))
        self.closeButton.setMaximumSize(QtCore.QSize(100, 26))
        self.closeButton.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout.addWidget(self.closeButton)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(preferencesDialog)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(preferencesDialog)

    def retranslateUi(self, preferencesDialog):
        preferencesDialog.setWindowTitle(_translate("preferencesDialog", "Preferences", None))
        self.defaultProjectionLabel.setText(_translate("preferencesDialog", "Default projection for loading layers (only if nothing has been loaded)", None))
        self.asDeclaredHere.setText(_translate("preferencesDialog", "Default from list", None))
        self.asFirstLayerRadio.setText(_translate("preferencesDialog", "Coordinate system as from first loaded layer", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.ViewAreaTab), _translate("preferencesDialog", "View options", None))
        self.stretchComboBox.setItemText(0, _translate("preferencesDialog", "None", None))
        self.stretchComboBox.setItemText(1, _translate("preferencesDialog", "Percentage Clip", None))
        self.stretchComboBox.setItemText(2, _translate("preferencesDialog", "Standard Deviation", None))
        self.stretchLabel.setText(_translate("preferencesDialog", "Default stretch for rasters: ", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.LoadedRastersTab), _translate("preferencesDialog", "Raster options", None))
        self.symbolLabel.setText(_translate("preferencesDialog", "Symbol", None))
        self.sizeLabel.setText(_translate("preferencesDialog", "Default size for vector", None))
        self.fillLabel.setText(_translate("preferencesDialog", "Fill", None))
        self.lineLabel.setText(_translate("preferencesDialog", "Line", None))
        self.opacityLabel.setText(_translate("preferencesDialog", "Opacity", None))
        self.setColorRadioButton.setText(_translate("preferencesDialog", "Set default color", None))
        self.randomColorRadioButton.setText(_translate("preferencesDialog", "Set random color", None))
        self.colorLabel.setText(_translate("preferencesDialog", "Color settings", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.LoadedVectorsTab), _translate("preferencesDialog", "Vector options", None))
        self.applyButton.setText(_translate("preferencesDialog", "Apply", None))
        self.closeButton.setText(_translate("preferencesDialog", "Close", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    preferencesDialog = QtGui.QWidget()
    ui = Ui_preferencesDialog()
    ui.setupUi(preferencesDialog)
    preferencesDialog.show()
    sys.exit(app.exec_())

