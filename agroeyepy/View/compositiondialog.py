# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
_fromUtf8=QString.fromUtf8
from ui_compositiondialog import Ui_compositionRGBDialog
from Additional.palette import Palette

class CompositionDialog(QDialog, Ui_compositionRGBDialog):

    def __init__(self, parent, rowSelected):
        super(CompositionDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)

        self.activateWindow()
        self.update()
        self.file = self.parent.model().files[rowSelected]
        self.setWindowTitle(_fromUtf8(_("Composition RGB - ")) + QString(self.file.name))

        selectedChannels = self.file.rasterAttributes.showingBands
        self.horizontalSlider.setValue(self.file.rasterAttributes.opacity)

        if self.file.bandsCount < 3:
            self.radioButtonRGB.setEnabled(False)

        if len(selectedChannels) == 3:
            self.radioButtonRGB.click()
            comboList = QStringList()
            for counter in range(self.file.bandsCount):
                comboList.append(_fromUtf8(self.file.rasterAttributes.bandName[counter+1]))
            self.comboChannelR.addItems(comboList)
            self.comboChannelG.addItems(comboList)
            self.comboChannelB.addItems(comboList)
            self.comboGrayscale.addItems(comboList)

            self.comboChannelR.setCurrentIndex(self.file.rasterAttributes.showingBands[0])
            self.comboChannelG.setCurrentIndex(self.file.rasterAttributes.showingBands[1])
            self.comboChannelB.setCurrentIndex(self.file.rasterAttributes.showingBands[2])

        if len(selectedChannels) == 1:
            self.radioButtonGrayscale.click()
            comboList = QStringList()
            for counter in range(self.file.bandsCount):
                comboList.append(_fromUtf8(self.file.rasterAttributes.bandName[counter+1]))
            self.comboGrayscale.addItems(comboList)
            self.comboGrayscale.setCurrentIndex(self.file.rasterAttributes.showingBands[0])

        self.palette = Palette()
        for singlePalette in self.palette.items:
            self.comboPalette.addItem(singlePalette[0], singlePalette[1])
        self.comboPalette.setIconSize(QSize(200, 30))
        self.comboPalette.setCurrentIndex(self.file.rasterAttributes.palette)

        self.connect(self.buttonApply, SIGNAL("clicked()"), self.applyChanges)
        self.isSingleChannelInverted = False

    def applyChanges(self):

        if self.pushButtonInverse.isChecked():
            if not self.isSingleChannelInverted:
                self.palette.invert(self.comboPalette.currentIndex())
                self.isSingleChannelInverted = True

        if not self.pushButtonInverse.isChecked():
            if self.isSingleChannelInverted:
                self.palette.invert(self.comboPalette.currentIndex())
                self.isSingleChannelInverted = False

        if self.radioButtonRGB.isChecked():
            del self.file.rasterAttributes.showingBands[:]
            self.file.rasterAttributes.showingBands.append(self.comboChannelR.currentIndex())
            self.file.rasterAttributes.showingBands.append(self.comboChannelG.currentIndex())
            self.file.rasterAttributes.showingBands.append(self.comboChannelB.currentIndex())
        elif self.radioButtonGrayscale.isChecked():
            del self.file.rasterAttributes.showingBands[:]
            self.file.rasterAttributes.showingBands.append(self.comboGrayscale.currentIndex())
            self.file.rasterAttributes.redPalette = self.palette.getRedLutArray(self.comboPalette.currentIndex())
            self.file.rasterAttributes.greenPalette = self.palette.getGreenLutArray(self.comboPalette.currentIndex())
            self.file.rasterAttributes.bluePalette = self.palette.getBlueLutArray(self.comboPalette.currentIndex())
            self.file.rasterAttributes.palette = self.comboPalette.currentIndex()

        self.file.rasterAttributes.opacity = self.spinBoxOpacityValue.value()

        self.emit(SIGNAL("compositionChanged()"))

