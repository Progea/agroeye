# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import re
from View import ui_addnewvectorlayerdialog
from View import addnewobjecttolayer
from Logic import vectorTableMechanism
from Control import exceptionModel



import os

_fromUtf8 = QString.fromUtf8

class AddNewVectorLayer(QDialog, ui_addnewvectorlayerdialog.Ui_addNewVectorLayer):
    def __init__(self, parent=None):
        super(AddNewVectorLayer, self).__init__(parent, flags = Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parentMain = parent
        self.setupUi(self)

        self.rowCount = 0
        self.fieldVals = []
        ## @var self.fieldType
        # Values of objects.
        self.fieldName = ['id']
        ## @var self.fieldName
        # Name of field.
        self.fieldTypeCol = ['Integer']
        ## @var self.fieldTypeCol
        # Type of field.
        self.fieldWidth = [10]
        ## @var self.fieldWidth
        # Width of field
        self.fieldPrec = [0]
        ## @var self.fieldPrec
        # Precision of field.
        self.geomType = 0
        ## @var self.geomType
        # Geometry type.
        self.addColumn = False
        self.delColumn = False
        self.renameColumn = []
        self.validatorClass = vectorTableMechanism.ValidateClass(self)

        self.epsg = self.parentMain.viewArea.projectionEPSG
        self.srs = self.parentMain.viewArea.viewAreaSpatialReference
        self.comboIndexEpsg = {0: 4326, 1: 2180, 2: 2176, 3: 2177, 4: 2178, 5: 2179,
                               6: 32633, 7: 32634, 8: 32635, 9: 3034, 10: 3035,
                               }

        self.coordComboBox.setCurrentIndex(self.parentMain.coordinateSystemComboBox.currentIndex())
        self.coordComboBox.activated.connect(self.chooseEPSG)

        self.cancelButton.clicked.connect(self.close)
        self.okButton.clicked.connect(self.okDialog)
        self.addAttrPushButton.clicked.connect(self.insertAttribute)
        self.deletePushButton.clicked.connect(self.deleteAttribute)
        self.renamePushButton.clicked.connect(self.renameAttribute)

        self.addCoordinateData()
        self.addDefaultData()


    def addDefaultData(self):
        """!
        @brief Create preview of column.
        """
        self.attrTableWidget.clear()
        self.attrTableWidget.setColumnCount(2)
        self.attrTableWidget.setRowCount(len(self.fieldName))

        for n, item in enumerate(self.fieldName):
                tempItem1 = QTableWidgetItem()
                tempItem1.setData(0, item)
                tempItem2 = QTableWidgetItem()
                text = self.validatorClass.setWidthAndPrecText(self.fieldTypeCol[n], n)
                tempItem2.setData(0, text)
                self.attrTableWidget.setItem(n, 0, tempItem1)
                self.attrTableWidget.setItem(n, 1, tempItem2)

        self.attrTableWidget.setHorizontalHeaderLabels((_fromUtf8(_('Name')), _fromUtf8(_('Type'))))
        self.attrTableWidget.itemClicked.connect(self.selectRow)

    def addCoordinateData(self):
        """!
        @brief Add data to combobox (epsg code).
        """

        for i in range(self.parentMain.coordinateSystemComboBox.count()):
            self.coordComboBox.addItem(self.parentMain.coordinateSystemComboBox.itemText(i))

        self.EPSG2Name = dict()
        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__, '..\\Logic', 'epsg'), 'r') as epsg_codes:
            tempDatumName = ''

            while 1:
                line = epsg_codes.readline()
                if not line:
                    break
                if '#' in line and len(line) > 2:
                    if re.match('# ', line):
                        tempDatumName = line.split('#')[1].strip()
                    line = epsg_codes.readline()
                    if re.match('^<\d{4,5}>', line):
                        datumName = tempDatumName
                        codeEPSG = int(re.findall('<\d{4,5}>', line)[0].strip("<>"))
                        try:
                            self.EPSG2Name[codeEPSG] = datumName
                        except UnboundLocalError:
                            pass

    def okDialog(self):
        """!
        @brief Accept raster calculator window.
        """
        # do sth
        if self.pointRadioButton.isChecked():
            self.geomType = 1
        elif self.lineRadioButton.isChecked():
            self.geomType = 2
        elif self.polygonRadioButton.isChecked():
            self.geomType = 3

        self.close()
        addObject = addnewobjecttolayer.AddNewVectorObjectToLayer(self)
        addObject.show()
        dw = self.parentMain.geometry().x()
        dh = self.parentMain.geometry().y()
        addObject.move(dw, dh)

    def chooseEPSG(self, newIndex):
        """!
        @brief Chooses EPSG code. User can choose EPSG code from list or enter new EPSG Code. If user EPSG Code is valid, new code is add to list.
        @param newIndex: Index of EPSG code list.
        """

        if newIndex == len(self.coordComboBox)-1:
            self.coordComboBox.setEditable(True)
            self.coordComboBox.clearEditText()
            self.coordComboBox.setDuplicatesEnabled(False)
            self.coordComboBox.setInsertPolicy(QComboBox.InsertBeforeCurrent)
            self.okButton.setDefault(False)


            lineEdit = self.coordComboBox.lineEdit()
            lineEdit.selectAll()
            regexp = QRegExp(r'\d{4,5}')
            validator = QRegExpValidator(regexp, lineEdit)
            lineEdit.setValidator(validator)
            lineEdit.textChanged.connect(self.checkComboBoxValues)
            lineEdit.textChanged.emit(lineEdit.text())
        else:
            self.coordComboBox.setEditable(False)
            self.epsgChanged(newIndex)
            #self.okButton.setDefault(True)

    def checkComboBoxValues(self, *args, **kwargs):
        """!
        @brief Simply styling line with editing EPSG code.
        When background is yellow it means that more digits are required,
        When background is green the number of digits is satisfactory
        Red background never appears because right format of characters is imposed
        by Regular Expression Validator
        """
        sender = self.sender()
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]

        if state == QValidator.Acceptable:
            color = '#c4df9b' # green
        elif state == QValidator.Intermediate:
            color = '#fff79a' # yellow
        elif state == QValidator.Invalid:
            color = '#f6989d' # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def epsgChanged(self, newIndex):
        """!
        @brief Sets EPSG code to new vector layer.
        @param newIndex: Index of QCombobox which refers EPSG code.
        """
        # print newIndex
        # index = [index for index, epsg in self.comboIndexEpsg.iteritems() if index == newIndex]
        # if index > 12:
        #     self.coordComboBox.removeItem(newIndex)
        #     newIndex = index[0]

        try:
            self.epsg_code = self.comboIndexEpsg[newIndex]
        except KeyError:
            self.comboIndexEpsg[newIndex] = int(self.coordComboBox.currentText())
            self.epsg_code = int(self.coordComboBox.currentText())
        self.coordComboBox.setCurrentIndex(newIndex)

        try:
            if not self.EPSG2Name.has_key(self.epsg_code):
                raise exceptionModel.EPSGCodeError(self.epsg_code)
            return True
        except exceptionModel.EPSGCodeError as e:
            self.coordComboBox.removeItem(newIndex)
            self.comboIndexEpsg.__delitem__(newIndex)
            self.chooseEPSG(newIndex)
            QMessageBox.critical(self, "%s"%e.title,"%s"%e.msg, QMessageBox.Close)
            return False

    def insertAttribute(self):
        """!
        @brief Window for insert new column in attribute table.
        """
        addColumn = vectorTableMechanism.ColumnAddDialog(self, self.attrTableWidget)
        addColumn.show()

    def deleteAttribute(self):
        """!
        @brief Window for delete column in attribute table.
        """
        delColumn = vectorTableMechanism.ColumnDelDialog(self, self.attrTableWidget)
        delColumn.show()

    def renameAttribute(self):
        """!
        @brief Window for rename column in attribute table.
        """
        rowSelected = self.attrTableWidget.currentRow()
        item = self.attrTableWidget.item(rowSelected, 0)

        renamenColumn = vectorTableMechanism.RenameDialog(self, self.attrTableWidget, item.text(), rowSelected)
        renamenColumn.show()

    def updateColumn(self):
        """!
        @brief Update column after edit.
        """
        self.fieldName = []
        for i in xrange(self.attrTableWidget.rowCount()):
            item = self.attrTableWidget.item(i, 0)
            self.fieldName.append(item.text())

    def selectRow(self):
        """!
        @brief Action after select row.
        """
        self.renamePushButton.setEnabled(True)

