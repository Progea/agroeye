# -*- coding: utf-8 -*-
import ui_measuredistancetool
from PyQt4 import QtCore
from PyQt4 import QtGui
import ogr

class MeasureDistanceToolMetaclass(type(QtGui.QDialog)):

    instance = None

    def __call__(cls, *args, **kwargs):
        if (cls.instance is None) or not (isinstance(cls.instance, cls)):
            cls.instance = super(MeasureDistanceToolMetaclass, cls).__call__(*args, **kwargs)
        else:
            cls.instance.connectSignals()
        return cls.instance

class MeasureDistanceTool(QtGui.QDialog, ui_measuredistancetool.Ui_measureDistanceDialog):

    __metaclass__ = MeasureDistanceToolMetaclass

    def __init__(self, parent=None):
        super(MeasureDistanceTool, self).__init__(parent, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint)
        self.viewArea = parent.viewArea
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowStaysOnTopHint)
        self.trackByPointsButton.setChecked(True)
        self.connectSignals()
        self.selectedLine = ogr.Geometry(ogr.wkbLineString)

    def connectSignals(self):
        self.connect(self.clearButton, QtCore.SIGNAL("pressed()"), self.clearPoints)
        self.connect(self.singleClickRadioButton, QtCore.SIGNAL("clicked()"), self.chooseClickType)
        self.connect(self.doubleClickRadioButton, QtCore.SIGNAL("clicked()"), self.chooseClickType)
        if self.singleClickRadioButton.isChecked():
            self.singleClickRadioButton.click()
        elif self.doubleClickRadioButton.isChecked():
            self.doubleClickRadioButton.click()
        self.connect(self.trackByPointsButton, QtCore.SIGNAL("clicked()"), self.chooseTrackType)
        self.connect(self.trackByMouseButton, QtCore.SIGNAL("clicked()"), self.chooseTrackType)
        if self.trackByPointsButton.isChecked():
            self.trackByPointsButton.click()
        elif self.trackByMouseButton.isChecked():
            self.trackByMouseButton.click()
        self.connect(self.viewArea, QtCore.SIGNAL("scenePrepared()"), self.redrawFeature)
        self.connect(self.viewArea, QtCore.SIGNAL("mouseRightClicked(QEvent)"), self.endTracking)
        self.connect(self.unitsComboBox, QtCore.SIGNAL("currentIndexChanged(int)"), lambda: self.setMeasurement())
        self.connect(self.viewArea, QtCore.SIGNAL("measureDistanceTool()"), self.redrawFeature)

    def chooseClickType(self):
        if self.singleClickRadioButton.isChecked():
            self.disconnect(self.viewArea, QtCore.SIGNAL("mouseDoubleClicked(QEvent)"), self.addPoint)
            self.connect(self.viewArea, QtCore.SIGNAL("mouseClicked(QEvent)"), self.addPoint)
        elif self.doubleClickRadioButton.isChecked():
            self.connect(self.viewArea, QtCore.SIGNAL("mouseDoubleClicked(QEvent)"), self.addPoint)
            self.disconnect(self.viewArea, QtCore.SIGNAL("mouseClicked(QEvent)"), self.addPoint)

    def chooseTrackType(self):
        if self.trackByMouseButton.isChecked():
            self.connect(self.viewArea, QtCore.SIGNAL("mouseMoved(QEvent)"), self.trackPosition)
        elif self.trackByPointsButton.isChecked():
            self.disconnect(self.viewArea, QtCore.SIGNAL("mouseMoved(QEvent)"), self.trackPosition)

    def trackPosition(self, event):
        if self.selectedLine.GetPointCount() == 1:
            points = self.selectedLine.GetPoints()
        elif self.selectedLine.GetPointCount() > 1:
            points = self.selectedLine.GetPoints()[:-1]
        self.selectedLine = ogr.Geometry(ogr.wkbLineString)
        try:
            for point in points:
                self.selectedLine.AddPoint(point[0], point[1])
            x, y = self.viewArea.getMouseCoordinates(event.pos())
            self.selectedLine.AddPoint(x, y)
            distance = self.selectedLine.Length()
            self.setMeasurement(distance)
            self.clearView()
            self.redrawFeature()
        except UnboundLocalError:
            pass

    def endTracking(self, event):
        self.disconnect(self.viewArea, QtCore.SIGNAL("mouseMoved(QEvent)"), self.trackPosition)
        self.trackByPointsButton.click()
        self.addPoint(event)

    def addPoint(self, event):
        x, y = self.viewArea.getMouseCoordinates(event.pos())
        self.selectedLine.AddPoint(x, y)
        distance = self.selectedLine.Length()
        try:
            self.setMeasurement(distance)
        except AttributeError:
            pass
        self.redrawFeature()

    def redrawFeature(self, border=None):
        self.clearView()
        try:
            if not border:
                visibleLine = self.selectedLine.Intersection(self.viewArea.viewBoundary)
            else:
                visibleLine = border
            lines = []
            if visibleLine.GetGeometryName() == 'MULTILINESTRING':
                for line in visibleLine:
                    lines.append(line)
            elif (visibleLine.GetGeometryName() == 'LINESTRING') or \
                 (visibleLine.GetGeometryName() == "LINEARRING"):
                    lines.append(visibleLine)

            for line in lines:
                visibleLinePoints = line.GetPoints()
                visPoints = []
                for singlePoint in visibleLinePoints:
                    x, y = self.viewArea.revertCoordinates(singlePoint[0], singlePoint[1])
                    visPoints.append((x, y))

                penStyle = StyleOfPen()
                for pointNo in range(len(visPoints)-1):
                    graphicLine = QtCore.QLineF(visPoints[pointNo][0], visPoints[pointNo][1],
                                                visPoints[pointNo+1][0], visPoints[pointNo+1][1])
                    self.viewArea.scene.addLine(graphicLine, penStyle)
                    
        except:
            pass

    def clearPoints(self):
        self.selectedLine.Empty()
        self.calculatedDistanceField.setText(QtCore.QString())
        self.distance = 0
        self.clearView()

    def clearView(self):
        items = self.viewArea.scene.items()[:-1]
        for item in items:
            self.viewArea.scene.removeItem(item)

    def setMeasurement(self, distance=None):
        """!
        @brief Set distance to box
        @param distance: in meters
        """
        if distance:
            self.distance = distance
        else:
            try:
                distance = self.distance
            except AttributeError:
                distance = 0
        if self.unitsComboBox.currentText() == "mm":
            distance *= 1000
            distance = str("%.0f" % round(distance, 0))
        elif self.unitsComboBox.currentText() == "cm":
            distance *= 100
            distance = str("%.0f" % round(distance, 0))
        elif self.unitsComboBox.currentText() == "dm":
            distance *= 10
            distance = str("%.1f" % round(distance, 1))
        elif self.unitsComboBox.currentText() == "m":
            distance = str("%.2f" % round(distance, 2))
        elif self.unitsComboBox.currentText() == "km":
            distance /= 1000.0
            distance = str("%.3f" % round(distance, 3))

        self.calculatedDistanceField.setText(QtCore.QString(distance))

    def closeEvent(self, event):
        self.disconnect(self.singleClickRadioButton, QtCore.SIGNAL("clicked()"), self.chooseClickType)
        self.disconnect(self.doubleClickRadioButton, QtCore.SIGNAL("clicked()"), self.chooseClickType)
        self.disconnect(self.viewArea, QtCore.SIGNAL("mouseClicked(QEvent)"), self.trackPosition)
        self.disconnect(self.viewArea, QtCore.SIGNAL("mouseRightClicked(QEvent)"), self.endTracking)
        self.disconnect(self.viewArea, QtCore.SIGNAL("mouseDoubleClicked(QEvent)"), self.trackPosition)
        self.disconnect(self.viewArea, QtCore.SIGNAL("scenePrepared()"), self.redrawFeature)
        self.disconnect(self.unitsComboBox, QtCore.SIGNAL("activated()"), self.setMeasurement)
        self.disconnect(self.unitsComboBox, QtCore.SIGNAL("currentIndexChanged(int)"), lambda: self.setMeasurement())
        self.disconnect(self.viewArea, QtCore.SIGNAL("mouseDoubleClicked(QEvent)"), self.addPoint)
        self.disconnect(self.viewArea, QtCore.SIGNAL("mouseClicked(QEvent)"), self.addPoint)
        self.disconnect(self.viewArea, QtCore.SIGNAL("measureDistanceTool()"), self.redrawFeature)
        self.clearPoints()

class StyleOfPen(QtGui.QPen):

    def __init__(self):
        super(StyleOfPen, self).__init__()
        self.setStyle(QtCore.Qt.SolidLine)
        self.setCapStyle(QtCore.Qt.RoundCap)
        self.setJoinStyle(QtCore.Qt.RoundJoin)
        self.setColor(QtGui.QColor(0, 255, 230))
        self.setWidth(2)




