# -*- coding: utf-8 -*-
import sys
import os
import gc

from PyQt4 import QtCore
from PyQt4 import QtGui
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import ui_identifydialog
import ogr
import osr

class IdentifyDialog(QtGui.QDialog, ui_identifydialog.Ui_identifyDialog):

    def __init__(self, parent, sourceFile):

        super(IdentifyDialog, self).__init__(parent, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint)
        self.parent = parent
        self.file = sourceFile
        self.setupUi(self)

        self.viewArea = self.parent.parent.viewArea
        self.connect(self.viewArea, QtCore.SIGNAL("mouseClicked(QEvent)"), self.updateTable)
        bandValues = self.file.bandsValuesInPoint(QtCore.QPoint(0, 0))
        self.model = IdentifyDialogModel(bandValues)
        self.canvas = PlotCanvas(bandValues, self.file.dataRange)
        self.tableView.setModel(self.model)
        self.plotLayout.addWidget(self.canvas)
        self.tableView.horizontalHeader().hide()
        self.tableView.verticalHeader().hide()
        self.tableView.resizeRowsToContents()
        self.tableView.verticalHeader().setStretchLastSection(True)
        self.tableView.horizontalHeader()


    def updateTable(self, event):
        x, y = self.viewArea.getMouseCoordinates(event.pos())
        tempPoint = ogr.Geometry(ogr.wkbPoint)
        tempPoint.AddPoint(x, y)
        transformation = osr.CoordinateTransformation(self.viewArea.viewAreaSpatialReference,
                                                      self.file.rasterSpatialReference)
        tempPoint.Transform(transformation)
        rasterPoint = self.file.fromCoordinatesToPixels([tempPoint.GetX(), tempPoint.GetY()])
        bandValues = self.file.bandsValuesInPoint(rasterPoint)
        self.model = IdentifyDialogModel(bandValues)
        self.tableView.setModel(self.model)
        self.canvas.redrawPlot(bandValues)
        self.resizeEvent(event)

    def resizeEvent(self, event):
        tableWidth = self.tableView.size().width()
        numberOfCols = self.model.columnCount()
        if numberOfCols <= 8:
            for col in range(numberOfCols):
                self.tableView.setColumnWidth(col, tableWidth/numberOfCols)
                self.tableView.horizontalScrollBar().hide()
        else:
            for col in range(numberOfCols):
                self.tableView.setColumnWidth(col, tableWidth/8)

    def closeEvent(self, event):
       self.disconnect(self.viewArea, QtCore.SIGNAL("mouseClicked(QEvent)"), self.updateTable)

class IdentifyDialogModel(QtCore.QAbstractTableModel):

    def __init__(self, values=None, parent=None):
        super(IdentifyDialogModel, self).__init__(parent)
        self.parent = parent
        self.values = values

    def rowCount(self, parent):
        return 2

    def columnCount(self, parent=None):
        return len(self.values)

    def data(self, index, role):
        if not index.isValid():
            return QtCore.QVariant()
        value = self.values[index.column()]
        row = index.row()
        if role == QtCore.Qt.DisplayRole:
            if row == 0:
                return QtCore.QVariant(value[0])
            elif row == 1:
                return str(value[1])

    def setData(self, index, value, role=QtCore.QModelIndex()):

        if index.isValid() and 0 <= index.row() < len(self.values):
            value = self.values[index.row()]
            if role == QtCore.Qt.DisplayRole:
                self.values[index.row()][index.column()] = value
        return True


class PlotCanvas(FigureCanvas):

    def __init__(self, initValues, dataRange):
        self.yMax = dataRange
        self.fig = Figure()
        self.fig.subplots_adjust(bottom=0.15)
        self.axes = self.fig.add_subplot(111)
        self.axes.set_ylim(0, self.yMax)
        self.column0 = []
        self.column1 = []

        super(PlotCanvas, self).__init__(self.fig)
        self.redrawPlot(initValues)

    def redrawPlot(self, values):
        self.column0 = []
        self.column1 = []
        for row in values:
            self.column0.append(row[0])
            self.column1.append(row[1])
        self.y = self.column1
        self.xTicks = self.column0
        self.fig.axes[0].cla()
        self.axes.set_ylim(0, self.yMax)
        self.axes.set_xticks(range(len(self.xTicks)))
        self.axes.set_xticklabels(self.xTicks, rotation=45)
        self.axes.plot(range(len(self.xTicks)), self.y)
        self.fig.canvas.draw()

