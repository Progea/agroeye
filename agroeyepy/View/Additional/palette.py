from PyQt4 import QtCore
from PyQt4 import QtGui
import numpy as np
_fromUtf8=QtCore.QString.fromUtf8


class Palette(QtCore.QObject):
    def __init__(self):

        super(Palette, self).__init__()
        self.items = []
        self.colorsDefinition = []

        self.height = 200
        self.width = 20

        #Grayscale
        grayscaleColor = [QtGui.QColor(0,0,0), QtGui.QColor(255,255,255)]
        greyscale = QtCore.QString(_fromUtf8(_("Grayscale")))
        greyscaleIcon = self.getIcon(grayscaleColor)
        self.items.append([greyscaleIcon, greyscale])
        self.colorsDefinition.append(grayscaleColor)

        #Hipsometric
        hipsometriclColors = [QtGui.QColor(255,0,0), QtGui.QColor(255,55,0), QtGui.QColor(255,115,0), QtGui.QColor(255,170,0),
                              QtGui.QColor(255,255,0), QtGui.QColor(228,245,0), QtGui.QColor(176,224,0), QtGui.QColor(131,207,0),
                              QtGui.QColor(90,186,0), QtGui.QColor(56,168,0)]
        hipsometriclColors.reverse()
        hipsometric = QtCore.QString(_fromUtf8(_("Hipsometric")))
        hipsometricIcon = self.getIcon(hipsometriclColors)
        self.items.append([hipsometricIcon, hipsometric])
        self.colorsDefinition.append(hipsometriclColors)

        #Custom 1
        custom1Colors = [QtGui.QColor(255,0,0), QtGui.QColor(255,111,0), QtGui.QColor(255,183,0), QtGui.QColor(255,255,0),
                              QtGui.QColor(210,255,105), QtGui.QColor(145,255,180), QtGui.QColor(0,255,255), QtGui.QColor(56,172,255),
                              QtGui.QColor(65,98,255), QtGui.QColor(0,0,255)]
        custom1Colors.reverse()
        custom1 = QtCore.QString(_fromUtf8(_("Custom #1")))
        custom1Icon = self.getIcon(custom1Colors)
        self.items.append([custom1Icon, custom1])
        self.colorsDefinition.append(custom1Colors)

        #Reds
        redsColors = [QtGui.QColor(255, 245, 240), QtGui.QColor(103,0,13)]
        reds = QtCore.QString(_fromUtf8(_("Reds")))
        redsIcon = self.getIcon(redsColors)
        self.items.append([redsIcon, reds])
        self.colorsDefinition.append(redsColors)

        #Red in Blue
        redsColors = [QtGui.QColor(225, 0, 12), QtGui.QColor(10, 80, 161)]
        reds = QtCore.QString(_fromUtf8(_("Red in Blue")))
        redsIcon = self.getIcon(redsColors)
        self.items.append([redsIcon, reds])
        self.colorsDefinition.append(redsColors)


    def getRedLutArray(self, number):
        palette = self.colorsDefinition[number]
        values = []
        for color in palette:
            values.append(color.red())
        x = np.linspace(start=0, stop=255, num=len(values), dtype=np.uint8)
        colorLut = np.interp(np.arange(256), x, values).astype(np.uint8)
        return colorLut

    def getGreenLutArray(self, number):
        palette = self.colorsDefinition[number]
        values = []
        for color in palette:
            values.append(color.green())
        x = np.linspace(start=0, stop=255, num=len(values), dtype=np.uint8)
        colorLut = np.interp(np.arange(256), x, values).astype(np.uint8)
        return colorLut

    def getBlueLutArray(self, number):
        palette = self.colorsDefinition[number]
        values = []
        for color in palette:
            values.append(color.blue())
        x = np.linspace(start=0, stop=255, num=len(values), dtype=np.uint8)
        colorLut = np.interp(np.arange(256), x, values).astype(np.uint8)
        return colorLut

    def invert(self, number):
        self.colorsDefinition[number].reverse()


    def getIcon(self, colorsList):
        pixmap = QtGui.QPixmap(QtCore.QSize(self.height, self.width))
        painter = QtGui.QPainter(pixmap)
        gradient = QtGui.QLinearGradient(QtCore.QPointF(pixmap.rect().bottomLeft()),
                                         QtCore.QPointF(pixmap.rect().bottomRight()))
        colorsCount = len(colorsList)
        for i, color in enumerate(colorsList):
            gradient.setColorAt(float(i)/colorsCount, color)

        brush = QtGui.QBrush(gradient)
        painter.fillRect(QtCore.QRectF(0, 0, self.height, self.width), brush)
        painter.end()

        icon = QtGui.QIcon(pixmap)

        return icon
