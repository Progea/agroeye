# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from View import ui_modelsegmentationdialog
from View import ui_modelstatisticsdialog
from View import ui_modelsyntheticdialog
from Control import agroeye
import ogr, gdal

_fromUtf8 = QString.fromUtf8




class Segmentation(QDialog, ui_modelsegmentationdialog.Ui_modelSegmentationDialog):
    """!
    The class Segmentation handles segmentation process.
    """
    def __init__(self, parent):
        super(Segmentation, self).__init__(parent, flags=Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.modelAttr = parent.modelAttr
        self.setupUi(self)
        self.typeComboBox.setCurrentIndex(self.modelAttr.segmentationType.index)
        self.typeComboBox.currentIndexChanged.connect(self.changeSegmentationType)
        self.runButton.clicked.connect(self.startSegmentation)
        self.runButton.setEnabled(False)
        self.okButton.clicked.connect(self.saveParam)
        self.changeButtonVisibility(False)
        self.cancelButton.clicked.connect(self.close)
        self.addPathToolButton.clicked.connect(lambda: self.addPath(self.pathLineEdit, 'SpatiaLite Database (*.spatialite)'))
        self.sizeSpinBox.setValue(self.modelAttr.segmentationType.sizeValue)
        self.sizeSpinBox.valueChanged.connect(lambda: self.changeButtonVisibility(True))
        self.addRasterFile()
        self.rasterComboBox.currentIndexChanged.connect(lambda: self.setRasterBand(True))
        self.pathLineEdit.textChanged.connect(self.changePath)
        self.changeSegmentationType()
        self.progresLabel.setText("To start new segmentation fill empty parameters.")

    def addRasterFile(self):
        """!
        @brief ComboBox object is filled name of raster file which are loaded into table of contents.
        """
        self.modelAttr.addRasterFile(self.rasterComboBox, False)
        if self.rasterComboBox.currentIndex() >= 0 and self.modelAttr.dbPath:
            self.changeButtonVisibility(False)

    def changeSegmentationType(self):
        """!
        @brief Change segmentation type in checkbox object.
        """

        self.modelAttr.saveSegmentationType(self.typeComboBox.currentIndex(),0)
        self.sizeLabel.setText(self.modelAttr.segmentationType.sizeName)
        self.sizeSpinBox.setMinimum(self.modelAttr.segmentationType.minValue)
        self.sizeSpinBox.setMaximum(self.modelAttr.segmentationType.maxValue)
        self.sizeSpinBox.setValue(self.modelAttr.segmentationType.sizeValue)
        if self.modelAttr.segmentationType == self.modelAttr.quadtreeType:
            self.rasterBandComboBox.setEnabled(True)
            self.bandLabel.setEnabled(True)
            self.modelIndexType = QStandardItemModel(self.rasterBandComboBox)
            self.rasterBandComboBox.setModel(self.modelIndexType)
            self.rasterBandComboBox.currentIndexChanged.connect(lambda: self.changeButtonVisibility(True))
            self.rasterBandComboBox.view().pressed.connect(lambda: self.modelAttr.handleItemPressed(self.rasterBandComboBox, self.modelIndexType, self.sender()))
            self.modelAttr.currentRaster = self.rasterComboBox.currentText()
            self.modelAttr.addRasterBands(self.rasterBandComboBox)
            self.modelAttr.setCheckedBands(self.rasterBandComboBox)
        else:
            self.rasterBandComboBox.setCurrentIndex(0)
            self.rasterBandComboBox.setEnabled(False)
            self.bandLabel.setEnabled(False)

        self.changeButtonVisibility(True)


    def setRasterBand(self, okButtonBool):
        self.modelAttr.currentRaster = self.rasterComboBox.currentText()
        if self.rasterBandComboBox.isEnabled() == True:
            self.modelAttr.addRasterBands(self.rasterBandComboBox)
        self.changeButtonVisibility(okButtonBool)

    def changeButtonVisibility(self, okButtonBool):
        """!
        @brief Change button visibility.
        @param okButtonBool: value of button visibility (True/False).
        """
        self.modelAttr.setButtonOkVisibility(self.okButton, okButtonBool)
        self.modelAttr.setButtonRunVisibility(self.runButton, self.pathLineEdit.text(), self.rasterComboBox.currentIndex(), self.rasterBandComboBox )
        self.progressBar.setValue(0)
        self.progresLabel.setText("To start new segmentation fill empty parameters.")

    def changePath(self):
        """!
        @brief Change path to database.
        """
        value = self.modelAttr.checkPathToDb(self.pathLineEdit.text())
        self.changeButtonVisibility(value)

    def saveParam(self):
        """!
        @brief Save all parameters.
        """
        self.modelAttr.saveRasterInput(self.rasterComboBox.currentText())
        self.modelAttr.savePathToDb(self.pathLineEdit.text())
        self.modelAttr.saveSegmentationType(self.typeComboBox.currentIndex(),self.sizeSpinBox.value(), self.modelAttr.getBandList(self.rasterBandComboBox))
        self.changeButtonVisibility(False)

    def addPath(self, lineEdit, ext):
        """!
        @brief Open file dialog to add path to database.
        """
        res = self.modelAttr.addPath(lineEdit, ext)
        self.changeButtonVisibility(res)

    def startSegmentation(self):
        """!
        @brief Start segmentation process.
        """

        self.saveParam()
        self.runButton.setEnabled(False)
        QApplication.processEvents()

        raster = agroeye.Raster(self.modelAttr.getPathToRaster())
        spatialite = agroeye.Sqlite(u'%s' % self.modelAttr.dbPath, True)
        spatialite.prepareForSegmentation(raster)
        if self.modelAttr.segmentationType == self.modelAttr.chessboardType:
            parameters = agroeye.SegmentationParameters(self.modelAttr.segmentationType.sizeValue)
            segmentation = agroeye.ChessboardSegmentation(raster, parameters, spatialite.openRepo())
        elif self.modelAttr.segmentationType == self.modelAttr.quadtreeType:
            bands = self.rasterBandComboBox.currentIndex()
            parameters = agroeye.SegmentationParameters([bands], self.modelAttr.segmentationType.sizeValue)
            segmentation = agroeye.QuadtreeSegmentation(raster, parameters, spatialite.openRepo())

        progressCallback = ProgressCallback(self.progressBar)
        def progress(value, types, message):
            progressCallback.callback(value, types, message)
            if message:
                self.progresLabel.setText(message)

        segmentation.setCallback(progress)
        segmentation.start()
        self.progressBar.setValue(100)
        self.modelAttr.saveSegmentation()
        self.pathLineEdit.clear()

        #self.fileGroupBox.setEnabled(False)
        #self.segmentationGroupBox.setEnabled(False)

class ProgressCallback(object):
    """!
    The class Progress handles callback from process to progress bar.
    """
    def __init__(self, progressBar):
        self.progressBar = progressBar
        #self.progressBar.setTextVisible(True)
        self.progressBar.setAlignment(Qt.AlignCenter)

    def callback(self, value, types, message):
        """!
        @brief Sets value in progress bar.
        @param value: progress value
        """
        if value > 0:
            self.progressBar.setValue(int(value*100))
            #self.progressBar.setFormat(message)
            QApplication.processEvents()



class Synthetic(QDialog, ui_modelsyntheticdialog.Ui_modelSyntheticDialog):
    """!
        The class Statistics handles statistics process.
        """

    def __init__(self, parent):
        super(Synthetic, self).__init__(parent, flags=Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.modelAttr = parent.modelAttr
        self.setupUi(self)
        self.modelAttr.currentRaster = ''
        self.runButton.clicked.connect(self.startDifference)
        self.runButton.setEnabled(False)
        self.okButton.clicked.connect(self.saveParam)
        self.changeButtonVisibility(False)
        self.cancelButton.clicked.connect(self.close)
        self.addRasterFile()
        self.diffCheckBox.stateChanged.connect(self.checkState)
        self.rasterComboBox.currentIndexChanged.connect(lambda: self.setRasterBand(True))


    def addRasterFile(self):
        """!
        @brief ComboBox object is filled name of raster file which are loaded into table of contents.
        """
        self.modelAttr.addRasterFile(self.rasterComboBox, False)


    def changePath(self):
        """!
        @brief Change path to database.
        """
        value = self.modelAttr.checkPathToDb(self.saveAddLineEdit.text())
        self.changeButtonVisibility(value)

    def addPath(self, lineEdit, ext):
        """!
        @brief Open file dialog to add path to database.
        """
        res = self.modelAttr.addPath(lineEdit, ext)
        self.changeButtonVisibility(res)


    def checkState(self):
        if self.diffCheckBox.isChecked():
            self.setDiffDialog(True)
            self.modelAttr.currentRaster = self.rasterComboBox.currentText()
            self.modelIndexDiff = QStandardItemModel(self.bandAddComboBox)
            self.bandAddComboBox.setModel(self.modelIndexDiff)
            self.bandAddComboBox.currentIndexChanged.connect(lambda: self.changeButtonVisibility(True))
            self.modelAttr.addRasterBands(self.bandAddComboBox)
            self.bandAddComboBox.view().pressed.connect(
                lambda: self.modelAttr.handleItemPressed(self.bandAddComboBox, self.modelIndexDiff, self.sender()))
            self.saveToolButton.clicked.connect(lambda: self.addPath(self.saveAddLineEdit, 'Tiff (*.tiff)'))
            self.saveAddLineEdit.textChanged.connect(self.changePath)
            self.sizeAddSpinBox.valueChanged.connect(lambda: self.changeButtonVisibility(True))
        else:
            self.setDiffDialog(False)

    def setDiffDialog(self, value):
        self.rasterLabel.setEnabled(value)
        self.rasterComboBox.setEnabled(value)
        self.saveLabel.setEnabled(value)
        self.bandLabel_2.setEnabled(value)
        self.sizeDiffLabel.setEnabled(value)
        self.saveAddLineEdit.setEnabled(value)
        self.saveToolButton.setEnabled(value)
        self.bandAddComboBox.setEnabled(value)
        self.sizeAddSpinBox.setEnabled(value)

    def startDifference(self):

        self.saveParam()
        self.runButton.setEnabled(False)
        QApplication.processEvents()

        analysis = agroeye.RasterAnalysisParameters(agroeye.DIFFERENCE_TO_NEIGHBOUR, 1, self.sizeAddSpinBox.value(),
                                                    self.sizeAddSpinBox.value())
        raster = agroeye.Raster(self.modelAttr.getPathToRaster())
        block = agroeye.BlockStatistics(raster, str(self.saveAddLineEdit.text()), analysis)
        progressCallback = ProgressCallback(self.progressBar)

        def progress(value, types, message):
            progressCallback.callback(value, types, message)
            if message:
                self.progresLabel.setText(message)

        block.setCallback(progress)
        self.progressBar.setValue(100)
        block.start()
        self.saveAddLineEdit.clear()

    def saveParam(self):
        """!
        @brief Save all parameters.
        """
        self.modelAttr.saveAdditionalLayer(self.saveAddLineEdit.text())
        self.changeButtonVisibility(False)


    def setRasterBand(self, okButtonBool):
        self.modelAttr.currentRaster = self.rasterComboBox.currentText()
        if self.bandAddComboBox.isEnabled() == True:
            self.modelAttr.addRasterBands(self.bandAddComboBox)
        self.changeButtonVisibility(okButtonBool)

    def changeButtonVisibility(self, okButtonBool):
        """!
        @brief Change button visibility.
        @param okButtonBool: value of button visibility (True/False).
        """

        if self.diffCheckBox.isChecked():
            if self.saveAddLineEdit.text() and self.modelAttr.checkBand(self.bandAddComboBox):
                self.runButton.setEnabled(True)
            else:
                self.runButton.setEnabled(False)
        else:
            self.runButton.setEnabled(False)
        self.progressBar.setValue(0)

class Statistics(QDialog, ui_modelstatisticsdialog.Ui_modelStatisticsDialog):
    """!
    The class Statistics handles statistics process.
    """
    def __init__(self, parent):
        super(Statistics, self).__init__(parent, flags=Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.modelAttr = parent.modelAttr
        self.setupUi(self)
        self.modelAttr.currentRaster = ''
        self.rasterComboBox.currentIndexChanged.connect(self.checkIndex)
        self.dbComboBox.currentIndexChanged.connect(self.changeButtonVisibility)
        self.addRasters()
        self.addDatabases()
        ### temporary is not avalailable
        self.loadFromFile.setEnabled(False)
        #####
        self.meanCheckBox.stateChanged.connect(lambda: self.checkState(self.meanCheckBox, self.meanComboBox))
        self.stddevCheckBox.stateChanged.connect(lambda: self.checkState(self.stddevCheckBox, self.stddevComboBox))
        self.ndviCheckBox.stateChanged.connect(lambda: self.checkState(self.ndviCheckBox, self.ndviComboBox))
        self.zabudCheckBox.stateChanged.connect(lambda: self.checkState(self.zabudCheckBox, self.zabudComboBox))
        self.runButton.clicked.connect(self.startStatistics)
        self.cancelButton.clicked.connect(self.close)
        self.runButton.setEnabled(False)
        #self.checkStatistics()

    def checkIndex(self):
        """!
        @brief Checks index of comboBox object which contains raster file.
        """
        value = self.modelAttr.saveDbToStatistics(self.rasterComboBox.currentText())
        self.statisticsGroupBox.setEnabled(value)
        self.newStats.setEnabled(value)
        self.changeButtonVisibility()

    def checkState(self, checkBox, comboBox):
        """!
        @brief Checks the state of checkBox.
        @param checkBox: checkBox object.
        @param comboBox: comboBox object.
        """
        if checkBox.isChecked():
            self.modelAttr.currentRaster = self.rasterComboBox.currentText()
            comboBox.setEnabled(True)
            comboBox.setModel(QStandardItemModel(comboBox))
            self.addBand(comboBox)
            comboBox.view().pressed.connect(lambda: self.handleItemPressed(comboBox))
            comboBox.currentIndexChanged.connect(self.changeButtonVisibility)
        else:
            comboBox.setEnabled(False)
        self.changeButtonVisibility()

    def addBand(self, comboBox):

        #if self.modelAttr.getRasterIndex():
        self.modelAttr.addRasterBands(comboBox)
        # else:
        #     comboBox.addItem(_fromUtf8(_('---- Select bands ----')))
        #     comboBox.addItem(_fromUtf8(_('Band 1')))
        #     item = comboBox.model().item(1, 0)
        #     item.setCheckState(True)

    def checkStatistics(self):
        """!
        @brief Checks the result of computing statistics.
        """
        value = self.modelAttr.getStatisticsResult()
        self.rasterGroupBox.setEnabled(not value)
        self.statisticsGroupBox.setEnabled(not value)
        self.runButton.setEnabled(not value)
        self.okButton.setEnabled(not value)

    def handleItemPressed(self, comboBox):
        list = self.sender()
        if list.selectedIndexes():
            index = list.selectedIndexes()[0]
            item = comboBox.model().itemFromIndex(index)
            if comboBox.objectName().contains('ndvi', Qt.CaseSensitive):
                valueNdvi = self.modelAttr.checkIsNdvi(comboBox.objectName(), item.row(), item.checkState())
                item.setCheckState(valueNdvi)
            elif comboBox.objectName().contains('zabud', Qt.CaseSensitive):
                valueZabud = self.modelAttr.checkIsZabud(comboBox.objectName(), item.row(), item.checkState())
                item.setCheckState(valueZabud)
            else:
                item.setCheckState(not item.checkState())


    def addDatabases(self):
        """!
        @brief Add database name to comboBox.
        """
        self.modelAttr.addDbFile(self.dbComboBox)


    def addRasters(self):
        """!
        @brief Add file name to comboBox.
        """
        self.modelAttr.addRasterFile(self.rasterComboBox, False)


    def startStatistics(self):
        """!
        @brief Start computing statistics.
        """
        self.modelAttr.saveRasterInput(self.rasterComboBox.currentText())
        if self.meanCheckBox.isChecked():
            bands = self.modelAttr.getBandList(self.meanComboBox)
            for band in bands:
                self.startMean(band)
        if self.stddevCheckBox.isChecked():
            bands = self.modelAttr.getBandList(self.stddevComboBox)
            for band in bands:
                print band
                self.startStdDev(band)
        if self.ndviCheckBox.isChecked():
            bands = self.modelAttr.getBandList(self.ndviComboBox)
            self.startNdvi(bands)
        if self.zabudCheckBox.isChecked():
            bands = self.modelAttr.getBandList(self.zabudComboBox)
            self.startZabud(bands)
        self.modelAttr.saveStatistics()
        self.checkStatistics()

    def startMean(self, bandNumber):
        self.runButton.setEnabled(False)
        self.okButton.setEnabled(False)
        QApplication.processEvents()
        raster = agroeye.Raster(self.modelAttr.getPathToRaster())
        parameters = agroeye.StatsParameters()
        parameters.addMeanBand(bandNumber)
        spatialite = agroeye.Sqlite(u'%s' % self.dbComboBox.currentText(), True)
        stats = agroeye.StatsComputation(raster, parameters, spatialite.openRepo())
        progressCallback = ProgressCallback(self.progressBar)

        def progress(value, types, message):
            progressCallback.callback(value, types, message)
            if message:
                self.progresLabel.setText(message)

        stats.setCallback(progress)
        stats.start()

    def startStdDev(self, bandNumber):
        self.runButton.setEnabled(False)
        self.okButton.setEnabled(False)
        QApplication.processEvents()
        raster = agroeye.Raster(self.modelAttr.getPathToRaster())
        parameters = agroeye.StatsParameters()
        parameters.addStddevBand(bandNumber)
        spatialite = agroeye.Sqlite(u'%s' % self.dbComboBox.currentText(), True)
        stats = agroeye.StatsComputation(raster, parameters, spatialite.openRepo())
        progressCallback = ProgressCallback(self.progressBar)

        def progress(value, types, message):
            progressCallback.callback(value, types, message)

        stats.setCallback(progress)
        stats.start()

    def startZabud(self, bands):
        self.runButton.setEnabled(False)
        self.okButton.setEnabled(False)
        QApplication.processEvents()
        raster = agroeye.Raster(self.modelAttr.getPathToRaster())
        parameters = agroeye.StatsParameters()
        parameters.setZabud(bands[0], bands[1], bands[2], bands[3])
        spatialite = agroeye.Sqlite(u'%s' % self.dbComboBox.currentText(), True)
        stats = agroeye.StatsComputation(raster, parameters, spatialite.openRepo())
        progressCallback = ProgressCallback(self.progressBar)

        def progress(value, types, message):
            progressCallback.callback(value, types, message)
            if message:
                self.progresLabel.setText(message)

        stats.setCallback(progress)
        stats.start()

    def startNdvi(self, bands):
        """!
        @brief Start computing statistics.
        """
        self.okButton.setEnabled(False)
        QApplication.processEvents()
        raster = agroeye.Raster(self.modelAttr.getPathToRaster())
        parameters = agroeye.StatsParameters()
        parameters.setNdvi(bands[0], bands[1])
        spatialite = agroeye.Sqlite(u'%s' % self.dbComboBox.currentText(), True)
        stats = agroeye.StatsComputation(raster, parameters, spatialite.openRepo())
        progressCallback = ProgressCallback(self.progressBar)

        def progress(value, types, message):
            progressCallback.callback(value, types, message)
            if message:
                self.progresLabel.setText(message)

        stats.setCallback(progress)
        stats.start()

    def changeButtonVisibility(self):
        """!
        @brief Change button visibility.
        @param okButtonBool: value of button visibility (True/False).
        """

        if self.rasterComboBox.currentIndex()>=0 and self.dbComboBox.currentIndex()>=0:
            if self.meanCheckBox.isChecked():
                self.runButton.setEnabled(self.modelAttr.checkBand(self.meanComboBox))
            if self.stddevCheckBox.isChecked():
                self.runButton.setEnabled(self.modelAttr.checkBand(self.stddevComboBox))
            if self.ndviCheckBox.isChecked():
                self.runButton.setEnabled(self.modelAttr.checkBand(self.ndviComboBox))
            if self.zabudCheckBox.isChecked():
                self.runButton.setEnabled(self.modelAttr.checkBand(self.zabudComboBox))
        else:
            self.runButton.setEnabled(False)
        self.progressBar.setValue(0)