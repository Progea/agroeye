# -*- coding: utf-8 -*-

from PyQt4 import QtCore
from PyQt4 import QtGui
import ogr
_fromUtf8=QtCore.QString.fromUtf8

import measuredistancetool
from Control import exceptionModel
class MeasureAreaTool(measuredistancetool.MeasureDistanceTool):

    __metaclass__ = measuredistancetool.MeasureDistanceToolMetaclass

    def __init__(self, parent=None):
        super(MeasureAreaTool, self).__init__(parent)
        self.setWindowTitle(QtCore.QString(_fromUtf8(_("Measure Area"))))
        self.label.setText(QtCore.QString(_fromUtf8(_("Area"))))
        self.unitsComboBox.clear()
        units = [u'm\u00B2', u'a', u'da', u'ha', u'km\u00B2']
        for unit in units:
            self.unitsComboBox.addItem(QtCore.QString(unit))
        self.selectedArea = ogr.Geometry(ogr.wkbLinearRing)

        self.trackByMouseButton.setEnabled(False)

    def setMeasurement(self, givenArea=None):
        if givenArea:
            self.area = givenArea
            area = givenArea
        else:
            try:
                area = self.area
            except AttributeError:
                area = 0
        if self.unitsComboBox.currentText() == u'm\u00B2':
            area = str("%.0f" % round(area, 0))
        elif self.unitsComboBox.currentText() == u'a':
            area /= 100
            area = str("%.2f" % round(area, 2))
        elif self.unitsComboBox.currentText() == u'da':
            area /= 1000
            area = str("%.3f" % round(area, 3))
        elif self.unitsComboBox.currentText() == u'ha':
            area /= 10000
            area = str("%.4f" % round(area, 4))
        elif self.unitsComboBox.currentText() == u'km\u00B2':
            area /= 1000000
            area = str("%.6f" % round(area, 6))

        self.calculatedDistanceField.setText(QtCore.QString(area))

    def addPoint(self, event):
        x, y = self.viewArea.getMouseCoordinates(event.pos())
        self.selectedArea.AddPoint(x, y)
        area = self.selectedArea.Area()
        try:
            self.setMeasurement(area)
        except AttributeError:
            pass
        self.redrawFeature()

    def redrawFeature(self):
        if self.selectedArea.GetPointCount() >= 3:
            polygon = ogr.Geometry(ogr.wkbPolygon)
            linearring = self.selectedArea.Clone()
            linearring.CloseRings()
            polygon.AddGeometry(linearring)
            try:
                viewArea = polygon.Intersection(self.viewArea.viewBoundary)
                if viewArea == None or not viewArea.IsValid():
                    raise exceptionModel.PolygonInvalidError
            except exceptionModel.PolygonInvalidError as e:
                QtGui.QMessageBox.critical(self, '%s'%e.title, '%s'%e.msg, QtGui.QMessageBox.Ok)
                self.clearPoints()
                return

            viewAreaBoundary = viewArea.GetBoundary()
            super(MeasureAreaTool, self).redrawFeature(viewAreaBoundary)
        elif self.selectedArea.GetPointCount() == 2:
            super(MeasureAreaTool, self).redrawFeature(self.selectedArea)
    #
    # def trackPosition(self, event):
    #     if self.selectedArea.GetPointCount() >= 2:
    #         points = self.selectedArea.GetPoints()
    #
    #     self.selectedArea.Empty()
    #     for point in points:
    #         self.selectedArea.AddPoint(point[0], point[1])
    #     x, y = self.viewArea.getMouseCoordinates(event.pos())
    #     self.selectedArea.AddPoint(x, y)
    #     self.selectedArea.CloseRings()
    #     area = self.selectedArea.Area()
    #     self.setMeasurement(area)
    #     self.clearView()
    #     self.redrawFeature()


    def clearPoints(self):
        self.selectedArea.Empty()
        self.area = 0

        super(MeasureAreaTool, self).clearPoints()

    def closeEvent(self, event):
        super(MeasureAreaTool, self).closeEvent(event)
        self.clearPoints()
