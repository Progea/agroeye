# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import ogr
import osr
import numpy as np
from View import ui_addnewobjecttolayer
from Control import exceptionModel
from Logic import vectorTableMechanism
from Logic import saveVectorLayerMechanism



import os

_fromUtf8 = QString.fromUtf8

try:
    _encoding = QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig)

class AddNewVectorObjectToLayer(QDialog, ui_addnewobjecttolayer.Ui_addNewObjectToLayerDialog):
    """!
    Class is used to add object during creating new vector file.
    """
    def __init__(self, parent=None):
        super(AddNewVectorObjectToLayer, self).__init__(parent, flags = Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.viewArea = self.parent.parentMain.viewArea
        self.pathTemp = self.viewArea.parent.tempDir
        self.setupUi(self)
        self.fieldName = self.parent.fieldName
        ## @var self.fieldName
        # Name of field.
        self.fieldTypeCol = self.parent.fieldTypeCol
        ## @var self.fieldTypeCol
        # Type of field.
        self.fieldWidth = self.parent.fieldWidth
        ## @var self.fieldWidth
        # Width of field
        self.fieldPrec = self.parent.fieldPrec
        ## @var self.fieldPrec
        # Precision of field.
        self.geomType = self.parent.geomType
        ## @var self.geomType
        # Geometry type.
        self.fieldVals = []
        ## @var self.fieldType
        # Values of objects.
        self.srs = self.parent.srs
        self.epsg = self.parent.epsg
        self.rowSelected = 0
        self.validateClass = vectorTableMechanism.ValidateClass(self)
        self.createActions()
        self.createToolbar()
        self.addDefaultDataToTable()


    def addDefaultDataToTable(self):
        """!
        @brief Add default data to Coordinates and Attribute Table.
        """
        self.setDefaultTableAction(self.coordTableWidget, 4, [_fromUtf8(_('Id_Object')), 'X', 'Y', _fromUtf8(_('Action'))], vectorTableMechanism.CoordinateDelegate)
        self.newObjectWidget.setCurrentIndex(0)
        self.coordTableWidget.setSelectionBehavior(QAbstractItemView.SelectItems)
        self.connect(self.coordTableWidget, SIGNAL("itemChanged(QTableWidgetItem *)"), self.changeRow)
        self.setDefaultTableAction(self.attrTableWidget, len(self.fieldName), self.fieldName, vectorTableMechanism.VariantDelegate)


    def setDefaultTableAction(self, table, numberCol, nameCol, action):
        """!
        @brief Set default action for table.
        @param table: table object to set action.
        @param numberCol: number of columns in table.
        @param nameCol: name of columns in table.
        @param action: table action to delegate.
        """
        table.clear()
        table.setColumnCount(numberCol)
        table.setSortingEnabled(False)
        table.setHorizontalHeaderLabels(nameCol)
        table.setEditTriggers(QTableWidget.NoEditTriggers)
        table.setItemDelegate(action(self))


    def addButtonToTable(self):
        """!
        @brief Add button remove to Coordinates Table
        """
        widget = QWidget()
        layout = QHBoxLayout()
        delButton = QToolButton()
        delButton.setFixedSize(QSize(50,15))
        delButton.setObjectName('%s'%(self.coordTableWidget.rowCount()-1))
        delButton.setIcon(QIcon(':/menuTools/resources/rubbish.png'))
        delButton.clicked.connect(self.geometry.deleteVerticleAction)
        delButton.setToolTip(_fromUtf8(_('Delete point')))
        layout.addWidget(delButton)
        widget.setLayout(layout)
        return widget

    def addCoordToTable(self, x, y):
        """!
        @brief Add coordinates to Coordinates Table, after clicked on View.
        """
        allPoint = self.coordTableWidget.rowCount()
        self.coordTableWidget.insertRow(allPoint)
        listNameItem = [self.geometry.objectId, y, x]
        for i, item in enumerate(listNameItem):
            self.coordTableWidget.setItem(allPoint, i, QTableWidgetItem(str(item)))

        widget = self.addButtonToTable()
        widget.setEnabled(False)
        self.coordTableWidget.setCellWidget(allPoint, 3, widget)
        self.updateRowNumber()

    def addAttribute(self):
        """!
        @brief Add data to Attribute Table.
        """
        self.viewArea.setCursor(QCursor(Qt.CrossCursor))
        self.addObjectToolButton.setEnabled(True)

        self.disconnect(self.viewArea, SIGNAL("mouseClicked(QEvent)"), self.geometry.getCoordinate)
        self.disconnect(self.viewArea, SIGNAL("mouseRightClicked(QEvent)"), self.addAttribute)

        self.attributeDialog = Attribute(self.parent)
        self.attributeDialog.show()
        self.connect(self.attributeDialog, SIGNAL("attributeAccept()"), self.geometry.saveObject)
        self.connect(self.attributeDialog, SIGNAL("attributeNotAccept()"), lambda: self.geometry.Objects.deleteObject(self.coordTableWidget.rowCount()-1, self.geometry.verticleCount, 0))
        if not self.attributeDialog.exec_():
            self.disconnect(self.attributeDialog, SIGNAL("attributeAccept()"), self.geometry.saveObject)
            self.disconnect(self.attributeDialog, SIGNAL("attributeNotAccept()"), lambda: self.geometry.Objects.deleteobject(self.coordTableWidget.rowCount()-1, self.geometry.verticleCount, 0))
            self.coordTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
            self.saveToolButton.setEnabled(True)

    def createToolbar(self):
        """!
        @brief Create toolbar for create vector layer
        """

        self.addObjectToolButton.setDefaultAction(self.objectAction)
        self.addObjectToolButton.setFixedSize(QSize(35,35))
        self.saveToolButton.setDefaultAction(self.saveObjectAction)
        self.saveToolButton.setFixedSize(QSize(35,35))
        self.saveToolButton.setEnabled(False)

    def createActions(self):
        """!
        @brief Create action for QToolButton on Toolbar.
        """
        self.objectAction = QAction(_fromUtf8(_('Draw object')), self)
        self.objectAction.setIcon(QIcon(':/menuEditShape/resources/addObject.png'))
        self.geometry = Object(self, self.geomType)
        self.saveObjectAction = QAction(_fromUtf8(_('Save')), self)
        self.saveObjectAction.triggered.connect(self.saveFile)
        self.saveObjectAction.setIcon(QIcon(':/menuFile/resources/save.png'))

    def setEnabledFunction(self, value):
        """!
        @brief Set enabled/disabled remove button.
        @param value: True or False value.
        """
        for i in range(self.coordTableWidget.rowCount()):
            widget = self.coordTableWidget.cellWidget(i, 3)
            widget.setEnabled(value)

    def setEditableTable(self, value):
        """!
        @brief Set editable Coordinates and Attribute Table.
        @param value:
        """
        self.coordTableWidget.setEditTriggers(QTableWidget.AllEditTriggers)
        self.attrTableWidget.setEditTriggers(QTableWidget.AllEditTriggers)

        for i in range(self.coordTableWidget.rowCount()):
           itemX = self.coordTableWidget.item(i, 0)
           itemX.setFlags(Qt.ItemIsEnabled)

    def updateRowNumber(self):
        """!
        @brief Update row numbers after removed.
        """
        allPoint = self.coordTableWidget.rowCount()
        listCoordHeader = [str(i) for i in range(allPoint)]
        listCoordHeader = QStringList(listCoordHeader)

        listAttrHeader = [str(i) for i in range(self.geometry.objectId)]
        listAttrHeader = QStringList(listAttrHeader)
        self.coordTableWidget.setVerticalHeaderLabels(listCoordHeader)
        self.attrTableWidget.setVerticalHeaderLabels(listAttrHeader)

        for i in range(allPoint):
             widget = self.coordTableWidget.cellWidget(i, 3)
             buttonList= widget.children()
             for button in buttonList:
                button.setObjectName('%s'%i)

    def updateObjectId(self):
        """!
        @brief Update object_id after removed.
        """
        listIdHeader = []

        keyList = self.geometry.verticlesId.keys()
        valueList = self.geometry.verticlesId.values()

        for obj in range(self.geometry.objectId):
            lenVert = valueList[obj]
            self.geometry.verticlesId[obj] = self.geometry.verticlesId[keyList[obj]]
            for coord in range(lenVert):
                 listIdHeader.append(str(obj))
        for i, elem in enumerate(listIdHeader):
             item1 = QTableWidgetItem(str(elem))
             self.coordTableWidget.setItem(i, 0, item1)

    def changeRow(self, item):
        """!
        @brief Change row in tables.
        """
        selectedRow = item.row()
        idObjectCoord = int((self.coordTableWidget.item(selectedRow, 0)).text())
        idInObject = selectedRow

        for key in self.geometry.verticlesId:
            if key < idObjectCoord:
                idInObject = idInObject - self.geometry.verticlesId[key]

        self.objectChange(item, idObjectCoord, idInObject)

    def objectChange(self, item, idObject, idVerticle):
        """!
        @brief Redraw all lines after removed one point from line.
        """
        if item.column() in (1, 2):
            if sum(self.geometry.verticlesId.values()) == self.coordTableWidget.rowCount():
                if item.column() == 1:
                    x = float(item.text())
                    y = self.geometry.objectCoord[idObject][idVerticle][1]
                elif item.column() == 2:
                    x = self.geometry.objectCoord[idObject][idVerticle][0]
                    y = float(item.text())
                self.geometry.objectCoord[idObject][idVerticle] = (x, y)

                self.geometry.drawAllObject()

    def saveFile(self):
        """!
        @brief Save drawn object as Shapefile.
        """
        path = self.parent.parentMain.lastDirPath
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFiles)
        dialog.setDirectory(path)

        filename, extension = dialog.getSaveFileNameAndFilter(self, _fromUtf8(_("Save file")), path, filter=self.tr('ESRI Shapefile (*.shp);;Keyhole Markup Language (*.kml);;Geography Markup Language (*.gml)'))

        head, tail  = os.path.split(str(filename))
        root, ext = os.path.splitext(tail)

        self.geomData = self.geometry.setCoordinate()
        self.setAttribute()

        saveVector = saveVectorLayerMechanism.NewVector(self.parent)
        if not filename.isEmpty():
            if saveVector.checkFileName(root):
                path = head

                if not self.epsg == self.viewArea.projectionEPSG:
                    self.convertCoord(self.viewArea.projectionEPSG, self.epsg)

                if saveVector.createNewShapefileWithField(filename, self, self.parent.parentMain):
                    self.saveToolButton.setEnabled(False)
                    self.disconnect(self.viewArea, SIGNAL("mouseClicked(QEvent)"), self.geometry.getCoordinate)
                    self.disconnect(self.viewArea, SIGNAL(self.geometry.signal), self.geometry.drawObject)

                    self.selectedLine = []
                    self.selectedPoint = []
                    self.selectedPoly = []
            else:
                self.saveFile()

    def setAttribute(self):
        """!
        @brief Set value in Attribute Table.
        """

        for col in range(self.attrTableWidget.columnCount()):
            tempVals = []
            for row in range(self.attrTableWidget.rowCount()):
                item = self.attrTableWidget.item(row, col)
                type = item.data(0).typeName()
                if type == 'QDate':
                    text = item.text().split('-')
                    val = QDate(int(text[0]), int(text[1]), int(text[2]))
                else:
                    val = self.validateClass.setValueFromType(type, item.text())
                tempVals.append(val)
            self.fieldVals.append(tempVals)

    def convertCoord(self, drawEPSG, newEPSG):
        """!
        @brief Converts drawed cooridnates to new coordinates.
        @param drawEPSG: old coordinates system saved as EPSG code.
        @param newEPSG: new coordinates system saved as EPSG code.
        """
        spatialReferenceOld = osr.SpatialReference()
        spatialReferenceOld.ImportFromEPSG(drawEPSG)
        spatialReferenceNew = osr.SpatialReference()
        spatialReferenceNew.ImportFromEPSG(newEPSG)
        coordTransform = osr.CoordinateTransformation(spatialReferenceOld, spatialReferenceNew)

        if self.selectedPoint:
            self.selectedPoint = self.transform(self.selectedPoint, coordTransform)
        elif self.selectedLine:
            self.selectedLine = self.transform(self.selectedLine, coordTransform)
        elif self.selectedPoly:
            self.selectedPoly = self.transform(self.selectedPoly, coordTransform)

    def transform(self, listGeom, coordTransform):
        """!
        @brief Transforms coordinates.
        @param listGeom: list of coordinates.
        @param coordTransform: spatial reference of transformed file.
        """
        tempGeom = []
        for point in listGeom:
            temp = coordTransform.TransformPoint(point[0], point[1])
            tempGeom.append((temp[0], temp[1]))
        return tempGeom

    def closeEvent(self, QCloseEvent):
        """!
        @brief Close event. Disconnect graphic signal.
        """
        self.disconnect(self.viewArea, SIGNAL("mouseClicked(QEvent)"), self.geometry.getCoordinate)
        self.disconnect(self.viewArea, SIGNAL("mouseRightClicked(QEvent)"), self.addAttribute)
        self.disconnect(self.viewArea, SIGNAL(self.geometry.Objects.signal), self.geometry.drawObject)

        self.viewArea.setCursor(QCursor(Qt.CrossCursor))
        self.viewArea.setCursor(Qt.CrossCursor)
        self.clear()
        self.close()
        self.viewArea.setCursor(Qt.CrossCursor)
        self.viewArea.setContextMenuPolicy(Qt.DefaultContextMenu)

    def clear(self):
        """!
        @brief Clear QGraphicsView, remove all drawn figures.
        """
        self.disconnect(self.viewArea, SIGNAL("mouseClicked(QEvent)"), self.geometry.getCoordinate)
        items = self.viewArea.scene.items()[:-1]
        for item in items:
            self.viewArea.scene.removeItem(item)

class Object:
    def __init__(self, parent, type):
        self.parent = parent
        self.alpha = 80
        self.objectId = 0
        self.verticlesNumber = 0
        self.objectSelected = []
        self.objectCoord = []
        self.verticlesId = {}
        self.Objects = self.setTypeOfObject(type)

        self.penStyle = QPen()
        self.penStyle.setColor(Qt.blue)
        self.penStyle.setWidth(2)

        self.brushStyle = QBrush()
        self.brushStyle.setColor(QColor(0, 255, 255, self.alpha))
        self.brushStyle.setStyle(Qt.SolidPattern)
        self.parent.objectAction.triggered.connect(self.connectObject)


    def setTypeOfObject(self, type):
        """!
        @brief Set geometry types of object.
        @param type: geometry type.
        """
        if type == 1:
            return Point(self)
        elif type == 2:
            return Line(self)
        elif type == 3:
            return Polygon(self)

    def connectObject(self):
        """!
        @brief Emit signal for add object.
        """

        ## self.parent.addObjectToolButton.setEnabled(False) for point object
        self.parent.saveToolButton.setEnabled(False)
        self.parent.objectAction.setEnabled(True)
        self.parent.connect(self.parent.viewArea, SIGNAL("mouseClicked(QEvent)"), self.getCoordinate)
        self.parent.connect(self.parent.viewArea, SIGNAL(self.Objects.signal), self.drawObject)
        self.parent.viewArea.setCursor(Qt.PointingHandCursor)
        self.parent.setEnabledFunction(False)
        self.parent.attrTableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.parent.coordTableWidget.setEditTriggers(QTableWidget.NoEditTriggers)

    def getCoordinate(self, event):
        """!
        @brief Add coordinates of object, coordinates are from mouse clicked on the QGraphicsView.
        @param event: Mouse click wheel triggers this event
        """
        x, y = self.parent.viewArea.getMouseCoordinates(event.pos())
        self.objectSelected.append((x, y))
        self.parent.addCoordToTable(x, y)
        self.verticlesNumber += 1
        self.Objects.drawOneObject(self.objectSelected)

    def setCoordinate(self):
        """!
        @brief Returns points coordinates as numpy array.
        """
        return np.array(self.objectCoord)

        #self.cleanLayer() for pol
        #self.close() for pol

    def getCoordinateId(self, rowNumber):
        """!
        @brief Returns id of object based on selected row.
        @param rowNumber: index of selected row.
        """
        return int((self.parent.coordTableWidget.item(rowNumber, 0)).text())

    def getAfterAndBeforeId(self, rowNumber):
        """!
        @brief Returns id of objects before and after selected row.
        @param rowNumber: index of selected row.
        """
        idBefore = -1
        idAfter = -1

        if rowNumber > 0:
            idBefore = self.getCoordinateId(rowNumber - 1)

        if rowNumber < (self.parent.coordTableWidget.rowCount() - 1):
            idAfter = self.getCoordinateId(rowNumber + 1)

        return idBefore, idAfter

    def deleteVerticleAction(self):
        """!
        @brief Action for deleting verticle of object.
        """
        sender = self.parent.sender()
        rowSelected = int(sender.objectName())
        msgRow = int(rowSelected)
        reply = QMessageBox.warning(self.parent, _fromUtf8(_('Confirm delete row')), _fromUtf8(_('Are you sure you want to delete row number %d?') % msgRow),
                                    QMessageBox.Yes | QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.Objects.deleteObject(rowSelected, 1, 1)

        if self.objectId == 0:
            self.parent.saveToolButton.setEnabled(False)
        self.parent.setEnabledFunction(True)
        self.parent.setEditableTable(True)
        self.parent.updateRowNumber()
        self.parent.updateObjectId()

    def deleteVerticleFromTable(self, rowNumber, objectNumber, verticleNumber):
        """!
        @brief Delete verticle from Coordinates Table.
        @param rowNumber: index of selected row.
        @param objectNumber: id of object.
        @param verticleNumber: number of verticle in object.
        """
        self.parent.coordTableWidget.removeRow(rowNumber)
        del self.objectCoord[objectNumber][verticleNumber]
        self.verticlesId[objectNumber] = self.verticlesId[objectNumber]-1
        self.objectSelected = []

    def deleteObjectFromTableWithAttribute(self, rowNumber, objectNumber):
        """!
        @brief Delete object from Coordinates Table.
        @param rowNumber: index of selected row.
        @param objectNumber: id of object.
        """
        self.parent.attrTableWidget.removeRow(objectNumber)
        for number in rowNumber:
            self.parent.coordTableWidget.removeRow(number)
        del self.objectCoord[objectNumber]
        del self.verticlesId[objectNumber]
        self.objectId -= 1
        self.parent.updateObjectId()

    def deleteObjectFromTableWithoutAttribute(self, verticlesNumber):
        """!
        @brief Delete object from Coordinates Table and Attribute Table.
        @param numVertices: number of verticles of deleted object.
        """
        self.objectSelected = []
        for i in range(verticlesNumber):
            self.parent.coordTableWidget.removeRow(self.parent.coordTableWidget.rowCount()-1)

    def deleteObjectFromScene(self, rowNumber):
        """!
        @brief Delete object from scene.
        @param rowNumber: index of selected row.
        """
        items = self.parent.viewArea.scene.items()[:-1]
        itemId = (len(items) - 1) - rowNumber
        self.parent.viewArea.scene.removeItem(items[itemId])

    def deleteAllObjectsFromScene(self):
        """!
        @brief Delete all objects from scene.
        """
        items = self.parent.viewArea.scene.items()[:-1]
        for item in items:
            self.parent.viewArea.scene.removeItem(item)

    def saveObject(self):
        """!
        @brief Save objects as shapefile.
        """
        self.parent.saveToolButton.setEnabled(False)
        attribute = self.parent.attributeDialog.getAttribute()
        self.parent.attrTableWidget.insertRow(self.objectId)

        for key, value in attribute.iteritems():
            for col in range(self.parent.attrTableWidget.columnCount()):
                if key == self.parent.attrTableWidget.horizontalHeaderItem(col).text():
                    item = QTableWidgetItem(value)
                    self.parent.attrTableWidget.setItem(self.objectId, col, item)

        self.verticlesId[self.objectId] = self.verticlesNumber
        self.objectId+= 1
        self.objectCoord.append(self.objectSelected)
        self.objectSelected = []
        self.verticlesNumber= 0
        self.parent.updateRowNumber()
        self.parent.setEnabledFunction(True)
        self.parent.saveToolButton.setEnabled(True)
        self.parent.setEditableTable(True)
        self.parent.viewArea.setContextMenuPolicy(Qt.NoContextMenu)

    def drawObject(self):
        """!
        @brief Action for drawing objects on scene.
        """
        self.Objects.drawOneObject(self.objectSelected)


class Point:
    def __init__(self, parent):
        self.object = parent
        self.viewArea = parent.parent.viewArea
        self.addNewObject = parent.parent
        self.signal = 'pointAdd()'

    def drawOneObject(self, selectedPoint):
        """!
        @brief Draw single object.
        @param selectedPoint: coordinates of drawn point.
        """
        x1, y1 = self.viewArea.revertCoordinates(selectedPoint[0][0], selectedPoint[0][1])
        self.viewArea.scene.addEllipse(x1, y1, 5, 5, self.object.penStyle, self.object.brushStyle)
        self.addNewObject.addAttribute()

    def drawAllObject(self):
        """!
        @brief Redraw all points.
        """
        self.object.deleteAllObjectsFromScene()
        for object in self.object.objectCoord:
            for coord in range(len(object) - 1):
                x, y = self.viewArea.revertCoordinates(object[coord][0], object[coord][1])
            self.viewArea.scene.addEllipse(x, y, 5, 5, self.object.penStyle, self.object.brushStyle)

    def deleteObject(self, rowNumber,  numVertices, signal,):
        """!
        @brief Delete object, from selected row.
        @param rowNumber: index of selected row.
        @param numVertices: number of vertices.
        @param signal: signal to remove object.
        """
        self.object.deleteObjectFromScene(rowNumber)
        if signal:
            self.object.deleteObjectFromTableWithAttribute([rowNumber], rowNumber)
        self.object.objectSelected = []



class Line:
    def __init__(self, parent):
        self.object = parent
        self.viewArea = parent.parent.viewArea
        self.addNewObject = parent.parent
        self.signal = 'lineAdd()'


    def drawOneObject(self, selectedLine):
        """!
        @brief Draw single object.
        @param selectedLine: coordinates of drawn line.
        """
        points = len(selectedLine)
        if points > 1:
            if points == 2:
                self.addNewObject.connect(self.viewArea, SIGNAL("mouseRightClicked(QEvent)"), self.addNewObject.addAttribute)
            x1, y1 = self.viewArea.revertCoordinates(selectedLine[points - 2][0], selectedLine[points - 2][1])
            x2, y2 = self.viewArea.revertCoordinates(selectedLine[points - 1][0], selectedLine[points - 1][1])
            self.viewArea.scene.addLine(x1, y1, x2, y2, self.object.penStyle)

    def deleteObject(self, rowNumber, verticlesNumber, signal):
        """!
        @brief Delete object, from selected row.
        @param rowNumber: index of selected row.
        @param verticlesNumber: number of vertices.
        @param signal: signal to remove object.
        """
        idObjectCoord = self.object.getCoordinateId(rowNumber)
        idInObject = rowNumber

        if signal:
            for key in self.object.verticlesId:
                if key < idObjectCoord:
                    idInObject = idInObject - self.object.verticlesId[key]
            countVert = self.object.verticlesId[idObjectCoord]


            if countVert < 3:
                coordList = []
                idBefore, idAfter = self.object.getAfterAndBeforeId(rowNumber)
                if idBefore == idObjectCoord:
                    coordList.extend((rowNumber-1, rowNumber-1))
                if idAfter == idObjectCoord:
                    coordList.extend((rowNumber+1, rowNumber))
                self.object.deleteObjectFromTableWithAttribute(coordList, idObjectCoord)
            else:
                self.object.deleteVerticleFromTable(rowNumber, idObjectCoord, idInObject)
        else:
            self.object.deleteObjectFromTableWithoutAttribute(verticlesNumber)
        self.drawAllObject()

    def drawAllObject(self):
        """!
        @brief Redraw all lines.
        """
        self.object.deleteAllObjectsFromScene()
        for object in self.object.objectCoord:
            for coord in range(len(object) - 1):
                x1, y1 = self.viewArea.revertCoordinates(object[coord][0], object[coord][1])
                x2, y2 = self.viewArea.revertCoordinates(object[coord + 1][0], object[coord + 1][1])
                self.viewArea.scene.addLine(x1, y1, x2, y2, self.object.penStyle)



class Polygon:
    def __init__(self, parent):
        self.object = parent
        self.viewArea = parent.parent.viewArea
        self.addNewObject = parent.parent
        self.signal = 'polyAdd()'

    def drawOneObject(self, selectedPoly):
        """!
        @brief Draw single object.
        @param selectedPoly: coordinates of drawn polygon.
        """
        points = len(selectedPoly)
        if points == 2:
            x1, y1 = self.viewArea.revertCoordinates(selectedPoly[-2][0], selectedPoly[-2][1])
            x2, y2 = self.viewArea.revertCoordinates(selectedPoly[-1][0], selectedPoly[-1][1])
            self.viewArea.scene.addLine(x1, y1, x2, y2, self.object.penStyle)
        if points > 2:
            if points == 3:
                self.addNewObject.connect(self.viewArea, SIGNAL("mouseRightClicked(QEvent)"), self.addNewObject.addAttribute)
            items = self.viewArea.scene.items()[:-1]
            self.viewArea.scene.removeItem(items[0])

            if points > 3:
                if not self.checkPolygonIsValid(selectedPoly):
                    selectedPoly.pop()
            pol = QPolygon(len(selectedPoly))
            for i, point in enumerate(selectedPoly):
                x, y = self.viewArea.revertCoordinates(point[0], point[1])
                pol.setPoint(i, x, y)
            polF = QPolygonF(pol)
            self.viewArea.scene.addPolygon(polF, self.object.penStyle, self.object.brushStyle)

    def checkPolygonIsValid(self, polNew):
        """!
        @brief Checks if the polygon is valid. Returns True if polygon is valid, False if not valid.
        @param polNew: list of coordinates of new polygon.
        """

        geomNew = ogr.Geometry(ogr.wkbLinearRing)
        for point in polNew:
            geomNew.AddPoint(point[0], point[1])

        geomNew.CloseRings()
        polyNew = ogr.Geometry(ogr.wkbPolygon)
        polyNew.AddGeometry(geomNew)
        try:
            if not polyNew.IsValid():
                raise exceptionModel.PolygonInvalidError
        except exceptionModel.PolygonInvalidError as e:
            QMessageBox.critical(self.parent, '%s' % e.title, '%s' % e.msg, QMessageBox.Ok)
            return False
        return True

    def deleteObject(self, rowNumber, verticlesNumber, signal):
        """!
        @brief Delete object, from selected row.
        @param rowNumber: index of selected row.
        @param verticlesNumber: number of vertices.
        @param signal: signal to remove object.
        """
        idObjectCoord = int((self.addNewObject.coordTableWidget.item(rowNumber, 0)).text())
        idInObject = rowNumber

        if signal:
            for key in self.object.verticlesId:
                if key < idObjectCoord:
                    idInObject = idInObject - self.object.verticlesId[key]

            idBefore, idAfter = self.object.getAfterAndBeforeId(rowNumber)
            countVert = self.object.verticlesId[idObjectCoord]

            if countVert < 4:
                coordList = []
                if idBefore == idObjectCoord and idAfter != idObjectCoord:
                    coordList.extend((rowNumber, rowNumber-1, rowNumber-2))
                elif idAfter == idObjectCoord and idBefore != idObjectCoord:
                    coordList.extend((rowNumber+1, rowNumber+1, rowNumber))
                elif idAfter == idObjectCoord and idBefore == idObjectCoord:
                    coordList.extend((rowNumber, rowNumber, rowNumber-1))
                self.object.deleteObjectFromTableWithAttribute(coordList, idObjectCoord)
            else:
                self.object.deleteVerticleFromTable(rowNumber, idObjectCoord, idInObject)
        else:
            self.object.deleteObjectFromTableWithoutAttribute(verticlesNumber)
        self.drawAllObject()

    def drawAllObject(self):
        """!
        @brief Redraw polygons.
        """
        self.object.deleteAllObjectsFromScene()

        for object in self.object.objectCoord:
            pol = QPolygon(len(object))
            for coord in range(len(object)):
                x, y = self.viewArea.revertCoordinates(object[coord][0], object[coord][1])
                pol.setPoint(coord, x, y)
            polF = QPolygonF(pol)
            self.viewArea.scene.addPolygon(polF, self.object.penStyle, self.object.brushStyle)



class Attribute(QDialog):
     def __init__(self, parent):
        super(Attribute, self).__init__(parent, flags = Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setModal(True)
        self.setWindowTitle(_fromUtf8(_('Set attribute')))
        self.setMinimumSize(QSize(150,200))
        self.attribute = self.parent.fieldName
        self.acceptValue = 0
        self.attributeField = {}
        ## @var self.attributeField
        # Type of field.
        self.acceptFlag = 0
        for elem in self.attribute:
            self.attributeField['%s'%elem] = 0
        self.addWidget()



     def addWidget(self):
         """!
         @brief Add QLabel with Attribute Name and QLineEdit to set Attribute Name.
         """
         self.attrHorizontalLayout = QGridLayout(self)
         self.gridLayout = QGridLayout()
         self.lineList = []


         for i, attr in enumerate(self.attribute):
             label = QLabel('%s'%attr)
             self.gridLayout.addWidget(label, i, 0)
             text = QLineEdit()
             text.setObjectName('%s'%str(attr))
             type = self.parent.fieldTypeCol[i]
             regExp = self.getValidator(type, i)
             validator = QRegExpValidator(regExp, text)
             text.setValidator(validator)
             self.lineList.append(text)
             self.gridLayout.addWidget(text, i, 1)
         self.attrHorizontalLayout.addLayout(self.gridLayout,0,0,1,1)
         spacer = QSpacerItem(20,40,QSizePolicy.Minimum, QSizePolicy.Expanding)
         self.gridLayout.addItem(spacer)

         self.buttonHorizontalLayout = QHBoxLayout()
         self.applyButton = QPushButton(_fromUtf8(_("Apply")), self)
         self.buttonHorizontalLayout.addWidget(self.applyButton)
         self.closeButton = QPushButton(_fromUtf8(_("Close")), self)
         self.buttonHorizontalLayout.addWidget(self.closeButton)
         self.attrHorizontalLayout.addLayout(self.buttonHorizontalLayout,1,0,1,1)
         self.closeButton.clicked.connect(self.close)
         self.applyButton.clicked.connect(self.acceptAttribute)

     def acceptAttribute(self):
         """!
        @brief Action after click Accept Button.
        """
         for lineEdit in self.lineList:
             for key, value in self.attributeField.iteritems():
                if lineEdit.objectName() == key:
                    self.attributeField[key] = lineEdit.text()

         self.acceptValue = 1
         self.close()

     def closeWithoutSave(self):
         self.close()

     def closeEvent(self, event):
        """!
        @brief Close event. Disconnect graphic signal.
        """
        if self.acceptValue:
            self.emit(SIGNAL("attributeAccept()"))
        else:
            self.emit(SIGNAL("attributeNotAccept()"))
        self.close()


     def getAttribute(self):
         """!
         @brief Returns Attribute Field
         """
         return self.attributeField

     def getValidator(self, type, index):
        """!
        @brief Returns QRegExp validator, for selected field.
        """
        if type == 'Integer':
            return QRegExp(r'-?[0-9]{1,%s}'%str(self.parent.fieldWidth[index]))
        elif type == 'Real':
            length = str(int(self.parent.fieldWidth[index]) - 1)
            prec = str(self.parent.fieldPrec[index])
            return QRegExp(r'[-]?([1-9]{1}[0-9]{0,%s}(\.[0-9]{0,%s})?|0(\.[0-9]{0,%s})?|\.[0-9]{1,%s})$'%(length,prec, prec, prec))
        elif type == 'String':
            return QRegExp(r'.{1,%s}$'%str(self.parent.fieldWidth[index]))
        elif type == 'Date':
            return QRegExp(r'\d\d\d\d-\d\d-\d\d')
        elif type == 'Boolean':
            return QRegExp(r'(i)^\w[tru]|[fals]e$]')




