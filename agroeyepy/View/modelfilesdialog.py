# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from View import ui_modelinputrasterdialog
from View import ui_modeloutputdbdialog
from View import ui_segmentationtabledialog
from Control import db_connection as db

_fromUtf8 = QString.fromUtf8


class InputFiles(QDialog, ui_modelinputrasterdialog.Ui_modelInputRasterDialog):
    """!
    The class InputFiles handles input files window to set raster file using in segmentation.
    """
    def __init__(self, parent):
        super(InputFiles, self).__init__(parent, flags=Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.modelAttr = parent.modelAttr
        self.setupUi(self)
        self.rasterComboBox.currentIndexChanged.connect(lambda: self.modelAttr.setButtonOkVisibility(self.okButton, True))
        self.modelAttr.addRasterFile(self.rasterComboBox)
        self.cancelButton.clicked.connect(self.close)
        self.okButton.clicked.connect(self.saveParam)
        #self.modelComp.setButtonOkVisibility(self.okButton, False)

    def saveParam(self):
        """!
        @brief Save the properties.
        """
        value = self.modelAttr.saveRasterInput(self.rasterComboBox.currentText())
        self.modelAttr.setButtonOkVisibility(self.okButton, value)


class OutputFiles(QDialog, ui_modeloutputdbdialog.Ui_modelOutputDbDialog):
    """!
    The class OutputFiles handles output files window to set spatialite files creating as results of segmentation.
    """
    def __init__(self, parent):
        super(OutputFiles, self).__init__(parent, flags=Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.modelAttr = parent.modelAttr
        self.setupUi(self)
        self.cancelButton.clicked.connect(self.close)
        self.okButton.clicked.connect(self.saveParam)
        self.okButton.setEnabled(False)
        self.pathLineEdit.setText(self.modelAttr.dbPath)
        self.addPathToolButton.clicked.connect(self.addPath)
        self.pathLineEdit.textChanged.connect(self.changePath)

    def addPath(self):
        """!
        @brief Ooen the FileDialog to set file to save.
        """
        location = self.modelAttr.getPathToDatabase()
        path = QFileDialog.getSaveFileName(self, _fromUtf8(_('Save as')), location, _fromUtf8(_('SpatiaLite Database (*.spatialite)')))
        if path:
            self.pathLineEdit.setText(path)
            self.modelAttr.setButtonOkVisibility(self.okButton, True)

    def changePath(self):
        """!
        @brief Change path to file.
        """
        value = self.modelAttr.checkPathToDb(self.pathLineEdit.text())
        self.modelAttr.setButtonOkVisibility(self.okButton, value)

    def saveParam(self):
        """!
        @brief Save the properties.
        """
        value = self.modelAttr.savePathToDb(self.pathLineEdit.text())
        self.modelAttr.setButtonOkVisibility(self.okButton, value)


class TableView(QDialog, ui_segmentationtabledialog.Ui_segmentTableDialog):
    """!
    The class TableView handles table to show results of segmentation.
    """
    def __init__(self, parent):
        super(TableView, self).__init__(parent, flags=Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.parentMain = parent.parentMain
        self.setWindowTitle(_fromUtf8(_('Segmentation results')))
        self.setupUi(self)
        self.name = self.parent.dbPath
        ## @var self.name
        # Name of database.
        self.tableName = 'total_view'
        self.cancelButton.clicked.connect(self.closeAll)
        self.okButton.clicked.connect(self.setValue)
        self.openDatabase()
        self.initTableHeader()
        self.initLineEdit()

    def openDatabase(self):
        """!
        @brief Open the database.
        """
        self.database = db.ConnectDatabase(self.name, self.parent.segmentationType)

    def initTableHeader(self):
        """!
        @brief Set the initial table header.
        """
        self.minLineEdit.setValidator(QDoubleValidator())
        self.maxLineEdit.setValidator(QDoubleValidator())
        self.segmentTableWidget.horizontalHeader().setStyleSheet(
            "QHeaderView::section{background-color: #43a047;color:white;}")
        self.segmentTableWidget.verticalHeader().setStyleSheet(
            "QHeaderView::section{background-color: #43a047;color:white;}")
        self.segmentTableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.segmentTableWidget.setColumnCount(2)
        self.segmentTableWidget.setRowCount(self.database.rowCount(self.tableName))
        self.segmentTableWidget.setHorizontalHeaderLabels(['id', 'ndvi'])
        idData = self.database.readOneColumn('id', self.tableName)

        for i, value in enumerate(idData):
            self.segmentTableWidget.setItem(i, 0, QTableWidgetItem("%s" % value[0]))

        res = self.database.checkIfExist(str(self.parent.segmentationType + '_ndvi'))
        if not res:
            self.database.addNdviTable()

        ndviData = self.database.readOneColumn('ndvi', str(self.parent.segmentationType + '_ndvi'))
        for i, value in enumerate(ndviData):
            self.segmentTableWidget.setItem(i, 1, QTableWidgetItem("%.3f" % round(value[0], 5)))

    def initLineEdit(self):
        """!
        @brief Set the initial text in LineEdit.
        """
        if self.parent.minValue:
            self.minLineEdit.setText(str(self.parent.minValue))
        else:
            self.minLineEdit.setText("%.6s" % str(self.initMaxMin('Min')[0]))
        if self.parent.maxValue:
            self.maxLineEdit.setText(str(self.parent.maxValue))
        else:
            self.maxLineEdit.setText("%.6s" % str(self.initMaxMin('Max')[0]))

    def initMaxMin(self, type):
        """!
        @brief Returns the minimum or maximum value in the table.
        @param type: type of statistics (Max or Min)
        """
        value = self.database.countMinMaxValueInColumn('ndvi', str(self.parent.segmentationType + '_ndvi'), type)
        return value

    def contextMenuEvent(self, QContextMenuEvent):
        """!
        @brief Set context menu on table.
        """
        model = self.segmentTableWidget.selectionModel().selection().indexes()
        if model:
            if model[0].column() == 1:
                menu = QMenu(self)
                setMin = QAction(_fromUtf8(_('Set as min')), self)
                setMin.triggered.connect(self.setMin)
                menu.addAction(setMin)
                setMax = QAction(_fromUtf8(_('Set as max')), self)
                setMax.triggered.connect(self.setMax)
                menu.addAction(setMax)
                show = QAction(_fromUtf8(_('Show segment')), self)
                show.triggered.connect(self.showSegment)
                menu.addAction(show)
                menu.popup(QCursor.pos())

    def setMin(self):
        """!
        @brief Set selected value as minimum value in LineEdit.
        """
        self.minLineEdit.clear()
        minText = self.segmentTableWidget.currentItem().text()
        maxText = self.maxLineEdit.text()
        if maxText:
            if float(minText) > float(maxText):
                QMessageBox.warning(self, _fromUtf8(_('Min value')), _fromUtf8(_('The min value is greater than max.')), QMessageBox.Ok)
        self.minLineEdit.setText(minText)

    def setMax(self):
        """!
        @brief Set selected value as maximum value in LineEdit.
        """
        self.maxLineEdit.clear()
        maxText = self.segmentTableWidget.currentItem().text()
        minText = self.minLineEdit.text()
        if minText:
            if float(maxText) < float(minText):
                QMessageBox.warning(self, _fromUtf8(_('Max value')), _fromUtf8(_('The max value is lesser than min.')), QMessageBox.Ok)
        self.maxLineEdit.setText(maxText)

    def showSegment(self):
        """!
        @brief Show segments in AgroEye Scene.
        """
        row = self.segmentTableWidget.currentRow()
        id = self.segmentTableWidget.item(row, 0).text()
        geom = self.database.readGeometry(self.parent.segmentationType, 'total_view', id)
        #self.draw = Draw(self.parentMain.viewArea)
        #self.draw.drawPolygon(geom)

    def setValue(self):
        """!
        @brief Save parameters.
        """
        self.parent.minValue = float(self.minLineEdit.text())
        self.parent.maxValue = float(self.maxLineEdit.text())

        range = db.getValueFromRange('ndvi', str(self.parent.segmentationType + '_ndvi'), self.parent.minValue,
                                     self.parent.maxValue)
        print range

        self.parent.rasterOut.setPen(QPen(QColor(0, 153, 0), 2))
        self.close()

    def closeAll(self):
        """!
        @brief Close window with closing connection to database.
        """
        self.database.closeDatabase()
        self.close()
