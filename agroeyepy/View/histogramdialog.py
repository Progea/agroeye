# -*- coding: utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import ui_histogramdialog
_fromUtf8=QString.fromUtf8


class HistogramDialog(QDialog, ui_histogramdialog.Ui_histogramDialog):

    def __init__(self, parent, rowSelected):
        super(HistogramDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.file = self.parent.model().files[rowSelected]
        self.stretchClass = self.file.stretchClass
        self.setupUi(self)

        self.percentageClipSpinBox.setSuffix("%")
        self.histogramTabWidget.setTabBar(CustomTabBar(width=80, height=25))
        self.histogramTabWidget.setTabPosition(QTabWidget.West)
        self.LUTLayersTabWidget.setTabBar(CustomTabBar(width=80, height=25))
        self.LUTLayersTabWidget.setTabPosition(QTabWidget.West)

        self.plotsTabWidget.setCurrentIndex(0)
        self.canvas = MplCanvas()
        self.connect(self.applyButton, SIGNAL("clicked()"), self.accept)
        self.connect(self.file, SIGNAL("newLocalHistogram()"), self.redrawHistogram)
        self.connect(self, SIGNAL("areaOfStatisticsChanged(int)"), self.histogramSourceChanged)
        self.connect(self, SIGNAL("stretchAreaChanged(int)"), self.stretchAreaChanged)
        self.connect(self.noStretchRadioButton, SIGNAL("toggled(bool)"), self.setNone)
        self.connect(self.percentageClipRadioButton, SIGNAL("toggled(bool)"), self.setVisibledPercentage)
        self.connect(self.percentageClipRadioButton, SIGNAL("toggled(bool)"), self.percentageClipLabel.setEnabled)
        self.connect(self.standardDeviationStrectchRadioButton, SIGNAL("toggled(bool)"), self.setVisibledStandard)
        self.connect(self.standardDeviationStrectchRadioButton, SIGNAL("toggled(bool)"), self.standardDeviationLabel.setEnabled)
        if not self.file.isInt:
            self.statisticsComboBox.setDisabled(True)
        self.statisticsComboBox.setCurrentIndex(int(self.file.rasterAttributes.showLocalHistograms))
        self.baseAreaStretchComboBox.setCurrentIndex(int(self.file.rasterAttributes.localAreaOfStretch))
        self.activateWindow()
        self.update()

    def accept(self):
        """!
        @brief Action after click accept.
        """
        if self.noStretchRadioButton.isChecked():
            self.stretchClass.saveStretchType(0, None)
            self.file.rasterAttributes.setStretchType(0)
        elif self.percentageClipRadioButton.isChecked():
            self.stretchClass.saveStretchType(1, [self.percentageClipSpinBox.value(),self.percentageClipSpinBox.value()])
            self.file.rasterAttributes.setStretchType(1)
        elif self.standardDeviationStrectchRadioButton.isChecked():
            self.stretchClass.saveStretchType(2, [self.standardDeviationClipSpinBox.value(), 0])
            self.file.rasterAttributes.setStretchType(2)

        self.emit(SIGNAL("areaOfStatisticsChanged"), self.baseAreaStretchComboBox.currentIndex())
        self.redrawHistogram()
        self.emit(SIGNAL("histogramChanged()"))


    def setButtonForStretch(self):
        """!
        @brief Sets active button for stretch type.
        """
        if self.stretchClass.stretchType == self.stretchClass.none:
            self.noStretchRadioButton.setChecked(True)
        elif self.stretchClass.stretchType == self.stretchClass.percentClip:
            self.percentageClipRadioButton.setChecked(True)
        elif self.stretchClass.stretchType == self.stretchClass.standardDev:
            self.standardDeviationStrectchRadioButton.setChecked(True)


    def showHistogramPlot(self):
        """!
        @brief Show histogram.
        """
        self.setWindowTitle(_fromUtf8(_("Histogram - ")) + QString(self.file.name))
        self.setButtonForStretch()

        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        widget = QWidget()
        widget.setLayout(layout)
        self.histogramTabWidget.addTab(widget, QString(_fromUtf8(_("All Layers"))))
        self.plotHistogramAllLayers(self.histogramTabWidget, 0)

        for i, singleHistogram in enumerate(self.file.getHistogram(), 1):
            layout = QVBoxLayout()
            widget = QWidget()
            widget.setLayout(layout)
            self.histogramTabWidget.addTab(widget, QString(_fromUtf8(_("Layer %i")% i)))

        for i, singleLUT in enumerate(self.file.rasterAttributes.stretchLUTs, 1):
            layout = QVBoxLayout()
            widget = QWidget()
            widget.setLayout(layout)
            self.LUTLayersTabWidget.addTab(widget, QString(_fromUtf8(_("Layer %i") % i)))

        self.connect(self.LUTLayersTabWidget, SIGNAL("currentChanged(int)"),
                     lambda numberOfTab: self.redrawPlot(self.LUTLayersTabWidget, numberOfTab))
        self.connect(self.histogramTabWidget, SIGNAL("currentChanged(int)"),
                     lambda numberOfTab: self.redrawPlot(self.histogramTabWidget, numberOfTab))
        self.connect(self.plotsTabWidget, SIGNAL("currentChanged(int)"),
                     lambda: self.redrawPlot(self.plotsTabWidget))

    def redrawHistogram(self):
        """!
        @brief Redraw histogram.
        """
        self.setWindowTitle(QString(self.file.name + _fromUtf8(_(" - Histogram"))))
        self.setButtonForStretch()

        if self.LUTLayersTabWidget.isVisible():
            self.LUTLayersTabWidget.emit(SIGNAL("currentChanged(int)"), self.LUTLayersTabWidget.currentIndex())
        elif self.histogramTabWidget.isVisible():
            self.histogramTabWidget.emit(SIGNAL("currentChanged(int)"), self.histogramTabWidget.currentIndex())

    def redrawPlot(self, tabWidget, numberOfTab=0):
        if tabWidget is self.plotsTabWidget:
            if self.histogramTabWidget.isVisible():
                tabWidget = self.histogramTabWidget
                numberOfTab = self.histogramTabWidget.currentIndex()
            elif self.LUTLayersTabWidget.isVisible():
                tabWidget = self.LUTLayersTabWidget
                numberOfTab = self.histogramTabWidget.currentIndex()

        if tabWidget is self.histogramTabWidget and numberOfTab == 0:
            self.plotHistogramAllLayers(tabWidget, numberOfTab)
        else:
            layerNumber = numberOfTab - 1
            self.canvas = MplCanvas()
            if tabWidget is self.histogramTabWidget:
                if not self.file.rasterAttributes.showLocalHistograms:
                    self.canvas.ax.bar(self.file.rasterAttributes.histograms[layerNumber][0].astype(np.float64),
                                       self.file.rasterAttributes.histograms[layerNumber][1],
                                        color=self.file.rasterAttributes.showingBands.dict2Change[layerNumber])
                else:
                    self.file.rasterAttributes.localHistograms[layerNumber][0]
                    self.canvas.ax.bar(self.file.rasterAttributes.localHistograms[layerNumber][0].astype(np.float64),
                                       self.file.rasterAttributes.localHistograms[layerNumber][1],
                                        color=self.file.rasterAttributes.showingBands.dict2Change[layerNumber])
            elif tabWidget is self.LUTLayersTabWidget and self.file.isInt:
                xAxisMax = self.file.rasterAttributes.stretchLUTs[numberOfTab][-1]
                xAxis = np.linspace(0, xAxisMax, num=xAxisMax+1)
                self.canvas.ax.bar(xAxis[:-1], self.file.rasterAttributes.statistics[numberOfTab][4],
                                    color=self.file.rasterAttributes.showingBands.dict2Change[layerNumber])
                ax2 = self.canvas.ax.twinx()
                ax2.plot(xAxis, self.file.rasterAttributes.stretchLUTs[numberOfTab], color='k')
            elif tabWidget is self.LUTLayersTabWidget and not self.file.isInt:
                try:
                    self.canvas.ax.bar(self.file.rasterAttributes.defaultHistogram[numberOfTab][0].astype(np.float64),
                                       self.file.rasterAttributes.defaultHistogram[numberOfTab][1].astype(np.float64),
                                       color='k')
                    ax2 = self.canvas.ax.twinx()
                    ax2.plot(self.file.rasterAttributes.defaultHistogram[numberOfTab][0].astype(np.float64),
                             self.file.rasterAttributes.stretchLUTs[numberOfTab],
                             color=self.file.rasterAttributes.showingBands.dict2Change[layerNumber])
                except:
                    self.canvas.ax.bar(self.file.rasterAttributes.defaultHistogram[numberOfTab-1][0].astype(np.float64),
                                       self.file.rasterAttributes.defaultHistogram[numberOfTab-1][1].astype(np.float64),
                                       color='k')
                    ax2 = self.canvas.ax.twinx()
                    ax2.plot(self.file.rasterAttributes.defaultHistogram[numberOfTab-1][0].astype(np.float64),
                             self.file.rasterAttributes.stretchLUTs[numberOfTab-1])


            try:
                layout = tabWidget.widget(numberOfTab).layout()
            except:
                layout = tabWidget.widget(numberOfTab-1).layout()

            layout.addWidget(self.canvas)
            self.canvas.draw()

    def plotHistogramAllLayers(self, tabWidget, numberOfTab):
        """!
        @brief Plot histogram for all layers.
        """
        self.canvas = MplCanvas()
        for i, singleHistogram in enumerate(self.file.getHistogram(), 1):
            if not self.file.rasterAttributes.showLocalHistograms:
                self.canvas.ax.bar(singleHistogram[0].astype(np.float64), singleHistogram[1], label=_fromUtf8(_("Layer %i") % i),
                                    color=self.file.rasterAttributes.showingBands.dict2Change[i-1])
            else:
                self.canvas.ax.bar(self.file.rasterAttributes.localHistograms[i-1][0].astype(np.float64),
                                   self.file.rasterAttributes.localHistograms[i-1][1], label=_fromUtf8(_("Layer %i") % i),
                                   color=self.file.rasterAttributes.showingBands.dict2Change[i-1])
            self.canvas.ax.legend()

        layout = tabWidget.widget(numberOfTab).layout()
        layout.addWidget(self.canvas)
        self.canvas.draw()

    def histogramSourceChanged(self, statisticsItem):
        if statisticsItem == 0:
            self.file.rasterAttributes.showLocalHistograms = False
        elif statisticsItem == 1:
            self.file.rasterAttributes.showLocalHistograms = True

    def stretchAreaChanged(self, stretchItem):
        if stretchItem == 0:
            self.file.rasterAttributes.localAreaOfStretch = False
        elif stretchItem == 1:
            self.file.rasterAttributes.localAreaOfStretch = True

    def setNone(self, state):
        self.setVisibleSpinBox(not state, not state)

    def setVisibledStandard(self, state):
        self.setVisibleSpinBox(not state, state)
        self.standardDeviationClipSpinBox.setValue(self.stretchClass.standardDev.param[0])

    def setVisibledPercentage(self, state):
        self.setVisibleSpinBox(state, not state)
        self.percentageClipSpinBox.setValue(self.stretchClass.percentClip.param[0])

    def setVisibleSpinBox(self, percentBool, standDevBool):
        self.percentageClipSpinBox.setEnabled(percentBool)
        self.standardDeviationClipSpinBox.setEnabled(standDevBool)




class MplCanvasMetaclass(type(FigureCanvas)):
    """!
    @brief: Metaclass for MplCanvas. Provides model: only one matplotlib figure with
    subplot and axes per one histogram dialog. If new instance is called the previous
    plot is cleared (axes).
    """
    instance = None

    def __call__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super(MplCanvasMetaclass, cls).__call__(*args, **kwargs)
        else:
            cls.instance.ax.cla()
            try:
                ax2 = cls.instance.fig.axes[1]
                cls.instance.fig.delaxes(ax2)
            except:
                pass
        return cls.instance

class MplCanvas(FigureCanvas):
    """!
    @brief: Class used for plotting histograms and LUTs.
    """
    __metaclass__ = MplCanvasMetaclass

    def __init__(self):
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)

        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

class CustomTabBar(QTabBar):
    """!
    @brief: Customized tab bar of plots. Tab bar will be placed on the left side and new tabs will be
    added vertically (new one below previous one).
    """
    def __init__(self, parent=None, *args, **kwargs):
        self.tabSize = QSize(kwargs.pop("width"), kwargs.pop("height"))
        super(CustomTabBar, self).__init__(parent, *args, **kwargs)

    def paintEvent(self, event):
        painter = QStylePainter(self)
        option = QStyleOptionTab()

        for index in range(self.count()):
            self.initStyleOption(option, index)
            shapeTab = self.tabRect(index)
            painter.drawControl(QStyle.CE_TabBarTabShape, option)
            painter.drawText(shapeTab, Qt.TextDontClip | Qt.AlignVCenter | Qt.AlignCenter, self.tabText(index))
        painter.end()

    def tabSizeHint(self, index):
        return self.tabSize
