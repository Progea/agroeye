# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_AE_Window(object):
    def setupUi(self, AE_Window):
        AE_Window.setObjectName(_fromUtf8("AE_Window"))
        AE_Window.setEnabled(True)
        AE_Window.resize(897, 783)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(AE_Window.sizePolicy().hasHeightForWidth())
        AE_Window.setSizePolicy(sizePolicy)
        AE_Window.setMinimumSize(QtCore.QSize(700, 550))
        AE_Window.setMouseTracking(True)
        AE_Window.setFocusPolicy(QtCore.Qt.WheelFocus)
        AE_Window.setAcceptDrops(True)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/AE.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        AE_Window.setWindowIcon(icon)
        AE_Window.setWindowOpacity(3.0)
        AE_Window.setLayoutDirection(QtCore.Qt.LeftToRight)
        AE_Window.setStyleSheet(_fromUtf8("/* * GLOBAL font and background SETTINGS.  \n"
" */  \n"
"   \n"
" * {  \n"
"     font-size: 11px;  \n"
"     font-family: Tahoma, Verdana, Arial, sans-serif;  \n"
"     background-color: white;  \n"
" }  \n"
"   \n"
" /*   \n"
" * COMBOBOXES - sets border shape, dimensions and colours.  \n"
" */  \n"
"   \n"
" QComboBox {  \n"
"     border: 1px solid gray;  \n"
"     border-radius: 2px;  \n"
"    \n"
"     min-width: 6em;  \n"
"     background-color: #ffffff;  \n"
"     color:black;  \n"
"       \n"
"      border-color: silver;  \n"
"     border-width: 1px;  \n"
"     border-style: solid;  \n"
"     padding: 1px 0px 1px 3px; /*This makes text colour work*/  \n"
" }  \n"
"   \n"
" QComboBox:editable {  \n"
"     /*This changes colour behind expand icon*/  \n"
"     background-color: #f2f2f2;   \n"
" }  \n"
"   \n"
" QComboBox:!editable, QComboBox::drop-down:editable {  \n"
"     background: #ffffff;  \n"
" }  \n"
"   \n"
" QComboBox:!editable:on, QComboBox::drop-down:editable:on {  \n"
"     background: #ffffff;  \n"
" }  \n"
"   \n"
" QComboBox::drop-down {   \n"
"     /*This sets up style of combobox list */  \n"
"     subcontrol-origin: padding;  \n"
"     subcontrol-position: top right;  \n"
"     width: 15px;  \n"
"     border-left-width: 1px;  \n"
"     border-left-color: darkgray;  \n"
"     border-left-style: solid;   \n"
"     border-top-right-radius: 2px;   \n"
"     border-bottom-right-radius: 2px;  \n"
" }  \n"
"   \n"
" QComboBox::down-arrow {  \n"
"     /* Custom expand icon source and dimensions, width forces proportional height resize*/  \n"
"     image: url(:/menuEditShape/resources/moveDown.png);   \n"
"     width: 12px;  \n"
" }  \n"
"   \n"
" QComboBox::down-arrow:on {   \n"
"     /* This moves activated arrow by 1px */  \n"
"     top: 1px;  \n"
"     left: 1px;  \n"
" }  \n"
"   \n"
" /* TABLE OF CONTENTS */  \n"
"   \n"
" QDockWidget {  \n"
"     border:none;  \n"
" }  \n"
"   \n"
" QDockWidget::title {  \n"
"     text-align: left;  \n"
"     border: 1px solid silver;  \n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,  \n"
"                       stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);  \n"
"     padding-left: 35px;  \n"
" }  \n"
"   \n"
" /* GROUPBOXES */  \n"
"   \n"
" QGroupBox {  \n"
"     background-color: white;  \n"
"     border: 1px solid gray;  \n"
"     border-radius: 2px;  \n"
"     margin-top: 1px; /* leave space at the top for the title */  \n"
" }  \n"
"   \n"
" QGroupBox::title {  \n"
"     subcontrol-origin: margin;  \n"
"     subcontrol-position: top center; /* position at the top center */  \n"
"      \n"
" }  \n"
"   \n"
" /* HEADERS */  \n"
"   \n"
" QHeaderView::section {  \n"
"     background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,  \n"
"                                   stop:0 #616161, stop: 0.5 #505050,  \n"
"                                   stop: 0.6 #434343, stop:1 #656565);  \n"
"     color: black;  \n"
"     padding-left: 4px;  \n"
"     border: 1px solid #6c6c6c;  \n"
" }  \n"
"   \n"
" QHeaderView::section:checked  \n"
" {  \n"
"     background-color: red;  \n"
" }  \n"
"   \n"
" QHeaderView::down-arrow {  \n"
"     image: url(down_arrow.png);  \n"
" }  \n"
"   \n"
" QHeaderView::up-arrow {  \n"
"     image: url(up_arrow.png);  \n"
" }  \n"
"   \n"
" /* MENU */  \n"
"   \n"
" QMenuBar {  \n"
"     /* These sets background colour of menus */  \n"
"     background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,  \n"
"                       stop:0 silver, stop:1 lightgray);   \n"
" }  \n"
"   \n"
" QMenu {  \n"
"  color: black; \n"
"     background-color: #EAEAEA \n"
" }  \n"
"   \n"
" QMenuBar::item {  \n"
"     spacing: 3px; /* spacing between menu bar items */  \n"
"     padding: 1px 4px;  \n"
"     border-radius: 2px;  \n"
"     color: black;  \n"
" }  \n"
"   \n"
" QMenuBar::item:selected { /* when selected using mouse or keyboard */  \n"
"     background: darkgray;  \n"
"     color: black;  \n"
" }  \n"
"   \n"
" QMenuBar::item:pressed {  \n"
"     background: #888888;  \n"
"     color: black;  \n"
" }  \n"
"   \n"
"   \n"
" /* PUSHBUTTONS */  \n"
"   \n"
" QPushButton {  \n"
"     border: 1px solid darkgray;  \n"
"     border-radius: 2px;  \n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                       stop: 0 #f6f7fa, stop: 1 #dadbde);  \n"
"     min-height: 24px;  \n"
"     max-height: 24px;  \n"
"     min-width: 100px;  \n"
" }  \n"
"   \n"
" QPushButton:pressed {  \n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                       stop: 0 #dadbde, stop: 1 #f6f7fa);  \n"
" }  \n"
"   \n"
" QPushButton:flat {  \n"
"     border: none; /* no border for a flat push button */  \n"
" }  \n"
"   \n"
" QPushButton:default {  \n"
"     border-color: navy; /* make the default button prominent */  \n"
" }  \n"
"   \n"
" /* RADIOBUTTONS */  \n"
"   \n"
" QRadioButton::indicator {  \n"
"     width: 13px;  \n"
"     height: 13px;  \n"
" }  \n"
"   \n"
" /* SCROLLBAR  \n"
" * I can\\\'t see you, but i know you\\\'re there.  \n"
" */  \n"
"   \n"
" QScrollBar{  \n"
"     width: 1px;  \n"
" }  \n"
"   \n"
"   \n"
" /* TABWIDGET */  \n"
"   \n"
"   \n"
" QTabWidget::pane {  \n"
"     /* Border settings for TabWidget */  \n"
"     border-top: 2px solid #C2C7CB;  \n"
" }  \n"
"   \n"
" QTabWidget::tab-bar {  \n"
"     /* This moves tab bar left by 5px */  \n"
"     left: 5px;   \n"
" }  \n"
"   \n"
" /* Styles single tab using tab sub-control, uses QTabBar instead of QTabWidget */  \n"
" QTabBar::tab {  \n"
"     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                 stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,  \n"
"                 stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);  \n"
"     border: 1px solid lightgrey;   \n"
"     border-top-left-radius: 2px;  \n"
"     border-top-right-radius: 2px;  \n"
"     min-width: 120px;  \n"
"     padding: 2px;  \n"
" }  \n"
"   \n"
" QTabBar::tab:selected, QTabBar::tab:hover {  \n"
"         background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                     stop: 0 #fafafa, stop: 0.4 #f4f4f4,  \n"
"                     stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);  \n"
" }  \n"
"   \n"
" QTabBar::tab:selected {  \n"
"     border-color: darkgray;  \n"
"     border-bottom-color: lightgray;   \n"
" }  \n"
"   \n"
" QTabBar::tab:!selected {  \n"
"     /* Unselected tab seems to be smaller */  \n"
"     margin-top: 2px;   \n"
" }  \n"
"   \n"
" QTabBar::tab:selected {  \n"
"     /* Push selected tabs 4px in both directions */  \n"
"     margin-left: -4px;  \n"
"     margin-right: -4px;  \n"
" }  \n"
"   \n"
" QTabBar::tab:first:selected {  \n"
"     /* But do not push the first one left since it would fall out of the window :( */  \n"
"     margin-left: 0;   \n"
" }  \n"
"   \n"
" QTabBar::tab:last:selected {  \n"
"     /* Same for the last one just to make it feel safe */  \n"
"     margin-right: 0;  \n"
" }  \n"
"   \n"
" QTabBar::tab:only-one {  \n"
"     /* If there\\\'s only one tab, it doesn\\\'t have to do anything */  \n"
"     margin: 0;   \n"
" }  \n"
" /* TABLES */  \n"
" QTableView {  \n"
"     \n"
" }  \n"
"   \n"
" QTableView QTableCornerButton::section {  \n"
"    \n"
"     border: 1px outset darkgray;  \n"
" }  \n"
"   \n"
"   \n"
" /* TOOLBAR WITH BASIC ICONS */  \n"
"   \n"
" QToolBar {  \n"
"     background: qlineargradient(x1:0, y1:0, x2:0, y2:1,  \n"
"                          stop:0 lightgray, stop:1 silver);  \n"
"     border: 0;  \n"
" }  \n"
"   \n"
" QToolButton {   \n"
"     /* All types of tool button */  \n"
"     border: none;   \n"
"        border-radius: 2px;  \n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                                   stop: 0 #f6f7fa, stop: 1 #dadbde);  \n"
" }  \n"
"   \n"
" QToolButton[popupMode=\\ 1\\ ] {   \n"
"     /* Only for MenuButtonPopup   \n"
"     Make way for the popup button */  \n"
"     padding-right: 20px;   \n"
" }  \n"
"   \n"
" QToolButton:selected {  \n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                       stop: 0 #dadbde, stop: 1 #f6f7fa);  \n"
" }  \n"
"   \n"
" QToolButton:on {  \n"
"     border: 1px solid #8f8f91;  \n"
"     border-radius: 2px;  \n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                       stop: 0 #dadbde, stop: 1 #f6f7fa);  \n"
"     width: 28px;  \n"
"     height: 28px;  \n"
" }  \n"
"   \n"
" /* the subcontrols below are used only in the MenuButtonPopup mode */  \n"
" QToolButton::menu-button {  \n"
"     border: 1px solid lightgray;  \n"
"     border-top-right-radius: 2px;  \n"
"     border-bottom-right-radius: 2px;  \n"
"     /*16px width + 4px for border = 20px allocated above */  \n"
"     width: 16px;  \n"
" }  \n"
"   \n"
" QToolButton::menu-arrow {  \n"
"     image: url(resources/moveDown.png);  \n"
" }  \n"
"   \n"
" QToolButton::menu-arrow:open {  \n"
"    top: 1px; left: 1px; /* Shift it a bit */  \n"
" }  \n"
"   \n"
" /* TOOLBOX */  \n"
"   \n"
" QToolBox::tab {  \n"
"     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  \n"
"                          stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,  \n"
"                          stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);  \n"
"     border-radius: 2px;  \n"
"     color: lightgray;  \n"
" }  \n"
"   \n"
" /* TOOLTIP MESSAGE STYLE */  \n"
"   \n"
" QToolTip {  \n"
"     border: 1px solid darkgray; \n"
"     border-radius: 4px; \n"
"     padding: 0; \n"
"     min-height: 20px; \n"
"     min-width: 60px; \n"
"     text-align: center; \n"
"     line-height: 20px; \n"
"     margin: 0; \n"
" }  \n"
"   \n"
" /* FRAME SETTINGS */  \n"
"   \n"
" QFrame{  \n"
"     border: none;  \n"
" \n"
" }  \n"
"   \n"
" /* LABEL SETTINGS */  \n"
"   \n"
" QLabel {  background-color: transparent;  \n"
"     border:none;  \n"
" }  \n"
"   \n"
" QLabel#label {  \n"
"     padding-left:30px;  \n"
" }  \n"
"   \n"
" QLabel#zoomLabel {  \n"
"     padding-right:30px;  \n"
" }  \n"
"   \n"
" /* SLIDER (PARAMETERS), USED MOSTLY FOR OPACITY */  \n"
" QSlider::groove:horizontal {  \n"
"     border: 1px solid #999999;  \n"
"     height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */  \n"
"     background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);  \n"
"     margin: 2px 0;  \n"
" }  \n"
"   \n"
" QSlider::handle:horizontal {  \n"
"     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);  \n"
"     border: 1px solid #5c5c5c;  \n"
"     width: 18px;  \n"
"     margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */  \n"
"     border-radius: 2px;  \n"
" }  \n"
"   \n"
" QSlider::groove:vertical {  \n"
"     background: qlineargradient(x1:0, y1:0, x2:0, y2:1,  \n"
"                 stop:0 lightgray, stop:1 darkgray);  \n"
"     position: absolute; /* absolutely position 4px from the left and right of the widget. setting margins on the widget should work too... */  \n"
"     left: 4px; right: 4px;  \n"
" }  \n"
"   \n"
" QSlider::handle:vertical {  \n"
"     height: 10px;  \n"
"     background: green;  \n"
"     margin: 0 -4px; /* expand outside the groove */  \n"
" }  \n"
"   \n"
" QSlider::add-page:vertical {  \n"
"     background: qlineargradient(x1:0, y1:0, x2:0, y2:1,  \n"
"                 stop:0 lightgray, stop:1 darkgray);  \n"
" }  \n"
"   \n"
" QSlider::sub-page:vertical {  \n"
"     background: qlineargradient(x1:0, y1:0, x2:0, y2:1,  \n"
"                 stop:0 lightgray, stop:1 darkgray);  \n"
" }  "))
        self.centralWidget = QtGui.QWidget(AE_Window)
        self.centralWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralWidget.setStyleSheet(_fromUtf8("border: none;"))
        self.centralWidget.setObjectName(_fromUtf8("centralWidget"))
        self.gridLayout_2 = QtGui.QGridLayout(self.centralWidget)
        self.gridLayout_2.setContentsMargins(0, -1, -1, -1)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        AE_Window.setCentralWidget(self.centralWidget)
        self.toolBar_OpenSave = QtGui.QToolBar(AE_Window)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolBar_OpenSave.sizePolicy().hasHeightForWidth())
        self.toolBar_OpenSave.setSizePolicy(sizePolicy)
        self.toolBar_OpenSave.setMinimumSize(QtCore.QSize(0, 0))
        self.toolBar_OpenSave.setMouseTracking(True)
        self.toolBar_OpenSave.setWindowTitle(_fromUtf8(""))
        self.toolBar_OpenSave.setAutoFillBackground(False)
        self.toolBar_OpenSave.setStyleSheet(_fromUtf8("     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                       stop: 0 #f1f1f1, stop: 1 #e2e2e2);"))
        self.toolBar_OpenSave.setMovable(False)
        self.toolBar_OpenSave.setAllowedAreas(QtCore.Qt.AllToolBarAreas)
        self.toolBar_OpenSave.setIconSize(QtCore.QSize(30, 30))
        self.toolBar_OpenSave.setObjectName(_fromUtf8("toolBar_OpenSave"))
        AE_Window.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar_OpenSave)
        self.toolBar_Zoom = QtGui.QToolBar(AE_Window)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolBar_Zoom.sizePolicy().hasHeightForWidth())
        self.toolBar_Zoom.setSizePolicy(sizePolicy)
        self.toolBar_Zoom.setWindowTitle(_fromUtf8(""))
        self.toolBar_Zoom.setStyleSheet(_fromUtf8("     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                       stop: 0 #f1f1f1, stop: 1 #e2e2e2);"))
        self.toolBar_Zoom.setMovable(False)
        self.toolBar_Zoom.setIconSize(QtCore.QSize(30, 30))
        self.toolBar_Zoom.setObjectName(_fromUtf8("toolBar_Zoom"))
        AE_Window.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar_Zoom)
        self.dockWidget_LoadedFiles = QtGui.QDockWidget(AE_Window)
        self.dockWidget_LoadedFiles.setEnabled(True)
        self.dockWidget_LoadedFiles.setMinimumSize(QtCore.QSize(160, 400))
        self.dockWidget_LoadedFiles.setMaximumSize(QtCore.QSize(160, 6060))
        self.dockWidget_LoadedFiles.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.dockWidget_LoadedFiles.setAcceptDrops(False)
        self.dockWidget_LoadedFiles.setAccessibleName(_fromUtf8(""))
        self.dockWidget_LoadedFiles.setStyleSheet(_fromUtf8("border: none;\n"
"margin: 0;\n"
"padding: 0;\n"
"\n"
" "))
        self.dockWidget_LoadedFiles.setFloating(False)
        self.dockWidget_LoadedFiles.setFeatures(QtGui.QDockWidget.DockWidgetFloatable|QtGui.QDockWidget.DockWidgetMovable)
        self.dockWidget_LoadedFiles.setAllowedAreas(QtCore.Qt.BottomDockWidgetArea|QtCore.Qt.LeftDockWidgetArea|QtCore.Qt.RightDockWidgetArea)
        self.dockWidget_LoadedFiles.setObjectName(_fromUtf8("dockWidget_LoadedFiles"))
        self.dockWidget_LoadedFilesContent = QtGui.QWidget()
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dockWidget_LoadedFilesContent.sizePolicy().hasHeightForWidth())
        self.dockWidget_LoadedFilesContent.setSizePolicy(sizePolicy)
        self.dockWidget_LoadedFilesContent.setMinimumSize(QtCore.QSize(0, 0))
        self.dockWidget_LoadedFilesContent.setStyleSheet(_fromUtf8("background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,\n"
"                       stop: 0 #fafafa, stop: 0.5 #fcfcfc, stop: 1 #ffffff);\n"
""))
        self.dockWidget_LoadedFilesContent.setObjectName(_fromUtf8("dockWidget_LoadedFilesContent"))
        self.verticalLayout = QtGui.QVBoxLayout(self.dockWidget_LoadedFilesContent)
        self.verticalLayout.setContentsMargins(-1, -1, 0, 23)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.dockWidget_LoadedFiles.setWidget(self.dockWidget_LoadedFilesContent)
        AE_Window.addDockWidget(QtCore.Qt.DockWidgetArea(1), self.dockWidget_LoadedFiles)
        self.menubar = QtGui.QMenuBar(AE_Window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 897, 16))
        self.menubar.setStyleSheet(_fromUtf8("     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                       stop: 0 #e2e2e2, stop: 1 #f1f1f1);\n"
"\n"
"\n"
"color: black;"))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuView = QtGui.QMenu(self.menubar)
        self.menuView.setObjectName(_fromUtf8("menuView"))
        self.menuZoom = QtGui.QMenu(self.menuView)
        self.menuZoom.setEnabled(False)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFileZoom/resources/search.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.menuZoom.setIcon(icon1)
        self.menuZoom.setObjectName(_fromUtf8("menuZoom"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        self.menuAbout_Extensions = QtGui.QMenu(self.menuHelp)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuHelp/resources/info.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.menuAbout_Extensions.setIcon(icon2)
        self.menuAbout_Extensions.setObjectName(_fromUtf8("menuAbout_Extensions"))
        self.menuEdit = QtGui.QMenu(self.menubar)
        self.menuEdit.setObjectName(_fromUtf8("menuEdit"))
        self.menuChange_Language = QtGui.QMenu(self.menuEdit)
        self.menuChange_Language.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.menuChange_Language.setTearOffEnabled(False)
        self.menuChange_Language.setObjectName(_fromUtf8("menuChange_Language"))
        self.menuTools = QtGui.QMenu(self.menubar)
        self.menuTools.setEnabled(False)
        self.menuTools.setObjectName(_fromUtf8("menuTools"))
        self.menuGAEC = QtGui.QMenu(self.menubar)
        self.menuGAEC.setEnabled(False)
        self.menuGAEC.setObjectName(_fromUtf8("menuGAEC"))
        AE_Window.setMenuBar(self.menubar)
        self.toolBar = QtGui.QToolBar(AE_Window)
        self.toolBar.setEnabled(True)
        self.toolBar.setMouseTracking(True)
        self.toolBar.setWindowTitle(_fromUtf8(""))
        self.toolBar.setStyleSheet(_fromUtf8("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                       stop: 0 #f1f1f1, stop: 1 #e7e7e7);\n"
" "))
        self.toolBar.setMovable(False)
        self.toolBar.setIconSize(QtCore.QSize(30, 30))
        self.toolBar.setFloatable(True)
        self.toolBar.setObjectName(_fromUtf8("toolBar"))
        AE_Window.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.dockWidget_Coordinates = QtGui.QDockWidget(AE_Window)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dockWidget_Coordinates.sizePolicy().hasHeightForWidth())
        self.dockWidget_Coordinates.setSizePolicy(sizePolicy)
        self.dockWidget_Coordinates.setMinimumSize(QtCore.QSize(831, 37))
        self.dockWidget_Coordinates.setMaximumSize(QtCore.QSize(524287, 37))
        self.dockWidget_Coordinates.setBaseSize(QtCore.QSize(0, 0))
        self.dockWidget_Coordinates.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.dockWidget_Coordinates.setStyleSheet(_fromUtf8("     background-color: #e2e2e2;\n"
"margin: 0;\n"
"padding: 0;"))
        self.dockWidget_Coordinates.setLocale(QtCore.QLocale(QtCore.QLocale.Polish, QtCore.QLocale.Poland))
        self.dockWidget_Coordinates.setFeatures(QtGui.QDockWidget.NoDockWidgetFeatures)
        self.dockWidget_Coordinates.setAllowedAreas(QtCore.Qt.BottomDockWidgetArea)
        self.dockWidget_Coordinates.setObjectName(_fromUtf8("dockWidget_Coordinates"))
        self.dockWidgetContents = QtGui.QWidget()
        self.dockWidgetContents.setMaximumSize(QtCore.QSize(16777215, 25))
        self.dockWidgetContents.setStyleSheet(_fromUtf8(" background-color: #e2e2e2;\n"
"margin: 0;\n"
"padding: 0;"))
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.gridLayout = QtGui.QGridLayout(self.dockWidgetContents)
        self.gridLayout.setContentsMargins(6, 0, 6, 0)
        self.gridLayout.setVerticalSpacing(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gridLayout_3 = QtGui.QGridLayout()
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.coordinatesLabel = QtGui.QLabel(self.dockWidgetContents)
        self.coordinatesLabel.setMinimumSize(QtCore.QSize(0, 21))
        self.coordinatesLabel.setMaximumSize(QtCore.QSize(70, 16777215))
        self.coordinatesLabel.setStyleSheet(_fromUtf8("margin: 0;\n"
"padding: 0;"))
        self.coordinatesLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.coordinatesLabel.setObjectName(_fromUtf8("coordinatesLabel"))
        self.gridLayout_3.addWidget(self.coordinatesLabel, 0, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(0, 21, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem, 0, 6, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(0, 21, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem1, 0, 0, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(0, 21, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem2, 0, 10, 1, 1)
        self.label = QtGui.QLabel(self.dockWidgetContents)
        self.label.setMinimumSize(QtCore.QSize(0, 21))
        self.label.setMaximumSize(QtCore.QSize(45, 16777215))
        self.label.setStyleSheet(_fromUtf8("margin: 0;\n"
"padding: 0;"))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_3.addWidget(self.label, 0, 4, 1, 1)
        self.zoomLabel = QtGui.QLabel(self.dockWidgetContents)
        self.zoomLabel.setMinimumSize(QtCore.QSize(0, 21))
        self.zoomLabel.setMaximumSize(QtCore.QSize(35, 16777215))
        self.zoomLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.zoomLabel.setStyleSheet(_fromUtf8("margin: 0;\n"
"padding: 0;"))
        self.zoomLabel.setLineWidth(3)
        self.zoomLabel.setTextFormat(QtCore.Qt.AutoText)
        self.zoomLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.zoomLabel.setObjectName(_fromUtf8("zoomLabel"))
        self.gridLayout_3.addWidget(self.zoomLabel, 0, 7, 1, 1)
        self.coordinateSystemComboBox = QtGui.QComboBox(self.dockWidgetContents)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.coordinateSystemComboBox.sizePolicy().hasHeightForWidth())
        self.coordinateSystemComboBox.setSizePolicy(sizePolicy)
        self.coordinateSystemComboBox.setMinimumSize(QtCore.QSize(80, 21))
        self.coordinateSystemComboBox.setMaximumSize(QtCore.QSize(250, 16777215))
        self.coordinateSystemComboBox.setSizeIncrement(QtCore.QSize(0, 0))
        self.coordinateSystemComboBox.setBaseSize(QtCore.QSize(0, 0))
        self.coordinateSystemComboBox.setAutoFillBackground(False)
        self.coordinateSystemComboBox.setStyleSheet(_fromUtf8("background-color: white;"))
        self.coordinateSystemComboBox.setEditable(True)
        self.coordinateSystemComboBox.setMaxVisibleItems(20)
        self.coordinateSystemComboBox.setObjectName(_fromUtf8("coordinateSystemComboBox"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(0, _fromUtf8("EPSG: 4326 WGS-84"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(1, _fromUtf8("EPSG: 2180 PL-1992"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(2, _fromUtf8("EPSG: 2176 PL-2000 zone 5 (15°E)"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(3, _fromUtf8("EPSG: 2177 PL-2000 zone 6 (18°E)"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(4, _fromUtf8("EPSG: 2178 PL-2000 zone 7 (21°E)"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(5, _fromUtf8("EPSG: 2179 PL-2000 zone 8 (24°E)"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(6, _fromUtf8("EPSG: 32633 PL-UTM zone 33N (15°E)"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(7, _fromUtf8("EPSG: 32634 PL-UTM zone 34N (21°E)"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(8, _fromUtf8("EPSG: 32635 PL-UTM zone 35N (27°E)"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(9, _fromUtf8("EPSG: 3034 PL-LCC"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.coordinateSystemComboBox.setItemText(10, _fromUtf8("EPSG: 3035 PL-LAEA"))
        self.coordinateSystemComboBox.addItem(_fromUtf8(""))
        self.gridLayout_3.addWidget(self.coordinateSystemComboBox, 0, 5, 1, 1)
        self.zoomComboBox = QtGui.QComboBox(self.dockWidgetContents)
        self.zoomComboBox.setMinimumSize(QtCore.QSize(80, 21))
        self.zoomComboBox.setMaximumSize(QtCore.QSize(100, 16777215))
        self.zoomComboBox.setStyleSheet(_fromUtf8("background-color: white;"))
        self.zoomComboBox.setEditable(True)
        self.zoomComboBox.setMaxVisibleItems(15)
        self.zoomComboBox.setObjectName(_fromUtf8("zoomComboBox"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(0, _fromUtf8("1:1 000 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(1, _fromUtf8("1:500 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(2, _fromUtf8("1:250 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(3, _fromUtf8("1:100 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(4, _fromUtf8("1:50 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(5, _fromUtf8("1:25 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(6, _fromUtf8("1:10 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(7, _fromUtf8("1:5 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(8, _fromUtf8("1:2 500"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(9, _fromUtf8("1:1 000"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(10, _fromUtf8("1:500"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(11, _fromUtf8("1:250"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(12, _fromUtf8("1:100"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(13, _fromUtf8("1:50"))
        self.zoomComboBox.addItem(_fromUtf8(""))
        self.zoomComboBox.setItemText(14, _fromUtf8("1:25"))
        self.gridLayout_3.addWidget(self.zoomComboBox, 0, 9, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(0, 21, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem3, 0, 3, 1, 1)
        self.coordinatesLineEdit = QtGui.QLineEdit(self.dockWidgetContents)
        self.coordinatesLineEdit.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.coordinatesLineEdit.sizePolicy().hasHeightForWidth())
        self.coordinatesLineEdit.setSizePolicy(sizePolicy)
        self.coordinatesLineEdit.setMinimumSize(QtCore.QSize(125, 21))
        self.coordinatesLineEdit.setMaximumSize(QtCore.QSize(150, 16777215))
        self.coordinatesLineEdit.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.coordinatesLineEdit.setStyleSheet(_fromUtf8("border: 1px solid lightgray;\n"
"border-radius: 2px;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,\n"
"                     stop: 0 #fafafa,     \n"
"                     stop: 0.5 #fcfcfc,\n"
"                     stop: 1 #fafafa);"))
        self.coordinatesLineEdit.setInputMethodHints(QtCore.Qt.ImhNone)
        self.coordinatesLineEdit.setDragEnabled(False)
        self.coordinatesLineEdit.setReadOnly(True)
        self.coordinatesLineEdit.setCursorMoveStyle(QtCore.Qt.LogicalMoveStyle)
        self.coordinatesLineEdit.setObjectName(_fromUtf8("coordinatesLineEdit"))
        self.gridLayout_3.addWidget(self.coordinatesLineEdit, 0, 2, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_3, 0, 0, 1, 1)
        self.dockWidget_Coordinates.setWidget(self.dockWidgetContents)
        AE_Window.addDockWidget(QtCore.Qt.DockWidgetArea(8), self.dockWidget_Coordinates)
        self.actionOpenProject_toolbar = QtGui.QAction(AE_Window)
        self.actionOpenProject_toolbar.setEnabled(True)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFile/resources/open.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionOpenProject_toolbar.setIcon(icon3)
        self.actionOpenProject_toolbar.setShortcut(_fromUtf8("Ctrl+O"))
        self.actionOpenProject_toolbar.setShortcutContext(QtCore.Qt.WindowShortcut)
        self.actionOpenProject_toolbar.setObjectName(_fromUtf8("actionOpenProject_toolbar"))
        self.actionSaveProject = QtGui.QAction(AE_Window)
        self.actionSaveProject.setCheckable(False)
        self.actionSaveProject.setEnabled(False)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFile/resources/save.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionSaveProject.setIcon(icon4)
        self.actionSaveProject.setShortcut(_fromUtf8("Ctrl+S"))
        self.actionSaveProject.setObjectName(_fromUtf8("actionSaveProject"))
        self.actionSaveProjectAs = QtGui.QAction(AE_Window)
        self.actionSaveProjectAs.setEnabled(False)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFile/resources/saveas.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionSaveProjectAs.setIcon(icon5)
        self.actionSaveProjectAs.setShortcut(_fromUtf8("Ctrl+Shift+S"))
        self.actionSaveProjectAs.setObjectName(_fromUtf8("actionSaveProjectAs"))
        self.actionQuit = QtGui.QAction(AE_Window)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFile/resources/cancel.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionQuit.setIcon(icon6)
        self.actionQuit.setShortcut(_fromUtf8("Ctrl+Q"))
        self.actionQuit.setObjectName(_fromUtf8("actionQuit"))
        self.actionZoomIn = QtGui.QAction(AE_Window)
        self.actionZoomIn.setEnabled(False)
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFileZoom/resources/plus.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionZoomIn.setIcon(icon7)
        self.actionZoomIn.setShortcut(_fromUtf8("Ctrl++"))
        self.actionZoomIn.setObjectName(_fromUtf8("actionZoomIn"))
        self.actionZoomOut = QtGui.QAction(AE_Window)
        self.actionZoomOut.setEnabled(False)
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuFileZoom/resources/minus.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionZoomOut.setIcon(icon8)
        self.actionZoomOut.setShortcut(_fromUtf8("Ctrl+-"))
        self.actionZoomOut.setObjectName(_fromUtf8("actionZoomOut"))
        self.actionNormalSize = QtGui.QAction(AE_Window)
        self.actionNormalSize.setEnabled(False)
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuContext/resources/zoomfull.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionNormalSize.setIcon(icon9)
        self.actionNormalSize.setShortcut(_fromUtf8("Ctrl+N"))
        self.actionNormalSize.setObjectName(_fromUtf8("actionNormalSize"))
        self.actionAbout_application = QtGui.QAction(AE_Window)
        self.actionAbout_application.setIcon(icon2)
        self.actionAbout_application.setShortcut(_fromUtf8(""))
        self.actionAbout_application.setObjectName(_fromUtf8("actionAbout_application"))
        self.actionAbout_extencions = QtGui.QAction(AE_Window)
        self.actionAbout_extencions.setIcon(icon2)
        self.actionAbout_extencions.setShortcut(_fromUtf8(""))
        self.actionAbout_extencions.setObjectName(_fromUtf8("actionAbout_extencions"))
        self.actionPreferences = QtGui.QAction(AE_Window)
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuEdit/resources/settings.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionPreferences.setIcon(icon10)
        self.actionPreferences.setShortcut(_fromUtf8("Ctrl+T"))
        self.actionPreferences.setObjectName(_fromUtf8("actionPreferences"))
        self.actionShow_Layers = QtGui.QAction(AE_Window)
        self.actionShow_Layers.setCheckable(True)
        self.actionShow_Layers.setChecked(True)
        self.actionShow_Layers.setShortcut(_fromUtf8(""))
        self.actionShow_Layers.setObjectName(_fromUtf8("actionShow_Layers"))
        self.actionToolBarShow_Layers = QtGui.QAction(AE_Window)
        self.actionToolBarShow_Layers.setCheckable(True)
        self.actionToolBarShow_Layers.setChecked(True)
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/layers.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionToolBarShow_Layers.setIcon(icon11)
        self.actionToolBarShow_Layers.setShortcut(_fromUtf8("Ctrl+L"))
        self.actionToolBarShow_Layers.setObjectName(_fromUtf8("actionToolBarShow_Layers"))
        self.actionToolBarShow_Coordinates = QtGui.QAction(AE_Window)
        self.actionToolBarShow_Coordinates.setCheckable(True)
        self.actionToolBarShow_Coordinates.setChecked(True)
        icon12 = QtGui.QIcon()
        icon12.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/coord.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionToolBarShow_Coordinates.setIcon(icon12)
        self.actionToolBarShow_Coordinates.setShortcut(_fromUtf8("Ctrl+C"))
        self.actionToolBarShow_Coordinates.setObjectName(_fromUtf8("actionToolBarShow_Coordinates"))
        self.actionShow_Coordinates = QtGui.QAction(AE_Window)
        self.actionShow_Coordinates.setCheckable(True)
        self.actionShow_Coordinates.setChecked(True)
        self.actionShow_Coordinates.setShortcut(_fromUtf8(""))
        self.actionShow_Coordinates.setObjectName(_fromUtf8("actionShow_Coordinates"))
        self.actionMeasureDistance = QtGui.QAction(AE_Window)
        self.actionMeasureDistance.setEnabled(True)
        icon13 = QtGui.QIcon()
        icon13.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuTools/resources/distance.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionMeasureDistance.setIcon(icon13)
        self.actionMeasureDistance.setShortcut(_fromUtf8("Ctrl+D"))
        self.actionMeasureDistance.setVisible(True)
        self.actionMeasureDistance.setIconVisibleInMenu(True)
        self.actionMeasureDistance.setObjectName(_fromUtf8("actionMeasureDistance"))
        self.actionOpen_file = QtGui.QAction(AE_Window)
        icon14 = QtGui.QIcon()
        icon14.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuTools/resources/addShape.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionOpen_file.setIcon(icon14)
        self.actionOpen_file.setShortcut(_fromUtf8(""))
        self.actionOpen_file.setObjectName(_fromUtf8("actionOpen_file"))
        self.actionMeasure_area = QtGui.QAction(AE_Window)
        icon15 = QtGui.QIcon()
        icon15.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuTools/resources/area.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionMeasure_area.setIcon(icon15)
        self.actionMeasure_area.setShortcut(_fromUtf8("Ctrl+M"))
        self.actionMeasure_area.setObjectName(_fromUtf8("actionMeasure_area"))
        self.actionPan_Action = QtGui.QAction(AE_Window)
        self.actionPan_Action.setCheckable(True)
        icon16 = QtGui.QIcon()
        icon16.addPixmap(QtGui.QPixmap(_fromUtf8(":/MainWindow/resources/palm1.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionPan_Action.setIcon(icon16)
        self.actionPan_Action.setShortcut(_fromUtf8(""))
        self.actionPan_Action.setObjectName(_fromUtf8("actionPan_Action"))
        self.actionOpenProject = QtGui.QAction(AE_Window)
        self.actionOpenProject.setIcon(icon3)
        self.actionOpenProject.setShortcut(_fromUtf8("Ctrl+O"))
        self.actionOpenProject.setObjectName(_fromUtf8("actionOpenProject"))
        self.actionCreate_Shapefile = QtGui.QAction(AE_Window)
        icon17 = QtGui.QIcon()
        icon17.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuSegmentation/resources/shapefile.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionCreate_Shapefile.setIcon(icon17)
        self.actionCreate_Shapefile.setShortcut(_fromUtf8("Ctrl+Alt+P"))
        self.actionCreate_Shapefile.setObjectName(_fromUtf8("actionCreate_Shapefile"))
        self.actionModelGAEC = QtGui.QAction(AE_Window)
        self.actionModelGAEC.setEnabled(False)
        icon18 = QtGui.QIcon()
        icon18.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuSegmentation/resources/kclusters.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionModelGAEC.setIcon(icon18)
        self.actionModelGAEC.setShortcut(_fromUtf8("Ctrl+K"))
        self.actionModelGAEC.setObjectName(_fromUtf8("actionModelGAEC"))
        self.actionRaster_Calculator = QtGui.QAction(AE_Window)
        icon19 = QtGui.QIcon()
        icon19.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuTools/resources/calculator.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionRaster_Calculator.setIcon(icon19)
        self.actionRaster_Calculator.setShortcut(_fromUtf8("Ctrl+R"))
        self.actionRaster_Calculator.setObjectName(_fromUtf8("actionRaster_Calculator"))
        self.actionEnglish_English = QtGui.QAction(AE_Window)
        self.actionEnglish_English.setCheckable(True)
        self.actionEnglish_English.setChecked(True)
        icon20 = QtGui.QIcon()
        icon20.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuEdit/resources/english.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionEnglish_English.setIcon(icon20)
        self.actionEnglish_English.setText(_fromUtf8("English (English)"))
        self.actionEnglish_English.setIconText(_fromUtf8("English (English)"))
        self.actionEnglish_English.setToolTip(_fromUtf8("English (English)"))
        self.actionEnglish_English.setMenuRole(QtGui.QAction.TextHeuristicRole)
        self.actionEnglish_English.setObjectName(_fromUtf8("actionEnglish_English"))
        self.actionPolski_Polish = QtGui.QAction(AE_Window)
        self.actionPolski_Polish.setCheckable(True)
        icon21 = QtGui.QIcon()
        icon21.addPixmap(QtGui.QPixmap(_fromUtf8(":/menuEdit/resources/polish.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionPolski_Polish.setIcon(icon21)
        self.actionPolski_Polish.setText(_fromUtf8("Polski (Polish)"))
        self.actionPolski_Polish.setIconText(_fromUtf8("Polski (Polish)"))
        self.actionPolski_Polish.setToolTip(_fromUtf8("Polski (Polish)"))
        self.actionPolski_Polish.setObjectName(_fromUtf8("actionPolski_Polish"))
        self.dockWidget_LoadedFiles.raise_()
        self.toolBar_OpenSave.addAction(self.actionOpen_file)
        self.toolBar_OpenSave.addAction(self.actionOpenProject_toolbar)
        self.toolBar_OpenSave.addAction(self.actionSaveProject)
        self.toolBar_Zoom.addAction(self.actionZoomIn)
        self.toolBar_Zoom.addAction(self.actionZoomOut)
        self.toolBar_Zoom.addAction(self.actionNormalSize)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionOpenProject)
        self.menuFile.addAction(self.actionSaveProject)
        self.menuFile.addAction(self.actionSaveProjectAs)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionQuit)
        self.menuZoom.addAction(self.actionZoomIn)
        self.menuZoom.addAction(self.actionZoomOut)
        self.menuZoom.addAction(self.actionNormalSize)
        self.menuView.addAction(self.menuZoom.menuAction())
        self.menuView.addSeparator()
        self.menuView.addAction(self.actionShow_Layers)
        self.menuView.addAction(self.actionShow_Coordinates)
        self.menuAbout_Extensions.addAction(self.actionAbout_extencions)
        self.menuHelp.addAction(self.actionAbout_application)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.menuAbout_Extensions.menuAction())
        self.menuEdit.addAction(self.actionPreferences)
        self.menuEdit.addAction(self.menuChange_Language.menuAction())
        self.menuTools.addAction(self.actionMeasureDistance)
        self.menuTools.addAction(self.actionMeasure_area)
        self.menuTools.addAction(self.actionCreate_Shapefile)
        self.menuTools.addAction(self.actionRaster_Calculator)
        self.menuGAEC.addAction(self.actionModelGAEC)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuGAEC.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuView.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.toolBar.addAction(self.actionToolBarShow_Layers)
        self.toolBar.addAction(self.actionToolBarShow_Coordinates)
        self.toolBar.addAction(self.actionPan_Action)

        self.retranslateUi(AE_Window)
        self.zoomComboBox.setCurrentIndex(0)
        QtCore.QObject.connect(self.actionQuit, QtCore.SIGNAL(_fromUtf8("triggered()")), AE_Window.close)
        QtCore.QObject.connect(self.actionShow_Layers, QtCore.SIGNAL(_fromUtf8("triggered(bool)")), self.dockWidget_LoadedFiles.setVisible)
        QtCore.QObject.connect(self.dockWidget_LoadedFiles, QtCore.SIGNAL(_fromUtf8("visibilityChanged(bool)")), self.actionShow_Layers.setChecked)
        QtCore.QObject.connect(self.actionToolBarShow_Layers, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.dockWidget_LoadedFiles.setVisible)
        QtCore.QObject.connect(self.dockWidget_LoadedFiles, QtCore.SIGNAL(_fromUtf8("visibilityChanged(bool)")), self.actionToolBarShow_Layers.setChecked)
        QtCore.QObject.connect(self.actionToolBarShow_Coordinates, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.dockWidget_Coordinates.setVisible)
        QtCore.QObject.connect(self.dockWidget_Coordinates, QtCore.SIGNAL(_fromUtf8("visibilityChanged(bool)")), self.actionToolBarShow_Coordinates.setChecked)
        QtCore.QObject.connect(self.actionShow_Coordinates, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.actionToolBarShow_Coordinates.setChecked)
        QtCore.QObject.connect(self.actionToolBarShow_Coordinates, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.actionShow_Coordinates.setChecked)
        QtCore.QObject.connect(self.actionOpenProject_toolbar, QtCore.SIGNAL(_fromUtf8("triggered()")), self.actionOpenProject.trigger)
        QtCore.QMetaObject.connectSlotsByName(AE_Window)

    def retranslateUi(self, AE_Window):
        AE_Window.setWindowTitle(_translate("AE_Window", "AgroEye", None))
        self.dockWidget_LoadedFiles.setWindowTitle(_translate("AE_Window", "Table of Contents", None))
        self.menuFile.setTitle(_translate("AE_Window", "File", None))
        self.menuView.setTitle(_translate("AE_Window", "View", None))
        self.menuZoom.setTitle(_translate("AE_Window", "Zoom", None))
        self.menuHelp.setTitle(_translate("AE_Window", "Help", None))
        self.menuAbout_Extensions.setTitle(_translate("AE_Window", "Extensions", None))
        self.menuEdit.setTitle(_translate("AE_Window", "Edit", None))
        self.menuChange_Language.setTitle(_translate("AE_Window", "Change Language", None))
        self.menuTools.setTitle(_translate("AE_Window", "Tools", None))
        self.menuGAEC.setTitle(_translate("AE_Window", "GAEC", None))
        self.coordinatesLabel.setText(_translate("AE_Window", "Coordinates:", None))
        self.label.setText(_translate("AE_Window", "System:", None))
        self.zoomLabel.setText(_translate("AE_Window", "Zoom:", None))
        self.coordinateSystemComboBox.setItemText(11, _translate("AE_Window", "Enter EPSG code ...", None))
        self.actionOpenProject_toolbar.setText(_translate("AE_Window", "Open project", None))
        self.actionOpenProject_toolbar.setToolTip(_translate("AE_Window", "Open project", None))
        self.actionSaveProject.setText(_translate("AE_Window", "Save", None))
        self.actionSaveProjectAs.setText(_translate("AE_Window", "Save as", None))
        self.actionQuit.setText(_translate("AE_Window", "Quit", None))
        self.actionQuit.setToolTip(_translate("AE_Window", "Quit application", None))
        self.actionZoomIn.setText(_translate("AE_Window", "Zoom In (10%)", None))
        self.actionZoomOut.setText(_translate("AE_Window", "Zoom Out (10%)", None))
        self.actionNormalSize.setText(_translate("AE_Window", "Normal Size (100%)", None))
        self.actionAbout_application.setText(_translate("AE_Window", "About AgroEye", None))
        self.actionAbout_extencions.setText(_translate("AE_Window", "About Qt", None))
        self.actionPreferences.setText(_translate("AE_Window", "Preferences", None))
        self.actionShow_Layers.setText(_translate("AE_Window", "Show Layers", None))
        self.actionShow_Layers.setIconText(_translate("AE_Window", "Show &Layers", None))
        self.actionToolBarShow_Layers.setText(_translate("AE_Window", "Show &Layers", None))
        self.actionToolBarShow_Coordinates.setText(_translate("AE_Window", "Show Coordinates", None))
        self.actionShow_Coordinates.setText(_translate("AE_Window", "Show Coordinates", None))
        self.actionMeasureDistance.setText(_translate("AE_Window", "Measure distance", None))
        self.actionOpen_file.setText(_translate("AE_Window", "Add file", None))
        self.actionMeasure_area.setText(_translate("AE_Window", "Measure area", None))
        self.actionPan_Action.setText(_translate("AE_Window", "Pan Action", None))
        self.actionOpenProject.setText(_translate("AE_Window", "Open project", None))
        self.actionCreate_Shapefile.setText(_translate("AE_Window", "Create new Shapefile", None))
        self.actionModelGAEC.setText(_translate("AE_Window", "Choose rules", None))
        self.actionRaster_Calculator.setText(_translate("AE_Window", "Raster Calculator", None))

import AE_stock_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    AE_Window = QtGui.QMainWindow()
    ui = Ui_AE_Window()
    ui.setupUi(AE_Window)
    AE_Window.show()
    sys.exit(app.exec_())

