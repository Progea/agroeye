from PyQt4.QtGui import *
from PyQt4.QtCore import *
import ui_propertiesdialog
_fromUtf8=QString.fromUtf8

class PropertiesDialog(QDialog, ui_propertiesdialog.Ui_propertiesDialog):

    def __init__(self, parent, file):
        super(PropertiesDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.setupUi(self)
        self.activateWindow()

        self.file = file
        self.parent = parent
        self.setMyData()
        self.update()

        self.propertiesTableView.horizontalHeader().hide()
        self.propertiesTableView.resizeColumnsToContents()
        self.propertiesTableView.verticalHeader().hide()
        self.propertiesTableView.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.propertiesTableView.resizeColumnsToContents()
        self.propertiesTableView.verticalHeader().setResizeMode(QHeaderView.ResizeToContents)
        self.propertiesTableView.horizontalHeader().setStretchLastSection(True)
        self.propertiesTableView.setColumnWidth(0, 200)


    def setMyData(self):

        data_list = []
        data_list.append([_fromUtf8(_('Layer type')), self.file.icon])
        data_list.append([_fromUtf8(_('File format')), self.file.fileFormat])
        data_list.append([_fromUtf8(_('Layer name')), self.file.name])
        data_list.append([_fromUtf8(_('Layer path')), self.file.pathLayer])
        data_list.append([_fromUtf8(_('Visibility')), self.file.visible])
        if not self.file.isVector:
            data_list.append([_fromUtf8(_('Stretch Type')), self.file.stretchClass.stretchType.name])
            data_list.append([_fromUtf8(_('Pixel type')), self.file.dataTypeName])
            data_list.append([_fromUtf8(_('Data Range')), str(self.file.dataTypeMinValue) + " - " + str(self.file.dataTypeMaxValue)])
            data_list.append([_fromUtf8(_('Number of bands')), len(self.file.bands)])
        data_list.append([_fromUtf8(_('EPSG code')), self.file.EPSGbase])
        data_list.append([_fromUtf8(_('Projection name')), self.parent.parent.viewArea.EPSG2Name[self.file.EPSG]])
        data_list.append([_fromUtf8(_('Projection definition')), self.file.proj4])

        self.tableModel = PropertiesDialogModel(data_list, self)
        self.propertiesTableView.setModel(self.tableModel)

    def contextMenuEvent(self, event):
        menu = QMenu(self)
        copyAction = menu.addAction(_fromUtf8(_("Copy")))
        action = menu.exec_(self.mapToGlobal(event.pos()))
        if action == copyAction:
            entry = self.tableModel.data(self.propertiesTableView.currentIndex(), Qt.DisplayRole)
            QApplication.clipboard().setText(entry)

class PropertiesDialogModel(QAbstractTableModel):

    def __init__(self, datain, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.arrayData = datain

    def rowCount(self, parent):
        return len(self.arrayData)

    def columnCount(self, parent):
        return len(self.arrayData[0])

    def data(self, index, role):
        if not index.isValid():
            return QVariant()

        if role == Qt.DisplayRole:
            return self.arrayData[index.row()][index.column()]
        elif role == Qt.DecorationRole:
            return self.arrayData[index.row()][index.column()]

