# -*- coding: utf-8 -*-

import os
import ui_editstylepolygondialog
import ui_editstylelinedialog
import ui_editstylepointdialog
from PyQt4.QtCore import *
from PyQt4.QtGui import *


try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

class Edit(object):

    def __init__(self, parent):
        self.parent = parent
        self.iconWidth = 80
        self.iconHeight = 25


    def setColor(self, colour, button):
        """!
        @brief Sets color of button.
        @param colour: colour to set.
        @param button: button to set new colour.
        """
        rgbIn = (colour.red(), colour.green(), colour.blue())
        button.setStyleSheet("QWidget {background-color: rgb(%d,%d,%d) }" % rgbIn)
        button.clicked.connect(lambda: self.colorDialog(colour, button))


    def colorDialog(self, colour, button):
        """!
        @brief Sets color of features. Opens QColorDialog to set new color.
        @param colour: new colour.
        @param button: button to set colour.
        """
        colourTemp = QColorDialog.getColor(colour, self.parent)
        if colourTemp.isValid():
                rgb = (colourTemp.red(), colourTemp.green(), colourTemp.blue())
                button.setStyleSheet("QWidget {background-color: rgb(%d,%d,%d)}" % rgb)

    def setLinePattern(self, patternLine, comboBox):
        """!
        @brief Set style for QPen.
        @param patternLine: current pattern of line.
        @param comboBox: combobox to add all pattern of line.
        """
        comboBox.setStyleSheet("QComboBox { background-color: white; border-color: black; border-width: 2px;}")
        penStyle = [_fromUtf8(_('Solid')), _fromUtf8(_('Dashed')), _fromUtf8(_('Dotted')), _fromUtf8(_('Dash-Dot')), _fromUtf8(_('Dash-Dot-Dot'))]
        comboBox.clear()

        comboBox.setIconSize(QSize(self.iconWidth, self.iconHeight))
        for i, style in enumerate(range(Qt.SolidLine, Qt.CustomDashLine)):
            pix = QPixmap(self.iconWidth, self.iconHeight)
            pix.fill(Qt.white)
            brush = QBrush(Qt.black)
            pen = QPen(brush, 2.5, Qt.PenStyle(style))
            paint = QPainter(pix)
            paint.setPen(pen)
            paint.drawLine(2,12,78,12)
            comboBox.addItem(QIcon(pix), '%s'%penStyle[i])
            del paint
        comboBox.setCurrentIndex(patternLine-1)

    def setFillPattern(self, patternFill, comboBox):
        """!
        @brief Set style for QBrush.
        @param patternFill: current pattern fill.
        @param comboBox: comboBox to add pattern fill.
        """
        comboBox.setStyleSheet("QComboBox { background-color: white; border-color: black; border-width: 2px; }")
        brushStyle = [_fromUtf8(_('Solid')), _fromUtf8(_('Dense 2')), _fromUtf8(_('Dense 2')), _fromUtf8(_('Dense 3')), _fromUtf8(_('Dense 4')),
                      _fromUtf8(_('Dense 5')), _fromUtf8(_('Dense 6')), _fromUtf8(_('Dense 7')), _fromUtf8(_('Horizontal')), _fromUtf8(_('Vertical')),
                      _fromUtf8(_('Cross')), _fromUtf8(_('Backward Diagonal')), _fromUtf8(_('Forward Diagonal')), _fromUtf8(_('Diagonal Cross'))]
        comboBox.clear()
        comboBox.setIconSize(QSize(self.iconWidth, self.iconHeight))
        for i, style in enumerate(range(Qt.SolidPattern, Qt.LinearGradientPattern)):
            pix = QPixmap(self.iconWidth, self.iconHeight)
            pix.fill(Qt.white)
            brush = QBrush(Qt.BrushStyle(style))
            brush.setColor(Qt.black)
            paint = QPainter(pix)
            paint.setBrush(brush)
            paint.drawRect(5 ,5, 70, 15)
            comboBox.addItem(QIcon(pix), '%s'%brushStyle[i])
            del paint
        comboBox.setCurrentIndex(patternFill-1)

    def setLineCap(self, capPattern, comboBox):
        """!
        @brief Set style for QBrush.
        @param capPattern: pattern of cap line.
        @param comboBox: comboBox to add pattern of line cap.
        """
        comboBox.setStyleSheet("QComboBox { background-color: white; border-color: black; border-width: 2px;}")
        comboBox.clear()
        capList = [_fromUtf8(_('Flat')), _fromUtf8(_('Square')), _fromUtf8(_('Round'))]
        capStyleList = [Qt.FlatCap, Qt.SquareCap, Qt.RoundCap]

        comboBox.setIconSize(QSize(self.iconWidth, self.iconHeight))
        for i, style in enumerate(capStyleList):
            pix = QPixmap(self.iconWidth, self.iconHeight)
            pix.fill(Qt.white)
            brush = QBrush(Qt.black)
            pen = QPen(brush, 10.5)
            pen.setCapStyle(style)
            paint = QPainter(pix)
            paint.setPen(pen)
            paint.drawLine(5,12,40,12)
            comboBox.addItem(QIcon(pix), '%s'%capList[i])
            del paint
        comboBox.setCurrentIndex(capPattern - 1)

    def setLineJoin(self, joinPattern, comboBox):
        """!
        @brief Set style for QBrush.
        @param capPattern: pattern of join line.
        @param comboBox: comboBox to add pattern of line join.
        """
        comboBox.setStyleSheet("QComboBox { background-color: white; border-color: black; border-width: 2px;}")
        comboBox.clear()
        joinList = [_fromUtf8(_('Miter')), _fromUtf8(_('Bevel')), _fromUtf8(_('Round'))]
        joinStyleList = [Qt.BevelJoin, Qt.MiterJoin, Qt.RoundJoin]

        comboBox.setIconSize(QSize(self.iconWidth, self.iconHeight))
        for i, style in enumerate(joinStyleList):
            pix = QPixmap(self.iconWidth, self.iconHeight)
            pix.fill(Qt.white)
            brush = QBrush(Qt.black)
            pen = QPen(brush, 6.5)
            pen.setJoinStyle(style)
            paint = QPainter(pix)
            paint.setPen(pen)
            path = QPainterPath()
            path.moveTo(5.0, 5.0)
            path.lineTo(15.0, 15.0)
            path.lineTo(25.0, 5.0)

            paint.drawPath(path)
            comboBox.addItem(QIcon(pix), '%s'%joinList[i])
            del paint
        comboBox.setCurrentIndex(joinPattern - 1)



    def setSymbol(self, symbol, comboBox):
        """!
         @brief Set style of point verticles.
         @param comboBox: comboBox to add symbol.
        """
        symbolList = [_fromUtf8(_('Circle')), _fromUtf8(_('Ellipse')), _fromUtf8(_('Square')), _fromUtf8(_('Round Square')), _fromUtf8(_('Pentagon')),
                      _fromUtf8(_('Hexagon')), _fromUtf8(_('Triangle 1')), _fromUtf8(_('Triangle 2')),_fromUtf8(_('Diamond')),_fromUtf8(_('Cross')), _fromUtf8(_('Plus'))]
        comboBox.setStyleSheet("QComboBox { background-color: white; border-color: black; border-width: 2px;}")
        comboBox.clear()
        penStyle = QPen()
        penStyle.setColor(Qt.black)
        penStyle.setWidth(3)

        brushStyle = QBrush()
        brushStyle.setColor(Qt.black)
        brushStyle.setStyle(Qt.SolidPattern)

        for shape in range(1,12):
            pix = QPixmap(100,90)
            pix.fill(Qt.transparent)
            paint = QPainter(pix)

            if shape == 10 or shape == 11:
                 penStyle.setColor(Qt.black)
                 penStyle.setWidth(8)

            paint.setPen(penStyle)
            paint.setBrush(brushStyle)
            path = self.drawSymbol(shape)
            paint.drawPath(path)
            comboBox.addItem(QIcon(pix), '%s'%symbolList[shape-1])
            del paint
        comboBox.setCurrentIndex(symbol - 1)

    def drawSymbol(self, shape):
         """!
         @brief Create point style in QComboBox
         @param shape: the type of selected style of point
         """

         path = QPainterPath()
         if shape == 1: # Point
                path.addEllipse(5, 5, 80, 80)
         elif shape == 2: # Ellipse
                path.addEllipse(5, 15, 80, 60)
         elif shape == 3: # Rectangle
               path.addRect(5, 5, 80, 80)
         elif shape == 4: # Rounded Rectangle
             path.addRoundedRect(5, 5, 80, 80, 50, 50, Qt.RelativeSize)
         elif shape == 5: # Pentagon
             path.moveTo(5.0, 45.0)
             path.lineTo(45.0, 5.0)
             path.lineTo(90.0, 45.0)
             path.lineTo(70.0, 90.0)
             path.lineTo(20.0, 90.0)
             path.closeSubpath()
         elif shape == 6: # Octagon
             path.moveTo(5.0, 45.0)
             path.lineTo(30.0, 5.0)
             path.lineTo(75.0, 5.0)
             path.lineTo(100.0, 45.0)
             path.lineTo(75.0, 90.0)
             path.lineTo(30.0, 90.0)
             path.closeSubpath()
         elif shape == 7: # Triangle Up
             path.moveTo(45.0, 5.0)
             path.lineTo(90.0, 90.0)
             path.lineTo(5.0, 90.0)
             path.closeSubpath()
         elif shape == 8: # Triangle Right
             path.moveTo(5.0, 5.0)
             path.lineTo(90.0, 45.0)
             path.lineTo(5.0, 90.0)
             path.closeSubpath()
         elif shape == 9: # Diamond
             path.moveTo(5.0, 45.0)
             path.lineTo(45.0, 5.0)
             path.lineTo(90.0, 45.0)
             path.lineTo(45.0, 90.0)
             path.closeSubpath()
         elif shape == 10: # Cross
             path.moveTo(5.0, 5.0)
             path.lineTo(90.0, 90.0)
             path.moveTo(90.0, 5.0)
             path.lineTo(5.0, 90.0)
             path.closeSubpath()
         elif shape == 11: # Plus
             path.moveTo(5.0, 45.0)
             path.lineTo(90.0, 45.0)
             path.moveTo(45.0, 5.0)
             path.lineTo(45.0, 90.0)
             path.closeSubpath()

         return path







class EditPointDialog(QDialog, ui_editstylepointdialog.Ui_editStylePointDialog):

    def __init__(self, parent, rowSelected):
        """!
        @brief Edit style of vector file - icon, color and size.
        @param rowSelected: selcted file.
        """
        super(EditPointDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)

        self.file = self.parent.model().files[rowSelected]
        fileName, ext = os.path.splitext(self.file.name)
        self.setWindowTitle(_fromUtf8(_("Edit Style - ")) + fileName)
        self.currentIndex = 0


        self.editObject = Edit(self)
        self.editObject.setColor(self.file.fillStyle.colour, self.fillColorButton)
        self.editObject.setColor(self.file.lineStyle.colour, self.outColorButton)
        self.editObject.setSymbol(self.file.pointStyle.symbol, self.symbolComboBox)


        self.sizeSpinBox.setValue(self.file.pointStyle.size)
        self.sizeSpinBox.setMinimum(1.0)
        self.outWidthSpinBox.setValue(self.file.lineStyle.width)
        self.angleSpinBox.setValue(self.file.pointStyle.symbol)
        self.angleSpinBox.setSingleStep(5)
        self.angleSpinBox.setValue(0.0)
        self.angleSpinBox.setSuffix(u' \N{DEGREE SIGN}')
        self.opacitySpinBox.setValue(self.file.fillStyle.opacity)

        self.applyButton.clicked.connect(self.acceptStyle)
        self.cancelButton.clicked.connect(self.close)

        self.symbolComboBox.setEditable(False)
        #self.symbolComboBox.currentIndexChanged.connect(self.setIndexChanged)


    def acceptStyle(self):
        """!
        @brief Action after click accept.
        """
        self.file.pointStyle.size = self.sizeSpinBox.value()
        self.file.pointStyle.angle = self.angleSpinBox.value()
        self.file.pointStyle.symbol = (self.symbolComboBox.currentIndex()+1)
        self.file.fillStyle.colour = self.fillColorButton.palette().color(1)
        self.file.fillStyle.opacity = self.opacitySpinBox.value()
        self.file.lineStyle.colour = self.outColorButton.palette().color(1)
        self.file.lineStyle.width = self.outWidthSpinBox.value()
        self.emit(SIGNAL("styleChanged()"))





class EditLineDialog(QDialog, ui_editstylelinedialog.Ui_editStyleLineDialog):

    def __init__(self, parent, rowSelected):
        """!
        @brief Edit style of vector file - icon, color and size.
        @param rowSelected: selcted file.
        """
        super(EditLineDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)

        self.file = self.parent.model().files[rowSelected]
        fileName, ext = os.path.splitext(self.file.name)
        self.setWindowTitle(_fromUtf8(_("Edit Style - ")) + fileName)
        self.currentIndex = 0

        self.editObject = Edit(self)
        self.editObject.setColor(self.file.lineStyle.colour, self.colorButton)
        self.editObject.setLinePattern(self.file.lineStyle.patternLine, self.linePatternComboBox)
        self.editObject.setLineJoin(self.file.lineStyle.joinLine, self.lineJoinComboBox)
        self.editObject.setLineCap(self.file.lineStyle.capLine, self.lineCapComboBox)

        self.widthSpinBox.setValue(self.file.lineStyle.width)
        self.widthSpinBox.setMinimum(1.0)
        self.opacitySpinBox.setValue(self.file.fillStyle.opacity)

        self.applyButton.clicked.connect(self.acceptStyle)
        self.cancelButton.clicked.connect(self.close)

    def acceptStyle(self):
        """!
        @brief Action after click accept.
        """
        self.file.lineStyle.colour = self.colorButton.palette().color(1)
        self.file.lineStyle.width = self.widthSpinBox.value()
        self.file.lineStyle.patternLine = (self.linePatternComboBox.currentIndex()+1)
        self.file.lineStyle.capLine = (self.lineCapComboBox.currentIndex()+1)
        self.file.lineStyle.joinLine = (self.lineJoinComboBox.currentIndex()+1)
        self.file.fillStyle.opacity = self.opacitySpinBox.value()
        self.emit(SIGNAL("styleChanged()"))

class EditPolygonDialog(QDialog, ui_editstylepolygondialog.Ui_editStylePolygonDialog):

    def __init__(self, parent, rowSelected):
        """!
        @brief Edit style of vector file - icon, color and size.
        @param rowSelected: selcted file.
        """
        super(EditPolygonDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        self.parent = parent
        self.setupUi(self)

        self.file = self.parent.model().files[rowSelected]
        fileName, ext = os.path.splitext(self.file.name)
        self.setWindowTitle(_fromUtf8(_("Edit Style - ")) + fileName)
        self.currentIndex = 0

        self.editObject = Edit(self)
        self.editObject.setColor(self.file.lineStyle.colour, self.outColorButton)
        self.editObject.setColor(self.file.fillStyle.colour, self.fillColorButton)
        self.editObject.setLinePattern(self.file.lineStyle.patternLine, self.outPatternComboBox)
        self.editObject.setFillPattern(self.file.fillStyle.pattern, self.fillPatternComboBox)

        self.outWidthSpinBox.setValue(self.file.lineStyle.width)
        self.opacitySpinBox.setValue(self.file.fillStyle.opacity)

        self.applyButton.clicked.connect(self.acceptStyle)
        self.cancelButton.clicked.connect(self.close)

    def acceptStyle(self):
        """!
        @brief Action after click accept.
        """
        self.file.lineStyle.colour = self.outColorButton.palette().color(1)
        self.file.lineStyle.width = self.outWidthSpinBox.value()
        self.file.lineStyle.patternLine = (self.outPatternComboBox.currentIndex()+1)
        self.file.fillStyle.colour = self.fillColorButton.palette().color(1)
        self.file.fillStyle.pattern = (self.fillPatternComboBox.currentIndex()+1)
        self.file.fillStyle.opacity = self.opacitySpinBox.value()
        self.emit(SIGNAL("styleChanged()"))


