# -*- coding: utf-8 -*-

import os
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import ui_addlayersdialog
_fromUtf8=QString.fromUtf8


class AddLayersDialog(QDialog, ui_addlayersdialog.Ui_addLayersDialog):
    """!
     Class is used to show all layers contain in vector file (only if vector file has more than one layer e.g GPX file).
    """
    def __init__(self, parent):
        super(AddLayersDialog, self).__init__()
        self.setupUi(self)
        self.parent = parent
        self.rowSelected = []
        ## @var self.rowSelected
        # Index of selected rows.
        self.typeGeometryDict = {1: 'POINT', 2: 'LINESTRING', 3: 'POLYGON', 4: 'MULTIPOINT', 5: 'MULTILINESTRING',
                            6: 'MULTIPOLYGON'}
        ## @var self.typeGeometryDict
        # Dictionary with geometry type as int value and name.
        self.layerTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.addButton.clicked.connect(self.addLayer)
        self.cancelButton.clicked.connect(self.close)
        self.addData()


    def addData(self):
        """!
        @brief Create table with layer.
        """
        fieldName = [_fromUtf8(_('Name of layer')),_fromUtf8(_('Count')),_fromUtf8(_('Geometry'))]
        self.layerTableWidget.clear()
        self.layerTableWidget.setHorizontalHeaderLabels(fieldName)
        for layer in range(self.parent.numLayer):
           vectorLayer = self.parent.vectorDataSource.GetLayerByIndex(layer)
           name = vectorLayer.GetName()
           type = self.parent.checkGeomType(vectorLayer)
           count = vectorLayer.GetFeatureCount()
           itemName = QTableWidgetItem(str(name))
           itemCount = QTableWidgetItem(str(count))
           try:
                itemType = QTableWidgetItem(self.typeGeometryDict[type])
           except:
                itemType = QTableWidgetItem(self.setEmptyLayer(layer))
           self.layerTableWidget.insertRow(layer)
           self.layerTableWidget.setItem(layer, 0, itemName)
           self.layerTableWidget.setItem(layer, 1, itemCount)
           self.layerTableWidget.setItem(layer, 2, itemType)
        self.layerTableWidget.setEditTriggers(QTableWidget.NoEditTriggers)

    def addLayer(self):
        """!
        @brief Load selected layer and close window.
        """
        indexList = self.layerTableWidget.selectionModel().selectedRows()
        for item in indexList:
           self.rowSelected.append(item.row())
        self.emit(SIGNAL("layerAccept()"))
        self.close()

    def getRow(self):
        """!
        @brief Returns selected row.
        """
        return self.rowSelected

    def setEmptyLayer(self, index):
        """!
        @brief Set geometry for empty field.
        @param index: Index of geometry.
        @return Returns type as string.
        """
        if index in (0, 3, 4):
            typeStr = 'POINT'
        elif index == 1:
            typeStr = 'LINESTRING'
        elif index == 2:
            typeStr = 'MULTILINESTRING'
        return typeStr

