import sys, os

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from View import histogramdialog
from View.compositiondialog import CompositionDialog
from View.editstyledialog import EditPointDialog, EditLineDialog, EditPolygonDialog
from View.attributetabledialog import TableDialog
from View.propertiesdialog2 import PropertiesDialog
from View.identifydialog import IdentifyDialog
from Logic import rasterLayerMechanism
from Logic import vectorLayerMechanism
_fromUtf8 =QString.fromUtf8



class LoadedFiles(QAbstractListModel):
    Mimetype = 'application/x-qabstractmodeldatalist'

    def __init__(self,parent=None):
        super(LoadedFiles, self).__init__(parent)
        self.parent = parent
        self.files = []




    def data(self, index, role=Qt.DisplayRole):
        """!
        @brief Returns the data stored under the given role for the item referred to by the index. If you do not have a value to return, return an invalid QVariant.
        @param index: Index of item in the model.
        @param role: Role for item which is used by the view to indicate to the model which type of data it needs. By default the key data to be rendered in the form of text (QString).
        """
        if not index.isValid():
            return None

        if index.row() > len(self.files):
            return None

        if role == Qt.DisplayRole:
            if self.files[index.row()].nameStyleFullPath:
                return self.files[index.row()].pathLayer
            else:
                fileName, ext = os.path.splitext(self.files[index.row()].name)
                return fileName

        elif role == Qt.DecorationRole:
            return self.files[index.row()].icon

        elif role == Qt.CheckStateRole:
            if self.files[index.row()].visible:
                return Qt.Checked
            else:
                return Qt.Unchecked
        return None

    def dropMimeData(self, data, action, row, column, parent):
        """!
        @brief Handles the data supplied by a drag and drop operation that ended with the given action.
        The specified row, column and parent indicate the location of an item in the model where the operation ended.

        When row and column are -1 it means that the dropped data should be considered as dropped directly on parent.
        Usually this will mean appending the data as child items of parent.
        If row and column are greater than or equal zero, it means that the drop occurred just before the specified row and column in the specified parent.
        @param data: The supplied data by a drag and drop operation.
        @param action: Action specifies what to do with the data supplied by a drag and drop operation.
        @param row: The row of dropped data.
        @param column: The column of dropped data.
        @param index: The index of dropped data.
        @param return Returns True if the data and action can be handled by the model; otherwise returns False.
        """

        if action == Qt.IgnoreAction:
            return True

        if not data.hasFormat(self.Mimetype):
            return False

        if column > 0:
            return False

        if not parent.isValid():
            if row < 0:
                endRow = len(self.files)
            else:
                endRow = min(row, len(self.files))
        else:
            endRow = parent.row()

        listObject = str(data.data(self.Mimetype)).split('\n')
        self.editRows(endRow, len(listObject))
        for i, text in enumerate(listObject):
            for obj in self.files:
                if isinstance(obj, rasterLayerMechanism.RasterFile) or isinstance(obj, vectorLayerMechanism.VectorFile):
                    fileName, ext = os.path.splitext(obj.name)
                    if text == fileName:
                        self.setData(self.index(endRow + i, 0), obj)
        return True

    def flags(self, index):
        """!
        @brief Returns flags for the given index.
        @param index: Index of item in the model.
        """
        flags = super(LoadedFiles, self).flags(index)

        if index.isValid():
            flags |= Qt.ItemIsSelectable
            flags |= Qt.ItemIsDragEnabled
            flags |= Qt.ItemIsEnabled
            flags |= Qt.ItemIsUserCheckable
        else:
            flags |= Qt.ItemIsDropEnabled
            flags |= Qt.ItemIsEnabled

        return flags

    def insertRows(self, file, position, count=1, index=QModelIndex()):
        """!
        @brief Insert count file into the model before the given position.
        @param file: The file which is about to be insterted.
        @param positon: The position where it will be placed.
        @param count: The quantity of inserting rows. By default is 1 if a single file is placed into the list.
        @param index: The index of inserting rows. By default creates new empty index from QModelIndex class.
        @return Returns True if the rows were successfully inserted; otherwise returns False.
        """
        if index.isValid():
            return False

        self.beginInsertRows(index, position, position + count - 1)
        for row in range(count):
                self.files.insert(position + row, file)
        self.endInsertRows()
        return True

    def editRows(self, row, count, parent=QModelIndex()):
        """!
        @brief Insert row into the model after the given position in drag and drop operation.
        @param row: The row which is about to be insterted.
        @param positon: The position where it will be placed.
        @param count: The quantity of inserting rows. By default is 1 if a single file is placed into the list.
        @param index: The index of inserting rows. By default creates new empty index from QModelIndex class.
        @return Returns True if the rows were successfully inserted; otherwise returns False.
        """

        self.beginInsertRows(QModelIndex(), row, row + count -1)
        self.files[row:row] = [object()] * count
        self.endInsertRows()
        return True

    def mimeData(self, indexes):
        """!
        @brief Returns an object that contains serialized items of data corresponding to the list of indexes specified.
        The formats used to describe the encoded data is obtained from the mimeTypes() function.
        @param indexes: The list of indexes of data.
        """

        mimeData = QMimeData()
        for index in indexes:
            if index.isValid():
                encodedData = self.data(index, Qt.DisplayRole)
        mimeData.setData(self.Mimetype, encodedData)
        return mimeData

    def mimeTypes(self):
        """!
        @brief Returns a list of MIME types that can be used to describe a list of model indexes.
        """
        return [self.Mimetype]

    def removeRows(self, row, count=1, parent=QModelIndex()):
       """!
       @brief Removes count rows starting with the given row under parent parent from the model.
       @param row: The rows which is about to be removed.
       @param count: The quantity of removing row.
       @param parent: The index of removing rows.
       @return Returns True if the rows were successfully removed; otherwise returns False.
       """
       self.beginRemoveRows(QModelIndex(), row, row + count - 1)
       self.files.pop(row)
       self.endRemoveRows()
       self.parent.update_ui()

       if self.rowCount() == 0:
           self.emit(SIGNAL("filesListEmpty()"))

       return True

    def rowCount(self, parent=QModelIndex()):
        """!
        @brief Returns the number of rows under the given parent.
        @param parent: Index of data.
        """

        return len(self.files)

    def setData(self, index, value, role=Qt.EditRole):
        """!
        @brief Sets the role data for the item at index to value.
        @param index: Index of setting data.
        @param value: The value to set.
        @param role: Role for item which is used by the view to indicate to the model which type of data it needs. By default the data in a form suitable for editing in an editor (QString).
        @return Returns True if successful; otherwise returns False.
        """
        if index.isValid() and 0 <= index.row() < len(self.files):
            file = self.files[index.row()]
            if role == Qt.CheckStateRole:
                file.visible = value.toBool()
                self.dataChanged.emit(index, index)
            else:
                self.files[index.row()] = value
            return True
        return False

    def supportedDropActions(self):
        """!
        @brief Sets the supported drag actions for the items in the model.
        """
        return Qt.MoveAction

    def getFiles(self):
        """!
        @brief Returns item into model
        """
        return self.files

    def isVector(self, index = QModelIndex):
        """!
        @brief Returns True if selected file is vector; otherwise returns False
        """
        return self.files[index.row()].isVector

class LoadedFiles_ListView(QListView):

    def __init__(self, parent = None):
        self.parent = parent
        self.lastDirPath = self.parent.lastDirPath
        self.right = False
        super(LoadedFiles_ListView, self).__init__(parent)
        self.setContextMenuPolicy(Qt.CustomContextMenu)

        # view
        self.setDropIndicatorShown(True)

        # interactive
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setDragDropMode(QAbstractItemView.DragDrop)
        self.setAcceptDrops(True)
        self.setDragEnabled(True)

        self.nameGroup = QActionGroup(self)
        self.fullPath = QAction(_fromUtf8(_("&Full path")), self)
        self.fullPath.setCheckable(True)
        self.nameGroup.addAction(self.fullPath)
        self.onlyName = QAction(_fromUtf8(_("&Only name")), self)
        self.onlyName.setCheckable(True)
        self.nameGroup.addAction(self.onlyName)
        self.checkIcon = False



    def menuRaster(self, pos):
        """!
        @brief Sets the context menu for the raster file.
        @param pos: Position of the context menu event.
        """

        menu = QMenu("Context Menu", self)
        menu.setStyleSheet(_fromUtf8("/*\n"
		"* GLOBAL font and background SETTINGS.\n"
		"*/\n"
		"\n"
		"* {\n"
		"    font-size: 11px;\n"
		"    font-family: Tahoma, Verdana, Arial, sans-serif;\n"
		"    color: black;\n"
		"}\n"
		))
        self.fileProperties = QAction(self)
        self.fileProperties.setText(_fromUtf8(_("&Properties")))
        self.fileProperties.setIcon(QIcon(_fromUtf8(":/neue/resources/rasterproperties.png")))
        self.connect(self.fileProperties, SIGNAL("triggered()"), self.properties)

        self.removeLayer = QAction(self)
        self.removeLayer.setText(_fromUtf8(_("&Remove layer")))
        self.removeLayer.setIcon(QIcon(_fromUtf8(":/neue/resources/minuslayers.png")))
        self.connect(self.removeLayer, SIGNAL("triggered()"), self.removeCurrentLayer)
        self.addAction(self.removeLayer)

        self.zoomFullExtent = QAction(self)
        self.zoomFullExtent.setText(_fromUtf8(_("&Zoom full extent")))
        self.zoomFullExtent.setIcon(QIcon(_fromUtf8(":/menuContext/resources/zoomfull.png")))
        self.connect(self.zoomFullExtent, SIGNAL("triggered()"), self.fullExtent)
        self.addAction(self.zoomFullExtent)

        self.showHistogramMenu = QAction(self)
        self.showHistogramMenu.setText(_fromUtf8(_("Show &Histogram and Adjust")))
        self.showHistogramMenu.setIcon(QIcon(_fromUtf8(":/neue/resources/histogram.png")))
        self.connect(self.showHistogramMenu, SIGNAL("triggered()"), self.showHistogram)
        self.addAction(self.showHistogramMenu)

        self.compositionRGBMenu = QAction(self)
        self.compositionRGBMenu.setText(_fromUtf8(_("Ra&ster RGB composition")))
        self.compositionRGBMenu.setIcon(QIcon(_fromUtf8(":/neue/resources/rgb.png")))
        self.connect(self.compositionRGBMenu, SIGNAL("triggered()"), self.compositionRGB)
        self.addAction(self.compositionRGBMenu)

        menu.addAction(self.fileProperties)
        menu.addAction(self.removeLayer)
        menu.addAction(self.zoomFullExtent)
        menu.addAction(self.showHistogramMenu)
        menu.addAction(self.compositionRGBMenu)

        nameStyleMenu = QMenu(_fromUtf8(_("Showing style")))
        nameStyleMenu.setIcon(QIcon(_fromUtf8(":/neue/resources/style.png")))
        menu.addAction(nameStyleMenu.menuAction())
        nameStyleMenu.addActions(self.nameGroup.actions())
        self.connect(self.fullPath, SIGNAL("triggered()"), self.setFullPath)
        self.connect(self.onlyName, SIGNAL("triggered()"), self.setOnlyName)
        if self.model().files[self.selectedIndexes()[0].row()].nameStyleFullPath:
            self.fullPath.setChecked(True)
        else:
            self.onlyName.setChecked(True)

        action = menu.exec_(self.mapToGlobal(pos))

    def menuVector(self, pos):
        """!
        @brief Sets the context menu for the vector file.
        @param pos: Position of the context menu event.
        """

        menu = QMenu("Context Menu", self)
        menu.setStyleSheet(_fromUtf8("/*\n"
		"* GLOBAL font and background SETTINGS.\n"
		"*/\n"
		"\n"
		"* {\n"
		"    font-size: 11px;\n"
		"    font-family: Tahoma, Verdana, Arial, sans-serif;\n"
		"    color: black;\n"
		"}\n"
		))
        self.fileProperties = QAction(self)
        self.fileProperties.setText(_fromUtf8(_("&Properties")))
        self.fileProperties.setIcon(QIcon(_fromUtf8(":/menuContext/resources/editfeat.png")))
        self.connect(self.fileProperties, SIGNAL("triggered()"), self.properties)

        self.removeLayer = QAction(self)
        self.removeLayer.setText(_fromUtf8(_("&Remove layer")))
        self.removeLayer.setIcon(QIcon(_fromUtf8(":/neue/resources/minuslayers.png")))
        self.connect(self.removeLayer, SIGNAL("triggered()"), self.removeCurrentLayer)
        self.addAction(self.removeLayer)

        self.zoomFullExtent = QAction(self)
        self.zoomFullExtent.setText(_fromUtf8(_("&Zoom full extent")))
        self.zoomFullExtent.setIcon(QIcon(_fromUtf8(":/menuContext/resources/zoomfull.png")))
        self.connect(self.zoomFullExtent, SIGNAL("triggered()"), self.fullExtent)
        self.addAction(self.zoomFullExtent)

        self.attributeTable = QAction(self)
        self.attributeTable.setText(_fromUtf8(_("&Open Attribute Table")))
        self.attributeTable.setIcon(QIcon(_fromUtf8(":/menuContext/resources/atrrtable.png")))
        self.connect(self.attributeTable, SIGNAL("triggered()"), self.openAttributeTable)
        self.addAction(self.attributeTable)

        self.editStyle = QAction(self)
        self.editStyle.setText(_fromUtf8(_("&Edit Style")))
        self.editStyle.setIcon(QIcon(_fromUtf8(":/neue/resources/style.png")))
        self.connect(self.editStyle, SIGNAL("triggered()"), self.editVectorStyle)
        self.addAction(self.editStyle)

        menu.addAction(self.fileProperties)
        menu.addAction(self.removeLayer)
        menu.addAction(self.zoomFullExtent)
        menu.addAction(self.attributeTable)
        menu.addAction(self.editStyle)

        nameStyleMenu = QMenu(_fromUtf8(_("Showing style")))
        menu.addAction(nameStyleMenu.menuAction())
        nameStyleMenu.addActions(self.nameGroup.actions())
        self.connect(self.fullPath, SIGNAL("triggered()"), self.setFullPath)
        self.connect(self.onlyName, SIGNAL("triggered()"), self.setOnlyName)

        action = menu.exec_(self.mapToGlobal(pos))

    def compositionRGB(self):
        """!
        @brief Display Raster RGB Composition dialog
        """
        try:
            self.compositionRGBDialog = CompositionDialog(self, self.selectedIndexes()[0].row())
            self.compositionRGBDialog.show()
            self.connect(self.compositionRGBDialog, SIGNAL("compositionChanged()"), self.parent.update_ui)
        except IndexError:
            self.emit(SIGNAL("statusBarChanged"), QString("Impossible to change raster composition"))

    def properties(self, file=None):
        """!
        @brief Display Properties dialog
        """
        if not file:
            sourceFile = self.model().files[self.selectedIndexes()[0].row()]
        else:
            sourceFile = file
        self.propertiesDialog = PropertiesDialog(parent=self, file=sourceFile)
        self.propertiesDialog.show()

    def fullExtent(self):
        """!
        @brief Emit signal to zoom full extent
        """
        self.emit(SIGNAL("zoomFullExtent(PyQt_PyObject)"), self.model().files[self.selectedIndexes()[0].row()])

    def removeCurrentLayer(self):
        """!
        @brief Remove selected layer
        """
        for i, row in enumerate(self.selectedIndexes()):
            if i == 0:
                self.model().removeRows(row.row())
            else:

                self.model().removeRows((row.row()-i))
        self.parent.update_ui()

    def showHistogram(self):
        """!
        @brief Display Histogram dialog
        """

        try:
            self.histogramDialog = histogramdialog.HistogramDialog(self, self.selectedIndexes()[0].row())
            self.histogramDialog.showHistogramPlot()
            self.histogramDialog.show()
            self.connect(self.histogramDialog, SIGNAL("histogramChanged()"), self.parent.update_ui)
        except IndexError:
            self.emit(SIGNAL("statusBarChanged"), QString(_fromUtf8(_("No files loaded into AgroEye!"))))

    def openAttributeTable(self):
        """!
        @brief Display Attribute Table dialog
        """
        try:
            self.attributeTableDialog = TableDialog(self, self.selectedIndexes()[0].row())
            self.attributeTableDialog.show()
        except IndexError:
            self.emit(SIGNAL("statusBarChanged"), QString(_fromUtf8(_("Impossible to show attribute table"))))

    def editVectorStyle(self):
        """!
        @brief Display Edit Style dialog
        """
        try:
            geomType = self.model().files[self.selectedIndexes()[0].row()].geomType
            if geomType in (1,4):
                self.editLayersStyleDialog = EditPointDialog(self, self.selectedIndexes()[0].row())
            elif geomType in (2,5):
                self.editLayersStyleDialog = EditLineDialog(self, self.selectedIndexes()[0].row())
            elif geomType in (3,6):
                self.editLayersStyleDialog = EditPolygonDialog(self, self.selectedIndexes()[0].row())

            self.editLayersStyleDialog.show()
            self.connect(self.editLayersStyleDialog, SIGNAL("styleChanged()"), self.parent.update_ui)
        except IndexError:
            self.emit(SIGNAL("statusBarChanged"), QString(_fromUtf8(_("Impossible to change vector style"))))

    def setFullPath(self):
        """!
        @brief Show full path of file
        """
        for row in self.selectedIndexes():
            raster = self.model().files[row.row()]
            raster.nameStyleFullPath = True

    def setOnlyName(self):
        """!
        @brief Show short name of file
        """
        for row in self.selectedIndexes():
            raster = self.model().files[row.row()]
            raster.nameStyleFullPath = False

    def identify(self, file=None):
        """!
        @brief Display window for identification of bands
        """
        if not file:
            sourceFile = self.model().files[self.selectedIndexes()[0].row()]
        else:
            sourceFile = file
        self.identificationDialog = IdentifyDialog(self, sourceFile)
        self.identificationDialog.show()
