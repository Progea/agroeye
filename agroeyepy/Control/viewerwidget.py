# coding: utf-8
import sys, os


from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4.QtGui import QAbstractScrollArea, QPainter, QRubberBand, QCursor, QApplication
from PyQt4.QtGui import QPixmap, QPainterPath, QPen
from PyQt4.QtCore import Qt, QRect, QSize, QPoint, SIGNAL


class ViewerWidget(QAbstractScrollArea):
    """
    Widget z wyswietlanym obrazem
    """
    def __init__(self, parent):
        QtGui.QAbstractScrollArea.__init__(self, parent)
        self.setMinimumSize()




