import gettext
import os
from PyQt4.QtCore import QString
_fromUtf8 =QString.fromUtf8

class Exception(Exception):
    'Base class for exception'

#QMessage, albo self.icon tutaj.
class OpenFileError(Exception):
    "Raised when the input file is invalid"
    def __init__(self, name):
        self.name = name
        self.msg = _fromUtf8(_('Geometry type is not supported.'))
        self.title = _fromUtf8(_("Open File Error"))

class LayerError(Exception):
    "Raised when the layer is invalid"
    def __init__(self, layer):
        self.layer = layer
        self.msg = _fromUtf8(_("No valid layers!"))
        self.title = _fromUtf8(_("Layer Error"))

class DriverError(Exception):
    "Raised when the driver is not available"
    def __init__(self, driverName):
        self.driverName = driverName
        self.msg = _fromUtf8(_("%s driver not available."%self.driverName))
        self.title = _fromUtf8(_("Driver Error"))

class PathError(Exception):
    "Raised when the filepath is invalid"
    def __init__(self, path):
        self.path = path
        self.msg = _fromUtf8(_("The path %s is not valid."%self.path))
        self.title = _fromUtf8(_("Path Error"))

class GeometryTypeException(Exception):
    "Raised when the geometry type is not supported"
    def __init__(self, geomType):
        self.geomType = geomType
        self.msg = _fromUtf8(_("Geometry type %s is not supported."%self.geomType))
        self.title = _fromUtf8(_("Geometry Error"))

class SpatialReferenceSystemError(Exception):
    "Raised when the srs is invalid"
    def __init__(self):
        self.msg = _fromUtf8(_("Spatial Reference System is unknown. This data can be invalid drawn."))
        self.title = _fromUtf8(_("Spatial Reference System Error"))

class FeatureError(Exception):
    "Raised when the layer has no features" 
    def __init__(self):
        self.msg = _fromUtf8(_("Layer has no features."))
        self.title = _fromUtf8(_("Feature Error"))

class UniqueNameError(Exception):
    "Raised when the name of field is not unique"
    def __init__(self):
        self.msg = _fromUtf8(_("Field with the same name is already exists.\nPlease choose another name."))
        self.title = _fromUtf8(_("Add new field Error"))

class PolygonInvalidError(Exception):
    "Raised when the polygon is valid"
    def __init__(self):
        self.msg = _fromUtf8(_("Drawn polygon is invalid."))
        self.title = _fromUtf8(_("Polygon Error"))

class EPSGCodeError(Exception):
    "Raised when the epsg is invalid"
    def __init__(self, code):
        self.code = code
        self.msg = _fromUtf8(_("Code %s not found in EPSG support files.\nEnter valid EPSG code."%self.code))
        self.title = _fromUtf8(_("Coordinates System Error"))

class FileFormatError(Exception):
    "Raised when format of loaded file is not supported"
    def __init__(self, ext):
        self.ext = ext
        self.msg = _fromUtf8(_("%s not recognized as a supported file format."%self.ext))
      #  self.img = QIcon(_fromUtf8(":/menuContext/resources/zoomfull.png"))
        self.title = _fromUtf8(_("Open File Error"))

class NumberCharWarning(Exception):
    "Raised when filename is too long"
    def __init__(self):
        self.msg = _fromUtf8(_("The name of file is invalid:\n name is limited to 15 characters."))
        self.title = _fromUtf8(_("Save shapefile warning"))

class SpecialCharWarning(Exception):
    "Raised when filename contain special characters, other than underscores"
    def __init__(self):
        self.msg = _fromUtf8(_("The name of file is invalid:\n valid name may contain letters, numbers or underscores."))
        self.title = _fromUtf8(_("Save shapefile warning"))

class StartCharWarning(Exception):
    "Raised when filename may not start with letters"
    def __init__(self):
        self.msg = _fromUtf8(_("The name of file is invalid:\n valid name may start with letters."))
        self.title = _fromUtf8(_("Save shapefile warning"))

class SaveFileError(Exception):
    "Raised when saving failed"
    def __init__(self):
        self.msg = _fromUtf8(_("Changes have not been saved!"))
        self.title = _fromUtf8(_("Save File Error"))

class ValidatorError(Exception):
    "Raised when validator is unknown"
    def __init__(self, name):
        self.name = name
        self.msg = _fromUtf8(_("Type of field %s is unknown."%self.name))
        self.title = _fromUtf8(_("Field type Error"))

class OverwriteFileError(Exception):
    "Raised when overwrite file is used by another program"
    def __init__(self):
        self.msg = _fromUtf8(_("File is used by another program."))
        self.title = _fromUtf8(_("Overwrite Error"))

class OpenProjectError(Exception):
    "Raised when input project is not AgroEye project"
    def __init__(self):
        self.msg = _fromUtf8(_('Project is not AgroEye project file.'))
        self.title = _fromUtf8(_('Project error'))

class InvalidRasterOrientated(Exception):
    "Raised when input raster is wrong orientated"
    def __init__(self):
        self.msg = _fromUtf8(_('Wrong file! Image must be north orientated.'))
        self.title = _fromUtf8(_('Raster error'))

class ScaleError(Exception):
    "Raised when input scale value is incorrect"
    def __init__(self):
        self.msg = _fromUtf8(_('Scale entered incorrect.'))
        self.title = _fromUtf8(_('Zoom scale error'))