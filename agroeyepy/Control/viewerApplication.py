# -*- coding: utf-8 -*-

import agroeye
import sys
import os
sys.path.insert(1, os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '')))
from PyQt4 import QtCore, QtGui, Qt
from PyQt4.Qt import *
from PyQt4.QtGui import QFileDialog
import atexit
import tempfile
import gettext
from shutil import rmtree
from osgeo import gdal


from View import MainWindow, viewarea, measuredistancetool, measureareatool, preferences, addnewvectorlayerdialog, rastercalculator, modeldialog
import ast
from Logic import rasterLayerMechanism
from Logic import vectorLayerMechanism
from Control import loadedfiles_model
from Control import exceptionModel

_fromUtf8 =QString.fromUtf8



"""@package docstring
Documentation for this module

More details
"""


class AgroEye(QtGui.QMainWindow, MainWindow.Ui_AE_Window):
    """!
    @brief Main Class
    Everything runs from this point
    """

    def __init__(self, parent=None):
        """!
        @brief Default constructor. Main body of the program.
        """
        super(AgroEye, self).__init__(parent)
        self.setupEnviromentalVariables()
        self.setTempDirectory()
        self.setupUi(self)
        self.printer = QtGui.QPrinter()
        self.scaleFactor = 5.0
        self.currentLang = 'eng'
        self.translator = QTranslator()
        self.currentLocale = self.currentLang + '_' + self.currentLang.upper()
        self.setLanguageMenu()
        self.statusBar().showMessage(_fromUtf8(_('AgroEye application is ready to work!')))

        self.viewArea = viewarea.ViewArea(self)
        self.setCentralWidget(self.viewArea)
        self.dockWidget_Coordinates.setTitleBarWidget(QWidget(self.dockWidget_Coordinates))
        self.dockWidget_Coordinates.setFloating(False)
        self.connect(self.viewArea, QtCore.SIGNAL("statusBarChanged"), self.updateMessage)
        self.coordinateSystemComboBox.setEnabled(False)
        self.zoomComboBox.setEnabled(False)
        self.coordinatesLineEdit.setEnabled(False)

        self.upLeft = None
        self.downRight = None
        self.ratio = None
        self.desktopWidget = QtGui.QDesktopWidget()

        userhome = os.path.expanduser('~')
        import platform
        if platform.system() == 'Windows':
            self.lastDirPath = userhome + '\Desktop'
            self.lastDirProjectPath = userhome + '\Desktop'
        else:
            self.lastDirPath = userhome
            self.lastDirProjectPath = userhome

        self.fileLocation = None

        self.onOffActions = [self.menuTools, self.menuGAEC, self.actionZoomIn, self.actionZoomOut,
                             self.actionNormalSize, self.coordinateSystemComboBox, self.menuZoom,
                             self.actionSaveProject, self.actionSaveProjectAs]
        """
        Establishing model of loaded files and
        connecting it with tabview inside dock widget on the left side
        """

        self.loadedfiles_tableview = loadedfiles_model.LoadedFiles_ListView(self)
        self.loadedfiles_tableview.setObjectName("loadedfiles_tableview")
        self.verticalLayout.addWidget(self.loadedfiles_tableview)
        self.loadedfiles_model = loadedfiles_model.LoadedFiles(self)
        self.loadedfiles_tableview.setModel(self.loadedfiles_model)
        self.loadedfiles_tableview.customContextMenuRequested.connect(self.onRightClick)
        self.preferencesDialog = preferences.Preferences(self)
        self.connect(self.loadedfiles_tableview, QtCore.SIGNAL("statusBarChanged"), self.updateMessage)
        self.viewArea.addFilesModel(self.loadedfiles_model.files)


        QtCore.QObject.connect(self.loadedfiles_model, QtCore.SIGNAL("dataChanged(QModelIndex, QModelIndex)"),
                               self.update_ui)
        QtCore.QObject.connect(self.viewArea, QtCore.SIGNAL("viewAreaResized()"), self.update_ui)
        QtCore.QObject.connect(self.viewArea, QtCore.SIGNAL("viewAreaZoomed()"), self.update_ui)
        QtCore.QObject.connect(self.viewArea, QtCore.SIGNAL("mouseCoordinates(QPointF)"), self.setCoordinatesLineEdit)
        QtCore.QObject.connect(self.viewArea, QtCore.SIGNAL("zoomFullExtent(PyQt_PyObject)"),
                               self.viewArea.fullExtent)
        QtCore.QObject.connect(self.viewArea, QtCore.SIGNAL("properties(PyQt_PyObject)"),
                               self.loadedfiles_tableview.properties)
        QtCore.QObject.connect(self.viewArea, QtCore.SIGNAL("identify(PyQt_PyObject)"),
                               self.loadedfiles_tableview.identify)
        QtCore.QObject.connect(self.actionZoomIn, QtCore.SIGNAL("triggered()"), self.viewArea.zoomIn)
        QtCore.QObject.connect(self.actionZoomOut, QtCore.SIGNAL("triggered()"), self.viewArea.zoomOut)
        QtCore.QObject.connect(self.actionPan_Action, QtCore.SIGNAL("toggled(bool)"), self.viewArea.panActionToggled)
        QtCore.QObject.connect(self.actionNormalSize, QtCore.SIGNAL("triggered()"),
                               lambda: self.viewArea.fullExtent(None))
        QtCore.QObject.connect(self.viewArea, QtCore.SIGNAL("setUpEPSG(int)"),
                               lambda epsg: self.viewArea.setUpEPSG(self.coordinateSystemComboBox, epsg))
        QtCore.QObject.connect(self.coordinateSystemComboBox, QtCore.SIGNAL("activated(int)"),
                               lambda chooseIndex: self.viewArea.chooseEPSG(chooseIndex, self.coordinateSystemComboBox))
        QtCore.QObject.connect(self.loadedfiles_tableview, QtCore.SIGNAL("zoomFullExtent(PyQt_PyObject)"),
                               self.viewArea.fullExtent) # Load files/Zoom full extent
        QtCore.QObject.connect(self.zoomComboBox.lineEdit(), QtCore.SIGNAL("returnPressed()"),
                               lambda: self.viewArea.changeZoom(lineEdit=self.zoomComboBox.lineEdit()))
        QtCore.QObject.connect(self.zoomComboBox, QtCore.SIGNAL("activated(QString)"),
                               lambda value: self.viewArea.changeZoom(expression=value))
        QtCore.QObject.connect(self.zoomComboBox, QtCore.SIGNAL("editTextChanged(QString)"),
                               lambda textEdited: self.viewArea.zoomTyped(textEdited, self.zoomComboBox))
        QtCore.QObject.connect(self.zoomComboBox, QtCore.SIGNAL("currentIndexChanged(int)"),
                               self.viewArea.setNoStyleZoomComboBox)

        QtCore.QObject.connect(self, QtCore.SIGNAL("fileLoaded()"), self.enableMenus)
        QtCore.QObject.connect(self.loadedfiles_model, QtCore.SIGNAL("filesListEmpty()"), self.disableMenus)


        QtCore.QObject.connect(self.actionOpen_file, QtCore.SIGNAL('triggered()'),self.openFile)
        QtCore.QObject.connect(self.actionOpenProject, QtCore.SIGNAL("triggered()"), self.openProject) # Menu File/Open project
        QtCore.QObject.connect(self.actionSaveProject, QtCore.SIGNAL("triggered()"), self.saveProject) # Menu File/Save
        QtCore.QObject.connect(self.actionSaveProjectAs, QtCore.SIGNAL("triggered()"), self.saveProjectAs) # Menu File/Save as
        QtCore.QObject.connect(self.actionModelGAEC, QtCore.SIGNAL("triggered()"), self.openGAECModel) # Menu GAEC/Choose rules
        QtCore.QObject.connect(self.actionPreferences, QtCore.SIGNAL("triggered()"), self.openPreferencesDialog) # Menu Edit/Preferences
        QtCore.QObject.connect(self.actionMeasureDistance, QtCore.SIGNAL("triggered()"),self.showMeasuringDistanceTool) # Menu Tools/Measure Distance
        QtCore.QObject.connect(self.actionMeasure_area, QtCore.SIGNAL("triggered()"),self.showMeasuringAreaTool) # Menu Tools/Measure Area
        QtCore.QObject.connect(self.actionCreate_Shapefile, QtCore.SIGNAL("triggered()"),self.addNewVectorLayer) # Menu Tools/Create shapefile
        QtCore.QObject.connect(self.actionRaster_Calculator, QtCore.SIGNAL("triggered()"),self.showRasterCalculator) # Menu Tools/Raster Calculator
        QtCore.QObject.connect(self.actionAbout_application, QtCore.SIGNAL('triggered()'),self.about)  # Menu Help/About Application
        QtCore.QObject.connect(self.actionAbout_extencions, QtCore.SIGNAL('triggered()'),self.about_ext)  # Menu Help/About Extensions



    def setLanguageMenu(self):
        self.langGroup = QActionGroup(self.menuChange_Language)
        actionEnglishLang = QAction(QString("English (English)"), self.langGroup)
        actionEnglishLang.setIcon(QIcon(_fromUtf8(":/menuEdit/resources/english.png")))
        actionEnglishLang.setObjectName(_fromUtf8("eng"))
        actionPolishLang = QAction(QString("Polski (Polish)"), self.langGroup)
        actionPolishLang.setIcon(QIcon(_fromUtf8(":/menuEdit/resources/polish.png")))
        actionPolishLang.setObjectName(_fromUtf8("pl"))
        actions = (actionEnglishLang, actionPolishLang)
        map(self.menuChange_Language.addAction, actions)
        map(lambda action: action.setCheckable(True), actions)
        map(lambda action: action.triggered.connect(lambda: self.setLanguage(action.objectName())), actions)
        actionEnglishLang.setChecked(True)
        self.setLanguage(self.currentLang)

    def setLanguage(self, lang):

        if self.currentLang != lang:
            self.currentLang = str(lang)
            self.currentLocale = self.currentLang + '_' + self.currentLang.upper()

        self.switchAppTranslator(self.currentLocale,'Translations', self.currentLang)
        transName = self.currentLocale + '.qm'
        self.switchQtTranslator(transName, 'Translations/'+self.currentLang+'/PYQT/')


    def switchAppTranslator(self, filename, dir, lang):
        local = os.getcwd()
        os.chdir('..')
        transApp = gettext.translation(filename, localedir=dir, languages=[lang])
        transApp.install()
        os.chdir(local)


    def switchQtTranslator(self, fileName, dir):
        local = os.getcwd()
        os.chdir('..')
        app.removeTranslator(self.translator)
        if self.translator.load(fileName, dir):
            app.installTranslator(self.translator)
            self.retranslateUi(self)
        os.chdir(local)


    def update_ui(self):
        """!
        @brief Updates display. Checks which layers are visible and show them.
        @Returns two list - all loaded files and all visible files.
        """
        files = self.loadedfiles_model.getFiles()
        visibleFileList = []
        if files:
            self.actionRaster_Calculator.setEnabled(True)
            self.actionMeasure_area.setEnabled(True)
            self.actionMeasureDistance.setEnabled(True)
            self.actionModelGAEC.setEnabled(True)
            self.actionCreate_Shapefile.setEnabled(True)
            for file in files:
                if file.visible:
                    visibleFileList.append(file)
        else:
            self.actionRaster_Calculator.setEnabled(False)
            self.actionMeasure_area.setEnabled(False)
            self.actionMeasureDistance.setEnabled(False)
            self.actionModelGAEC.setEnabled(False)
            self.actionCreate_Shapefile.setEnabled(False)
        self.viewArea.convertLayersToData(visibleFileList, files)


    def setCoordinatesLineEdit(self, point):
        """!
        @brief Sets received signal into bottom widget with coordinates.
        @param point: Coordinates of received point which are about to be set in the bottom.
        """
        try:
            if self.viewArea.viewAreaSpatialReference.GetAttrValue('UNIT') == 'degree':
                y = str(round(point.x(), 5))
                x = str(round(point.y(), 5))
                unit2 = u'\u00B0E'
                unit1 = u'\u00B0N'

            #if self.viewArea.viewAreaSpatialReference.GetAttrValue('UNIT') == ('metre' or 'meter'):
            else:
                unit1 = 'm'
                unit2 = unit1
                x = str(round(point.x(), 2))
                y = str(round(point.y(), 2))

            self.coordinatesLineEdit.setText(x + unit1 + ', ' + y + unit2)
        except AttributeError:
            self.coordinatesLineEdit.setText(QString(_("None")))

    def openFile(self, fname=None):
        """!
        @brief Opens raster or vector file and adds it into model list.
        Returns nothing.
        """

        gdal = '*.tif *.tiff *.jpg *.jpeg *.img *.asc *.ecw *.ers *.xyz'
        ogr = '*.shp *.gml *kml *.gpx *.gdb *.PLScenes *.csv *.spatialite, *.sqlite'
        filters = '[GDAL] All (%s);;'  \
                  '[OGR] All (%s);;' \
                  '[GDAL] GeoTIFF (*.tif *.tiff);;' \
                  '[GDAL] Standard Raster Product (*.img );;' \
                  '[GDAL] JPEG JFIF (*.jpg *.jpeg);;' \
                  '[GDAL] Arc/Info ASCII Grid (*.asc);;' \
                  '[GDAL] ERDAS Compressed Wavelets (*.ecw);;' \
                  '[GDAL] ERMapper (*.ers);;' \
                  '[GDAL] ASCII Gridded XYS (*.xyz);;' \
                  '[GDAL] Sentinel SAFE (manifest.safe);;' \
                  '[OGR] ESRI Shapefile (*.shp);;' \
                  '[OGR] Geography Markup Language (*.gml);;' \
                  '[OGR] Keyhole Markup Language (*.kml);;' \
                  '[OGR] Format GPS eXchange (*.gpx );;' \
                  '[OGR] ESRI FileGDB (*.gdb);;' \
                  '[OGR] Planet Labs Scenes API (*.PLScenes);;'\
                  '[OGR] Comma Separated Value (*.csv);;'\
                  'All files (*)'% (gdal, ogr)

        if not fname:
            dialog = QFileDialog()
            dialog.setFileMode(QFileDialog.ExistingFiles)
            dialog.setDirectory(self.lastDirPath)
            fname = dialog.getOpenFileNamesAndFilter(self, _fromUtf8(_('Add data')), '', filters, _fromUtf8(_('All files (*)')))

        if not fname[0].isEmpty():
            for file in fname[0]:
                checkName, isVector = self.checkFileIsValid(file)

                if checkName:
                        if not isVector:
                            rasterDataset = rasterLayerMechanism.RasterFile(checkName, self.viewArea)
                            self.loadedfiles_model.insertRows(rasterDataset, 0)
                            self.connect(rasterDataset, QtCore.SIGNAL("updateUI()"), self.update_ui)
                        else:
                            checkObj= vectorLayerMechanism.VectorCheckLayer(self, checkName)
                            for i in range(len(checkObj.row)):
                                vectorDataset = vectorLayerMechanism.VectorFile(self, checkObj, checkObj.row[i], self.tempDir)
                                if vectorDataset:
                                    self.loadedfiles_model.insertRows(vectorDataset, 0)
                                    self.connect(vectorDataset, QtCore.SIGNAL("updateUI()"), self.update_ui)

                        self.update_ui()
                        self.emit(QtCore.SIGNAL("fileLoaded()"))

    def openProject(self):
        dom = QDomDocument()
        error = None
        fh = None

        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFiles)
        dialog.setDirectory(self.lastDirProjectPath)
        fname = dialog.getOpenFileNameAndFilter(self, _fromUtf8(_('Open project')),'', '*.xml', _('XML project files'))


        if not fname[0].isEmpty():
            rep = QMessageBox.Yes
            try:
                if self.loadedfiles_model.files:
                    rep = QMessageBox.warning(self, _fromUtf8(_('Open project warning')), _fromUtf8(_('Opening project will remove current file and properties. Do you want to continue ?')), QMessageBox.Yes | QMessageBox.No)

                if rep == QMessageBox.Yes:
                    self.loadedfiles_model.files = []
                    self.fileLocation = fname[0]
                    fh = QFile(fname[0])
                    fh.open(QFile.ReadOnly | QFile.Text)
                    dom.setContent(fh)

                    root = dom.documentElement()
                    if root.tagName() != 'AGROEYE':
                        raise exceptionModel.OpenProjectError()
                    node = root.firstChild()
                    while not node.isNull():
                        if node.toElement().tagName() == "file":
                            self.readFile(node.toElement())
                        if node.toElement().tagName() == "viewarea":
                            self.setupViewAreaFromXML(node.toElement())
                        elif node.toElement().tagName() == "toolbars":
                            self.setupToolbars(node.toElement())
                        node = node.nextSibling()
                    self.statusBar().showMessage(_fromUtf8(_("Files from XML file loaded")))
                    self.loadedfiles_model.files.reverse()
                    self.setWindowTitleFromFilename(fh)
                    head, tail  = os.path.split(str(fname[0]))
                    self.lastDirProjectPath = head
            except exceptionModel.OpenProjectError as e:
                QMessageBox.critical(self, '%s'% e.title, '%s'% e.msg, QMessageBox.Ok)

    def readFile(self, element):
        files = QtCore.QStringList()
        if element.attribute("type"):
            child = element.firstChild()
            while not child.isNull():
                if child.toElement().tagName() == "source":
                    sourceFile = child.firstChild().toText().data()
                    files.append(sourceFile)
                elif child.toElement().tagName() == "visible":
                    visible = str(child.firstChild().toText().data())
                child = child.nextSibling()
        self.openFile([files])
        self.loadedfiles_model.files[0].visible = ast.literal_eval(visible)

    def setupViewAreaFromXML(self, element):
        child = element.firstChild()
        while not child.isNull():
            if child.toElement().tagName() == "EPSG":
                epsg = child.firstChild().toText().data()
            elif child.toElement().tagName() == "location":
                xy = child.firstChild().toText().data()
                x, y = xy.split(';')
            elif child.toElement().tagName() == "pixel_size":
                pixel_size = child.firstChild().toText().data()
            child = child.nextSibling()

        self.viewArea.setUpEPSG(self.coordinateSystemComboBox, int(epsg))
        self.viewArea.centralPaintPoint.AddPoint(float(x), float(y))
        self.viewArea.pixSize = float(pixel_size)
        self.viewArea.zoomIn()
        self.viewArea.zoomOut()

    def setupToolbars(self, element):
        element.toText().data()
        child = element.firstChild()
        while not child.isNull():
            if child.toElement().tagName() == "main":
                self.setupMainToolbar(child.firstChild())
            child = child.nextSibling()

    def setupMainToolbar(self, element):
        while not element.toElement().isNull():
            if element.toElement().tagName() == "pan":
                self.actionPan_Action.setChecked(ast.literal_eval(str(element.firstChild().toText().data())))
            elif element.toElement().tagName() == "layers":
                self.actionToolBarShow_Layers.setChecked(ast.literal_eval(str(element.firstChild().toText().data())))
            elif element.toElement().tagName() == "coordinates":
                self.actionToolBarShow_Coordinates.setChecked(ast.literal_eval(str(element.firstChild().toText().data())))
            element = element.nextSibling()

    def saveProject(self, fileLocation=None):
        if not fileLocation:
            if not self.fileLocation:
                self.saveProjectAs()

        if fileLocation:
            self.fileLocation = fileLocation[0]
        fh = self.fileLocation

        try:
            fh = QFile(fh)
            fh.open(QFile.WriteOnly | QFile.Text)
            stream = QTextStream(fh)
            stream << ("<?xml version='1.0' encoding='UTF-8'?>\n"\
                       "<!DOCTYPE AGROEYE>\n"\
                       "<AGROEYE VERSION='0.1'>\n")
            for file in self.loadedfiles_model.files:
                if file.isVector:
                    stream << ("<file type='vector'>\n")
                else:
                    stream << ("<file type='raster'>\n")
                stream << ("\t<source>%s</source>\n" % file.path)
                stream << ("\t<visible>%s</visible>\n" % file.visible)
                stream << ("</file>\n")
            stream << ("<viewarea>\n")
            stream << ("\t<EPSG>%s</EPSG>\n" % str(self.viewArea.projectionEPSG))
            stream << ("\t<location>%s;%s</location>\n"
                       % (str(self.viewArea.centralPaintPoint.GetX()), str(self.viewArea.centralPaintPoint.GetY())))
            stream << ("\t<pixel_size>%s</pixel_size>\n" % str(self.viewArea.pixSize))
            stream << ("</viewarea>\n")
            stream << ("<toolbars>\n")
            stream << ("\t<main>\n")
            stream << ("\t\t<pan>%s</pan>\n" % self.viewArea.panActionActive)
            stream << ("\t\t<layers>%s</layers>\n" % self.actionShow_Layers.isChecked())
            stream << ("\t\t<coordinates>%s</coordinates>\n" % self.actionToolBarShow_Coordinates.isChecked())
            stream << ("\t</main>\n")
            stream << ("</toolbars>\n")
            self.setWindowTitleFromFilename(fh)
        except:
            pass

    def saveProjectAs(self):

        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFiles)
        dialog.setDirectory(self.lastDirProjectPath)
        fname = dialog.getSaveFileNameAndFilter(self, _fromUtf8(_('Save Project As ...')), '*.xml', _('XML project files'))
        if not fname[0].isEmpty():
            self.saveProject(fname)
            head, tail  = os.path.split(str(fname[0]))
            self.lastDirProjectPath = head

    def setWindowTitleFromFilename(self, fh):
        if len(fh.fileName().split("/")) == 1:
            windowTitle = QString("AgroEye - " + fh.fileName().split(os.sep)[-1])
        else:
            windowTitle = QString("AgroEye - " + fh.fileName().split("/")[-1])
        self.setWindowTitle(windowTitle)

    def checkFileIsValid(self, fname):
        """!
        @brief Check if open file is supported file format
        @param fname: Name of file
        @param return Returns name of file if file format is valid; otherwise returns None.
        """
        root, self.ext = os.path.splitext(unicode(fname))
        head, tail  = os.path.split(str(fname))
        self.lastDirPath = head

        try:
            if self.ext in ('.tif', '.tiff', '.TIF ', '.TIFF', '.img', '.IMG', '.kea', '.KEA'):
                return unicode(fname), False
            elif self.ext in ('.shp', '.SHP','.shx', '.SHX', '.dbf', '.DBF', '.csv', '.CSV', '.gml', '.GML', '.gpx', '.GPX', '.kml', '.KML', '.spatialite', '.sqlite'):
                return unicode(fname), True
            else:
                shortName = os.path.basename(unicode(fname))
                raise exceptionModel.FileFormatError(shortName)
        except exceptionModel.FileFormatError as e:
            QMessageBox.critical(self, '%s'%e.title, '%s'%e.msg, QMessageBox.Ok)
            return None, None

    def enableMenus(self):

        for item in self.onOffActions:
            item.setEnabled(True)

    def disableMenus(self):

        for item in self.onOffActions:
            item.setEnabled(False)

    def onRightClick(self, pos):
        """!
        @brief Shows context menu on right click on list of loaded files.
        @param pos: Position of the context menu event.
        """

        self.selection = self.loadedfiles_tableview.selectionModel().selectedRows()
        if self.selection:
            check = self.loadedfiles_model.isVector(self.selection[0])
            if check:
                self.loadedfiles_tableview.menuVector(pos)
            else:
                self.loadedfiles_tableview.menuRaster(pos)

    def keyPressEvent(self, event):
        """!
        @brief Quit AgroEye when Escape is pressed
        @param event If any key is pressed, checks an action to do
        """
        if event.key() == QtCore.Qt.Key_F5:
            print self.update_ui()

    def openPreferencesDialog(self):
        """!
        @brief Shows window to set preferences for view area, raster and vector file.
        """
        self.preferencesDialog.show()

    def updateMessage(self, message):
        """!
        @brief Update status bar with given message.
        @var message String which will be displayed
        """
        self.statusBar().showMessage(message)

    def about(self):
        """!
        @brief Shows window about AgroEye
        """
        text = (_("<p>The political transformation, that took place in Poland "
                "over 25 years ago, as well as Poland's accession to "
                "European Union entailed a large number of changes,"
                "including agriculture. The most important of them were "
                "liquidation of national owned farms,free market development "
                "and increase manufactures competitiveness. "
                "These political changes caused also deep socio-economic "
                "transformations and landscape changes as well.</p>"

                "<p>The remote sensing applications in agriculture sector "
                "began with sensors for soil organic matter, and have "
                "quickly diversified to "
                "include satellite (e.g. Landsat), airborne (e.g. digital "
                "hyperspectral cameras), UAV (e.g. with multispectrals camera) "
                "and hand held or tractor mounted sensors. At present there is "
                "considerable interest in collecting remote sensing data as "
                "multitemporal series in order to conduct near real time: "
                "soil, crop and pest management. There are known many "
                "examples of using remote sensing data for agricultural "
                "landscapes management in the "
                "world. Exemplary institution dealing with this topic is "
                "the Monitoring Agricultural Resources (MARS) Unit of "
                "European Commission’s Joint Research Centre, focuses on crop "
                "production, agricultural activities and rural development."
                "</p>"))

        QtGui.QMessageBox.about(self, _('AgroEye Messenger'), text)

    def showMeasuringDistanceTool(self):
        """!
        @brief Shows window to measure distance.
        """
        self.actionPan_Action.setChecked(False)
        self.measureDistance = measuredistancetool.MeasureDistanceTool(self)
        self.measureDistance.show()
        self.measureDistance.activateWindow()

    def showMeasuringAreaTool(self):
        """!
        @brief Shows window to measure area.
        """
        self.actionPan_Action.setChecked(False)
        self.measureArea = measureareatool.MeasureAreaTool(self)
        self.measureArea.show()

    def openGAECModel(self):
        """!
        @brief Shows window to check GAEC rules.
        """
        self.model = modeldialog.ModelGAEC(self)
        self.model.show()

    def addNewVectorLayer(self):
        """!
        @brief Shows window to add new vector layer shapefile.
        """
        self.actionPan_Action.setChecked(False)
        self.viewArea.panActionToggled(False)
        self.newVectorLayer = addnewvectorlayerdialog.AddNewVectorLayer(self)
        self.newVectorLayer.show()
        self.viewArea.setContextMenuPolicy(Qt.NoContextMenu)


    def showRasterCalculator(self):
        """!
        @brief Shows window to raster calculator.
        """
        self.rasterCalc = rastercalculator.RasterCalculator(self)
        self.rasterCalc.show()

    # ------------------------------------------------------------------------
    # The graphics window about extensions

    def about_ext(self):
        try:
            QtGui.QMessageBox.about(self, QtGui.QApplication.aboutQt())
        except TypeError:
            pass


    def closeEvent(self, event):
        reply = QtGui.QMessageBox.warning(self,_fromUtf8(_('Save ?')), _fromUtf8(_('Are you sure to quit?')), QMessageBox.Yes | QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def setupEnviromentalVariables(self):
        currentFilepath = os.path.realpath(__file__)
        currentDirectory = os.path.dirname(currentFilepath)
        oneLevelUp = os.path.realpath(os.path.join(currentDirectory, os.pardir))
        os.environ['AGROEYE_GDAL_DATA'] = os.path.join(oneLevelUp, "gdal_data")
        gdal.SetConfigOption('GDAL_DATA', os.environ['AGROEYE_GDAL_DATA'])

    def setTempDirectory(self):
        self.tempDir = os.path.join(tempfile.gettempdir(), '.{}'.format(hash(os.times())))
        os.environ['AGROEYE_TEMP'] = self.tempDir
        os.makedirs(self.tempDir)

##############################################################################

def removeAfterClose(path):
        """!
        @brief After close vector file, remove created svg file (in temp svg directory).
        """
        rmtree(path, ignore_errors=True)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    locale = QtCore.QLocale.system().name()
    myapp = AgroEye()
    myapp.show()
    atexit.register(removeAfterClose, myapp.tempDir)
    sys.exit(app.exec_())
