import sqlite3 as db
import platform
import os
from PyQt4.QtCore import *


class ConnectDatabase(object):
    """!
    @brief Opens database
    """

    def __init__(self, name, epsg, parent=None):

            self.epsg = epsg
            self.nameDB = self.checkName(name)
            tempName = self.nameDB + '.sqlite'
            self.conn = db.connect('%s' %tempName)
            self.conn.enable_load_extension(True)

            if platform.system() == "Windows":
                spatialite_source_folder = os.path.dirname(db.__file__)
                cwd = os.getcwd()
                os.chdir(spatialite_source_folder)
                self.conn.load_extension("mod_spatialite.dll")
                os.chdir(cwd)



    def checkName(self, name):
        """!
        @brief Check if filename is valid.
        @return Returns the valid filename.
        """
        # check name validation
        symbol = "~`!@#$%^&*()-+={}[]:>;',</?*-+"
        tempName = name
        for i, word in enumerate(name):
            if word in symbol:
                tempName = tempName.replace(word, "_")
        root, ext = os.path.splitext(tempName)

        # set directory for database
        os.chdir('../Databases')

        return root

    def initColumn(self, numBand):
        """!
        @brief Init the column name in table.
        @return Returns the list of column name and column type.
        """

        columnName = []
        columnType = []

        columnName.extend(['numPix'])
        columnType.extend(['INTEGER'])

        for num in range(numBand):
            columnName.append('Band%d'%(num+1))
            columnType.append('REAL')

        return columnName, columnType


    def addSegmentTable(self, source):
        """!
        @brief Create table with object from segmentation
        """

        tableName = 'segment_' + str(self.nameDB)
        curs = self.conn.cursor()

        #Init, delete old database
        try:
            res = curs.execute('SELECT * FROM geometry_columns WHERE f_geometry_column LIKE "geometry" ')
            if res.fetchall():
                curs.execute('SELECT DiscardGeometryColumn("%s", "Geometry")'%tableName)
        except:
            curs.execute('SELECT InitSpatialMetaData(%d)'%self.epsg)

        curs.execute('DROP TABLE IF EXISTS %s' %tableName)

        # Create table
        curs.execute('CREATE TABLE %s('
                     'id INTEGER NOT NULL PRIMARY KEY,'
                     'geometry BLOB NOT NULL,'
                     'max REAL,'
                     'min REAL,'
                     'mean REAL,'
                     'count INTEGER,'
                     'area REAL,'
                     'stddev REAL,'
                     'id_class INTEGER,'
                     'id_after_class INTEGER)'
                     %tableName)

        # Add geometry column
        #curs.execute('SELECT AddGeometryColumn("%s", "Geometry", %d, "POLYGON", "XY")' %(tableName, epsg_code))


        # Add data to table
        # geom = 'GeomFromText("POLYGON(('
        # geom += '%f ' % (-10.0 - (0.5))
        # geom += '%f, ' % (-10.0 - (0.5))
        # geom += '%f ' % (10.0 + (0.5))
        # geom += '%f, ' % (-10.0 - (0.5))
        # geom += '%f ' % (10.0 + (0.5))
        # geom += '%f, ' % (10.0 + (0.5))
        # geom += '%f ' % (-10.0 - (0.5))
        # geom += '%f, ' % (10.0 + (0.5))
        # geom += '%f ' % (-10.0 - (0.5))
        # geom += '%f ' % (-10.0 - (0.5))
        # geom += '))", 4326)'


        curs.execute('INSERT INTO "%s" (id, max, Geometry) VALUES (1, 2.6, GeomFromText("POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))", %d))' %(tableName, self.epsg))


        # Print column in table
        # b = curs.execute('PRAGMA TABLE_INFO(%s)' %tableName)
        # print b.fetchall()

        # Print data in table
        a = curs.execute('SELECT id, AsText(geometry) FROM %s' %tableName)
        print a.fetchall()

        # Close connect with database
        self.conn.commit()
        self.conn.close()

    def addClusterTable(self, source, epsg_code):
        """!
        @brief Create table for cluster.
        """
        tableName = 'cluster_All'
        curs = self.conn.cursor()

        #Init, delete old database
        try:
            res = curs.execute('''SELECT * FROM geometry_columns WHERE f_geometry_column LIKE "geometry" ''')
            if res.fetchall():
               curs.execute('SELECT DiscardGeometryColumn("%s", "geometryClass")'%tableName)
        except:
            curs.execute('SELECT InitSpatialMetaData(%d)'%epsg_code)

        curs.execute('DROP TABLE IF EXISTS %s' %tableName)

        numBand = len(source[0]['center'])
        columnName, columnType = self.initColumn(numBand)


        # Create table with column id
        curs.execute('CREATE TABLE {tn} (id INTEGER PRIMARY KEY)'\
                     .format(tn = tableName))

        # Add column
        for j in range(len(columnName)):
            curs.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' {ct}"\
                         .format(tn = tableName, cn = columnName[j], ct = columnType[j]))

        # Add data to table
        for i in range(len(source)):
            for key, value in source[i].iteritems():
                if key == 'numPxl':
                    curs.execute('INSERT INTO {tn}({cn}) VALUES ( {tb} )'\
                         .format(tn = tableName, cn = columnName[0], tb = value))
                elif key == 'center':
                    for j in range(numBand):
                        curs.execute('UPDATE {tn} SET {cn} = {tb} WHERE id = {num}'\
                            .format(tn = tableName, cn = columnName[j+1], tb = value[j], num = i+1))

        self.conn.commit()
        #Print column in table
        # b = curs.execute('PRAGMA TABLE_INFO(%s)' %tableName)
        # print b.fetchall()

        # # #Print data in table
        # a = curs.execute('SELECT * FROM %s' %tableName)
        # print a.fetchall()

        # Close connect with database
        print 'Database create'
        self.conn.close()


#
# class OpenDatabase():
#     def __init__(self, name, parent=None):
#
#
#             self.nameDB = name
#             self.conn = db.connect('%s' %self.nameDB)
#             self.conn.enable_load_extension(True)
#
#             if platform.system() == "Windows":
#                 spatialite_source_folder = os.path.dirname(db.__file__)
#                 cwd = os.getcwd()
#                 os.chdir(spatialite_source_folder)
#                 self.conn.load_extension("mod_spatialite.dll")
#                 os.chdir(cwd)
#
#
#
#     def readGeometry(self):
#         curs = self.conn.cursor()
#         cursGeom = curs.execute('SELECT AsText(the_geom) FROM total_view LIMIT 100')
#         geom = cursGeom.fetchall()
#         cursGeomType = curs.execute('SELECT geometry_type FROM geometry_columns')
#         geomType = cursGeomType.fetchone()
#         #boundGeomType = curs.execute('SELECT ')


#        return (geom, geomType[0], str(self.nameDB))


