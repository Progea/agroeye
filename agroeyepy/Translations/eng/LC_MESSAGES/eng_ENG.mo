��    �      �    �      P     Q     ^  -   w     �     �     �     �     �     �  �  
     �     �     �     �          %  %   7  
   ]     h  "   �     �     �     �  /   �  +        -     2     E     W     _  '   e     �     �     �     �  ?   �               ,     E     J     P     V     _     l  
   s     ~     �     �     �     �     �     �     �     �     �     �     �                    (     A  	   N     X     f     s     {     �     �  G   �     �      �          1     6  	   G     Q     Z  !   i     �     �     �  
   �  	   �     �  !   �  "   �  +  "     N     [     b     k     w  
   �  
   �  
   �  	   �     �     �  	   �     �     �  	   �     �     �          %     1     9     >  j   C     �     �     �     �               /     4     D     Y  (   j  )   �  	   �     �     �     �  R   �     L      b      r      �   
   �      �      �   
   �      �      �      �   #   �      !     !  
   .!     9!     Q!     c!     p!     }!     �!     �!     �!     �!     �!     �!     �!     �!     �!     "  	   "     "     2"     A"     X"     d"     q"     �"  
   �"  
   �"     �"     �"     �"     �"     �"  "   �"     #     7#  N   V#  C   �#     �#     �#     $     $  
   4$     ?$     L$     _$  !   t$  "   �$  >   �$  T   �$  ?   M%     �%  
   �%  
   �%     �%     �%     �%  
   �%  +   �%     &     /&     @&     T&  
   [&  �  f&     	-     -  -   /-     ]-     i-     -     �-     �-     �-  �  �-     �2     �2     �2     �2     �2     �2  %   �2  
   3      3  "   <3     _3     m3     s3  /   �3  +   �3     �3     �3     �3     4     4  '   4     E4     b4     i4     x4  ?   ~4     �4     �4     �4     �4     5     5     5     5     $5  
   +5     65     Q5     X5     e5     r5     �5     �5     �5     �5     �5     �5     �5     �5     �5     �5     �5     �5  	   6     6     6     +6     36     :6     H6  G   Y6     �6      �6     �6     �6     �6  	   �6     	7     7  !   !7     C7     b7     j7  
   w7  	   �7     �7  !   �7  "   �7  +  �7     9     9     9     #9     /9  
   F9  
   Q9  
   \9  	   g9     q9     w9  	   �9     �9     �9  	   �9     �9     �9     �9     �9     �9     �9     �9  j   �9     f:     t:     �:     �:     �:     �:     �:     �:     �:     ;  (   ";  )   K;  	   u;     ;     �;     �;  R   �;     <     <     *<     <<  
   L<     W<     `<  
   p<     {<     �<     �<  #   �<     �<     �<  
   �<     �<     	=     =     (=     5=     <=     M=     c=     i=     v=     �=     �=     �=     �=     �=  	   �=     �=     �=     �=     >     >     )>     >>  
   S>  
   ^>     i>     w>     �>     �>     �>  "   �>     �>     �>  N   ?  C   ]?     �?     �?     �?     �?  
   �?     �?     @     @  !   ,@  "   N@  >   q@  T   �@  ?   A     EA  
   ^A  
   iA     tA     yA     �A  
   �A  +   �A     �A     �A     �A     B  
   B             �   �       �   v   +       M   �       ~       �   I   �   >      �               �       S      �   �       �   \   	   �   �       E   �   g       �      �   `       �           #   �           y       l          %   z       )   Y   �      D   �       P   �   �   $   {   }   5   6   7   8   9   :   �       a       s   �   K   �   �              �       ^   @       d   �   U   1   '   k           u   �   r           e   �   .   �   �       �       �   J   �   �   �       3   F   (             N       2   �   �   W   �      �                     =   <         �   �   A   �       Q       �      H      _   �   G           �       �   �   o       �       -       �       c           �      �   j   ,   �   "   �   h   b       �   x   p      /   �   n              �              [   w          q   !      �   �   �      �   R       �   i   &       �   �   m      Z   V   �   0   �   t   
          �       �   ;   B   �           �   �              |   f   ]      4   X   C      L   *               �   �      T   �   �       �      �   �   ?   O                 - Histogram %s driver not available. %s not recognized as a supported file format. &Edit Style &Open Attribute Table &Properties &Remove layer &Zoom full extent ---- Select bands ---- <p>The political transformation, that took place in Poland over 25 years ago, as well as Poland's accession to European Union entailed a large number of changes,including agriculture. The most important of them were liquidation of national owned farms,free market development and increase manufactures competitiveness. These political changes caused also deep socio-economic transformations and landscape changes as well.</p><p>The remote sensing applications in agriculture sector began with sensors for soil organic matter, and have quickly diversified to include satellite (e.g. Landsat), airborne (e.g. digital hyperspectral cameras), UAV (e.g. with multispectrals camera) and hand held or tractor mounted sensors. At present there is considerable interest in collecting remote sensing data as multitemporal series in order to conduct near real time: soil, crop and pest management. There are known many examples of using remote sensing data for agricultural landscapes management in the world. Exemplary institution dealing with this topic is the Monitoring Agricultural Resources (MARS) Unit of European Commissions Joint Research Centre, focuses on crop production, agricultural activities and rural development.</p> Action Add data Add new field Add new field Error Add new vector object to layer AgroEye Messenger AgroEye application is ready to work! All Layers All changes have been saved All changes have been saved to:
%s All files (*) Apply Are you sure to quit? Are you sure you want to delete row number %d ? Are you sure you want to quit without save? Area Attribute table -  Backward Diagonal Band %s Bevel Building overviews for raster <b>%s</b> Changes have not been saved! Circle Classification Close Code %s not found in EPSG support files.
Enter valid EPSG code. Composition RGB -  Confirm delete row Coordinates System Error Copy Count Cross Dash-Dot Dash-Dot-Dot Dashed Data Range Date format is: yyyy-mm-dd Delete Delete field Delete point Delete selected row Dense 2 Dense 3 Dense 4 Dense 5 Dense 6 Dense 7 Diagonal Cross Diamond Dotted Draw object Drawn polygon is invalid Driver Error EPSG code Edit Style -  Editing mode Ellipse Error! Feature Error Field type Error Field with the same name is already exists.
Please choose another name. File format File is used by another program. Files from XML file loaded Flat Forward Diagonal Full path Geometry Geometry Error Geometry type %s is not supported Geometry type is not supported Hexagon Histogram -  Horizontal Id_Object Identify Impossible to change vector style Impossible to show attribute table In provided raster file no overviews were detected
It could really slow down the browsing through raster
Would you like to create them?

In case of yes:
- You must have write access in the source folder
- You will have to wait (Progress bar will be displayed
In case of no:
- AgroEye can run slower
 Input Raster Insert Layer %i Layer Error Layer has no features. Layer name Layer path Layer type Max value Max:  Measure Area Min value Min:  Miter Move down Move selected down Move selected to top Move selected up Move to top Move up NULL Name Name contains only letters, numbers and underscores.
It is limited to 15 characters.
It starts with letter Name of layer New vector layer No files loaded into AgroEye! No files were loaded so far! No overviews detected No valid layers! None Number of bands Number of characters Number of digits Number of digits after the decimal point Number of digits before the decimal point Only name Open File Error Open project Open project warning Opening project will remove current file and properties. Do you want to continue ? Output classification Output database Output statistics Overwrite Error Path Error Pentagon Percentage Clip Pixel type Plus Polygon Error Project error Project is not AgroEye project file Projection definition Projection name Properties Ra&ster RGB composition Raster calculator Raster error Refresh (F5) Rename Rename field: %s Rename selected field Round Round Square Sale entered incorrect!!! Save Save ? Save File Error Save Project As ... Save as Save file Save raport to the file Save shapefile Save shapefile warning Save status Segmentation Segmentation results Select layers to add Set as max Set as min Set attribute Show &Histogram and Adjust Show segment Showing style Solid SpatiaLite Database (*.spatialite) Spatial Reference System Spatial Reference System Error Spatial Reference System is other than current. The layer will be transformed. Spatial Reference System is unknown. This data can be invalid drawn Square Standard Deviation Standard Deviation:  Start editing single values Statistics Stretch Type Stretch parameters Text Files (*.txt);; The max value is lesser than min. The min value is greater than max. The name of file is invalid:
 name is limited to 15 characters The name of file is invalid:
 valid name may contain letters, numbers or underscores The name of file is invalid:
 valid name may start with letters The path %s is not valid Triangle 1 Triangle 2 Type Type of field %s is unknown Vertical Visibility Wrong file! Image must be north orientated! XML project files Zoom scale error Zoom to full extent after  to the top Project-Id-Version: AgroEye_v.1
POT-Creation-Date: 2017-01-20 13:22+0100
PO-Revision-Date: 2017-01-20 13:38+0100
Language-Team: MPapiez <malgorzata.papiez@progea.pl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
Last-Translator: MPapiez <malgorzata.papiez@progea.pl>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: en_GB
X-Poedit-SearchPath-0: Control/viewerApplication.py
X-Poedit-SearchPath-1: Control/exceptionModel.py
X-Poedit-SearchPath-2: Control/loadedfiles_model.py
X-Poedit-SearchPath-3: Logic/rasterattributes.py
X-Poedit-SearchPath-4: Logic/rasterLayerMechanism.py
X-Poedit-SearchPath-5: Logic/vectorTableMechanism.py
X-Poedit-SearchPath-6: Logic/vectorLayerMechanism.py
X-Poedit-SearchPath-7: Logic/saveVectorLayerMechanism.py
X-Poedit-SearchPath-8: View/addlayersdialog.py
X-Poedit-SearchPath-9: View/addnewobjecttolayer.py
X-Poedit-SearchPath-10: View/addnewvectorlayerdialog.py
X-Poedit-SearchPath-11: View/attributetabledialog.py
X-Poedit-SearchPath-12: View/compositiondialog.py
X-Poedit-SearchPath-13: View/editstyledialog.py
X-Poedit-SearchPath-14: View/histogramdialog.py
X-Poedit-SearchPath-15: View/measureareatool.py
X-Poedit-SearchPath-16: View/modelattributedialog.py
X-Poedit-SearchPath-17: View/modeldialog.py
X-Poedit-SearchPath-18: View/modelfilesdialog.py
X-Poedit-SearchPath-19: View/modelfunctiondialog.py
X-Poedit-SearchPath-20: View/preferences.py
X-Poedit-SearchPath-21: View/progressdialog.py
X-Poedit-SearchPath-22: View/propertiesdialog2.py
X-Poedit-SearchPath-23: View/rastercalculator.py
X-Poedit-SearchPath-24: View/viewarea.py
  - Histogram %s driver not available. %s not recognized as a supported file format. &Edit Style &Open Attribute Table &Properties &Remove layer &Zoom full extent ---- Select bands ---- <p>The political transformation, that took place in Poland over 25 years ago, as well as Poland's accession to European Union entailed a large number of changes,including agriculture. The most important of them were liquidation of national owned farms,free market development and increase manufactures competitiveness. These political changes caused also deep socio-economic transformations and landscape changes as well.</p><p>The remote sensing applications in agriculture sector began with sensors for soil organic matter, and have quickly diversified to include satellite (e.g. Landsat), airborne (e.g. digital hyperspectral cameras), UAV (e.g. with multispectrals camera) and hand held or tractor mounted sensors. At present there is considerable interest in collecting remote sensing data as multitemporal series in order to conduct near real time: soil, crop and pest management. There are known many examples of using remote sensing data for agricultural landscapes management in the world. Exemplary institution dealing with this topic is the Monitoring Agricultural Resources (MARS) Unit of European Commissions Joint Research Centre, focuses on crop production, agricultural activities and rural development.</p> Action Add data Add new field Add new field Error Add new vector object to layer AgroEye Messenger AgroEye application is ready to work! All Layers All changes have been saved All changes have been saved to:
%s All files (*) Apply Are you sure to quit? Are you sure you want to delete row number %d ? Are you sure you want to quit without save? Area Attribute table -  Backward Diagonal Band %s Bevel Building overviews for raster <b>%s</b> Changes have not been saved! Circle Classification Close Code %s not found in EPSG support files.
Enter valid EPSG code. Composition RGB -  Confirm delete row Coordinates System Error Copy Count Cross Dash-Dot Dash-Dot-Dot Dashed Data Range Date format is: yyyy-mm-dd Delete Delete field Delete point Delete selected row Dense 2 Dense 3 Dense 4 Dense 5 Dense 6 Dense 7 Diagonal Cross Diamond Dotted Draw object Drawn polygon is invalid Driver Error EPSG code Edit Style -  Editing mode Ellipse Error! Feature Error Field type Error Field with the same name is already exists.
Please choose another name. File format File is used by another program. Files from XML file loaded Flat Forward Diagonal Full path Geometry Geometry Error Geometry type %s is not supported Geometry type is not supported Hexagon Histogram -  Horizontal Id_Object Identify Impossible to change vector style Impossible to show attribute table In provided raster file no overviews were detected
It could really slow down the browsing through raster
Would you like to create them?

In case of yes:
- You must have write access in the source folder
- You will have to wait (Progress bar will be displayed
In case of no:
- AgroEye can run slower
 Input Raster Insert Layer %i Layer Error Layer has no features. Layer name Layer path Layer type Max value Max:  Measure Area Min value Min:  Miter Move down Move selected down Move selected to top Move selected up Move to top Move up NULL Name Name contains only letters, numbers and underscores.
It is limited to 15 characters.
It starts with letter Name of layer New vector layer No files loaded into AgroEye! No files were loaded so far! No overviews detected No valid layers! None Number of bands Number of characters Number of digits Number of digits after the decimal point Number of digits before the decimal point Only name Open File Error Open project Open project warning Opening project will remove current file and properties. Do you want to continue ? Output classification Output database Output statistics Overwrite Error Path Error Pentagon Percentage Clip Pixel type Plus Polygon Error Project error Project is not AgroEye project file Projection definition Projection name Properties Ra&ster RGB composition Raster calculator Raster error Refresh (F5) Rename Rename field: %s Rename selected field Round Round Square Sale entered incorrect!!! Save Save ? Save File Error Save Project As ... Save as Save file Save raport to the file Save shapefile Save shapefile warning Save status Segmentation Segmentation results Select layers to add Set as max Set as min Set attribute Show &Histogram and Adjust Show segment Showing style Solid SpatiaLite Database (*.spatialite) Spatial Reference System Spatial Reference System Error Spatial Reference System is other than current. The layer will be transformed. Spatial Reference System is unknown. This data can be invalid drawn Square Standard Deviation Standard Deviation:  Start editing single values Statistics Stretch Type Stretch parameters Text Files (*.txt);; The max value is lesser than min. The min value is greater than max. The name of file is invalid:
 name is limited to 15 characters The name of file is invalid:
 valid name may contain letters, numbers or underscores The name of file is invalid:
 valid name may start with letters The path %s is not valid Triangle 1 Triangle 2 Type Type of field %s is unknown Vertical Visibility Wrong file! Image must be north orientated! XML project files Zoom scale error Zoom to full extent after  to the top 