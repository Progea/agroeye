��    �      |    �      0     1     >  -   W     �     �     �     �     �     �  �  �     �     �     �     �     �  %   �  
        *  "   F     i     w     }  .   �  +   �     �     �                  '   &     N     k     r     �  ?   �     �     �     �                 	        !     *     7  
   >     I     d     k     x     �     �     �     �     �     �     �     �     �     �     �     �       	        $     2     ?     G     N     \  G   m     �      �     �     �       	             &  "   5     X  	   x     �     �     �  
   �  	   �     �  !   �  "   �  +       2     ?     F     O     [  
   r  
   }  
   �  	   �     �     �  	   �     �     �  	   �     �     �     �     	               "  j   '     �     �     �     �     �                 (   ,  )   U  	        �     �     �  R   �           $      4      F   
   V      a   
   j      u      z      �   $   �      �      �   
   �      �      !     !     !     "!     /!     6!     G!     ]!     c!     p!     u!     |!     �!     �!  	   �!     �!     �!     �!     �!     �!     "     ""  
   7"  
   B"     M"     ["     v"     �"     �"  "   �"     �"     �"  N   �"  D   A#     �#     �#     �#  
   �#     �#     �#     �#  !   �#  "    $  ?   C$  U   �$  @   �$     %  
   4%  
   ?%     J%     O%     l%  
   u%  +   �%     �%     �%     �%     �%  
   �%  V  �%     S3      _3  0   �3     �3     �3     �3     �3     �3     4  E  *4     p8  
   v8     �8     �8     �8     �8     �8  "   �8  )    9     J9     ^9  #   g9  8   �9  .   �9  	   �9     �9     :  	   :  	   (:  &   2:     Y:     w:     }:     �:  H   �:     �:      �:     ;     -;     4;     ;;     D;     V;     j;     �;     �;     �;     �;     �;     �;     �;     �;      <     <     <     <      <  
   (<     3<     8<     G<  $   T<     y<     �<     �<     �<     �<     �<     �<     �<  @   �<     =  &   ,=  $   S=     x=     �=     �=  	   �=     �=  $   �=  %   �=     >     >     >     ,>     9>  
   A>     L>  /   b>  %   �>  N  �>     @     @  	   @     )@  !   8@     Z@     h@     �@     �@     �@     �@     �@     �@     �@     �@  4   �@  +   A  6   ?A     vA     �A     �A     �A  j   �A     B  #    B  #   DB  "   hB     �B     �B     �B     �B     �B     �B     �B     C     !C     1C  i   MC     �C     �C     �C     �C     D     D     D     ,D     1D     AD  D   QD     �D     �D  
   �D     �D     �D     �D     �D     E     E     $E  #   5E     YE     fE     {E  
   �E     �E     �E     �E     �E     �E     �E     �E     F     %F     DF     PF  
   cF  	   nF  
   xF     �F     �F     �F     �F  %   �F     �F     G  i    G  F   �G  
   �G     �G  &   �G  
   H     &H     :H     QH  0   jH  0   �H  C   �H  W   I  A   hI     �I     �I     �I     �I     �I      J     J  ;   J     QJ     dJ     qJ     �J     �J             �   �           w   *       M   �       ~       �   I   �   >      �       �       �       T      �   �           ^   	      �       E   �   i      �      �   b       �           "   �           z       n          $   {       (   [   �      D           P   �   �   #   �   }   5   6   7   8   9   :   |       c       R   U   K   �   �          �   �       `   @       f   �   W   1   &   m   �       v       t           g   �   .   �   �       �       �   J   �   �   �   �   3   F   '       +      N   �   2       �   Y   �      S         �           =   <             �   A      �   �              H      a   �   G   �       �           �   q       �       -       �       e           �      �   l   ,   �   !   �   j   d       �   y   r      /       p                         �   ]   x   �      s          �       �      �   �   �   �   k   %       �   �   o      \   X   �   0   �   u   
          �           ;   B   �           �   �              Q   h   _      4   Z   C       L   )       ?       �   �      V   �   �       �   �   �   �       O                 - Histogram %s driver not available. %s not recognized as a supported file format. &Edit Style &Open Attribute Table &Properties &Remove layer &Zoom full extent ---- Select bands ---- <p>The political transformation, that took place in Poland over 25 years ago, as well as Poland's accession to European Union entailed a large number of changes,including agriculture. The most important of them were liquidation of national owned farms,free market development and increase manufactures competitiveness. These political changes caused also deep socio-economic transformations and landscape changes as well.</p><p>The remote sensing applications in agriculture sector began with sensors for soil organic matter, and have quickly diversified to include satellite (e.g. Landsat), airborne (e.g. digital hyperspectral cameras), UAV (e.g. with multispectrals camera) and hand held or tractor mounted sensors. At present there is considerable interest in collecting remote sensing data as multitemporal series in order to conduct near real time: soil, crop and pest management. There are known many examples of using remote sensing data for agricultural landscapes management in the world. Exemplary institution dealing with this topic is the Monitoring Agricultural Resources (MARS) Unit of European Commission’s Joint Research Centre, focuses on crop production, agricultural activities and rural development.</p> Action Add data Add new field Add new field Error AgroEye Messenger AgroEye application is ready to work! All Layers All changes have been saved All changes have been saved to:
%s All files (*) Apply Are you sure to quit? Are you sure you want to delete row number %d? Are you sure you want to quit without save? Area Attribute table -  Backward Diagonal Band %s Bevel Building overviews for raster <b>%s</b> Changes have not been saved! Circle Classification Close Code %s not found in EPSG support files.
Enter valid EPSG code. Composition RGB -  Confirm delete row Coordinates System Error Copy Count Cross Custom #1 Dash-Dot Dash-Dot-Dot Dashed Data Range Date format is: yyyy-mm-dd Delete Delete field Delete point Delete selected row Dense 2 Dense 3 Dense 4 Dense 5 Dense 6 Dense 7 Diagonal Cross Diamond Dotted Draw object Drawn polygon is invalid. Driver Error EPSG code Edit Style -  Editing mode Ellipse Error! Feature Error Field type Error Field with the same name is already exists.
Please choose another name. File format File is used by another program. Files from XML file loaded Flat Forward Diagonal Full path Geometry Geometry Error Geometry type %s is not supported. Geometry type is not supported. Grayscale Hexagon Hipsometric Histogram -  Horizontal Id_Object Identify Impossible to change vector style Impossible to show attribute table In provided raster file no overviews were detected
It could really slow down the browsing through raster
Would you like to create them?

In case of yes:
- You must have write access in the source folder
- You will have to wait (Progress bar will be displayed
In case of no:
- AgroEye can run slower
 Input Raster Insert Layer %i Layer Error Layer has no features. Layer name Layer path Layer type Max value Max:  Measure Area Min value Min:  Miter Move down Move selected down Move selected to top Move selected up Move to top Move up NULL Name Name contains only letters, numbers and underscores.
It is limited to 15 characters.
It starts with letter Name of layer No files loaded into AgroEye! No files were loaded so far! No overviews detected No valid layers! None Number of bands Number of characters Number of digits after the decimal point Number of digits before the decimal point Only name Open File Error Open project Open project warning Opening project will remove current file and properties. Do you want to continue ? Output classification Output database Output statistics Overwrite Error Path Error Pentagon Pixel type Plus Polygon Error Project error Project is not AgroEye project file. Projection definition Projection name Properties Ra&ster RGB composition Raster error Red in Blue Reds Refresh (F5) Rename Rename field: %s Rename selected field Round Round Square Save Save ? Save File Error Save Project As ... Save as Save file Save raport to the file Save shapefile Save shapefile warning Save status Scale entered incorrect. Segmentation Segmentation results Set as max Set as min Set attribute Show &Histogram and Adjust Show segment Showing style Solid SpatiaLite Database (*.spatialite) Spatial Reference System Spatial Reference System Error Spatial Reference System is other than current. The layer will be transformed. Spatial Reference System is unknown. This data can be invalid drawn. Square Standard Deviation:  Start editing single values Statistics Stretch Type Stretch parameters Text Files (*.txt);; The max value is lesser than min. The min value is greater than max. The name of file is invalid:
 name is limited to 15 characters. The name of file is invalid:
 valid name may contain letters, numbers or underscores. The name of file is invalid:
 valid name may start with letters. The path %s is not valid. Triangle 1 Triangle 2 Type Type of field %s is unknown. Vertical Visibility Wrong file! Image must be north orientated. XML project files Zoom scale error Zoom to full extent after column  to the top Project-Id-Version: AgroEye_v.1
POT-Creation-Date: 2017-01-19 13:30+0100
PO-Revision-Date: 2017-01-19 13:31+0100
Last-Translator: MPapiez <malgorzata.papiez@progea.pl>
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: Logic/rasterLayerMechanism.py
X-Poedit-SearchPath-1: Logic/saveVectorLayerMechanism.py
X-Poedit-SearchPath-2: Logic/vectorLayerMechanism.py
X-Poedit-SearchPath-3: Logic/vectorTableMechanism.py
X-Poedit-SearchPath-4: Control/agroeye_modules.py
X-Poedit-SearchPath-5: Control/exceptionModel.py
X-Poedit-SearchPath-6: Control/loadedfiles_model.py
X-Poedit-SearchPath-7: Control/viewerApplication.py
X-Poedit-SearchPath-8: Control/viewerwidget.py
X-Poedit-SearchPath-9: View/addlayersdialog.py
X-Poedit-SearchPath-10: View/addnewobjecttolayer.py
X-Poedit-SearchPath-11: View/addnewvectorlayerdialog.py
X-Poedit-SearchPath-12: View/attributetabledialog.py
X-Poedit-SearchPath-13: View/compositiondialog.py
X-Poedit-SearchPath-14: View/editstyledialog.py
X-Poedit-SearchPath-15: View/histogramdialog.py
X-Poedit-SearchPath-16: View/identifydialog.py
X-Poedit-SearchPath-17: View/loadedfilesdockwidget.py
X-Poedit-SearchPath-18: View/MainWindow.py
X-Poedit-SearchPath-19: View/measureareatool.py
X-Poedit-SearchPath-20: View/measuredistancetool.py
X-Poedit-SearchPath-21: View/modelattributedialog.py
X-Poedit-SearchPath-22: View/modeldialog.py
X-Poedit-SearchPath-23: View/modelfilesdialog.py
X-Poedit-SearchPath-24: View/modelfunctiondialog.py
X-Poedit-SearchPath-25: View/preferences.py
X-Poedit-SearchPath-26: View/progressdialog.py
X-Poedit-SearchPath-27: View/propertiesdialog2.py
X-Poedit-SearchPath-28: View/rastercalculator.py
X-Poedit-SearchPath-29: View/ui_addcolumndialog.py
X-Poedit-SearchPath-30: View/ui_addlayersdialog.py
X-Poedit-SearchPath-31: View/ui_addnewobjecttolayer.py
X-Poedit-SearchPath-32: View/ui_addnewvectorlayerdialog.py
X-Poedit-SearchPath-33: View/ui_attributetabledialog.py
X-Poedit-SearchPath-34: View/ui_compositiondialog.py
X-Poedit-SearchPath-35: View/ui_delcolumndialog.py
X-Poedit-SearchPath-36: View/ui_editstylelinedialog.py
X-Poedit-SearchPath-37: View/ui_editstylepointdialog.py
X-Poedit-SearchPath-38: View/ui_editstylepolygondialog.py
X-Poedit-SearchPath-39: View/ui_histogramdialog.py
X-Poedit-SearchPath-40: View/ui_identifydialog.py
X-Poedit-SearchPath-41: View/ui_measuredistancetool.py
X-Poedit-SearchPath-42: View/ui_modeldialog.py
X-Poedit-SearchPath-43: View/ui_modelinputrasterdialog.py
X-Poedit-SearchPath-44: View/ui_modeloutputdbdialog.py
X-Poedit-SearchPath-45: View/ui_modelsegmentationdialog.py
X-Poedit-SearchPath-46: View/ui_modelstatisticsdialog.py
X-Poedit-SearchPath-47: View/ui_preferencesdialog.py
X-Poedit-SearchPath-48: View/ui_progressdialog.py
X-Poedit-SearchPath-49: View/ui_propertiesdialog.py
X-Poedit-SearchPath-50: View/ui_rastercalculatordialog.py
X-Poedit-SearchPath-51: View/ui_renamecolumndialog.py
X-Poedit-SearchPath-52: View/ui_segmentationtabledialog.py
X-Poedit-SearchPath-53: View/viewarea.py
X-Poedit-SearchPath-54: View/viewscene.py
X-Poedit-SearchPath-55: View/Additional/palette.py
X-Poedit-SearchPath-56: View/addlayersdialog.py
 - Histogram %s sterownik nie jest dostępny. %s nie został rozpoznany jako wspierany format. &Edytuj styl &Otwórz tabelę atrybutów &Właściwości &Usuń warstwę &Powiększ do warstwy ---- Wybierz kanały ---- <p> Program AgroEye ma w swoim założeniu ułatwić proces kontroli przestrzegania norm Dobrej Kultury Rolnej (DKR) poprzez ograniczenie manualnej oceny działek dla ubiegających się o dopłaty 
z Unii Europejskiej. Głównym celem projektu jest wykonanie aplikacji służącej do integracji danych rastrowych i wektorowych z użyciem klasyfikacji obiektowej (GEOBIA, ang. Geographic Object Based Image Analysis) przy wykorzystaniu najnowszej technologii satelitarnej oraz algorytmów analizy obrazu. Do realizacji wykorzystuje wielospektralne i wieloczasowe serie zobrazowań satelitarnych SENTINEL-2A programu Copernicus (dawny GMES). Zastosowanie klasyfikacji obiektowej pozwala na połączenie zalet ludzkiej interpretacji obrazów z automatyczną analizą danych, działającą według ściśle określonych reguł. To sprawia że interpretacja uzyskanych wyników jest łatwiejsza niż przy standardowych metodach klasyfikacji i co najważniejsze umożliwia integrację danych pochodzących z różnych źródeł różniących się rozdzielczością przestrzenną i spektralną.
 </p> Akcja Dodaj plik Dodaje nowe pole do tabeli Błąd dodawania nowego pola Komunikator AgroEye AgroEye jest gotowy do pracy! Wszystkie kanały Wszystkie zmiany zostały zapisane Wszystkie zmiany zostaną zapisane do: %s Wszystkie pliki (*) Zastosuj Jesteś pewien, że chcesz wyjść? Czy jesteś pewien, że chcesz usunąć wiersz numer %d? Jesteś pewien, że chcesz wyjść bez zapisu? Pole pow. Tabel atrybutów -  Przekątne B Kanał %s  Ścięty Budowanie piramid dla rastra <b>%s</b> Zmiany nie zostały zapisane! Koło Klasyfikacja Zamknij Kod %s nie został znaleziony w pliku EPSG.
Wprowadź poprawny kod EPSG. Kompozycja RGB - Potwierdzenie usunięcia wiersza Błąd układu współrzędnych Kopiuj Liczba Siatkowy Niestandardowa #1 Linia kreska-kropka Linia kreska-kropka-kropka Linia kreskowa Zakres danych Format daty: rrrr-mm-dd Usuń Usuwa pole z tabeli Usuń punkt Usuwa zaznaczony wiersz Wzór 2 Wzór 3 Wzór 4 Wzór 5 Wzór 6 Wzór 7 Diagonalny Romb Linia kropkowa Rysuj obiekt Narysowany poligon jest niepoprawny. Błąd sterownika Kod EPSG Styl edycji -  Tryb edycji Elipsa Błąd! Błąd obiektu Błąd typu pola Pole o takiej samej nazwie już istnieje. 
Wybierz inną nazwę. Format pliku Plik jest używany przez inny program. Pliki z pliku XML zostały wczytane  Płaski Przekątne F Pełna ścieżka Geometria Błąd geometrii Typ geometrii %s nie jest wspierany. Ten typ geometrii nie jest wspierany. Skala szarości Heksagon Hipsometryczna Histogram -  Poziomy Id obiektu Informacja o obiekcie Nie można zmienić stylu wyświetlania wektora Nie można pokazać tabeli atrybutów We wczytywanym pliku nie wykryto żadnych piramid
Może to spowolnić pracę AgroEye
Czy chcesz utworzyć piramidy?

W przypadku Tak:
- Musisz mieć prawa zapisu w folderze źródłowym
- Musisz poczekać na utworzenie się piramid (Postęp będzie wyświetlany na pasku postępu)
W przypadku Nie:
- AgroEye będzie wolniej działał
 Raster wejściowy Wstaw Kanał %i Błąd warstwy Warstwa nie ma żadnych obiektów Nazwa warstwy Ścieżka dostępu warstwy Typ warstwy Wartość maks Maks: Pomiar pola pow. Wartość min Min: Ostry Przesuń niżej Przesuwa zaznaczony wiersz o jedną pozycję w dół Przesuwa zaznaczony wiersz na sam początek Przesuwa zaznaczony wiersz o jedną pozycję do góry  Przesuń na początek Przesuń wyżej NULL Nazwa Nazwa zawiera tylko litery, liczby i podkreślenia.
Jest ograniczona do 15 znaków.
Zaczyna się od liter. Nazwa warstwy Brak wczytanych plików w AgroEye!  Nie zostały wczytane żadne pliki! Nie wykryto piramid dla tego pliku Brak poprawnych warstw! Brak Liczba kanałów Liczba znaków Liczba cyfr po przecinku Liczba cyfr przed przecinkiem Tylko nazwa Błąd otwarcia pliku Otwórz projekt Ostrzeżenie otwarcia pliku Otwarcie projektu spowoduje usunięcie plików i wszystkich ustawień. Czy na pewno chcesz kontynuować ? Klasyfikacja wyjściowa Baza wyjściowa Statystyki wyjściowe Błąd zapisu Błąd ścieżki Pentagon Rodzaj piksela Plus Błąd poligonu Błąd projektu Wczytany plik nie jest plikiem projektu obsługiwanym przez AgroEye. Definicja projekcji Nazwa projekcji Ustawienia Kompozycja RGB ra&stra  Błąd rastra Czerwono-niebieska Czerwona Odśwież (F5) Zmień nazwę Zmiana nazwy: %s Zmienia nazwę zaznaczonego wiersza Zaokrąglony Zaokrąglony kwadrat Zapisz Zapisać ? Błąd zapisu pliku Zapisz projekt jako ... Zapisz jako Zapisz plik Zapisz raport do pliku Zapisz shapefile Błąd zapisu pliku Shapefile Zapisz status Wprowadzona niepoprawna skala. Segmentacja Wyniki segmentacji Ustaw maks Ustaw min Ustaw pole Wyświetl &Histogram Wyświetl segmenty Styl wyświetlania Linia ciągła Baza danych SpatiaLite (*.spatialite) Układ odniesienia Błąd układu odniesień Układ odniesienia jest inny niż układ projektu. Warstwa zostanie przetransformowana do nowego układu. Układ odniesienia jest nieznany. Dane będą narysowane niepoprawnie. Kwadratowy Odchylenie standardowe: Zaczyna edycję pojedynczych wartości Statystyki Rodzaj rozciągania Parametry rozciągania Pliki tekstowe (*.txt);; Wartość maks jest mniejsza niż wartość min. Wartość min jest większa niż wartość maks. Nazwa pliku jest niepoprawna:
nazwa jest ograniczona do 15 znaków. Nazwa pliku jest niepoprawna:
nazwa powinna zawierać litery, liczby lub podkreślenia. Nazwa pliku jest niepoprawna:
nazwa musi zaczynać się od liter. Ścieżka %s jest niepoprawna. Trójkąt 1 Trójkąt 2 Typ Typ pola %s jest nieznany. Pionowy Widoczność Niepoprawny plik! Raster musi być północno zorientowane. pliki projektu XML Błąd skali Powiększ do warstwy po kolumnie na początku 