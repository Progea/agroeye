# AgroEye #

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Data](#data)
* [Links](#links)

## General info
The AgroEye concept is an advanced Open Source application exclusively designed for monitoring agricultural lands in terms of GAEC. 
AgroEye will provide an easy to use spatial analysis system for agriculture, which can be used to support decision making and preparing spatio-temportal analysis. 
Any organizations interested in using application for purpose of monitoring and managing the environment, will get access to source code and the compiled application as well.

## Technologies
Project is developed with:

* Python 2.7.
* GDAL/OGR
* pySpatialite
* pybind11
* PyQt5
* Qt Linguist
* NumPy
* Matplotlib
* Doxygen

## Setup 
To run this project,compile the C++ modules and run with Python Interpreter.

## Data
AgroEye was developed for using raster data primarily Sentinel-2 images in format JPEG2000. Software can open raster file stored as .tiff, .jp2, .kea and .img.
AgroEye supports also vector data formats as ESRI Shapefile, KML, GPX and SpatialiteDB.

## Links

[Documentation (ENG)](https://agroeye.bitbucket.io/index.html)

[Technical guide (ENG) ](https://agroeye.bitbucket.io/docs/Agroeye_technical_guide.pdf)

[User guide and training materials (PL)](https://agroeye.bitbucket.io/docs/Agroeye_materialy_szkoleniowe.pdf)

[Install setup file (.exe) ](https://bitbucket.org/progea/agroeye)