# Find FreeXL
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(MINGW)
    find_path(FREEXL_INCLUDE_DIR freexl.h ${MINGW_ROOT_DIR}/usr/local/include)
    find_library(FREEXL_LIBRARY NAMES freexl PATH ${MINGW_ROOT_DIR}/usr/local/lib)
endif(MINGW)
