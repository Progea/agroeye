# Find iconv
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(MINGW)
    find_path(ICONV_INCLUDE_DIR iconv.h ${MINGW_ROOT_DIR}/msys64/mingw64/include)
    find_library(ICONV_LIBRARY iconv ${MINGW_ROOT_DIR}/msys64/mingw64/lib)
endif(MINGW)
