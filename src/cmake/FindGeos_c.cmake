# Find GEOS_C
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(MINGW)
    find_path(GEOS_C_INCLUDE_DIR geos_c.h ${MINGW_ROOT_DIR}/usr/local/include)
    find_library(GEOS_C_LIBRARY NAMES geos_c PATH ${MINGW_ROOT_DIR}/usr/local/lib)
endif(MINGW)
