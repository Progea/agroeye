# Find Open JPEG 2000
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(MINGW)
    find_path(JP2000_INCLUDE_DIR openjpeg.h ${MINGW_ROOT_DIR}/usr/local/include/openjpeg-2.1)
    find_library(JP2000_LIBRARY openjp2 ${MINGW_ROOT_DIR}/usr/local/lib)
endif(MINGW)
