# Find Eigen3
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(WIN32)
	find_path(EIGEN3_INCLUDE_DIR NAMES Eigen Dense Core
		PATHS
	    ${MINGW_ROOT_DIR}/usr/local/include
	    ${MINGW_ROOT_DIR}/mingw64/include
	    PATH_SUFFIXES eigen3
	    )
else(WIN32)

	find_path(EIGEN3_INCLUDE_DIR NAMES Eigen Dense Core
        PATHS
		/usr/include
		/usr/local/include
	    PATH_SUFFIXES eigen3
		)

endif(WIN32)

if(EIGEN3_INCLUDE_DIR)
	set(EIGEN3_FOUND TRUE)
endif(EIGEN3_INCLUDE_DIR)

if(EIGEN3_FOUND)
	message(STATUS "Found Eigen3 path: ${EIGEN3_INCLUDE_DIR}")
endif(EIGEN3_FOUND)
	

	
