# Find GDAL Custom
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(WIN32)
    find_path(GDAL_CUSTOM_INCLUDE_DIR gdal_priv.h ${MINGW_ROOT_DIR}/usr/local/include)
    find_library(GDAL_CUSTOM_LIBRARY gdal ${MINGW_ROOT_DIR}/usr/local/lib)
else(WIN32)

	find_path(GDAL_CUSTOM_INCLUDE_DIR gdal_priv.h
		/usr/include
		/usr/include/gdal
		/usr/local/include
		)

	find_library(GDAL_CUSTOM_LIBRARY libgdal.so
		/usr/lib
		/usr/lib64
		/usr/local/lib
		)

endif(WIN32)

if(GDAL_CUSTOM_LIBRARY AND GDAL_CUSTOM_INCLUDE_DIR)
	set(GDAL_CUSTOM_FOUND TRUE)
endif(GDAL_CUSTOM_LIBRARY AND GDAL_CUSTOM_INCLUDE_DIR)

if(GDAL_CUSTOM_FOUND)
	message(STATUS "Found GDAL Custom: ${GDAL_CUSTOM_LIBRARY}")
endif(GDAL_CUSTOM_FOUND)
	
