# Find GEOS
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(MINGW)
    find_path(GEOS_INCLUDE_DIR geos.h ${MINGW_ROOT_DIR}/usr/local/include)
    find_library(GEOS_LIBRARY NAMES geos PATH ${MINGW_ROOT_DIR}/usr/local/lib)
endif(MINGW)
