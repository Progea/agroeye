# Find Spatialite
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(WIN32)
    find_path(SPATIALITE_INCLUDE_DIR spatialite.h ${MINGW_ROOT_DIR}/usr/local/include)
    find_library(SPATIALITE_LIBRARY spatialite ${MINGW_ROOT_DIR}/usr/local/lib)
else(WIN32)
	find_path(SPATIALITE_INCLUDE_DIR spatialite
		/usr/include
		/usr/include/spatialite
		/usr/local/include
		/usr/local/include/spatialite
		)

	find_library(SPATIALITE_LIBRARY libspatialite.so
		/usr/lib
		/usr/local/lib
		)

endif(WIN32)

if(SPATIALITE_INCLUDE_DIR AND SPATIALITE_LIBRARY)
	set(SPATIALITE_FOUND TRUE)
endif(SPATIALITE_INCLUDE_DIR AND SPATIALITE_LIBRARY)

if(SPATIALITE_FOUND)
	message(STATUS "Found Spatialite: ${SPATIALITE_LIBRARY}")
endif(SPATIALITE_FOUND)
	
