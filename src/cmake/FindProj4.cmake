# Find Proj4
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(MINGW)
    find_path(PROJ4_INCLUDE_DIR proj_api.h ${MINGW_ROOT_DIR}/usr/local/include)
    find_library(PROJ4_LIBRARY NAMES proj PATH ${MINGW_ROOT_DIR}/usr/local/lib)
endif(MINGW)
