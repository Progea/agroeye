# Find Google Mock
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(WIN32)
    #find_path(GDAL_CUSTOM_INCLUDE_DIR gdal_priv.h ${MINGW_ROOT_DIR}/usr/local/include)
    #find_library(GDAL_CUSTOM_LIBRARY gdal ${MINGW_ROOT_DIR}/usr/local/lib)
else(WIN32)

	find_path(GMOCK_INCLUDE_DIR gmock.h
                PATHS
		/usr/include
		PATH_SUFFIXES
		gmock
		)

	find_library(GMOCK_LIBRARY libgmock_main.so                
		/usr/local/lib
		/lib
		)

endif(WIN32)

if(GMOCK_LIBRARY AND GMOCK_INCLUDE_DIR)
	set(GMOCK_FOUND TRUE)
endif(GMOCK_LIBRARY AND GMOCK_INCLUDE_DIR)

if(GMOCK_FOUND)
	message(STATUS "Found GMock: ${GMOCK_LIBRARY}")
endif(GMOCK_FOUND)
	
