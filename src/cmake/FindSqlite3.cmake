# Find Sqlite3
# ~~~~~~~~~~~~~~~~~~~~~~~~

if(WIN32)
    find_path(SQLITE3_INCLUDE_DIR sqlite3.h ${MINGW_ROOT_DIR}/usr/local/include)
    find_library(SQLITE3_LIBRARY sqlite3 ${MINGW_ROOT_DIR}/usr/local/lib)
else(WIN32)

	find_path(SQLITE3_INCLUDE_DIR sqlite3.h
		/usr/include
		/usr/local/include
		)

	find_library(SQLITE3_LIBRARY libsqlite3.so
		/usr/lib
		/usr/local/lib
		)

endif(WIN32)

if(SQLITE3_INCLUDE_DIR AND SQLITE3_LIBRARY)
	set(SQLITE3_FOUND TRUE)
endif(SQLITE3_INCLUDE_DIR AND SQLITE3_LIBRARY)

if(SQLITE3_FOUND)
	message(STATUS "Found Sqlite3: ${SQLITE3_LIBRARY}")
endif(SQLITE3_FOUND)
	
