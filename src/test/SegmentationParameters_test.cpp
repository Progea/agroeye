#include "SegmentationParameters.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace ::testing;
using namespace agroeye::operations::segmentation;

class DefaultTestParameters : public Test {
public:
    Parameters parameters;
};

TEST_F(DefaultTestParameters, ChessboardSquareSideIs256) {
    ASSERT_THAT(parameters.getSquareSide(), Eq(256));
};
