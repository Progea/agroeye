#include "SegmentationChessboard.h"
#include "Raster.h"
#include "DatabaseSqlite.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace ::testing;
using namespace agroeye::operations::segmentation;
using namespace agroeye::errors;

using Raster = agroeye::handlers::Raster;
using Sqlite = agroeye::handlers::Sqlite;

#ifdef __linux__
    #define temp std::string{"/tmp/"}
    #define rasters std::string {"../../../test/rasters/"}
#endif

namespace samples {
    std::string malo {rasters + "malo.tif"};    
    std::string maloDb {temp + "malo.sqlite"};
    std::string duzy {rasters + "stack_rect_sub_12bands_20160827_10m_aoi51.tif"};
    std::string duzyDb {temp + "stack_rect_sub_12bands_20160827_10m_aoi51.sqlite"};
}

TEST(Chessboard, DISABLED_CorrectValues) {
    std::remove(samples::maloDb.c_str());
    std::remove(samples::duzyDb.c_str());
    Sqlite db {samples::duzyDb,  true};
    Raster raster {samples::duzy.c_str()};
    db.prepareForSegmentation(raster); 
    
    Chessboard chess {
        raster,
        Parameters{5}, 
        db.openFeatureRepository()
    };
    chess.setCallback(agroeye::general::defaultCallback);
    chess.start();
}

TEST(Quadtree, CorrectValues) {
    std::remove(samples::maloDb.c_str());
    std::remove(samples::duzyDb.c_str());
    Sqlite db {samples::duzyDb,  true};
    Raster raster {samples::duzy.c_str()};
    db.prepareForSegmentation(raster); 
    
    Quadtree quad  {
        raster,
        Parameters{{4}, 500}, 
        db.openFeatureRepository()
    };
    quad.setCallback(agroeye::general::defaultCallback);
    quad.start();
}
