#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "AnalysisParameters.h"
#include "RasterAnalysis.h"
#include "Raster.h"
#include "Callback.h"

#ifdef __linux__ 
    #define temp std::string{"/tmp/"}
    //#define temp std::string{"/home/lukasz/Pulpit/Dysk_D/temp/"}
    #define rasters std::string {"../../../test/rasters/"}
#endif

namespace samples {
    std::string malo {rasters + "malo.tif"};    
    std::string duzy {rasters + "stack_rect_sub_12bands_20160827_10m_aoi51.tif"};
    std::string malo_out {temp + "malo_out.tif"};
    std::string duzy_out {temp + "stack_rect_sub_12bands_20160827_10m_aoi51_out.tif"};
}

using namespace ::testing;
using namespace agroeye::errors;

using Parameters = agroeye::operations::raster_analysis::Parameters;
using Anal_type = agroeye::operations::raster_analysis::Analysis_type;
using Raster = agroeye::handlers::Raster;
using BlockStatistics = agroeye::operations::raster_analysis::BlockStatistics;
using agroeye::general::defaultCallback;

TEST(Parameters, AreCreateable) {
  Parameters params;   
}

TEST(Parameters, DefualtValues) {
  Parameters params {Anal_type::DIFFERENCE_TO_NEIGHBOUR, 1};
  ASSERT_THAT(params.getType(), Eq(Anal_type::DIFFERENCE_TO_NEIGHBOUR));
  ASSERT_THAT(params.getNeighbourhood(), Eq(Eigen::Matrix<int, 3, 3>().setOnes()));
}

TEST(Parameters, SetupKernel) {
  Eigen::Matrix<int, 5, 8> size;
  size.fill(1);
  Parameters params {Anal_type::DIFFERENCE_TO_NEIGHBOUR, 4, size};
  ASSERT_THAT(params.getNeighbourhood().rows(), Eq(5));
  ASSERT_THAT(params.getNeighbourhood().cols(), Eq(8));
}

class SampleRaster : public Test {
public:
  Raster inRaster;
  std::string outPath {samples::duzy_out};
  Parameters params_empty;
  Parameters params_3_3 {Anal_type::DIFFERENCE_TO_NEIGHBOUR, 8, 3, 3};
  
  void SetUp() override {
      GDALAllRegister();
     auto drv = GetGDALDriverManager()->GetDriverByName("GTiff");
     drv->QuietDelete(outPath.c_str());
     inRaster = Raster {samples::duzy};
  }
  void TearDown() override {
      
  }
};

TEST_F(SampleRaster, CanBeAssignedToStats){
   ASSERT_THROW(BlockStatistics (inRaster, outPath, params_empty), IncorrectNeighbourhood);
}

TEST_F(SampleRaster, CanBeCoppied) {
   //inRaster.setCallback(defaultCallback);
   //BlockStatistics bs {inRaster, outPath, params_3_3};
   BlockStatistics bs {inRaster, outPath, Parameters{Anal_type::DIFFERENCE_TO_NEIGHBOUR, 8, 5, 5}};
   bs.setCallback(defaultCallback);
   
   bs.start();
   
    
}

