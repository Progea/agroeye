#include "DatabaseSqlite.h"
#include "Feature.h"
#include "FeatureRepository.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#ifdef __linux__
#define temp                                                                   \
  std::string {                                                                \
    "/tmp/"                                                                    \
  }
#endif

namespace samples {
const std::string dbFolder{"../../../test/databases/"};
const std::string rasterFolder{"../../../test/rasters/"};
const std::string empty_test{temp + "empty_test.sqlite"};
const std::string stack_12bands_db{
    dbFolder + "stack_rect_sub_12bands_20160827_10m_aoi51.sqlite"};
const std::string stack_12bands_raster {
    rasterFolder + "stack_rect_sub_12bands_20160827_10m_aoi51.tif"};
const std::string stack_12bands_name {"stack_rect_sub_12bands_20160827_10m_aoi51.tif"};
}
using namespace ::testing;
using namespace agroeye::handlers;
using namespace agroeye::handlers::image_objects;
using namespace agroeye::errors;

TEST(EmptyFeature, CanBeCreated) {
  image_objects::Feature f;
}

TEST(EmptyFeature, HasIdMinus1) {
  image_objects::Feature f;
  ASSERT_THAT(f.getId(), Eq(-1));
}

TEST(FeatureRepository, IsCreatable) {
  Sqlite s;
  s.openFeatureRepository();
}

class ExistingDatabase : public Test {
public:
  Sqlite db;

  virtual void SetUp() override {
    db = Sqlite{samples::stack_12bands_db, true};
  }
  OGRLinearRing getSampleRing() const {
    OGRLinearRing lr;
    lr.setPoint(0, 0, 0);
    lr.setPoint(1, 100, 100);
    lr.setPoint(2, 100, 0);
    return lr;
  }
};

TEST_F(ExistingDatabase, ReturnsRepository) {
  const auto someId                     = 234;
  auto fr = db.openFeatureRepository();
  ASSERT_THAT(fr->read(someId).getId(), Eq(someId));
}

TEST_F(ExistingDatabase, ReturnsGeometry) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();
  image_objects::Feature feat           = fr->read(1);
  auto polygon                          = feat.getPolygon();
  ASSERT_THAT(polygon.get_Area(), Gt(100));
}

TEST_F(ExistingDatabase, ReturnsMeanValues) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();
  auto feat = fr->read(1);
  const auto& raster1 = feat[samples::stack_12bands_name];
  ASSERT_THAT(raster1.getMean(1), DoubleNear(0.25, 0.0001));
  ASSERT_THAT(raster1.getMean(2), DoubleNear(2.66, 0.0001));

  auto feat2 = fr->read(2);
  auto& raster2 = feat2[samples::stack_12bands_name];
  ASSERT_THAT(raster2.getMean(1), DoubleNear(213, 0.0001));
}


TEST_F(ExistingDatabase, ReturnsNdviValues) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();

  auto feat = fr->read(1);
  auto& raster1 = feat[samples::stack_12bands_name];
  //ASSERT_THAT(raster1.getNdvi(), DoubleNear(0, 1.0));

  auto feat2 = fr->read(1910);
  auto& raster2 = feat2[samples::stack_12bands_name];
  //ASSERT_THAT(raster2.getNdvi(), DoubleNear(0.865, 0.01));
}


TEST_F(ExistingDatabase, ReturnsStdDevValues) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();

  auto feat = fr->read(1);
  auto& raster1 = feat[samples::stack_12bands_name];
  ASSERT_THAT(raster1.getStddev(1), DoubleNear(34.432, 0.001));
  ASSERT_THAT(raster1.getStddev(2), DoubleNear(3.58, 0.01));
}

TEST_F(ExistingDatabase, UpdatesNdvi) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();

  auto feat              = fr->read(1);
  auto& stats            = feat[samples::stack_12bands_name];
  const auto defaultNdvi = stats.getNdvi();
  const auto newNdvi     = -0.23;
  //stats.setNdvi(newNdvi);
  std::vector<image_objects::Feature> f;
  f.push_back(std::move(feat));
  fr->update(f);
}

TEST_F(ExistingDatabase, UpdatesMean) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();

  auto feat = fr->read(1);
  auto& stats = feat[samples::stack_12bands_name];
  stats.setMean(1, 2.56);
  std::vector<image_objects::Feature> f;
  f.push_back(std::move(feat));
  fr->update(f);

  auto feat2 = fr->read(1);
  auto& raster2 = feat2[samples::stack_12bands_name];
  ASSERT_THAT(raster2.getMean(1), DoubleNear(2.56, 0.01));

  raster2.setMean(1, 0.25);
  std::vector<image_objects::Feature> f2;
  f2.push_back(std::move(feat2));
  fr->update(f2);
}


TEST_F(ExistingDatabase, UpdatesStandardDeviation) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();

  auto feat = fr->read(1);
  auto& stats = feat[samples::stack_12bands_name];
  stats.setStddev(1, 50);
  stats.setStddev(2, 99.99);
  
  std::vector<decltype(feat)> f;
  f.push_back(std::move(feat));

  fr->update(f);

  auto feat2 = fr->read(1);
  auto& raster2 = feat[samples::stack_12bands_name];
  ASSERT_THAT(raster2.getStddev(1), DoubleNear(50, 1));
  ASSERT_THAT(raster2.getStddev(2), DoubleNear(99.99, 0.01));

  stats.setStddev(1, 34.432);
  stats.setStddev(2, 3.58);
  std::vector<decltype(feat2)> f2;
  f2.push_back(std::move(feat2));
  fr->update(f2);
}


TEST_F(ExistingDatabase, CreatesSegmentWithGeometry) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();

  const auto id = 12345678;
  OGRPolygon poly;
  auto sampleRing = getSampleRing();
  poly.addRing(&sampleRing);
  
  std::vector<image_objects::Feature> f;
  f.emplace_back(id, poly);

  fr->create(f);
  
  auto newFeat = fr->read(id);
  
  fr->remove(id);
  
  ASSERT_THAT(newFeat.getPolygon().get_Area(), DoubleNear(poly.get_Area(), 0.01));
}

TEST_F(ExistingDatabase, CreatesSegmentWithAttributes) {
   std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();
   const auto id = 23456789;
   fr->remove(id);
   
   OGRPolygon poly;
   auto ring = getSampleRing();
   poly.addRing(&ring);
   
   image_objects::Feature feat {id, poly};  
   auto& raster1 = feat[samples::stack_12bands_name];
   const auto band1 = 1; 
   const auto mean1 = 666.666;
   
   const auto band2 = 2;
   const auto mean2 = 555.555;
   
   raster1.setMean(band1, mean1);
   raster1.setMean(band2, mean2);
   
   std::vector<decltype(feat)> f;
   f.push_back(std::move(feat));
   fr->create(f);
   
   auto feat2 = fr->read(id);
   auto& raster2 = feat2[samples::stack_12bands_name];
   
   fr->remove(feat.getId());
   ASSERT_THAT(raster2.getMean(band1), DoubleNear(mean1, 0.001));
   ASSERT_THAT(raster2.getMean(band2), DoubleNear(mean2, 0.001));
}


TEST_F(ExistingDatabase, RemovesFeatureFromSegmentsTable) {
  std::shared_ptr<FeatureRepository> fr = db.openFeatureRepository();
  const int idToDelete{12345678};
  fr->remove(idToDelete);

  ASSERT_THROW(fr->read(idToDelete), FeatureDoesNotExist);
}

