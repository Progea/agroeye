#include "ContainerMath.h"

#include <vector>
#include <set>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;

class SampleVector : public Test {
public:
    std::vector<int> vectorInt {0, 2, 6, 4, 10, 8};    
    std::vector<int> vectorInt2 {0, 3};    
    std::vector<double> vectorInt3 {0, 6, 4, 0, -3, 0, 64};
    std::vector<double> vectorInt4 {64};
    std::vector<double> vectorDouble {-3, -5.1, 34.6, 54.3, -43.7, 0.4, 32255};
    std::vector<double> vectorDouble2 {0.4, -3, -5.1, 256, 34.6, 54.3, -43.7};
};

TEST_F(SampleVector, MeanIsCorrect){
    ASSERT_THAT(agroeye::algorithm::math::mean(vectorInt.begin(), vectorInt.end()),
                Eq(5));
    EXPECT_THAT(agroeye::algorithm::math::mean(vectorDouble.begin(), vectorDouble.end()),
                DoubleNear(4613.21, 0.1));
    EXPECT_THAT(agroeye::algorithm::math::mean(vectorDouble2.begin(), vectorDouble2.end()),
                DoubleNear(41.93, 0.01));
    ASSERT_THAT(agroeye::algorithm::math::mean(vectorInt2.begin(), vectorInt2.end()),
                Eq(1.5));
};

TEST(QuadraticDiffernce, isCorrect) {
    using qDiff = agroeye::algorithm::math::quadraticdifference_accumulator<double>;
    ASSERT_THAT(qDiff(5)(0,5), DoubleEq(0));    
    ASSERT_THAT(qDiff(5)(-63,5), DoubleEq(-63+0));    
    ASSERT_THAT(qDiff(0)(0,1), DoubleEq(1));    
    ASSERT_THAT(qDiff(1)(0,2), DoubleEq(1));
    ASSERT_THAT(qDiff(0)(0,2), DoubleEq(4));
    ASSERT_THAT(qDiff(2.45)(0, 235.91), DoubleNear(54503.57, 0.01));
    ASSERT_THAT(qDiff(-44.6)(0, 23.89), DoubleNear(4690.88, 0.01));
    ASSERT_THAT(qDiff(-44.6)(123, 23.89), DoubleNear(123+4690.88, 0.01));
}

TEST_F(SampleVector, StdDevIsCorrect){
    ASSERT_THAT(agroeye::algorithm::math::stddev(vectorInt4.begin(), vectorInt4.end()),
                DoubleNear(0.00, 0.01));
    ASSERT_THAT(agroeye::algorithm::math::stddev(vectorInt2.begin(), vectorInt2.end()),
                DoubleNear(1.5, 0.01));
    ASSERT_THAT(agroeye::algorithm::math::stddev(vectorDouble.begin(), vectorDouble.end()),
                DoubleNear(11284.749, 0.01));
    ASSERT_THAT(agroeye::algorithm::math::stddev(vectorDouble2.begin(), vectorDouble2.end()),
                DoubleNear(92.056, 0.01));
}

TEST_F(SampleVector, StdDeVvalueIgnoringOneValue){
    ASSERT_THAT(agroeye::algorithm::math::stddev(vectorDouble.begin(), vectorDouble.end(), 32255), DoubleNear(31.241, 0.01));
    ASSERT_THAT(agroeye::algorithm::math::stddev(vectorDouble2.begin(), vectorDouble2.end(), 256), DoubleNear(31.241, 0.01));
}
