#include "DatabaseTable.h"

#include <gmock/gmock.h>


using namespace ::testing;
using namespace agroeye::handlers;

class ADbTable : public Test {
public:
    DbTable table {"empty_table"};    
};

TEST_F(ADbTable, IsEmptyWhenCreated) {
    ASSERT_THAT(table.columnsCount(), Eq(0));    
}

TEST_F(ADbTable, HasCorrectName) {
    ASSERT_THAT(table.getName(), StrEq("empty_table"));
}

TEST_F(ADbTable, ColumnFirstIteratorIsLast) {
    ASSERT_EQ(table.begin(), table.end());
}

class ADbTableWithOneSimpleColumn : public Test {
public:
    DbTable table {"one_column_table"};
    void SetUp() override {
        table.addColumn(DbColumn{"one", ColumnType::INTEGER});
    }
};

TEST_F(ADbTableWithOneSimpleColumn, HasCorrectSize) {
    ASSERT_THAT(table.columnsCount(), Eq(1));
}

TEST_F(ADbTableWithOneSimpleColumn, CanBeAddedPrimaryKeyColumn) {
    ASSERT_NO_THROW(table.addColumn(DbColumn{"two", ColumnType::NONE}));
    ASSERT_THAT(table.columnsCount(), Eq(2));
}

class ADbTableWithMoreColumns : public Test {
public:
    DbTable table {"complex_table"};
    void SetUp() override {
        table.addColumn(DbColumn{"one", ColumnType::INTEGER});
        table.addColumn(DbColumn{"two", ColumnType::INTEGER});
        table.addColumn(DbColumn{"three", ColumnType::INTEGER});
        table.addColumn(DbColumn{"four", ColumnType::INTEGER});
    }    
};

TEST_F(ADbTableWithMoreColumns, NoMorePrimaryColumnsCanBeAdded) {
    ASSERT_THROW(table.addColumn(DbColumn{"five", ColumnType::NONE}),
                 agroeye::errors::TableError);
    ASSERT_THAT(table.columnsCount(), Eq(4));
}

TEST_F(ADbTableWithMoreColumns, ColumnsFirstIteratorIsNotLast) {
    ASSERT_NE(table.begin(), table.end());
}

TEST_F(ADbTableWithMoreColumns, ColumnsIteratorArithemiticsWorks) {
    auto cols = table.columnsCount();
    ASSERT_EQ(table.begin()+cols, table.end());    
    ASSERT_EQ(table.end()-cols, table.begin());
}

