#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "StatsParameters.h"
#include "StatsComputation.h"
#include "DatabaseSqlite.h"
#include "Raster.h"

using namespace ::testing;
using namespace agroeye::operations::stats;
using Sqlite = agroeye::handlers::Sqlite;
using Raster = agroeye::handlers::Raster;

#ifdef __linux__
    #define temp std::string{"/tmp/"}
    #define databases std::string {"../../../test/databases/"}
    #define rasters std::string {"../../../test/rasters/"}
#endif


namespace samples {
    std::string malo {rasters + "malo.tif"};    
    std::string maloDb {temp + "malo.sqlite"};
    std::string duzy {rasters + "stack_rect_sub_12bands_20160827_10m_aoi51.tif"};
    std::string duzyDb {temp + "stack_rect_sub_12bands_20160827_10m_aoi51.sqlite"};
}

TEST(Params, canBeCreated) {
    Parameters params;
}

class EmptyParams : public Test {
public:
    Parameters params;
};

TEST_F(EmptyParams, CanBeMeanBandAssinged) {
    params.setNdvi(2, 5);
};

TEST_F(EmptyParams, ThrowErrorIfInvalidBand) {
    ASSERT_ANY_THROW(params.setNdvi(-1, 4));
}

TEST_F(EmptyParams, NdviGetIsTheSameSet) {
    Ndvi ndvi {4, 6};
    params.setNdvi(ndvi.vis, ndvi.nir);
    ASSERT_EQ(params.getNdvi(), ndvi);
}

class OpenedDatabase : public Test {
public:
    Parameters params;
    Parameters params2;
    Parameters params_stddev;
    Parameters params_stddev2;
    Parameters params_ndvi;
    Parameters params_zabud;
    void SetUp() override {
        params.addMeanBand(1);
        params2.addMeanBand(2);
        params_stddev.addStddevBand(1);
        params_stddev2.addStddevBand(2);
        params_ndvi.setNdvi(4, 8);
        params_zabud.setZabud(2, 3, 4, 8);
    }
    
};

TEST_F(OpenedDatabase, DISABLED_CanBeFilledWithMean) {
    Sqlite db {samples::duzyDb, true};
    Raster raster {samples::duzy.c_str()};
    StatsComputation stats {raster, params, db.openFeatureRepository()};
    stats.setCallback(agroeye::general::defaultCallback);
    stats.start();
}

TEST_F(OpenedDatabase, DISABLED_CanBeFilledWithStdDev) {
    Sqlite db {samples::duzyDb, true};
    Raster raster {samples::duzy.c_str()};
    StatsComputation stats {raster, params_stddev, db.openFeatureRepository()};
    stats.setCallback(agroeye::general::defaultCallback);
    stats.start();
}

TEST_F(OpenedDatabase, DISABLED_CanBeFilledWithNdvi) {
    Sqlite db {samples::duzyDb, true};
    Raster raster {samples::duzy.c_str()};
    StatsComputation stats {raster, params_ndvi, db.openFeatureRepository()};
    stats.setCallback(agroeye::general::defaultCallback);
    stats.start();
}

TEST_F(OpenedDatabase, CanBeFilledWithZabud) {
    Sqlite db {samples::duzyDb, true};
    Raster raster {samples::duzy.c_str()};
    StatsComputation stats {raster, params_zabud, db.openFeatureRepository()};
    stats.setCallback(agroeye::general::defaultCallback);
    stats.start();
}

TEST_F(OpenedDatabase, DISABLED_CanBeFilledWithZabudTest) {
    Sqlite db {"/tmp/zabud.sqlite", true};
    Raster raster {"/tmp/summer.tif"};
    StatsComputation stats {raster, params_zabud, db.openFeatureRepository()};
    stats.setCallback(agroeye::general::defaultCallback);
    stats.start();
}

TEST(Compute, DISABLED_Zabud) {
  Sqlite db {"/tmp/chessboard/stack_rect_sub_12bands_20160827_10m_aoi51.sqlite", true};
  Raster raster {"/tmp/chessboard/stack_rect_sub_12bands_20160827_10m_aoi5.tif"};
  Parameters params;
  params.setZabud(2, 3, 4, 8);
  
  StatsComputation stats {raster, params, db.openFeatureRepository()};
  stats.setCallback(agroeye::general::defaultCallback);
  stats.start();
  
}


TEST(Compute, DISABLED_NDVI) {
   Sqlite db {"/tmp/stack_rect_sub_12bands_20160827_10m_aoi51.sqlite", true}; 
   Raster raster {samples::duzy.c_str()};
   
   Parameters params;
   params.setNdvi(4, 8);
   
   params.addMeanBand(4);
   params.addMeanBand(8);
   
  StatsComputation stats {raster, params, db.openFeatureRepository()};
  stats.setCallback(agroeye::general::defaultCallback);
  stats.start();
  
}

