#include "RasterDataGroup.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace ::testing;
using namespace agroeye::handlers;
using namespace agroeye::errors;

class ARasterDataGroup : public Test {
public:
    RasterDataGroup group;
    void addDataToBand1(){
        group.addData(1, RasterData {100, 100});
    }
};

TEST_F(ARasterDataGroup, IsEmptyWhenCreated) {
    ASSERT_THAT(group.getBandsList().size(), Eq(0));
}

TEST_F(ARasterDataGroup, CanBeAddedData) {
    ASSERT_NO_THROW(addDataToBand1());
}

TEST_F(ARasterDataGroup, AfterAdditionHasSizeOne){
    addDataToBand1();
    ASSERT_THAT(group.getBandsList().size(), Eq(1));
}

TEST_F(ARasterDataGroup, AllowsNoDuplicatingBands){
    addDataToBand1();
    ASSERT_ANY_THROW(group.addData(1, RasterData{10, 10}));
}

TEST_F(ARasterDataGroup, ProvidesAccessForStoredData) {
    addDataToBand1();
    ASSERT_NO_THROW(group.getBandData(1));
}

TEST_F(ARasterDataGroup, PreventsAccessToNotAvilableData) {
    addDataToBand1();
    ASSERT_THROW(group.getBandData(2), IncorrectBand);
}

