#include "Raster.h"
#include "RasterData.h"
#include "Callback.h"
#include "Errors.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace ::testing;

namespace samples {
#ifndef WIN32
    std::string raster_duzy {"/home/lukasz/Rastry/duzy.tif"};
    std::string sentinel_maly {"/home/lukasz/Rastry/sent/Sentinel_Stack.tif"};
    std::string sentinel_maly_out {"/tmp/sentinel_maly_out.tif"};
    std::string sentinel_12band {"/home/lukasz/Pulpit/agroeye_koncowe/dane/lvl2A_CALIBRATED_SUBSETS/stack_rect_sub_12bands_20160307_10m_aoi1.tif"};
#else
    std::string raster_duzy {"D:\\Rastry\\duzy.tif"};
    std::string sentinel_maly {"D:\\Rastry\\Sentinel_Stack.tif"};
    std::string sentinel_maly_out {"D:\\Rastry\\sentinel_maly_out.tif"};
#endif
    size_t small_dimension {120};
    size_t too_big_dimension {9999999};
    auto   negative_dimension = -1;
}

using agroeye::handlers::Raster;
using agroeye::general::silenceCallback;
using namespace samples;

TEST(IncorrectParameters, CheckRasterFilepath) {
    using agroeye::handlers::Raster;
    ASSERT_THROW(Raster ("zly_raster.tif", agroeye::general::silenceCallback), agroeye::errors::IncorrectRaster);
    ASSERT_NO_THROW(Raster (samples::raster_duzy.c_str(), agroeye::general::silenceCallback));
};


TEST(IncorrectParameters, CheckRasterData) {
    using namespace samples;
    using agroeye::handlers::RasterData;
    ASSERT_NO_THROW(RasterData(small_dimension, small_dimension));
    ASSERT_THROW(RasterData(too_big_dimension, too_big_dimension), agroeye::errors::RasterDataTooBig);
    ASSERT_THROW(RasterData(negative_dimension, negative_dimension), agroeye::errors::RasterDataTooBig);    
};

TEST(RasterDataTest, CheckAccess) {
    agroeye::handlers::RasterData r (samples::small_dimension, samples::small_dimension);
    r(23, 23) = 5;
    r(100, 100) = -123.567;
    ASSERT_THAT(r(23,23), DoubleNear(5, 0.01));
    ASSERT_THAT(r(100, 100), DoubleNear(-123.567, 0.001));
}

TEST(RasterDataTest, CheckCopyAndMove){
    using agroeye::handlers::RasterData;
    RasterData r (samples::small_dimension, samples::small_dimension);
    r(23, 23) = 5;
    r(100, 100) = -123.567;
    
    RasterData r2 (std::move(r));
    ASSERT_THAT(r2(23, 23), DoubleNear(5, 0.01));
    ASSERT_THROW(r(23, 23), agroeye::errors::PixelOutsideRasterData);
    
    RasterData r3 (1, 1);
    r3 = r2;    
    ASSERT_THAT(r3(23, 23), DoubleEq(r3(23, 23)));
    ASSERT_THAT(r3(100, 100), DoubleEq(r3(100, 100)));    
}

TEST(IncorrectParameters, TooBigExtentToRead){
    using namespace agroeye::handlers;
    Raster r (samples::raster_duzy.c_str(), agroeye::general::silenceCallback);
    auto band = r.getBand(2);
    ASSERT_THROW(band.read(0, 0, samples::too_big_dimension, samples::too_big_dimension), agroeye::errors::IncorrectExtent);
}

TEST(CheckOperation, StdDevValue) {
    using agroeye::handlers::Raster;
    Raster r (samples::sentinel_maly.c_str(), agroeye::general::silenceCallback);
    auto band = r.getBand(3);
    auto read = band.read(band.getWidth()/3, band.getHeight()/5, band.getWidth()*0.2, band.getHeight()*0.35);
    
}
