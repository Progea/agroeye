#include "DatabaseSqlite.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stdio.h>

using namespace ::testing;
using namespace agroeye::errors;
using agroeye::handlers::Sqlite;

#ifdef __linux__
#define temp                                                                   \
  std::string {                                                                \
    "/tmp/"                                                                    \
  }
#endif

namespace samples {
const std::string dbFolder{"../../../test/databases/"};
const std::string empty_test{temp + "empty_test.sqlite"};
const std::string stack_12bands_db{
    dbFolder + "stack_rect_sub_12bands_20160827_10m_aoi51.sqlite"};
}

class SimpleSqliteDatabase : public Test {
public:
  virtual void SetUp() override {
    using samples::empty_test;
    std::remove(empty_test.c_str());
  }

  virtual void TearDown() override {
    using samples::empty_test;
    std::remove(empty_test.c_str());
  }
};

class CreatedDatabase : public Test {

public:
  Sqlite db;
  virtual void SetUp() override {
    using samples::empty_test;
    std::remove(empty_test.c_str());
    db = Sqlite{empty_test, true};
  }
  virtual void TearDown() override {
    using samples::empty_test;
    // std::remove(empty_test.c_str());
  }
};

TEST_F(SimpleSqliteDatabase, CreatableInMemory) {
  Sqlite s;
}

TEST_F(SimpleSqliteDatabase, SavesOnDisk) {
  using samples::empty_test;
  Sqlite s{empty_test, true};
  auto file = fopen(empty_test.c_str(), "r");
  ASSERT_THAT(file, Ne(nullptr));
  fclose(file);
}

TEST_F(SimpleSqliteDatabase, ThrowsErrorWhenOpensNotExistingFile) {
  ASSERT_THROW(Sqlite("some not existing database", false),
               agroeye::errors::IncorrectDatabase);
}

TEST_F(SimpleSqliteDatabase, IsOpenable) {
  using samples::empty_test;

  // First create database somewhere
  Sqlite s{empty_test, true};

  // Reaopen it
  Sqlite s2{empty_test, false};
}

class ExistingDatabase : public Test {
public:
  Sqlite db;

  virtual void SetUp() override {
    db = Sqlite{samples::stack_12bands_db};
  }
};
