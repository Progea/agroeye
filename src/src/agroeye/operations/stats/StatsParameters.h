#ifndef STATSPARAMETERS_H
#define STATSPARAMETERS_H

#include <vector>
#include <algorithm>
#include <string>
#include <set>
#include "Errors.h"

namespace agroeye {
namespace operations {
namespace stats {

/**
 *  @brief Types of statistics that can be computed
 */
enum class Stat {
    MEAN,   /**< Mean value */
    STDDEV, /**< Standard deviation value */
    NDVI,   /**< NDVI value, coeficient from two bands */
    ZABUD,
    NONE
};

std::string& operator += (std::string& s, const Stat& t);

/**
 *  @brief Encapsulation of NDVI bands
 */
struct Ndvi {
    int nir {-1}; /**< Near InfraRed channel */
    int vis {-1}; /**< VISible red channel */
    
    Ndvi(const int vis = -1, const int nir = -1) : vis(vis), nir(nir) {};
    
    /**
     *  @brief Examination if two Ndvi objects holds the same bands
     *  @param other The other Ndvi object
     *  @return If true to objects have the same Ndvi properties
     */
    bool operator==(const Ndvi& other) const;
    
    bool isSet() const;
};

struct Zabud {
   int blue {-1};   
   int green {-1};
   int red {-1};
   int nir {-1};
   
   bool isSet() const;
    
};

/**
 *  @brief Encapsulation of parameters taking part in calculating statistics
 */
class Parameters {
public:
    /**
     *  @brief Sets the ndvi bands into computation the coeficient
     * 
     *  @param nir Band with information at near infra-red
     *  @param vis Band with information at visible red
     */
    void setNdvi(const int vis, const int nir);
    
    /**
     *  @brief The return of Ndvi object 
     * 
     *  @return The ndvi bands
     */
    Ndvi getNdvi() const {return ndvi;};
    
    void setZabud(const int blue, const int green, const int red, const int nir);
    
    Zabud getZabud() const { return zabud;};
    
    void addMeanBand(const int band);
    std::set<int> getMeanBands() const;
    
    void addStddevBand(const int band);
    std::set<int>getStddevBands() const;
    
    std::set<int> getStatBands() const;
private:
    void checkBands(const std::vector<int>& bands) const;
    Ndvi ndvi;
    Zabud zabud;
    std::set<int> meanBands;
    std::set<int> stddevBands;
};
    
} // namespace stats
} // namespace operations
} // namespace agroeye

#endif
