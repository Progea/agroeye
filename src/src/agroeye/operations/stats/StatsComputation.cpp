#include "StatsComputation.h"

namespace agroeye {
namespace operations {
namespace stats {

StatsComputation::StatsComputation(const Raster &raster,
                                   const Parameters &parameters,
                                   std::shared_ptr<FeatureRepository> featRepo)
    : raster(raster), params(parameters), featRepo(featRepo),
      polyToSeg(raster) {
}

void StatsComputation::setCallback(Callback aCallback) {
  callback = aCallback;
  featRepo->setCallback(callback);
}

void StatsComputation::start() {
  features = featRepo->readAll();

  // Compute stats for all readable bands;
  for (auto band : params.getStatBands()) {

    callback(0.0, {cb::INFO}, "Calculating statistics for band " +
                                  std::to_string(band) + " started");

    // Read whole band
    auto rasterBand       = raster.getBand(band);
    const auto width      = rasterBand.getWidth();
    const auto height     = rasterBand.getHeight();
    const auto rasterData = rasterBand.read(0, 0, width, height);

    // Compute different statistics
    if (isBandComputedFor(Stat::MEAN, band)) {
      computeMean(band, rasterData);
    }
    if (isBandComputedFor(Stat::STDDEV, band)) {
      computeStddev(band, rasterData);
    }

    callback(0.0, {cb::INFO}, "Calculating statistics for band " +
                                  std::to_string(band) + " finished");
  }

  if (params.getNdvi().isSet()) {
    computeNdvi();
  }

  if (params.getZabud().isSet()) {
    computeZabud();
  }

  // Save computed features
  saveAllFeatures();
}

bool StatsComputation::isBandComputedFor(const Stat type,
                                         const int band) const {
  std::set<int> bands;
  if (type == Stat::MEAN) {
    bands = params.getMeanBands();
  } else if (type == Stat::STDDEV) {
    bands = params.getStddevBands();
  }
  auto found = bands.find(band);
  if (found != bands.end()) {
    return true;
  } else
    return false;
}

void StatsComputation::saveAllFeatures() {
  featRepo->update(features);
}

void StatsComputation::computeMean(const int band,
                                   const handlers::RasterData &rasterData) {
  // Prepare to save
  callback(0.0, {cb::INFO},
           "Calculating mean value for segments in band " +
               std::to_string(band));

  // Estimate out segments number
  const size_t totalSegmentsCount = features.size();
  // Set up info of last callbacked segment
  double lastSent{0.00};
  size_t current = 0;

  // For all read features
  for (auto &feature : features) {

    // Process callback after every 1 percent
    const double currentProgress = static_cast<double>(current++) /
                                   static_cast<double>(totalSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::PROGRESS}, "");
    }

    const auto segment = polyToSeg(feature.getPolygon());
    const auto tl      = segment.getTopLeft();
    const auto lr      = segment.getLowRight();
    const auto rdata   = handlers::RasterData::extractPiece(
        rasterData, tl.x, tl.y, lr.x - tl.x, lr.y - tl.y);

    auto &stat = feature[raster.getName()];
    stat.setMean(band, rdata.getMean());
  }
}

void StatsComputation::computeStddev(const int band,
                                     const handlers::RasterData &rasterData) {
  // Prepare to save
  callback(0.0, {cb::INFO},
           "Calculating standard deviation value for segments in band " +
               std::to_string(band));

  // Estimate out segments number
  const size_t totalSegmentsCount = features.size();
  // Set up info of last callbacked segment
  double lastSent{0.00};
  size_t current = 0;

  // For all read features
  for (auto &feature : features) {

    // Process callback after every 1 percent
    const double currentProgress = static_cast<double>(current++) /
                                   static_cast<double>(totalSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::PROGRESS}, "");
    }

    const auto segment = polyToSeg(feature.getPolygon());
    const auto tl      = segment.getTopLeft();
    const auto lr      = segment.getLowRight();
    const auto rdata   = handlers::RasterData::extractPiece(
        rasterData, tl.x, tl.y, lr.x - tl.x, lr.y - tl.y);

    auto &stat = feature[raster.getName()];
    stat.setStddev(band, rdata.getStdDev());
  }
}

void StatsComputation::computeNdvi() {

  callback(0.0, {cb::INFO}, "Calculating NDVI");

  // Get Bands data
  const auto ndviBands = params.getNdvi();
  const auto visBand   = raster.getBand(ndviBands.vis).read();
  const auto nirBand   = raster.getBand(ndviBands.nir).read();

  const size_t totalSegmentsCount = features.size();
  size_t current                  = 0;
  double lastSent                 = 0.00;

  // For all features
  for (auto &feature : features) {
    const double currentProgress = static_cast<double>(current++) /
                                   static_cast<double>(totalSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::PROGRESS},
               "");
    }

    const auto segment = polyToSeg(feature.getPolygon());
    const auto visMean = getMean(visBand, segment);
    const auto nirMean = getMean(nirBand, segment);

    double ndviValue = (nirMean - visMean) / (nirMean + visMean);

    // If value there is division zero by zero set ndvi value out of range
    if (isnan(ndviValue))
      ndviValue = -2.0;

    auto &stat = feature[raster.getName()];

    handlers::image_objects::Ndvi ndvi;
    ndvi.vis   = ndviBands.vis;
    ndvi.nir   = ndviBands.nir;
    ndvi.value = ndviValue;

    stat.setNdvi(ndvi);
  }
}

void StatsComputation::computeZabud() {

  callback(0.0, {cb::INFO}, "Calculating Zabud");

  // Get Zabud Bands
  const auto zabudBands = params.getZabud();
  const auto blueBand   = raster.getBand(zabudBands.blue).read();
  const auto greenBand  = raster.getBand(zabudBands.green).read();
  const auto redBand    = raster.getBand(zabudBands.red).read();
  const auto nirBand    = raster.getBand(zabudBands.nir).read();

  const size_t totalSegmentsCount = features.size();
  size_t current                  = 0;
  double lastSent                 = 0.00;

  // For all features
  for (auto &feature : features) {
    const double currentProgress = static_cast<double>(current++) /
                                   static_cast<double>(totalSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::PROGRESS},
               "");
    }

    const auto segment = polyToSeg(feature.getPolygon());
    try {
      const auto blue = getMean(blueBand, segment);
      const auto green = getMean(greenBand, segment);
      const auto red = getMean(redBand, segment);
      const auto nir = getMean(nirBand, segment);

      double zabudValue = pow(
          pow(blue - green, 2) + pow(green - red, 2) + pow(red - nir, 2), 0.5);

      handlers::image_objects::Zabud zabud;
      zabud.blue  = zabudBands.blue;
      zabud.green = zabudBands.green;
      zabud.red   = zabudBands.red;
      zabud.nir   = zabudBands.nir;
      zabud.value = zabudValue;

      auto &stat = feature[raster.getName()];
      stat.setZabud(zabud);
    } catch (errors::IncorrectExtent &e) {
    }
  }
}

double StatsComputation::getMean(const handlers::RasterData& rasterData, const general::ImageSegment& segment) const {
    const auto tl      = segment.getTopLeft();
    const auto lr      = segment.getLowRight();
      
    const auto value = handlers::RasterData::extractPiece(
                            rasterData, tl.x, tl.y, lr.x - tl.x, lr.y - tl.y).getMean();
        
    return value;
}

} // namespace stats
} // namespace operations
} // namespace agroeye
