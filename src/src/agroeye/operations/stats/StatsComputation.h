#ifndef STATSCOMPUTATION_H
#define STATSCOMPUTATION_H

#include <algorithm>
#include "Callback.h"
#include "Raster.h"
#include "StatsParameters.h"
#include "Feature.h"
#include "RasterData.h"
#include "FeatureRepository.h"
#include "Segment.h"
#include <chrono>
#include <cmath>
#include <unordered_map>
#include <future>

namespace agroeye {
namespace operations {
namespace stats {

/**
 *  @brief Encapsulation of calculating statistics from raster into database
 */
class StatsComputation {
    using cb = agroeye::general::CallbackTypes;
    using Callback = agroeye::general::CallbackFunction;
    using Raster = agroeye::handlers::Raster;
    using FeatureRepository = agroeye::handlers::image_objects::FeatureRepository;
public:
    //StatsComputation() {}; /**< Dummy constructor */
    
    /**
     *  @brief Constructor of Computing Statistics
     * 
     *  @param raster The input raster based upon the statistics will be computed
     *  @param parametrs Parameters for computing statistics
     *  @param database Database where statistics will be inserted
     */
    StatsComputation(const Raster& raster,
                     const Parameters& parameters,
                     std::shared_ptr<FeatureRepository> featRepo);
    
    /**
     *  @brief Starts computing statistics
     */
    void start();
    
    /**
     *  @brief Sets callback function substituting the default
     * 
     *  @param callback The new callback function
     */
    void setCallback(Callback callback);

private:
    Raster raster;
    Parameters params;
    std::shared_ptr<FeatureRepository> featRepo;
    Callback callback {general::silenceCallback};
    bool isBandComputedFor(const Stat type, const int band) const;
    void computeMean(const int band, const handlers::RasterData &rasterData);
    void computeStddev(const int band, const handlers::RasterData &rasterData);
    void computeNdvi();
    void computeZabud();
    std::vector<handlers::image_objects::Feature> features;
    void saveAllFeatures();
    general::PolygonToSegment polyToSeg;
    int getRasterId() const;
    int rasterId {-1};
    
    double getMean(const handlers::RasterData& rasterData, const general::ImageSegment& segment) const;
};
    
} // namespace stats
} // namespace operations
} // namespace agroeye

#endif
