#include "StatsParameters.h"

namespace agroeye {
namespace operations {
namespace stats {

bool Ndvi::operator==(const Ndvi &other) const {
  return (nir == other.nir && vis == other.vis);
}

bool Ndvi::isSet() const {
  if (vis != -1 && nir != -1) {
    return true;
  } else {
    return false;
  }
}

bool Zabud::isSet() const {
  if (blue != -1 && green != -1 && red != 1 && nir != -1) {
    return true;
  } else {
    return false;
  }
}

std::string &operator+=(std::string &s, const Stat &t) {
  switch (t) {
  case Stat::MEAN:
    s += "mean";
    break;

  case Stat::STDDEV:
    s += "stddev";
    break;

  case Stat::NDVI:
    s += "ndvi";
    break;

  case Stat::ZABUD:
    s += "zabud";
    break;

  default:
    break;
  }
  return s;
}

void Parameters::setNdvi(const int vis, const int nir) {
  checkBands({nir, vis});
  ndvi.nir = nir;
  ndvi.vis = vis;
}

void Parameters::setZabud(const int blue, const int green, const int red, const int nir) {
   zabud.blue = blue;
   zabud.green = green;
   zabud.red = red;
   zabud.nir = nir;
}

void Parameters::addMeanBand(const int band) { meanBands.insert(band); }

std::set<int> Parameters::getMeanBands() const { return meanBands; }

void Parameters::addStddevBand(const int band) { stddevBands.insert(band); }

std::set<int> Parameters::getStddevBands() const { return stddevBands; }

std::set<int> Parameters::getStatBands() const {
  std::set<int> statBands;
  statBands.insert(meanBands.begin(), meanBands.end());
  statBands.insert(stddevBands.begin(), stddevBands.end());
  return statBands;
}

void Parameters::checkBands(const std::vector<int> &aBands) const {
  const auto result =
      std::find_if(aBands.begin(), aBands.end(), [](const int &band) {
        if (band < 1)
          return true;
        else
          return false;
      });
  if (result != aBands.end()){
    throw errors::IncorrectBand("");
  }
}

} // namespace stats
} // namespace operations
} // namespace agroeye
