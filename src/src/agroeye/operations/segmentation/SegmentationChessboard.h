#ifndef SEGMENTATIONCHESSBOARD_H
#define SEGMENTATIONCHESSBOARD_H

#include "Callback.h"
#include "DatabaseSqlite.h"
#include "FeatureRepository.h"
#include "Raster.h"
#include "Segment.h"
#include "Errors.h"
#include "SegmentationParameters.h"
#include <ogr_geometry.h>

namespace agroeye {
namespace operations {
namespace segmentation {

using cb = agroeye::general::CallbackTypes;
using agroeye::general::ImageSegment;
using agroeye::general::ImagePoint;

class Segmentation {
public:
  virtual ~Segmentation(){};
  void setCallback(general::CallbackFunction callback);
  virtual void start() = 0;

protected:
  Segmentation(
      const handlers::Raster &raster,
      const segmentation::Parameters &parameters,
      std::shared_ptr<handlers::image_objects::FeatureRepository> featureRepo);
  general::CallbackFunction callback{general::silenceCallback};
  std::shared_ptr<handlers::image_objects::FeatureRepository> featRepo{nullptr};
  general::SegmentToPolygon segToPoly;

  std::vector<handlers::image_objects::Feature> features;

  handlers::Raster raster;
  segmentation::Parameters parameters;

  void addSegment(const ImageSegment &segment);
  void saveAll();
};

/**
 *  @brief Implentation of Chessboard Segmentation
 */
class Chessboard : public Segmentation {
public:
  /**
   *  @brief Constructor of Chessboard Segmentation
   *
   *  @param raster Input raster to be segmented
   *  @param parameters Paremeters of segmentation
   *  @param featureRepo The output database
   */
  Chessboard(
      const handlers::Raster &raster,
      const segmentation::Parameters &parameters,
      std::shared_ptr<handlers::image_objects::FeatureRepository> featureRepo);

  /**
   *  @brief Starts the algorithm of segmentation
   */
  virtual void start() override;

  virtual ~Chessboard(){};

protected:
  size_t approximateSegmentsCount() const;
  ImageSegment getInitialSegment();
  ImageSegment getNextSegment(const ImageSegment &segment);

private:
  void checkParameters();
};

class Quadtree : public Chessboard {
public:
  virtual ~Quadtree(){};
  Quadtree(
      const handlers::Raster &raster,
      const segmentation::Parameters &parameters,
      std::shared_ptr<handlers::image_objects::FeatureRepository> featureRepo);

  virtual void start() override;

private:
  using RasterDataGroup = agroeye::handlers::RasterDataGroup;
  void checkParameters();
  void performQuadtree(const ImageSegment &segment);
  bool doSplitInto4(const ImageSegment &segment);
  std::vector<ImageSegment> splitInto4(const ImageSegment &segment);
};

} // namespace segmentation
} // namespace operations
} // namespace agroeye

#endif
