#include "SegmentationChessboard.h"

namespace agroeye {
namespace operations {
namespace segmentation {

Segmentation::Segmentation(
    const handlers::Raster &raster, const segmentation::Parameters &parameters,
    std::shared_ptr<handlers::image_objects::FeatureRepository> featureRepo)
    : raster(raster), parameters(parameters), featRepo(featureRepo),
      segToPoly(raster) {
}

void Segmentation::setCallback(general::CallbackFunction callback) {
  this->callback = callback;
  featRepo->setCallback(callback);
}

void Segmentation::saveAll() {
  featRepo->create(features);
}

void Segmentation::addSegment(const ImageSegment &segment) {
   int64_t id {0};
   if (features.size() != 0){
     id = features.back().getId() + 1;
   }
    
  handlers::image_objects::Feature feat{id, segToPoly(segment)};
  features.push_back(std::move(feat));
}

Chessboard::Chessboard(
    const handlers::Raster &aRaster,
    const segmentation::Parameters &aParameters,
    std::shared_ptr<handlers::image_objects::FeatureRepository> featureRepo)
    : Segmentation(aRaster, aParameters, featureRepo) {
  // check input parameters
  checkParameters();
}

void Chessboard::start() {
  // Prepare to save
  callback(0.0, {cb::INFO, cb::PROGRESS}, "Chessboard segmentation started");

  // Estimate out segments number
  const size_t totalSegmentsCount = approximateSegmentsCount();
  // Set up info of last callbacked segment
  double lastSent{0.00};

  // Get first segment
  ImageSegment segment = getInitialSegment();
  features.reserve(totalSegmentsCount);
  
  // Loop until empty segment is returned
  while (segment.getTopLeft() != segment.getLowRight()) {
    // Process callback after every 1 percent
    const double currentProgress = static_cast<double>(segment.getId()) /
                                   static_cast<double>(totalSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::INFO, cb::PROGRESS},
               "Chessboard segmentation in progress");
    }
    addSegment(segment);
    segment = getNextSegment(segment);
  }

  callback(1.0, {cb::INFO, cb::PROGRESS}, "Chessboard segmentation finished");

  saveAll();
}

void Chessboard::checkParameters() {
  if (parameters.getSquareSide() > 256) {
      throw errors::IncorrectSquareSize("Square side of %d is above maximum 256", parameters.getSquareSide());
  }
}

size_t Chessboard::approximateSegmentsCount() const {
  const size_t rows = static_cast<size_t>(std::ceil(
      static_cast<double>(raster.getWidth()) / parameters.getSquareSide()));
  const size_t cols = static_cast<size_t>(std::ceil(
      static_cast<double>(raster.getHeight()) / parameters.getSquareSide()));
  return (rows * cols);
}

ImageSegment Chessboard::getInitialSegment() {
  return ImageSegment{
      0,      // Id first element
      {0, 0}, // Coordinates of top left corner
      {parameters.getSquareSide(),
       parameters.getSquareSide()} // Coordinates of boottom right corner
  };
}

ImageSegment Chessboard::getNextSegment(const ImageSegment &segment) {
  // Get prevoius origin of segment
  ImagePoint imPoint = segment.getTopLeft();

  // Move to right as square side value
  imPoint.x += parameters.getSquareSide();

  // Default values of width and height
  size_t width  = parameters.getSquareSide();
  size_t height = parameters.getSquareSide();

  // Check if next element is still in the row
  // Set width zero if it outside of raster
  if (imPoint.x >= raster.getWidth())
    width = 0;
  // Change width to be appropriate to remaining pixels in row
  else if (imPoint.x + width >= raster.getWidth())
    width = raster.getWidth() - imPoint.x;

  // If width is zero then go to next row
  if (width == 0) {
    imPoint.x = 0;
    width     = parameters.getSquareSide();
    imPoint.y += parameters.getSquareSide();
  }

  // Set height zero if y is beyond raster
  if (imPoint.y >= raster.getHeight())
    height = 0;
  // or change height to slot into height
  else if (imPoint.y + height >= raster.getHeight())
    height = raster.getHeight() - imPoint.y;

  // If there is any height read data from it and return
  if (height != 0)
    return ImageSegment{
        segment.getId() + 1, imPoint, {imPoint.x + width, imPoint.y + height}};

  return ImageSegment();
}

Quadtree::Quadtree(
    const handlers::Raster &aRaster,
    const segmentation::Parameters &aParameters,
    std::shared_ptr<handlers::image_objects::FeatureRepository> featureRepo)
    : Chessboard(aRaster, aParameters, featureRepo) {
  // check input parameters
  checkParameters();
}

void Quadtree::checkParameters() {
}

void Quadtree::start() {
  // Prepare to save

  callback(0.0, {cb::INFO, cb::PROGRESS}, "Quadtree segmentation started");

  // Estimate out segments number
  const size_t bigSegmentsCount = approximateSegmentsCount();
  features.reserve(bigSegmentsCount);

  // Set up info of last callbacked segment
  double lastSent{0.00};
  // Set up big segments callback information
  size_t lastBigProcessed = 0;

  // Get first segment
  ImageSegment segment = getInitialSegment();

  // Loop until empty segment is returned
  while (segment.getTopLeft() != segment.getLowRight()) {
    // Process callback after every 1 percent
    const double currentProgress = static_cast<double>(lastBigProcessed++) /
                                   static_cast<double>(bigSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::INFO, cb::PROGRESS},
               "Quadtree segmentation in progress");
    }
    performQuadtree(segment);
    segment    = getNextSegment(segment);
  }

  callback(1.0, {cb::INFO, cb::PROGRESS}, "Quadtree segmentation finished");

  saveAll();
}

void Quadtree::performQuadtree(const ImageSegment &segment) {

  if (doSplitInto4(segment)) {
      const auto quadrants = splitInto4(segment);
      for (auto& q : quadrants) {
          performQuadtree(q);
      }
  } else {
    addSegment(segment);
  }
}

bool Quadtree::doSplitInto4(const ImageSegment &segment) {
  const auto tl = segment.getTopLeft();
  const auto lr = segment.getLowRight();  
  
  const auto minX = tl.x;
  const auto minY = tl.y;
  const auto maxX = lr.x;
  const auto maxY = lr.y;
  const auto width = maxX - minX;
  const auto height = maxY - minY;
  
  if (width <= 3 || height <= 3){
      return false;
  }
  
  auto rasterData = raster.read(segment, parameters.getBands());
  double sumStddev{0.0};
  for (auto band : rasterData.getBandsList()) {
    sumStddev += rasterData.getBandData(band).getStdDev();
  }
  if (sumStddev > parameters.getThreshold()) { 
    return true;
  } else {
    return false;
  }
}

std::vector<ImageSegment> Quadtree::splitInto4(const ImageSegment &segment) {
  std::vector<ImageSegment> quadrants;
  quadrants.reserve(4);
 
  const auto minX = segment.getTopLeft().x;
  const auto minY = segment.getTopLeft().y;
  const auto maxX = segment.getLowRight().x;
  const auto maxY = segment.getLowRight().y;
  const auto midX = (maxX + minX) / 2;
  const auto midY = (maxY + minY) / 2;
  
  ImageSegment i1 {ImagePoint{minX, minY}, ImagePoint{midX, midY}};
  ImageSegment i2 {ImagePoint{midX, minY}, ImagePoint{maxX, midY}};
  ImageSegment i3 {ImagePoint{midX, midY}, ImagePoint{maxX, maxY}};
  ImageSegment i4 {ImagePoint{minX, midY}, ImagePoint{midX, maxY}};
  
  quadrants.push_back(std::move(i1));
  quadrants.push_back(std::move(i2));
  quadrants.push_back(std::move(i3));
  quadrants.push_back(std::move(i4));
  
  return quadrants;
}

} // namespace segmentation
} // namespace operations
} // namespace agroeye
