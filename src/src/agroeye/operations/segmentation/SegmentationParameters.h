#ifndef SEGMENTATIONPARAMETERS_H
#define SEGMENTATIONPARAMETERS_H

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <iostream>

namespace agroeye {
namespace operations {
        
/**
 *  @brief Type of statistics that can be calculated
 */    
enum class StatisticsTypes { 
    MEAN,   /**< Mean value */
    STDDEV  /**< Standard deviation value */
};

using BandsList = std::vector<size_t>;  /**< List with bands numbers */
using StatisticsList = std::vector<StatisticsTypes>;    /**< List with types of statistics */
using BandsToStatsMap = std::unordered_map<size_t, StatisticsList>; /**< Corelation between band and statistics */
using StatsToBandsMap = std::unordered_map<StatisticsTypes, BandsList>; /**< Corelation betwneen type of statistic and bands */
  
  
/**
 *  @brief Class encapsulating different segmentation parameters
 */
class Parameters {
public:
    Parameters() {};
    
    /**
     *  @brief All given band will take part in segmentation and calucation statistics
     *
     *  @param bands Bands values 
     */
    Parameters(BandsList bands);
    Parameters(const Parameters& other) = default;
    Parameters& operator=(const Parameters& other) = default;
    Parameters(Parameters&& other) = default;
    Parameters& operator=(Parameters&& other) = default;
    virtual ~Parameters() {};
    
    /**
     *  @brief Sets square side in Chessboard segmentation
     *  @param  side    Square side
     */
    void setSquareSide(size_t side);
    size_t getSquareSide() const;   /**< Returns the side of square */
    
    /**
     *  @brief Sets threshold value for quadtree segmentation
     *  @param  value Threshold value
     */
    void setThreshold(double value) { threshold = value; };
    double getThreshold() const { return threshold; }; //*< Obtains stored threshold value */
    
    BandsList getReadBands() const;         /**< Get all bands that have to be read */
    BandsList getSegmentationBands() const; /**< Get all bands pariticapting in segmentation */    
    BandsList getStatisticsBands() const;   /**< Get all bands that require counting statistics */
    
    StatisticsList getStatsTypes() const;   /**< Get all type of statistic that have to be computed */
    
    /**
     *  @brief Get statisics that have to be computed for given band
     *  @param band Give band
     */
    StatisticsList getStatsToBand(size_t band) const;
    
    /**
     *  @brief  Get all bands for which given type of statistic is computed
     *  @param  type Given statistic type
     */
    BandsList getBandsToStat(StatisticsTypes type) const;

    /**
     *  @brief Convert statistic enum to string value
     *  @param  type Type of statistic
     *  @return Corresponding text name
     */
    static std::string statisticEnumToName(const StatisticsTypes type);
    
private:
    // Bands participating in creation of statistics database
    BandsToStatsMap bandsToStats;
    
    // Statistic type with bands pariticpating
    StatsToBandsMap statsToBands;
        
    // Bands participating in segmentation
    BandsList segmentationBands;
    
    // Bands participating in read from raster
    BandsList readBands;
    
    StatisticsList statsTypes;
    // Square Side used in chessboard segmentation
    size_t squareSide {static_cast<size_t>(-1)};
    
    // Threshold in Quadtree segmentation
    double threshold {-1.0};   
};

namespace segmentation {

class Parameters {
public:
    Parameters(const size_t squareSide = 256);
    Parameters(const std::vector<int>& bands, const double threshold);
    size_t getSquareSide() const { return squareSide; };
    std::vector<int> getBands() const {return bands;};
    double getThreshold() const { return threshold; };
    virtual ~Parameters() {};
private:
    size_t squareSide {256};
    std::vector<int> bands;
    double threshold {0};
};
    
} // namespace segmentation   
} // namespace operations
} // namespace agroye


#endif
