#include "SegmentationParameters.h"

namespace agroeye {
namespace operations {    
    
std::string Parameters::statisticEnumToName(const StatisticsTypes type) {
    switch (type) {
        case StatisticsTypes::MEAN:     return "mean";
        case StatisticsTypes::STDDEV:   return "stddev";
        
        default:                        return "unknown";
    }
}
    
Parameters::Parameters(BandsList bands) {
    // Sort given bands
    std::sort(bands.begin(), bands.end());
    
    // Assign them
    segmentationBands = bands;
    readBands = bands;
    
    // Create maps
    for (auto band : bands){        
        bandsToStats.insert(std::pair<size_t, StatisticsList>(band, {StatisticsTypes::MEAN, StatisticsTypes::STDDEV}));
    };
    
    statsToBands.insert(std::pair<StatisticsTypes, BandsList>(StatisticsTypes::MEAN, bands));
    statsToBands.insert(std::pair<StatisticsTypes, BandsList>(StatisticsTypes::STDDEV, bands));
    
    statsTypes = {StatisticsTypes::MEAN, StatisticsTypes::STDDEV};
}

void Parameters::setSquareSide(size_t side) {
    squareSide = side;
}

size_t Parameters::getSquareSide() const {
    return squareSide;
}

BandsList Parameters::getReadBands() const {        
    return readBands;
}


BandsList Parameters::getSegmentationBands() const {    
    return segmentationBands;    
}

StatisticsList Parameters::getStatsTypes() const {
    return statsTypes;    
}
  
StatisticsList Parameters::getStatsToBand(const size_t bandNo) const {
    // Find statistics associated to the band
    auto result = bandsToStats.find(bandNo);
    
    // If there is none return empty list
    if (result == bandsToStats.end())
        return StatisticsList();
    
    return result->second;
}

BandsList Parameters::getBandsToStat(StatisticsTypes type) const {
    auto result = statsToBands.find(type);
    if (result == statsToBands.end())
        return BandsList();
    
    return result->second;
}

namespace segmentation {

Parameters::Parameters(const size_t aSquareSide) : squareSide(aSquareSide) {
}

Parameters::Parameters(const std::vector<int>& bands, const double threshold) : 
bands(bands), threshold(threshold){
  std::sort(this->bands.begin(), this->bands.end());
  std::unique(this->bands.begin(), this->bands.end());
  this->bands.shrink_to_fit();
}
    
    
    
} // namespace segmentation
} // namespace operations
} // namespace agroeye
