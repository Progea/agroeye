#include "RasterAnalysis.h"

namespace agroeye {
namespace operations {
namespace raster_analysis {

BlockStatistics::BlockStatistics(handlers::Raster &inRaster,
                                 const std::string &outPath,
                                 const raster_analysis::Parameters parameters)
    : inRaster(inRaster), outPath(outPath), params(parameters),
      freeCores(std::max(1u, std::thread::hardware_concurrency())) {

  checkParameters();
}

void BlockStatistics::checkParameters() {
  const auto matrix = params.getNeighbourhood();
  if (matrix.rows() < 1 || matrix.cols() < 1) {
    throw errors::IncorrectNeighbourhood("Neighbourhood of pixel must be at "
                                         "least 1 pixel width and 1 pixel "
                                         "height instead of %dx%d",
                                         matrix.cols(), matrix.rows());
  }
}

void BlockStatistics::setCallback(general::CallbackFunction cb) {
  callback = cb;
}

void BlockStatistics::createNewEmptyRaster() {

  callback(0.0, {cb::INFO}, "Creating new empty, one band raster");
  outRaster =
      handlers::Raster::create(outPath, inRaster, 1, GDALDataType::GDT_Float64);
  callback(0.0, {cb::INFO}, "Raster dataset created");
}

void BlockStatistics::start() {

  // Create empty one band dataset with place for result of analysis
  createNewEmptyRaster();

  // Read all data from band
  callback(0.0, {cb::INFO}, "Reading data from input band");
  auto &&inData = inRaster.getBand(params.getBand()).read();

  // Create out Data container
  callback(0.0, {cb::INFO}, "Creating containter for output data");
  handlers::RasterData outData{inData.getWidth(), inData.getHeight(),
                               inData.getXPos(), inData.getYPos()};

  // Create function that runs asynchronisly                               
  auto filterAsync = [this, &inData, &outData] (int first, int last) -> void {filter(inData, outData, first, last);};
  
  // Create storage for async tasks
  std::vector<std::future<void>> asyncFunctions;
  asyncFunctions.reserve(freeCores);
  
  // Run processes asynchronisly
  callback(0.0, {cb::INFO}, errors::string_sprintf("Starting %d tasks asynchronously", freeCores));
  for (auto i = 0; i < freeCores; ++i){
    const int first = inData.getWidth() / freeCores * i;
    const int last = inData.getWidth() / freeCores * (i+1);
    asyncFunctions.push_back(std::async(std::launch::async, filterAsync, first, last));
  }
  
  // Get async results
  for (auto && result : asyncFunctions){
     result.get(); 
  }
 
  // Save into out raster
  callback(0.0, {cb::INFO}, "Saving filtered data to disk");
  outRaster.getBand(1).save(outData);
  
  callback(0.0, {cb::INFO}, "Filtering finished");
}

void BlockStatistics::filter(const handlers::RasterData &inData,
                             handlers::RasterData &outData, int firstRow, int lastRow) {

  // Get results of all pixels from input data
  for (auto x = 0; x < outData.getWidth(); ++x) {
    for (auto y = firstRow; y < lastRow; ++y) {
        try  {
        outData(x, y) = getValue(x, y, inData);
        } catch (errors::PixelOutsideRasterData &e) {
        }
    }
  }
}

double BlockStatistics::getValue(const size_t x, const size_t y,
                                 const handlers::RasterData &inData) {
  
  // Default output value
  double value    = 0.0;
  
  // value from the origin
  const double xy = inData(x, y);

  // Get size of kernel
  auto kernel = params.getNeighbourhood();

  try {
    for (auto i = x - kernel.rows() / 2; i <= x + kernel.rows() / 2; ++i) {
      for (auto j = y - kernel.cols() / 2; j <= y + kernel.cols() / 2; ++j) {
        value += std::abs(xy - inData(i, j));
      }
    }

  } catch (errors::PixelOutsideRasterData &e) {
  }

  return value;
}

} // namespace raster_analysis
} // namespace operations
} // namespace agroeye
