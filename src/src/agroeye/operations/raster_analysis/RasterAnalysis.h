#ifndef RASTERANALYSIS_H
#define RASTERANALYSIS_H

#include "AnalysisParameters.h"
#include "Callback.h"
#include "Errors.h"
#include "Raster.h"
#include "Raster.h"
#include <thread>
#include <future>
#include <string>

namespace agroeye {
namespace operations {
namespace raster_analysis {

class BlockStatistics {
public:
  BlockStatistics(handlers::Raster& inRaster, const std::string& outPath, const raster_analysis::Parameters parameters);
  
  void setCallback(general::CallbackFunction callback);
  void start();
  
private:
  handlers::Raster inRaster;
  handlers::Raster outRaster;
  std::string outPath;
  raster_analysis::Parameters params;
  general::CallbackFunction callback {general::silenceCallback};
  int freeCores;

  void checkParameters();
  void createNewEmptyRaster();
  void filter(const handlers::RasterData& in, handlers::RasterData& out, int firstRow, int lastRow); 
  double getValue(const size_t x, const size_t y, const handlers::RasterData& inData);
  
  using cb = agroeye::general::CallbackTypes;
};

} // namespace raster_analysis
} // namespace operations
} // namespace agroeye

#endif
