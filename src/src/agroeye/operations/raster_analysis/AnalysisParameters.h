#ifndef ANALYSISPARAMETERS_H
#define ANALYSISPARAMETERS_H

#include <Eigen/Dense>

namespace agroeye {
namespace operations {
namespace raster_analysis {
    
enum class Analysis_type {
  DIFFERENCE_TO_NEIGHBOUR 
};
    
class Parameters {
public:
    Parameters() {};
    Parameters(const Analysis_type type, const int band, const int rows = 3, const int cols = 3);
    Parameters(const Analysis_type type, const int band, const Eigen::MatrixXi &matrix);
    
    Eigen::MatrixXi getNeighbourhood() const;
    Analysis_type getType() const;
    int getBand() const;
    
private:
    int band;
    Analysis_type type;
    Eigen::MatrixXi matrix;
};
    
} // namespace raster_analysis
} // namespace operations
} // namespace agroeye
    
#endif
