#include "AnalysisParameters.h"

namespace agroeye {
namespace operations {
namespace raster_analysis {
    
    
Parameters::Parameters(const Analysis_type type, const int band, const Eigen::MatrixXi &matrix) : 
 type(type), matrix(matrix) {
    
}

Parameters::Parameters(const Analysis_type type, const int band, const int rows, const int cols) : band(band), type(type) {
   matrix = Eigen::MatrixXi (rows, cols);
   matrix.setOnes();
}

Eigen::MatrixXi Parameters::getNeighbourhood() const {
   return matrix; 
}
    
Analysis_type Parameters::getType() const {
   return type; 
}

int Parameters::getBand() const {
   return band; 
}
    
} // namespace raster_analysis
} // namespace operations
} // namespace agroeye
