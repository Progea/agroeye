#include "Segment.h"

namespace agroeye {
namespace general {

ImageSegment::ImageSegment(size_t aId, ImagePoint aTopLeft,
                           ImagePoint aLowRight)
    : id(aId), topLeftCorner(aTopLeft), lowRightCorner(aLowRight) {}
    
ImageSegment::ImageSegment(ImagePoint topLeftCorner, ImagePoint bottomRightCorner) : topLeftCorner(topLeftCorner), lowRightCorner(bottomRightCorner) {
    
}

SegmentToPolygon::SegmentToPolygon(const handlers::RasterInterface &raster) {
  geoTransform = raster.getGeoTransform();
}

OGRPolygon SegmentToPolygon::operator()(const ImageSegment &segment) const {
  OGRPolygon polygon;
  OGRLinearRing ring;
  const auto tl = segment.getTopLeft();
  const auto lr = segment.getLowRight();

  // Add four top corners
  ring.addPoint(getXSpace(tl.x), getYSpace(tl.y));
  ring.addPoint(getXSpace(lr.x), getYSpace(tl.y));
  ring.addPoint(getXSpace(lr.x),
                getYSpace(lr.y));
  ring.addPoint(getXSpace(tl.x), getYSpace(lr.y));

  ring.closeRings();
  polygon.addRing(&ring);

  return polygon;
}

double SegmentToPolygon::getXSpace(const double xImage) const {
  return geoTransform[0] + xImage * geoTransform[1];
}

double SegmentToPolygon::getYSpace(const double yImage) const {
  return geoTransform[3] + yImage * geoTransform[5];
}

PolygonToSegment::PolygonToSegment(const handlers::RasterInterface &raster) {
  geoTransform = raster.getGeoTransform();
}

ImageSegment PolygonToSegment::operator()(const OGRPolygon &polygon) const {
  OGREnvelope envelope;
  polygon.getEnvelope(&envelope);

  envelope.MinX -= geoTransform[0];
  envelope.MinY -= geoTransform[3];

  envelope.MaxX -= geoTransform[0];
  envelope.MaxY -= geoTransform[3];

  envelope.MinX /= geoTransform[1];
  envelope.MinY /= geoTransform[5];

  envelope.MaxX /= geoTransform[1];
  envelope.MaxY /= geoTransform[5];

  ImageSegment is;
  ImagePoint tl {static_cast<size_t>(std::round(envelope.MinX)),
                 static_cast<size_t>(std::round(envelope.MaxY))};
  is.setTopLeft(tl);
  
  ImagePoint lr {static_cast<size_t>(std::round(envelope.MaxX - 1)),
                 static_cast<size_t>(std::round(envelope.MinY - 1))};
  is.setLowRight(lr);

  return is;
}

} // namespace general
} // namespace agroeye
