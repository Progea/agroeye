#include "Callback.h"

namespace agroeye {
namespace general {
    
void defaultCallback(double progress, ListCallbackTypes listTypes, std::string message) {
    std::cout << message << ' ';
    
    auto result = std::find(listTypes.begin(), listTypes.end(), CallbackTypes::PROGRESS);
    if (result != listTypes.end())
        std::cout << "Progress: " << static_cast<unsigned int>(progress*100) << "%";
    
    std::cout << '\n';
    std::cout << std::flush;
}

void silenceCallback(double progress, ListCallbackTypes listTypes, std::string message) {
    return;
}

} // namespace general
} // namespace agroeye
    
