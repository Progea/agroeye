#ifndef CALLBACK_H
#define CALLBACK_H

#include <functional>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>

namespace agroeye {
namespace general {
   
/**
 *	@brief Type of callback that can be called function
 *
 */ 
enum class CallbackTypes {
    PROGRESS,	/**< contains inforamtion about progress of process */
    INFO,		/**< contains message */
    DEBUG,		/**< debugging information */
    ERROR		/**< process encountered an error */
};

using ListCallbackTypes = std::vector<CallbackTypes>; /**<	List containg types of callback types when function is called */
using CallbackFunction = std::function<void(double, ListCallbackTypes, std::string)>; /**<	Typedef for callback function */

/**
 *	@brief A default callback
 *	
 *	When this function is called the list of callback types is checked. 
 *		- If it has INFO type the message is printed in the stdcout.
 *		- If it has PROGRESS type the messege is printed with associated progress value in percentage
 *		- In all other cases there is no printout
 */
void defaultCallback(double progress, ListCallbackTypes listTypes, std::string message);


/**
 *	@brief Silencing callback
 *
 *	All information passed to this callback are silenced (not displayed or processed in any way)
 */
void silenceCallback(double progress, ListCallbackTypes listTypes, std::string message);





} // namespace general
} // namespace agroeye
    
#endif
