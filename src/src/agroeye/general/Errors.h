#ifndef ERRORS_H
#define ERRORS_H

#include <stdexcept>
#include <string>
#include <cstdio>
#include <memory>

namespace agroeye {
namespace errors { 
    
template<typename First, typename... Args>
//inline std::string string_sprintf(const char* format, Args... args) {
std::string string_sprintf(First format, Args... args) {
    auto length = std::snprintf(nullptr, 0, format, args...);
    
    std::unique_ptr<char[]> buf {new char[length+1]};
    std::snprintf(buf.get(), length + 1, format, args...);
    
    return std::string(buf.get());
}
     
/**--------------------------------------------**/
/**        RASTER                              **/
/**--------------------------------------------**/

class IncorrectRaster : public std::runtime_error {
public:
    IncorrectRaster(const std::string& msg) : std::runtime_error(msg) {};
};
    
/**--------------------------------------------**/
/**        BAND                                **/
/**--------------------------------------------**/

class IncorrectBand : public std::runtime_error {
public:
    IncorrectBand() : std::runtime_error("Given band does not exist") {};
    template<typename... Args>
    IncorrectBand(Args... args) : std::runtime_error(string_sprintf(args...)) {}
};

class IncorrectExtent : public std::runtime_error {
public:
    template<typename... Args>
    IncorrectExtent(Args... args) : std::runtime_error(string_sprintf(args...)) {}
};

class ReadWriteError : public std::runtime_error {
public:
    template<typename... Args>
    ReadWriteError(Args... args) : std::runtime_error(string_sprintf(args...)) {};
};
    
/**--------------------------------------------**/
/**            RASTER DATA                     **/
/**--------------------------------------------**/

class RasterDataTooBig : public std::runtime_error {
public:
    template<typename... Args>
    RasterDataTooBig(Args... args) : std::runtime_error(string_sprintf(args...)) {}
};

class PixelOutsideRasterData : public std::runtime_error {
public:
    template<typename... Args>
    PixelOutsideRasterData(Args... args) : std::runtime_error(string_sprintf(args...)) {}
};
        
/**--------------------------------------------**/
/**            RASTER DATA GROUP               **/
/**--------------------------------------------**/

class RasterDataDifferentKind : public std::runtime_error {
public:
    template<typename... Args>
    RasterDataDifferentKind(Args... args) : std::runtime_error(string_sprintf(args...)) {}
};

/**--------------------------------------------**/
/**        SQLITE DATABASE                     **/
/**--------------------------------------------**/

class IncorrectDatabase : public std::runtime_error {
public:
  template<typename... Args>
  IncorrectDatabase(Args... args) : std::runtime_error(string_sprintf(args...)) {}
};

class IncorrectLayer : public std::runtime_error {
public:
    template<typename... Args>
    IncorrectLayer(Args... args) : std::runtime_error(string_sprintf(args...)) {};
};

class CreateFeatureError : public std::runtime_error {
public:
    template<typename... Args>
    CreateFeatureError(Args... args) : std::runtime_error(string_sprintf(args...)) {};
};

class FeatureDoesNotExist : public std::runtime_error {
public:
    template <typename... Args>   
    FeatureDoesNotExist(Args... args) : std::runtime_error(string_sprintf(args...)) {};
};

/**--------------------------------------------**/
/**                FEATURE                     **/
/**--------------------------------------------**/
class AddGeometryError : public std::runtime_error {
public:
    template<typename... Args>
    AddGeometryError(Args... args) : std::runtime_error(string_sprintf(args...)) {};
};

/**--------------------------------------------**/
/**           SEGMENTATION                     **/
/**--------------------------------------------**/
class IncorrectSquareSize : public std::runtime_error {
public:
    template<typename... Args>
    IncorrectSquareSize(Args... args) : std::runtime_error(string_sprintf(args...)){}
};


/**--------------------------------------------**/
/**        RASTER ANALYSIS                     **/
/**--------------------------------------------**/

class IncorrectNeighbourhood : public std::runtime_error {
public:
    template<typename... Args>
    IncorrectNeighbourhood(Args... args) : std::runtime_error(string_sprintf(args...)) {};
};
    
} // namespace errors
} // namespace agroeye



#endif
