#ifndef SEGMENT_H
#define SEGMENT_H

#include <cstddef>
#include <cstdint>
#include <cmath>
#include <ogr_geometry.h>
#include <memory>
#include "RasterInterface.h"

namespace agroeye {
namespace general {
    
/**
 *  @brief Struct representing pair of coordinates in raster
 */
struct ImagePoint {    
    ImagePoint() {};    /**< Dummy constructor */
    /**
     *  @brief Creates object from pair of cooridnates
     * 
     *  @param aX   The X coordinate
     *  @param aY   The Y coordinate
     */
    ImagePoint(size_t aX, size_t aY) : x(aX), y(aY) {};
    size_t x {0};   /**< X coordinate */
    size_t y {0};   /**< Y coordinate */
    
    /**
     *  @brief Checks if two points are the same
     *  
     *  @param other The other point (right hand side)
     *  @return If true two points are the same, if false they are not the same
     */
    bool operator==(const ImagePoint& other) const {
        return ((other.x == x) && (other.y == y));
    }
    
    /**
     *  @brief  Checks if two points are different
     * 
     *  @param other The other image point (right hand side)
     *  @return If True are different, if false then they are the same
     */
    bool operator!=(const ImagePoint& other) const {
        return !operator==(other);
    }
};

/**
 *  @brief Encapsulation of Image Segment with its properties
 */
class ImageSegment {
public:
    ImageSegment() {}; /**< Dummy constructor */
    
    /**
     *  @brief  Creates image segment from given coordinates
     *  
     *  @param id The id of new segment
     *  @param topLeftCorner    Position of top left corner of image segment
     *  @param bottomRightCorner    Position of bottom right corner of image segment
     */
    ImageSegment(size_t id, ImagePoint topLeftCorner, ImagePoint bottomRightCorner);
    
    ImageSegment(ImagePoint topLeftCorner, ImagePoint bottomRightCorner);
    
    /**
     *  @brief Gets the id of image segment
     * 
     *  @return The id number 
     */
    size_t getId() const {return id;};
    void setId(const size_t aId) { id = aId;};
    
    /**
     *  @brief Gets the point of top left corner of image segment
     * 
     *  @return The corner coordinates
     */
    ImagePoint getTopLeft() const {return topLeftCorner; };
    
    /**
     *  @brief Gets the point of bottom right corner of image segment
     * 
     *  @return The corner coordinates
     */
    ImagePoint getLowRight() const {return lowRightCorner; };
    
    void setTopLeft(const ImagePoint& point) { topLeftCorner = point; };
    void setLowRight(const ImagePoint& point) { lowRightCorner = point; };
    
private:
    size_t id {0};    
    ImagePoint topLeftCorner {0, 0};
    ImagePoint lowRightCorner {0, 0};    
};    

class SegmentToPolygon {
public:
   SegmentToPolygon(const handlers::RasterInterface &raster);    
   OGRPolygon operator()(const ImageSegment &segment) const;
private:
   std::unique_ptr<double[]> geoTransform {nullptr};
   double getXSpace(const double xImage) const;
   double getYSpace(const double yImage) const; 
};

class PolygonToSegment {
public:
  PolygonToSegment(const handlers::RasterInterface &raster);
  ImageSegment operator()(const OGRPolygon &polygon) const;
private:
  std::unique_ptr<double[]> geoTransform {nullptr};
};
    
} // namespace general
} // namespace agroeye

#endif
