#ifndef FEATURE_H
#define FEATURE_H

#include "StatsParameters.h"
#include "Errors.h"
#include "Segment.h"
#include <cstdint>
#include <iostream>
#include <ogr_geometry.h>
#include <unordered_map>
#include <vector>
#include <functional>

namespace agroeye {
namespace handlers {
namespace image_objects {
    
struct Ndvi {
  int vis {-1};
  int nir {-1};
  double value {-2.0};
};

struct Zabud {
    int blue {-1};
    int green {-1};
    int red {-1};
    int nir {-1};
    double value {-1};
};

class StatsSource {
  using Stat = agroeye::operations::stats::Stat;
public:
  StatsSource(const StatsSource&) = delete;
  StatsSource(StatsSource&&) = default;
  StatsSource& operator=(const StatsSource&) = delete;
  StatsSource& operator=(StatsSource&&) = default;
  virtual ~StatsSource() = default;
  StatsSource(const std::string& name);
  
  void set(const Stat type, const int band, const double value);
  
  double getMean(const int band) const;
  void setMean(const int band, const double value);
  std::vector<int> getMeanKeys() const;
  
  double getStddev(const int band) const;
  void setStddev(const int band, const double value);
  std::vector<int> getStddevKeys() const;
  
  Ndvi getNdvi() const;
  void setNdvi(const Ndvi& value);
  bool isNdviSet() const;
  
  Zabud getZabud() const;
  void setZabud(const Zabud& value);
  bool isZabudSet() const;

private:
  std::string name;
  std::unordered_map<int, double> meanValue;
  std::unordered_map<int, double> stddevValue;
  Ndvi ndviValue;
  Zabud zabudValue;
};

class Feature {
public:
  Feature();
  Feature(const int64_t id, const OGRGeometry *geometry);
  Feature(const int64_t id, const OGRPolygon &polygon);
  Feature(const general::ImageSegment &segment);
  Feature(const Feature&) = delete;
  Feature(Feature&&) = default;
  Feature& operator=(const Feature&) = delete;
  Feature& operator=(Feature&&) = default;
  virtual ~Feature() = default;

  OGRPolygon getPolygon() const;
  int64_t getId() const;
  
  StatsSource &operator[](const std::string &rasterName);
  const std::unordered_map<std::string, StatsSource>& getAllStats() const;
  
private:
  int64_t id{-1};     /**< Id of the polygon */
  OGRPolygon polygon; /**< Polygon geometry of the feature */

  //! Maps with different statistics
  std::unordered_map<std::string, StatsSource> statsSource;
};
}
}
}

#endif
