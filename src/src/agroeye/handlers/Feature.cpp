#include "Feature.h"

namespace agroeye {
namespace handlers {
namespace image_objects {

StatsSource::StatsSource(const std::string &rasterName) : name(rasterName) {
}

void StatsSource::set(const Stat type, const int band, const double value) {
  if (type == Stat::MEAN) {
    setMean(band, value);
  } else if (type == Stat::STDDEV) {
    setStddev(band, value);
  }
}

/**
 *  @brief Access the mean value from raster statistic
 *  @param band The band number
 *  @return The value of statistic
 */
double StatsSource::getMean(const int band) const {
  auto it = meanValue.find(band);
  if (it == meanValue.end()) {
    throw errors::IncorrectBand();
  }
  return it->second;
}

/**
 * @brief Sets the mean value for band
 * @param band Band of the mean value
 * @param value The value of mean statistic
 */
void StatsSource::setMean(const int band, const double value) {
  meanValue[band] = value;
}

/**
 * @brief Get bands with calculated mean stats
 * @return Vector of bands
 */
std::vector<int> StatsSource::getMeanKeys() const {
  std::vector<int> keys;
  keys.reserve(meanValue.size());
  for (auto kv : meanValue) {
    keys.push_back(kv.first);
  }

  return keys;
}

/**
 *  @brief Access the stddev value from raster statistic
 *  @param band The band number
 *  @return The value of statistic
 */
double StatsSource::getStddev(const int band) const {
  auto it = stddevValue.find(band);
  if (it == stddevValue.end()) {
    throw errors::IncorrectBand();
  }
  return it->second;
}

/**
 * @brief Sets the mean value for band
 * @param band Band of the stddev value
 * @param value The value of mean statistic
 */
void StatsSource::setStddev(const int band, const double value) {
  stddevValue[band] = value;
}

/**
 * @brief Get bands with calculated stddev stats
 * @return Vector of bands
 */
std::vector<int> StatsSource::getStddevKeys() const {
  std::vector<int> keys;
  keys.reserve(stddevValue.size());
  for (auto kv : stddevValue) {
    keys.push_back(kv.first);
  }

  return keys;
}

/**
 * @brief Sets the ndvi value of raster
 * @param value The ndvi value.
 */
void StatsSource::setNdvi(const Ndvi &value) {
  ndviValue = value;
}

bool StatsSource::isNdviSet() const {
  if (ndviValue.value >= -1 && ndviValue.value <= 1)
    return true;
  else
    return false;
}

/**
 * @brief Accessses the Ndvi value of the statistic
 * @return Value
 */
Ndvi StatsSource::getNdvi() const {
  return ndviValue;
}

Zabud StatsSource::getZabud() const {
  return zabudValue;
}

void StatsSource::setZabud(const Zabud &value) {
  zabudValue = value;
}

bool StatsSource::isZabudSet() const {
  if (zabudValue.value >= 0)
    return true;
  else
    return false;
}

/**
 * Dummy constructor
 */
Feature::Feature() {
}

/**
 * @brief Constructor with Id and polygon geometry
 * @param aId The new Id
 * @param geoemtry The pointer to castable geometry
 */
Feature::Feature(const int64_t aId, const OGRGeometry *geometry) : id(aId) {
  if (geometry != nullptr) {
    polygon = *dynamic_cast<const OGRPolygon *>(geometry);
  }
}

/**
 *  @brief Constructor of new feature
 *  @param aId The Id of new feature
 *  @param aPolygon The polygon creating new feature
 */
Feature::Feature(const int64_t aId, const OGRPolygon &aPolygon)
    : id(aId), polygon(aPolygon) {
}

Feature::Feature(const general::ImageSegment &segment) {
}

/**
 * @brief Get Id of the feature
 * @return Id of the feature
 */
int64_t Feature::getId() const {
  return id;
}

/**
 *  @brief Accessing the feature polygon
 *  @return The feature's polygon
 */
OGRPolygon Feature::getPolygon() const {
  return polygon;
}

/**
 * @brief Access the source of statistic
 * @param rasterId Id of raster in rasters table in database
 * @return The statistics
 */
StatsSource &Feature::operator[](const std::string &rasterName) {
  const auto it = statsSource.find(rasterName);
  if (it == statsSource.end()) {
    statsSource.emplace(rasterName, rasterName);
  }
  return statsSource.at(rasterName);
}

const std::unordered_map<std::string, StatsSource> &
Feature::getAllStats() const {
  return statsSource;
}

} // namespace image_objects
} // namespace handlers
} // namespace agroeye
