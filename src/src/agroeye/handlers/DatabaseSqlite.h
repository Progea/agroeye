#ifndef DATABASESQLITE_H
#define DATABASESQLITE_H

#include "FeatureRepository.h"
#include "Errors.h"
#include "Raster.h"
#include <cpl_conv.h>
#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <ogrsf_frmts.h>
#include <string>
#include <vector>

#include <chrono>

namespace agroeye {
namespace handlers {

class FeatureRepositorySqlite;

/**
 * @brief Class encapsulating connection to SQLite database
 * Developed basing on GDAL OGR Driver
 */
class Sqlite {
public:
  Sqlite(); /**< Opens in memory database */
  /**
   *  @brief Opens database from specified path
   *
   *  @param path Path of Spatialite database
   *  @param update Option whether to update database or not
   */
  Sqlite(const std::string &path, const bool update = false);
  
  void prepareForSegmentation(const Raster &raster);
  
  std::string getPath() const {return path;};
  
  std::shared_ptr<FeatureRepositorySqlite> openFeatureRepository();

private:
  void initializeTables();
  GDALDriver *getDriver() const;
  void openSqliteDataset(const std::string &path, const bool update);
  void createSqliteDataset(const std::string &path);
  void executeSql(const std::string& sql);
  std::string proj4toSrid(std::string proj4);

  std::string path;
  std::shared_ptr<GDALDataset> db{nullptr,
                                  [](GDALDataset *ds) { GDALClose(ds); }};
};

class FeatureUniq {
public:
  FeatureUniq(OGRFeature* feat) : feature(feat) {};
  FeatureUniq(OGRFeatureDefn* defn);
  FeatureUniq(const FeatureUniq&) = delete;
  FeatureUniq(FeatureUniq&&) = default;
  FeatureUniq& operator=(FeatureUniq&) = delete;
  FeatureUniq& operator=(FeatureUniq&&) = default;
  ~FeatureUniq() {if (feature != nullptr) OGRFeature::DestroyFeature(feature);};  
  OGRFeature* const operator->() const {return feature;};
  OGRFeature* const get() const {return feature;};
private:
  OGRFeature* feature {nullptr};
};

class Layer {
  using Stat = agroeye::operations::stats::Stat;
public:
  Layer();
  Layer(std::shared_ptr<GDALDataset> db, const std::string &sqlQuery);
  Layer(OGRLayer *layer);
  ~Layer();
  FeatureUniq getNextFeature();
  FeatureUniq getFeature(const image_objects::Feature &feature);
  void filter(const std::string& field, const std::string& relation, const int64_t value);
  void filter(const std::string& field, const std::string& relation, const std::string& value);
  void filter(const std::string& field, const std::string& relation, const int64_t value, const std::string& field2, const std::string& relation2, const int64_t value2);
  void resetFilter();
  void remove(const int64_t id);
  bool isEmpty() const;
  OGRLayer* operator->();
  class iterator {
  public:
    iterator(){};
    iterator(OGRLayer *layer);
    ~iterator();
    void operator++();
    bool operator!=(const iterator &other) const;
    OGRFeature *operator->() const {
      return feature;
    };
    OGRFeature& operator*() {
      return *feature;
    }

  private:
    OGRFeature *feature{nullptr};
    OGRLayer *owningLayer{nullptr};
  };
  iterator begin();
  iterator end();

private:
  OGRLayer *layer{nullptr};
  std::shared_ptr<GDALDataset> db{nullptr};
  bool isGeometric() const;
};

class FeatureRepositorySqlite : public image_objects::FeatureRepository {
  using Feature = agroeye::handlers::image_objects::Feature;
public:
  FeatureRepositorySqlite() {};
  FeatureRepositorySqlite(std::shared_ptr<GDALDataset> db);
  void create(const std::vector<image_objects::Feature>& feature);
  image_objects::Feature read(const int64_t id) const;
  std::vector<Feature> readAll() const;
  void update(const std::vector<Feature>& features);
  void remove(const int64_t id);
  void setCallback(general::CallbackFunction callback);
  virtual ~FeatureRepositorySqlite() {};

private:
  std::shared_ptr<GDALDataset> db{nullptr};
  OGRLayer *primitivesLayer{nullptr};

  using Stat = operations::stats::Stat;
  using cb = agroeye::general::CallbackTypes;
  void doesExist(const FeatureUniq&) const;
  Layer getLayer(const std::string &name) const;
  int getLayerId(const std::string &rasterName, const Stat type, const int band = 0);
  int getLayerId(const std::string &rasterName, const image_objects::Ndvi& ndvi);
  int getLayerId(const std::string &rasterName, const image_objects::Zabud& zabud);
  int getRasterId(const std::string &rasterName);
  
  void populateWithStats(image_objects::Feature &feature) const;
  void populateWith(image_objects::Feature &feature, Layer &layer, const Stat type) const;
  void populateMean(image_objects::Feature &feature) const;
  void populateStdDev(image_objects::Feature &feature) const;
  void populateNdvi(image_objects::Feature &feature) const;
  void populateZabud(image_objects::Feature &feature) const;

  void updateStats(const image_objects::Feature &feature);
  void updateTable(Layer& layer, const int64_t id, const int layerId, const double value);
  void updateMean(const image_objects::Feature &feature);
  void updateStdDev(const image_objects::Feature &feature);
  void updateNdvi(const image_objects::Feature &feature);
  void updateZabud(const image_objects::Feature &feature);
  
  void createSegmentInDb(const image_objects::Feature &feature);
  
  void initializeTables();
  void initStat(const Stat type);
  void executeSql(const std::string& sql);
  FeatureUniq getEmptyFeature(Layer& layer) const;
  int createNewLayer(const int rasterId);
  general::CallbackFunction callback {general::silenceCallback};
};

} // namespace handlers
} //  namespace agroeye

#endif
