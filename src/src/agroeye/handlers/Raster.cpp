#include "Raster.h"

namespace agroeye {
namespace handlers {

Raster::Raster(const Raster &other)
    : gdalDataset(other.gdalDataset), callbackFunc(other.callbackFunc),
      datasetPath(other.datasetPath), bandsList(other.bandsList) {
}

Raster::Raster(Raster &&other) {
  gdalDataset        = std::move(other.gdalDataset);
  callbackFunc       = other.callbackFunc;
  other.callbackFunc = silenceCallback;

  datasetPath = std::move(other.datasetPath);
  bandsList   = std::move(other.bandsList);
}

Raster &Raster::operator=(const Raster &other) {
  gdalDataset  = other.gdalDataset;
  callbackFunc = other.callbackFunc;
  datasetPath  = other.datasetPath;
  bandsList    = other.bandsList;
  return *this;
}

Raster &Raster::operator=(Raster &&other) {
  gdalDataset        = std::move(other.gdalDataset);
  callbackFunc       = other.callbackFunc;
  other.callbackFunc = silenceCallback;

  datasetPath = std::move(other.datasetPath);
  bandsList   = std::move(other.bandsList);
  return *this;
}

Raster::~Raster() {
}

Raster::Raster(const char *path)
    : Raster(path, agroeye::general::silenceCallback) {
}

Raster::Raster(const std::string& path) : Raster(path.c_str()) {};

Raster::Raster(const char *path, CallbackFunction func, const bool readOnly) {
  callbackFunc = func;

  // Register all drivers
  callbackFunc(0, {agroeye::general::CallbackTypes::INFO},
               "Registering drivers");
  GDALAllRegister();

  // Open dataset, by default only to read
  openDataset(path, readOnly);

  // Open bands and populate bandsList
  openBands();
}

Raster Raster::create(std::string &outPath, const Raster &other,
                      const int bandsNo, const GDALDataType datatype) {
  // Get driver
  GDALDriver *poDriver = other.gdalDataset->GetDriver();

  // Pointer to dataset
  GDALDataset *poDataset{nullptr};

  // Create empty copy
  poDataset = poDriver->Create(outPath.c_str(), other.gdalDataset->GetRasterXSize(),
                               other.gdalDataset->GetRasterYSize(), bandsNo,
                               datatype, nullptr);
  GDALClose(poDataset);

  // Open it
  Raster newRaster{outPath.c_str(), other.callbackFunc, false};

  // Copy attributes
  newRaster.copyAttributesFrom(other);

  // Return
  return newRaster;
}

void Raster::openDataset(const char *path, bool readOnly) {
  callbackFunc(0, {agroeye::general::CallbackTypes::INFO}, "Opening dataset");

  auto accessType = GDALAccess::GA_ReadOnly;
  if (readOnly == false)
    accessType = GDALAccess::GA_Update;

  void *rawDataset = GDALOpen(path, accessType);
  if (rawDataset == nullptr)
    throw errors::IncorrectRaster("Raster cannot be opened!");

  datasetPath = path;
  gdalDataset.reset(static_cast<GDALDataset *>(rawDataset),
                    [](GDALDataset *ptr) { GDALClose(ptr); });
}

RasterDataGroup Raster::read(size_t x, size_t y, size_t width, size_t height) {
  RasterDataGroup group;
  for (auto &it : bandsList) {
    group.addData(it.first, it.second.read(x, y, width, height));
  }
  return group;
}

RasterDataGroup Raster::read(const ImageSegment &segment,
                             const std::vector<int> &bands) {
   RasterDataGroup group;
   for (auto band : bands){
      group.addData(band, getBand(band).read(segment));
   }
   return group;
}

size_t Raster::getBandsCount() const {
  return static_cast<size_t>(gdalDataset->GetRasterCount());
}

size_t Raster::getWidth() const {
  const int val = gdalDataset->GetRasterXSize();
  return static_cast<size_t>(val);
}

size_t Raster::getHeight() const {
  const int val = gdalDataset->GetRasterYSize();
  return static_cast<size_t>(val);
}

RasterBand Raster::getBand(const size_t bandNo) {
  auto result = bandsList.find(bandNo);
  if (result == bandsList.end())
    throw errors::IncorrectBand();
  return result->second;
}

std::string Raster::getName() const {
  std::string path = getPath();
  std::regex fileWithExtension{"([\\w-]+\\..*)$"};
  std::smatch match;
  std::string result;
  if (std::regex_search(path, match, fileWithExtension) && match.size() > 1) {
    result = match.str(1);
  } else {
    result = path;
  }
  return result;
}

std::string Raster::getProj4() const {
  const char *constProj = gdalDataset->GetProjectionRef();

  std::unique_ptr<char[]> dst{new char[strlen(constProj) + 1]};
  char *c_dst = dst.get();
  strcpy(c_dst, constProj);

  OGRSpatialReference spatial;
  spatial.importFromWkt(&c_dst);
  char *proj4;
  spatial.exportToProj4(&proj4);
  std::string projection = proj4;

  free(proj4);

  return projection;
}

void Raster::setCallback(agroeye::general::CallbackFunction callback) {
    callbackFunc = callback;
}

std::string Raster::getProjectionRef() const {
  return gdalDataset->GetProjectionRef();
}

std::unique_ptr<double[]> Raster::getGeoTransform() const {
  std::unique_ptr<double[]> geoTransform{new double[6]};
  gdalDataset->GetGeoTransform(geoTransform.get());
  return geoTransform;
};

void Raster::openBands() {
  callbackFunc(0, {agroeye::general::CallbackTypes::INFO},
               "Populating raster bands");

  const size_t bandsCount = gdalDataset->GetRasterCount();
  for (size_t i = 1; i <= bandsCount; ++i) {
    GDALRasterBand *rawBand = gdalDataset->GetRasterBand(i);
    bandsList.insert(std::pair<size_t, RasterBand>(i, {rawBand, callbackFunc}));
  };
}

void Raster::setGeoTransform(std::unique_ptr<double[]> geotransform) {
  gdalDataset->SetGeoTransform(geotransform.get());
}

void Raster::setProjection(const std::string &projection) {
  gdalDataset->SetProjection(projection.c_str());
}

void Raster::copyAttributesFrom(const Raster &other) {
  setGeoTransform(other.getGeoTransform());
  setProjection(other.getProjectionRef().c_str());
}

} // namspace handlers
} // namespace agroeye
