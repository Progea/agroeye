#ifndef RASTER_H
#define RASTER_H

#include <memory>
#include <regex>
#include <string>
#include <unordered_map>

#include <gdal_priv.h>
#include <ogr_spatialref.h>

#include "Callback.h"
#include "Errors.h"
#include "RasterBand.h"
#include "RasterData.h"
#include "RasterDataGroup.h"
#include "RasterInterface.h"
#include "Segment.h"

namespace agroeye {
namespace handlers {

using agroeye::general::CallbackTypes;
using agroeye::general::CallbackFunction;
using agroeye::general::silenceCallback;
using agroeye::general::ImageSegment;

/**
 *  @brief Class encapsulating Raster file object
 *
 *	When new instances of this class are created using copy
 *	operators they are still sharing pointer to the same object.
 *	Not thread safe (yet).
 */
class Raster : public RasterInterface {
public:
  Raster() {
  }                                       /**< Dummy constructor */
  Raster(const Raster &other);            /**< Copy constructor */
  Raster(Raster &&other);                 /**< Move constructor */
  Raster &operator=(const Raster &other); /**< Copy assignment */
  Raster &operator=(Raster &&other);      /**< Move assignment */
  virtual ~Raster(); /**< Destructor deallocating resources */

  /**
   *	@brief Opens file
   *
   *	Opens file with given filepath. The silence callback is used to silence
   *all feedback.
   *	@param	path	Path to the file
   */
  Raster(const char *path);
  
  Raster(const std::string& path);
  
  /**
   *	@brief Opens file
   *
   *	Opens file with given filepath. The given function is used to feedback.
   *	@param	path	Path to the file
   *	@param	func	Callback function
   *      @param  readOnly Open raster in read only mode, by default yes
   */
  Raster(const char *path, agroeye::general::CallbackFunction func,
         const bool readOnly = true);

  /**
   *	@brief Copies the raster
   *
   *	The given raster is used to create to exact copy
   *	@param	outPath	Output location
   *	@param	other	Raster to be copy
   *      @param  bandsNo Number of output bands
   *      @param  datatype Data type of new raster
   *	@return			Newly created raster
   */
  static Raster create(std::string &outPath, const Raster &other,
                       const int bandsNo, const GDALDataType datatype);

  size_t
  getBandsCount() const;    /**< Number of bands that raster is consited of */
  size_t getWidth() const;  /**< Width of the raster */
  size_t getHeight() const; /**< Height of the raster */

  /**
   *  @brief Reads pixels data from all bands inside raster
   *
   *  @param  x   Column number of top left corner
   *  @param  y   Row number of top left corner
   *  @param  width   Width of reading fragment
   *  @param  height  Height of reading fragment
   */
  RasterDataGroup read(size_t x, size_t y, size_t width, size_t height);

  RasterDataGroup read(const ImageSegment &segment,
                       const std::vector<int> &bands);

  void setCallback(agroeye::general::CallbackFunction callback);
  /**
  *	@brief Gets the band of raster
  *
  *	@param 	bandNo	Number of band
  *	@return			Raster Band object
  */
  RasterBand getBand(const size_t bandNo);
  std::string getName() const; /**< Name of raster */
  std::string getPath() const {
    return datasetPath;
  }; /**< Path to the location of raster */
  std::string getProj4()
      const; /**< Gets the geografic definition of raster in Proj4 format */
  std::string getProjectionRef() const;
  std::unique_ptr<double[]> getGeoTransform()
      const; /**< Gets the parameters of spatial location of raster */

private:
  void openDataset(const char *path, bool readOnly = true);
  void openBands();
  void setGeoTransform(std::unique_ptr<double[]> geotransform);
  void setProjection(const std::string &projection);
  void copyAttributesFrom(const Raster &other);
  std::shared_ptr<GDALDataset> gdalDataset;

  agroeye::general::CallbackFunction callbackFunc;
  std::string datasetPath;
  std::unordered_map<size_t, RasterBand> bandsList;

  Raster(const Raster &other, const char *path, bool empty, int bandsNo = 0);
};

} // namspace handlers
} // namespace agroeye

#endif
