#include "RasterDataGroup.h"

namespace agroeye {
namespace handlers {
 
void RasterDataGroup::addData(const int bandNo, RasterData&& rasterData) {
    throwIfBandExists(bandNo);
    assignMetadataIfFirst(rasterData);
    checkAttributes(rasterData);
    
    bandsData.insert({bandNo, rasterData});
}    
    
std::vector<int> RasterDataGroup::getBandsList() const {
    std::vector<int> bands;
    for (const auto& entry : bandsData)
        bands.push_back(entry.first);
    return bands;
}

const RasterData& RasterDataGroup::getBandData(const int bandNo) const {
    throwIfBandDoesNotExist(bandNo);
    return bandsData.find(bandNo)->second;
}

void RasterDataGroup::assignMetadataIfFirst(const RasterData& rasterData) {
    if (bandsData.size() == 0) {
        xPos = rasterData.getXPos();
        yPos = rasterData.getYPos();
        width = rasterData.getWidth();
        height = rasterData.getHeight();
    }
}

void RasterDataGroup::checkAttributes(const RasterData& rasterData) const {
    if (rasterData.getXPos() != getXPos() ||
        rasterData.getYPos() != getYPos() ||
        rasterData.getWidth() != getWidth() ||
        rasterData.getHeight() != getHeight() )        
            throw errors::RasterDataDifferentKind("");    
}
    
void RasterDataGroup::throwIfBandExists(const int bandNo) const {    
    auto result = bandsData.find(bandNo);
    if (result != bandsData.end()){
        throw errors::IncorrectBand("Band %d already exists in RasterDataGroup", bandNo);
    }
}

void RasterDataGroup::throwIfBandDoesNotExist(const int bandNo) const {
    auto result = bandsData.find(bandNo);
    if (result == bandsData.end()) {
       throw errors::IncorrectBand("Band %d does not exist int bands group", bandNo); 
    }
}

RasterDataGroup RasterDataGroup::extractPiece(const RasterDataGroup& rasterData,
                                             const size_t x, const size_t y,
                                             const size_t width, const size_t height) {
    RasterDataGroup newGroup;
    for (const auto& it : rasterData.bandsData)
        newGroup.addData(it.first, RasterData::extractPiece(it.second, x, y, width, height));    
    
    return newGroup;
}

} // namespace handlers
} // namespace agroeye
