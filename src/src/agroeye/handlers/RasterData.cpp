#include "RasterData.h"

namespace agroeye {
namespace handlers {

RasterData::RasterData(const size_t aWidth, const size_t aHeight,
                       const size_t x, const size_t y) {
  checkAndAssign(x, y, aWidth, aHeight);
  pixelData.resize(width * height);
  std::fill(pixelData.begin(), pixelData.end(), 0);
}

RasterData::RasterData(const RasterData &other) {
  xPos      = other.xPos;
  yPos      = other.yPos;
  width     = other.width;
  height    = other.height;
  pixelData = other.pixelData;
}

RasterData::RasterData(RasterData &&other) {
  xPos      = other.xPos;
  yPos      = other.yPos;
  width     = other.width;
  height    = other.height;
  pixelData = std::move(other.pixelData);

  other.xPos   = 0;
  other.yPos   = 0;
  other.width  = 0;
  other.height = 0;
}

RasterData &RasterData::operator=(const RasterData &other) {
  xPos      = other.xPos;
  yPos      = other.yPos;
  width     = other.width;
  height    = other.height;
  pixelData = other.pixelData;

  return *this;
}

RasterData &RasterData::operator=(RasterData &&other) {
  xPos      = other.xPos;
  yPos      = other.yPos;
  width     = other.width;
  height    = other.height;
  pixelData = std::move(other.pixelData);

  other.xPos   = 0;
  other.yPos   = 0;
  other.width  = 0;
  other.height = 0;

  return *this;
}

RasterData::~RasterData() {
}

void RasterData::checkAndAssign(const size_t x, const size_t y,
                                const size_t aWidth, const size_t aHeight) {

  xPos = x;
  yPos = y;

  if (aWidth > 50000 || aHeight > 50000) {
    throw errors::RasterDataTooBig(
        "You want to create object with incorrect dimension width=%d height=%d",
        aWidth, aHeight);
  }
  width  = aWidth;
  height = aHeight;
}

bool RasterData::checkInsidePosition(const size_t x, const size_t y) const {

  if (x < width && y < height) {
    return true;
  } else {
    throw errors::PixelOutsideRasterData("You try to access the pixel at X=%d "
                                         "Y=%d when dimensions are width=%d "
                                         "height=%d",
                                         x, y, width, height);
  }
};

size_t RasterData::getIndex(const size_t x, const size_t y) const {
  return y * getWidth() + x;
}

double &RasterData::operator()(size_t x, size_t y) {
  checkInsidePosition(x, y);
  return pixelData[getIndex(x, y)];
}

const double &RasterData::operator()(size_t x, size_t y) const {
  checkInsidePosition(x, y);
  return pixelData[getIndex(x, y)];
}

Eigen::MatrixXd RasterData::getBlock(size_t x, size_t y, size_t width,
                                     size_t height) const {
  Eigen::MatrixXd block(width, height);
  const size_t xOffset = (width - 1) / 2;
  const size_t yOffset = (height - 1) / 2;
  const size_t x_min   = x - xOffset;
  const size_t y_min   = y - yOffset;
  const size_t x_max   = x + xOffset;
  const size_t y_max   = y + yOffset;

  checkInsidePosition(x_min, y_min);
  checkInsidePosition(x_max, y_max);

  for (size_t j = y_min; j <= y_max; ++j) {
    for (size_t i = x_min; i <= x_max; ++i) {
      block(i - x_min, j - y_min) = operator()(i, j);
    }
  }

  return block;
}

double RasterData::getMean() const {
  return agroeye::algorithm::math::mean(pixelData.begin(), pixelData.end());
}

double RasterData::getStdDev() const {
  return agroeye::algorithm::math::stddev(pixelData.begin(), pixelData.end());
}

double RasterData::getMean(const double noDataValue) const {
  return agroeye::algorithm::math::mean(pixelData.begin(), pixelData.end(),
                                        noDataValue);
}

double RasterData::getStdDev(const double noDataValue) const {
  return agroeye::algorithm::math::stddev(pixelData.begin(), pixelData.end(),
                                          noDataValue);
}

double RasterData::calculate(agroeye::operations::StatisticsTypes type) const {
  if (type == agroeye::operations::StatisticsTypes::MEAN)
    return getMean();
  else if (type == agroeye::operations::StatisticsTypes::STDDEV)
    return getStdDev();

  return 0;
}

RasterData RasterData::extractPiece(const RasterData &rasterData, size_t x,
                                    size_t y, size_t width, size_t height) {
  RasterData newData{width, height, rasterData.xPos + x, rasterData.yPos + y};
  for (size_t j = 0; j < height; ++j) {
    for (size_t i = 0; i < width; ++i) {
      newData(i, j) = rasterData(x + i, y + j);
    }
  }
  return newData;
}

} // namespace handlers
} // namespace agroeye
