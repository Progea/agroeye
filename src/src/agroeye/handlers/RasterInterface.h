#ifndef RASTERINTERFACE_H
#define RASTERINTERFACE_H 

#include <memory>

namespace agroeye {
namespace handlers {

class RasterInterface {
public:
  virtual std::unique_ptr<double[]> getGeoTransform() const = 0;

};
    
}    
}

#endif
