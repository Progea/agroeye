#ifndef RASTERBAND_H
#define RASTERBAND_H

#include <memory>

#include <gdal_priv.h>

#include "Errors.h"
#include "RasterData.h"
#include "Callback.h"
#include "Segment.h"

namespace agroeye {
namespace handlers {
    
using agroeye::general::CallbackTypes;
using agroeye::general::CallbackFunction;
using agroeye::general::silenceCallback;

class RasterBand {
public:
	/**
	 *	@brief Opens raster band.
	 *	The opening of raster is only created by Raster
	 */
    RasterBand(GDALRasterBand* rawBand, CallbackFunction callbackFunc);
	
	/**
	 *	@brief	Copy constructor	 	
	 *	@param	other	The other Band to be copied
	 */
    RasterBand(const RasterBand& other);
	
	/**
	 *	@brief	Move constructor	 
	 *	@param	other	The other Band to be moved
	 */
    RasterBand(RasterBand&& other);
	
	/**
	 *	@brief	Copy assignment operator	 
	 *	@param	other	The other Band to be copied
	 */
    RasterBand& operator=(const RasterBand& other);
	
	/**
	 *	@brief	Move assignment operator	 
	 *	@param	other	The other Band to be moved
	 */
    RasterBand& operator=(RasterBand&& other);
	
	/**
	 *	@brief Does NOT free any resources
	 *
	 *	Cleaning up after raster is preformed after deletion of Raster object
	 */
    virtual ~RasterBand() {};
    
	/**
	 *	@brief	Reads the given extent of raster band
	 *
	 *	@param	x		Number of column (X) of top left corner
	 *	@param	y		Number of row (Y) of top left corner
	 *	@param	width	Number of columns to be read
	 *	@param 	height	Number of rows to be read
	 */
    RasterData read(size_t x, size_t y, size_t width, size_t height);
    
    RasterData read(const general::ImageSegment&);
    
    RasterData read();
    
    /**
     *  @brief Saves raster data into band
     *  
     *  @param rasterData Data to save
     */
    void save(RasterData& rasterData);
    int getWidth() const;	/**<	Gets width of the band */
    int getHeight() const;	/**<	Gets height of the band */
    double getNoDataValue() const;	/**<	Gets value of no data pixels */
        
private:
    void checkPosition(size_t x, size_t y) const;
    void checkExtent(size_t x, size_t y, size_t width, size_t height) const;
    void inOutRaster(RasterData& rasterData, GDALRWFlag inOutType);    
    
    // DO NOT delete this pointer
    // RasterBand is NOT owned by thist pointer
    GDALRasterBand* ptrBand {nullptr};     
     
    agroeye::general::CallbackFunction callbackFunc; 
};

} // namespace handlers
} // namespace agroeye

#endif 
