#include "DatabaseSqlite.h"

namespace agroeye {
namespace handlers {

Sqlite::Sqlite() : path(":memory:") {
  GDALAllRegister();
  createSqliteDataset(path);
}

Sqlite::Sqlite(const std::string &aPath, const bool update) : path(aPath) {
  GDALAllRegister();
  // If it was told to update database that not exists it means that
  // a database must be created
  {
    std::ifstream p{path.c_str()};
    if (update && !p.good()) {
      createSqliteDataset(path.c_str());
    }
  }

  // Open database in appropriate mode
  openSqliteDataset(path.c_str(), update);
}

void Sqlite::initializeTables() {
  // rasters
  const std::string rasters{
      "CREATE TABLE IF NOT EXISTS rasters\n"
      "  (\n"
      "     raster_id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
      "     name      TEXT\n"
      "  );"};

  // layers
  const std::string layers{
      "CREATE TABLE IF NOT EXISTS layers\n"
      "  (\n"
      "     layer_id  INTEGER PRIMARY KEY AUTOINCREMENT,\n"
      "     raster_id INTEGER,\n"
      "     FOREIGN KEY(raster_id) REFERENCES rasters(raster_id)\n"
      "  );"};

  // bands
  const std::string bands{
      "CREATE TABLE IF NOT EXISTS bands\n"
      "  (\n"
      "     layer_id INTEGER,\n"
      "     band_no  INTEGER,\n"
      "     FOREIGN KEY(layer_id) REFERENCES layers(layer_id)\n"
      "  );"};

  const std::string layer_view{
      "CREATE VIEW IF NOT EXISTS layer_view\n"
      "AS\n"
      "   SELECT rasters.name,\n"
      "          layers.raster_id,\n"
      "          layers.layer_id,\n"
      "          bands.band_no\n"
      "   FROM   bands\n"
      "          LEFT JOIN layers\n"
      "                 ON bands.layer_id = layers.layer_id\n"
      "          LEFT JOIN rasters\n"
      "                 ON rasters.raster_id = layers.raster_id;"};

  executeSql(rasters);
  executeSql(layers);
  executeSql(bands);
  executeSql(layer_view);
}

GDALDriver *Sqlite::getDriver() const {
  GDALAllRegister();
  const std::string driverName{"SQLite"};
  GDALDriver *driver =
      GetGDALDriverManager()->GetDriverByName(driverName.c_str());

  return driver;
}

void Sqlite::createSqliteDataset(const std::string &path) {
  // Get Driver
  auto driver = getDriver();

  // Setup opening options
  CPLStringList options;
  options.AddNameValue("SPATIALITE", "YES");

  CPLSetConfigOption("OGR_SQLITE_SYNCHRONOUS", "ON");
  CPLSetConfigOption("OGR_SQLITE_JOURNAL", "MEMORY");
  CPLSetConfigOption("OGR_SQLITE_CACHE", "1024");
  CPLSetConfigOption("OGR_SQLITE_PRAGMA",
                     "count_changes=OFF,temp_store=MEMORY");

  // Open dataset
  GDALDataset *dataset =
      driver->Create(path.c_str(), 0, 0, 0, GDT_Unknown, options.List());

  // Assign opened dataset to the shared ptr
  db.reset(dataset);

  initializeTables();
}

void Sqlite::executeSql(const std::string &sql) {
  if (db.get() != nullptr) {
    db->ExecuteSQL(sql.c_str(), nullptr, "SQLite");
  }
}

std::string Sqlite::proj4toSrid(std::string proj4) {
  proj4.pop_back();

  // Fix solving problem
  // https://trac.osgeo.org/proj/ticket/11
  // GDAL doesn't add into proj4 definition of Polish SRS
  // towgs84 parameter which is present in db
  auto position = proj4.find("ellps=GRS80");
  if (position != std::string::npos)
    proj4.insert(position + 12, "\%");

  // Find SRID definition from Proj4
  std::string sridStmt = "SELECT srid FROM spatial_ref_sys WHERE proj4text ";
  sridStmt += "LIKE \"\%" + proj4 + "\%\";";

  Layer results{db, sridStmt};

  auto sridRow = results.getNextFeature();

  return sridRow->GetFieldAsString("srid");
}

void Sqlite::openSqliteDataset(const std::string &path, const bool update) {
  // Allow only SQLite driver to open file
  CPLStringList allowedDrivers;
  allowedDrivers.AddString("SQLite");

  // Set proper opening flags
  auto openingFlag = GDAL_OF_VECTOR;
  if (update == true) {
    openingFlag |= GDAL_OF_UPDATE;
  }

  // Open dataset
  void *dataset = GDALOpenEx(path.c_str(), openingFlag, allowedDrivers.List(),
                             nullptr, nullptr);

  // Check if it isn't empty
  if (dataset == nullptr) {
    throw errors::IncorrectDatabase("Datbase at path %s cannot be opened",
                                    path);
  }

  // Substitue pointer if it was in create mode
  db.reset(static_cast<GDALDataset *>(dataset));
}

void Sqlite::prepareForSegmentation(const Raster &raster) {
  // segmentation_primitives
  const std::string segmentation_primitives{
      "CREATE TABLE IF NOT EXISTS segmentation_primitives\n"
      "  (\n"
      "     id INTEGER NOT NULL PRIMARY KEY\n"
      "  );"};
  executeSql(segmentation_primitives);

  // geometry column
  std::string the_geom{
      "SELECT AddGeometryColumn('segmentation_primitives', 'the_geom', "};
  the_geom += proj4toSrid(raster.getProj4());
  the_geom += ", 'POLYGON', 2);";
  executeSql(the_geom);

  // index column
  std::string indexStmt{"SELECT CreateSpatialIndex('segmentation_primitives', "
                        "                           'the_geom');"};
  executeSql(indexStmt);

  openSqliteDataset(path.c_str(), true);
}

std::shared_ptr<FeatureRepositorySqlite> Sqlite::openFeatureRepository() {
  return std::make_shared<FeatureRepositorySqlite>(db);
}

Layer::Layer() {
}

Layer::Layer(std::shared_ptr<GDALDataset> aDb, const std::string &sqlQuery)
    : db(aDb) {
  layer = db->ExecuteSQL(sqlQuery.c_str(), nullptr, nullptr);
}

Layer::Layer(OGRLayer *aLayer) : layer(aLayer) {
}

void Layer::filter(const std::string &field, const std::string &relation,
                   const int64_t value) {
  std::string filterExpression;
  filterExpression.reserve(256);
  filterExpression += field + " ";
  filterExpression += relation + " ";
  filterExpression += std::to_string(value) + ';';
  layer->SetAttributeFilter(filterExpression.c_str());
}

void Layer::filter(const std::string &field, const std::string &relation,
                   const std::string &value) {
  std::string filterExpression;
  filterExpression.reserve(256);
  filterExpression += field + " ";
  filterExpression += relation + " ";
  filterExpression += "\"" + value + "\";";
  layer->SetAttributeFilter(filterExpression.c_str());
}

void Layer::filter(const std::string &field, const std::string &relation,
                   const int64_t value, const std::string &field2,
                   const std::string &relation2, const int64_t value2) {
  std::string filterExpresssion;
  filterExpresssion.reserve(1024);
  filterExpresssion += field + " ";
  filterExpresssion += relation + " ";
  filterExpresssion += std::to_string(value) + " AND ";
  filterExpresssion += field2 + " ";
  filterExpresssion += relation2 + " ";
  filterExpresssion += std::to_string(value2) + ";";
  layer->SetAttributeFilter(filterExpresssion.c_str());
}

void Layer::resetFilter() {
  layer->ResetReading();
}

void Layer::remove(const int64_t id) {
  throw std::runtime_error("Deletion of features from DB not supported yet!");
}

bool Layer::isEmpty() const {
  if (layer == nullptr) {
    return true;
  }

  if (layer->GetFeatureCount() == 0) {
    return true;
  } else {
    return false;
  }
}

OGRLayer *Layer::operator->() {
  return layer;
}

Layer::~Layer() {
  if (db != nullptr) {
    db->ReleaseResultSet(layer);
  }
}

Layer::iterator Layer::begin() {
  return Layer::iterator{layer};
}

Layer::iterator Layer::end() {
  return Layer::iterator();
}

Layer::iterator::iterator(OGRLayer *layer) : owningLayer(layer) {
  owningLayer->ResetReading();
  feature = owningLayer->GetNextFeature();
}

Layer::iterator::~iterator() {
  if (feature != nullptr) {
    OGRFeature::DestroyFeature(feature);
    feature = nullptr;
  }
}

bool Layer::iterator::operator!=(const Layer::iterator &other) const {
  return !(feature == other.feature || owningLayer == other.owningLayer);
}

void Layer::iterator::operator++() {
  OGRFeature::DestroyFeature(feature);
  feature = owningLayer->GetNextFeature();
}

FeatureUniq::FeatureUniq(OGRFeatureDefn *defn) {
  feature = OGRFeature::CreateFeature(defn);
}

FeatureUniq Layer::getNextFeature() {
  return FeatureUniq{layer->GetNextFeature()};
}

FeatureUniq Layer::getFeature(const image_objects::Feature &feature) {
  return layer->GetFeature(feature.getId());
}

bool Layer::isGeometric() const {
  if (layer->GetGeomType() == wkbNone) {
    return false;
  } else {
    return true;
  }
}

FeatureRepositorySqlite::FeatureRepositorySqlite(
    std::shared_ptr<GDALDataset> aDb)
    : db(aDb) {
  primitivesLayer = db->GetLayerByName("segmentation_primitives");
}

void FeatureRepositorySqlite::create(
    const std::vector<image_objects::Feature> &features) {
  db->StartTransaction();

  callback(0.0, {cb::INFO}, "Creating features in database");

  const size_t totalSegmentsCount = features.size();
  size_t current                  = 0;
  double lastSent                 = 0.0;

  for (auto &feature : features) {
    const double currentProgress = static_cast<double>(current++) /
                                   static_cast<double>(totalSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::PROGRESS}, "");
    }
    createSegmentInDb(feature);
  }
  db->CommitTransaction();

  callback(1.0, {cb::INFO, cb::PROGRESS},
           "Creating features in database finished");
}

handlers::image_objects::Feature
FeatureRepositorySqlite::read(const int64_t id) const {
  // Create Feature with id and geometry
  FeatureUniq ogrFeat{primitivesLayer->GetFeature(id)};
  // Check if Feature exists
  doesExist(ogrFeat);
  // Create new feature from Id and geometry
  image_objects::Feature feat{ogrFeat->GetFID(), ogrFeat->GetGeometryRef()};

  // Populate with stats
  populateWithStats(feat);

  return feat;
}

std::vector<image_objects::Feature> FeatureRepositorySqlite::readAll() const {

  callback(0.0, {cb::INFO}, "Reading all features from database");

  // Get Layer with geometries
  auto lyr                       = getLayer("segmentation_primitives");
  const size_t totalFeatureCount = lyr->GetFeatureCount();

  // Create place for read features
  std::vector<image_objects::Feature> features;
  features.reserve(totalFeatureCount);

  // Setup base callback information
  size_t current  = 0;
  double lastSent = 0.0;

  // Loop through all feature and read them
  for (auto it = lyr.begin(); it != lyr.end(); ++it) {

    // Calculate current progress
    const double currentProgress =
        static_cast<double>(current++) / static_cast<double>(totalFeatureCount);

    // Check if it is need to emit callback with progress
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::PROGRESS}, "");
    }

    // Emplace object an the end
    features.emplace_back(it->GetFID(), it->GetGeometryRef());

    // Populate its statistics
    populateWithStats(features.back());
  }

  // Transmit the successful finish of operation
  callback(1.0, {cb::PROGRESS}, "");

  return features;
}

void FeatureRepositorySqlite::setCallback(general::CallbackFunction callback) {
  this->callback = callback;
}

void FeatureRepositorySqlite::update(
    const std::vector<image_objects::Feature> &features) {

  callback(0.0, {cb::INFO}, "Updating features into database started");

  const size_t totalSegmentsCount = features.size();
  size_t current                  = 0;
  double lastSent                 = 0.0;

  db->StartTransaction();

  for (auto &feature : features) {

    const double currentProgress = static_cast<double>(current++) /
                                   static_cast<double>(totalSegmentsCount);
    if (currentProgress > lastSent) {
      lastSent += 0.01;
      callback(currentProgress, {cb::PROGRESS}, "");
    }

    updateStats(feature);
  }

  db->CommitTransaction();

  callback(1.0, {cb::PROGRESS}, "");
}

void FeatureRepositorySqlite::remove(const int64_t id) {
  throw std::runtime_error("Not supported yet!");
}

Layer FeatureRepositorySqlite::getLayer(const std::string &name) const {
  OGRLayer *lyr = db->GetLayerByName(name.c_str());
  if (lyr == NULL) {
    throw errors::IncorrectLayer("No %s layer in database", name);
  }

  return Layer{lyr};
}

int FeatureRepositorySqlite::getLayerId(const std::string &rasterName,
                                        const Stat type, const int band) {
  auto rasterId  = getRasterId(rasterName);
  auto layerView = getLayer("layer_view");
  layerView.resetFilter();
  layerView.filter("raster_view", "=", rasterId);
  layerView.filter("band_no", "=", band);

  if (layerView.isEmpty()) {

    const auto newLayerId = createNewLayer(rasterId);

    // Create new band_no
    auto bandLyr  = getLayer("bands");
    auto bandFeat = getEmptyFeature(bandLyr);
    bandFeat->SetField("band_no", band);
    bandFeat->SetField("layer_id", newLayerId);
    if (bandLyr->CreateFeature(bandFeat.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError(
          "Cannot add new band (band_no=%d layer_id=%d) into bands table", band,
          newLayerId);
    }

    // Set up back filter
    layerView.resetFilter();
    layerView.filter("raster_view", "=", rasterId);
    layerView.filter("band_no", "=", band);
  }

  auto feat = layerView.getNextFeature();
  return feat->GetFieldAsInteger("layer_id");
}

int FeatureRepositorySqlite::getLayerId(const std::string &rasterName,
                                        const image_objects::Ndvi &ndvi) {
  auto rasterId = getRasterId(rasterName);
  std::string ndviLyrStmt;
  ndviLyrStmt.reserve(1024);
  ndviLyrStmt += "SELECT layer_id\n";
  ndviLyrStmt += "FROM   layer_view\n";
  ndviLyrStmt += "WHERE  raster_id = " + std::to_string(rasterId) + "\n";
  ndviLyrStmt += "       AND band_no IN (" + std::to_string(ndvi.nir) + ", " +
                 std::to_string(ndvi.vis) + ")\n";
  ndviLyrStmt += "GROUP  BY layer_id\n";
  ndviLyrStmt += "HAVING Count(layer_id) = 2;";

  int ndviLyrId = -1;
  Layer lyr{db, ndviLyrStmt};

  if (lyr.isEmpty()) {
    ndviLyrId     = createNewLayer(rasterId);
    auto bandLyr  = getLayer("bands");
    auto bandFeat = getEmptyFeature(bandLyr);
    bandFeat->SetField("layer_id", ndviLyrId);
    bandFeat->SetField("band_no", ndvi.vis);
    if (bandLyr->CreateFeature(bandFeat.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError(
          "Cannot add new band (band_no=%d layer_id=%d) into bands table", ndviLyrId,
          ndvi.vis);
    }

    auto bandFeat2 = getEmptyFeature(bandLyr);
    bandFeat2->SetField("layer_id", ndviLyrId);
    bandFeat2->SetField("band_no", ndvi.nir);
    if (bandLyr->CreateFeature(bandFeat2.get()) != OGRERR_NONE) {
              throw errors::CreateFeatureError(
          "Cannot add new band (band_no=%d layer_id=%d) into bands table", ndviLyrId,
          ndvi.vis);
    }
  } else {
    auto f    = lyr.getNextFeature();
    ndviLyrId = f->GetFieldAsInteger("layer_id");
  }
  return ndviLyrId;
}

int FeatureRepositorySqlite::getLayerId(const std::string &rasterName,
                                        const image_objects::Zabud &zabud) {
  auto rasterId = getRasterId(rasterName);
  std::string zabudLyrStmt;
  zabudLyrStmt.reserve(1024);
  zabudLyrStmt += "SELECT layer_id\n";
  zabudLyrStmt += "FROM   layer_view\n";
  zabudLyrStmt += "WHERE  raster_id = " + std::to_string(rasterId) + "\n";
  zabudLyrStmt += "       AND band_no IN (";
  zabudLyrStmt += std::to_string(zabud.blue);
  zabudLyrStmt += ", " + std::to_string(zabud.green);
  zabudLyrStmt += ", " + std::to_string(zabud.red);
  zabudLyrStmt += ", " + std::to_string(zabud.nir);
  zabudLyrStmt += ") ";
  zabudLyrStmt += "GROUP  BY layer_id\n";
  zabudLyrStmt += "HAVING Count(layer_id) = 4;";

  int zabudLyrId = -1;
  Layer lyr{db, zabudLyrStmt};

  if (lyr.isEmpty()) {
    zabudLyrId    = createNewLayer(rasterId);
    auto bandLyr  = getLayer("bands");
    auto bandFeat = getEmptyFeature(bandLyr);
    bandFeat->SetField("layer_id", zabudLyrId);
    bandFeat->SetField("band_no", zabud.blue);
    if (bandLyr->CreateFeature(bandFeat.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError("Cannot add band %d to layer %d for ZABUD factor", zabud.blue, zabudLyrId);
    }

    auto bandFeat2 = getEmptyFeature(bandLyr);
    bandFeat2->SetField("layer_id", zabudLyrId);
    bandFeat2->SetField("band_no", zabud.green);
    if (bandLyr->CreateFeature(bandFeat2.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError("Cannot add band %d to layer %d for ZABUD factor", zabud.green, zabudLyrId);
    }

    auto bandFeat3 = getEmptyFeature(bandLyr);
    bandFeat3->SetField("layer_id", zabudLyrId);
    bandFeat3->SetField("band_no", zabud.red);
    if (bandLyr->CreateFeature(bandFeat3.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError("Cannot add band %d to layer %d for ZABUD factor", zabud.red, zabudLyrId);
    }

    auto bandFeat4 = getEmptyFeature(bandLyr);
    bandFeat4->SetField("layer_id", zabudLyrId);
    bandFeat4->SetField("band_no", zabud.nir);
    if (bandLyr->CreateFeature(bandFeat4.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError("Cannot add band %d to layer %d for ZABUD factor", zabud.nir, zabudLyrId);
    }

  } else {
    auto f     = lyr.getNextFeature();
    zabudLyrId = f->GetFieldAsInteger("layer_id");
  }
  return zabudLyrId;
}

int FeatureRepositorySqlite::getRasterId(const std::string &rasterName) {

  // Get layer and filter

  auto rasterLyr = getLayer("rasters");
  rasterLyr.filter("name", "=", rasterName);
  if (rasterLyr.isEmpty()) {
    // Get definition of the layer
    OGRFeatureDefn *defn = rasterLyr->GetLayerDefn();

    // Create new empty feature
    FeatureUniq feat{OGRFeature::CreateFeature(defn)};
    feat->SetField("name", rasterName.c_str());
    if (rasterLyr->CreateFeature(feat.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError("Cannat add raster %s into database", rasterName);
    }

    // Re-open table
    rasterLyr = getLayer("rasters");
  }

  auto feat = rasterLyr.getNextFeature();
  return feat->GetFID();
}

void FeatureRepositorySqlite::doesExist(const FeatureUniq &ogrFeat) const {
  if (ogrFeat.get() == nullptr) {
    throw errors::FeatureDoesNotExist("");
  }
}

void FeatureRepositorySqlite::populateWithStats(
    image_objects::Feature &feature) const {
  // Populate mean
  populateMean(feature);
  // Populate ndvi
  populateNdvi(feature);
  // Populate stddev
  populateStdDev(feature);
}

void FeatureRepositorySqlite::populateWith(image_objects::Feature &feature,
                                           Layer &layer,
                                           const Stat type) const {
  layer.resetFilter();
  layer.filter("segment_id", "=", feature.getId());
  for (auto it = layer.begin(); it != layer.end(); ++it) {
    // Create in feature or get from feature source stats
    auto &stats = feature[it->GetFieldAsString("raster_name")];

    // Get band of stat if it is mean or standard deviation
    int band = 0;
    if (type == Stat::MEAN || type == Stat::STDDEV) {
      band = it->GetFieldAsInteger("nth_band");
    }

    /**
     *  TO DO!
     *  POPULATE FEATURE WITH VALUES FROM NDVI
     *  and
     *  ZABUD
     */

    // Get proper value of statistic
    const double value = it->GetFieldAsDouble("value");

    // Set the statistic value
    stats.set(type, band, value);
  }
  layer.resetFilter();
}

void FeatureRepositorySqlite::populateMean(
    image_objects::Feature &feature) const {
  try {
    auto lyr = getLayer("mean_view");
    populateWith(feature, lyr, Stat::MEAN);

  } catch (errors::IncorrectLayer &e) {
    // Do nothing
    // It's okay -> this statistic may not be computed yet
  }
}

void FeatureRepositorySqlite::populateStdDev(
    image_objects::Feature &feature) const {
  try {
    auto lyr = getLayer("stddev_view");
    populateWith(feature, lyr, Stat::STDDEV);
  } catch (errors::IncorrectLayer &e) {
    // Do nothing
    // It's okay -> this statistic may not be computed yet
  }
}

void FeatureRepositorySqlite::populateNdvi(
    image_objects::Feature &feature) const {
  try {
    auto lyr = getLayer("ndvi_view");
    populateWith(feature, lyr, Stat::NDVI);
  } catch (errors::IncorrectLayer &e) {
    // Do nothing
    // It's okay -> this statistic may not be computed yet
  }
}

void FeatureRepositorySqlite::populateZabud(
    image_objects::Feature &feature) const {
  try {
    auto lyr = getLayer("zabud_view");
    populateWith(feature, lyr, Stat::ZABUD);
  } catch (errors::IncorrectLayer &e) {
    // Do nothing
    // It's okay -> this statistic may not be computed yet
  }
}

void FeatureRepositorySqlite::updateStats(
    const image_objects::Feature &feature) {
  // Update mean
  updateMean(feature);

  // Update stddev
  updateStdDev(feature);

  // Update ndvi
  updateNdvi(feature);

  // Update zabud
  updateZabud(feature);
}

void FeatureRepositorySqlite::updateTable(Layer &layer, const int64_t id,
                                          const int layerId,
                                          const double value) {

  // Filter the layer to get proper tables
  layer.resetFilter();
  layer.filter("layer_id", "=", layerId, "segment_id", "=", id);

  // Check if filtered layer is emptye in order to create row
  // or just to update it
  if (layer.isEmpty()) {
    auto feat = getEmptyFeature(layer);
    feat->SetField("segment_id", static_cast<GIntBig>(id));
    feat->SetField("layer_id", layerId);
    feat->SetField("value", value);
    if (layer->CreateFeature(feat.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError("Cannot create stats for feature %d", id);
    }
  } else {
    auto feat = layer.getNextFeature();
    feat->SetField("value", value);
    if (layer->SetFeature(feat.get()) != OGRERR_NONE) {
      throw errors::CreateFeatureError("Cannot create stats for feature %d", id);
    }
  }
}

void FeatureRepositorySqlite::updateNdvi(
    const image_objects::Feature &feature) {

  // Update all source rasters
  for (auto &kv : feature.getAllStats()) {

    const auto &raster  = kv.first;
    const auto &stats   = kv.second;
    const auto rasterId = getRasterId(raster);

    if (!stats.isNdviSet())
      continue;

    Layer lyr;
    try {
      lyr = getLayer("ndvi");
    } catch (errors::IncorrectLayer &e) {
      initStat(Stat::NDVI);
      lyr = getLayer("ndvi");
    }

    // Get Ndvi stats from featuer
    const auto ndvi = stats.getNdvi();

    // Update ndvi value for raster
    const int layerId = getLayerId(raster, ndvi);

    // Update proper value in the table
    updateTable(lyr, feature.getId(), layerId, ndvi.value);
  }
}

void FeatureRepositorySqlite::updateZabud(
    const image_objects::Feature &feature) {

  // Update all source rasters
  for (auto &kv : feature.getAllStats()) {

    const auto &raster  = kv.first;
    const auto &stats   = kv.second;
    const auto rasterId = getRasterId(raster);

    if (!stats.isZabudSet())
      continue;

    Layer lyr;
    try {
      lyr = getLayer("zabud");
    } catch (errors::IncorrectLayer &e) {
      initStat(Stat::ZABUD);
      lyr = getLayer("zabud");
    }

    // Get zabud stats from the feature
    const auto zabud = stats.getZabud();

    // Update ndvi value for raster
    const int layerId = getLayerId(raster, zabud);

    // Update or create zabud value in proper table
    updateTable(lyr, feature.getId(), layerId, zabud.value);
  }
}

void FeatureRepositorySqlite::updateMean(
    const image_objects::Feature &feature) {
  Layer lyr;
  try {
    lyr = getLayer("mean");
  } catch (errors::IncorrectLayer &e) {
    initStat(Stat::MEAN);
    lyr = getLayer("mean");
  }

  // Update all source rasters
  for (auto &kv : feature.getAllStats()) {
    const auto &raster  = kv.first;
    const auto &stats   = kv.second;
    const auto rasterId = getRasterId(raster);

    // Update all bands in stat source
    for (auto band : stats.getMeanKeys()) {
      const int layerId = getLayerId(raster, Stat::MEAN, band);

      // Update specific value in table
      updateTable(lyr, feature.getId(), layerId, stats.getMean(band));
    }
  }
}

void FeatureRepositorySqlite::updateStdDev(
    const image_objects::Feature &feature) {
  Layer lyr;
  try {
    lyr = getLayer("stddev");
  } catch (errors::IncorrectLayer &e) {
    initStat(Stat::STDDEV);
    lyr = getLayer("stddev");
  }

  // Update all source rasters
  for (auto &kv : feature.getAllStats()) {
    const auto &raster  = kv.first;
    const auto &stats   = kv.second;
    const auto rasterId = getRasterId(raster);

    // Update all bands in stat source
    for (auto band : stats.getStddevKeys()) {
      const int layerId = getLayerId(raster, Stat::STDDEV, band);

      // Update specific value in table
      updateTable(lyr, feature.getId(), layerId, stats.getMean(band));
    }
  }
}

void FeatureRepositorySqlite::createSegmentInDb(
    const image_objects::Feature &feature) {
  // Get layer with geometry
  auto lyr = getLayer("segmentation_primitives");

  auto feat = getEmptyFeature(lyr);

  // Set its id
  feat->SetFID(feature.getId());

  // Fetch polygon
  auto polygon = feature.getPolygon();

  // Set its geometry
  if (feat->SetGeometry(&polygon) != OGRERR_NONE) {
    throw errors::AddGeometryError("Cannot add geometry to feature no %d", feature.getId());
  }

  // Create feature in proper layer
  if (lyr->CreateFeature(feat.get()) != OGRERR_NONE) {
    throw errors::CreateFeatureError("Cannot create feature no %d in database", feature.getId());
  }
}

void FeatureRepositorySqlite::initStat(const Stat type) {
  // mean
  const std::string mean{
      "CREATE TABLE IF NOT EXISTS mean\n"
      "  (\n"
      "     segment_id INTEGER,\n"
      "     layer_id   INTEGER,\n"
      "     value      DOUBLE,\n"
      "     FOREIGN KEY (segment_id) REFERENCES "
      "segmentation_primitives(id),\n"
      "     FOREIGN KEY (layer_id)   REFERENCES layers(layer_id)\n"
      "  );"};

  // mean_index
  const std::string mean_index{"CREATE UNIQUE INDEX IF NOT EXISTS mean_index "
                               "ON mean (segment_id, layer_id);"};

  // mean_view
  const std::string mean_view{
      "CREATE VIEW IF NOT EXISTS mean_view\n"
      "AS\n"
      "  SELECT mean.segment_id                  AS segment_id,\n"
      "         segmentation_primitives.the_geom AS the_geom,\n"
      "         bands.band_no                    AS nth_band,\n"
      "         mean.layer_id                    AS layer_id,\n"
      "         mean.value                       AS value,\n"
      "         rasters.raster_id                AS raster_id,\n"
      "         rasters.name                     AS raster_name\n"
      "  FROM   mean\n"
      "         INNER JOIN segmentation_primitives\n"
      "                 ON segmentation_primitives.id = mean.segment_id"
      "         INNER JOIN layers\n"
      "                 ON layers.layer_id = mean.layer_id\n"
      "         INNER JOIN bands\n"
      "                 ON bands.layer_id = mean.layer_id\n"
      "         INNER JOIN rasters\n"
      "                 ON layers.raster_id = rasters.raster_id;"};

  // stddev
  const std::string stddev{
      "CREATE TABLE IF NOT EXISTS stddev\n"
      "  (\n"
      "     segment_id INTEGER,\n"
      "     layer_id   INTEGER,\n"
      "     value      DOUBLE,\n"
      "     FOREIGN KEY (segment_id) REFERENCES "
      "segmentation_primitives(id),\n"
      "     FOREIGN KEY (layer_id)   REFERENCES layers(layer_id)\n"
      "  );"};

  // stddev_index
  const std::string stddev_index{
      "CREATE UNIQUE INDEX IF NOT EXISTS stddev_index "
      "ON stddev (segment_id, layer_id);"};

  // stddev_view
  const std::string stddev_view{
      "CREATE VIEW IF NOT EXISTS stddev_view\n"
      "AS\n"
      "  SELECT stddev.segment_id                AS segment_id,\n"
      "         segmentation_primitives.the_geom AS the_geom,\n"
      "         bands.band_no                    AS nth_band,\n"
      "         stddev.layer_id                  AS layer_id,\n"
      "         stddev.value                     AS value,\n"
      "         rasters.raster_id                AS raster_id,\n"
      "         rasters.name                     AS raster_name\n"
      "  FROM   stddev\n"
      "         INNER JOIN segmentation_primitives\n"
      "                 ON segmentation_primitives.id = stddev.segment_id\n"
      "         INNER JOIN layers\n"
      "                 ON layers.layer_id = stddev.layer_id\n"
      "         INNER JOIN bands\n"
      "                 ON bands.layer_id = stddev.layer_id\n"
      "         INNER JOIN rasters\n"
      "                 ON layers.raster_id = rasters.raster_id;"};

  // ndvi
  const std::string ndvi{
      "CREATE TABLE IF NOT EXISTS ndvi\n"
      "  (\n"
      "     segment_id INTEGER,\n"
      "     layer_id   INTEGER,\n"
      "     value      REAL,\n"
      "     FOREIGN KEY (segment_id) REFERENCES "
      "segmentation_primitives(id),\n"
      "     FOREIGN KEY (layer_id)   REFERENCES layers(layer_id)"
      "  );"};

  // ndvi_index
  const std::string ndvi_index{"CREATE UNIQUE INDEX IF NOT EXISTS ndvi_index "
                               "ON ndvi (segment_id, layer_id);"};

  // ndvi_view
  const std::string ndvi_view{
      "CREATE VIEW IF NOT EXISTS ndvi_view\n"
      "AS\n"
      "  SELECT ndvi.segment_id                  AS segment_id,\n"
      "         segmentation_primitives.the_geom AS the_geom,\n"
      "         ndvi.layer_id                    AS layer_id,\n"
      "         ndvi.value                       AS value,\n"
      "         rasters.raster_id                AS raster_id,\n"
      "         rasters.name                     AS raster_name\n"
      "  FROM   ndvi\n"
      "         INNER JOIN segmentation_primitives\n"
      "                 ON segmentation_primitives.id = ndvi.segment_id\n"
      "         INNER JOIN layers\n"
      "                 ON layers.layer_id = ndvi.layer_id\n"
      "         INNER JOIN rasters\n"
      "                 ON rasters.raster_id = layers.raster_id;"};

  // zabud
  const std::string zabud{
      "CREATE TABLE IF NOT EXISTS zabud\n"
      "  (\n"
      "     segment_id INTEGER,\n"
      "     layer_id   INTEGER,\n"
      "     value      REAL,\n"
      "     FOREIGN KEY (segment_id) REFERENCES "
      "segmentation_primitives(id),\n"
      "     FOREIGN KEY (layer_id)   REFERENCES layers(layer_id)"
      "  );"};

  // zabud_index
  const std::string zabud_index{"CREATE UNIQUE INDEX IF NOT EXISTS zabud_index "
                                "ON zabud (segment_id, layer_id);"};

  // zabud_view
  const std::string zabud_view{
      "CREATE VIEW IF NOT EXISTS zabud_view\n"
      "AS\n"
      "  SELECT zabud.segment_id                 AS segment_id,\n"
      "         segmentation_primitives.the_geom AS the_geom,\n"
      "         zabud.layer_id                   AS layer_id,\n"
      "         zabud.value                      AS value,\n"
      "         rasters.raster_id                AS raster_id,\n"
      "         rasters.name                     AS raster_name\n"
      "  FROM   zabud\n"
      "         INNER JOIN segmentation_primitives\n"
      "                 ON segmentation_primitives.id = zabud.segment_id\n"
      "         INNER JOIN layers\n"
      "                 ON layers.layer_id = zabud.layer_id\n"
      "         INNER JOIN rasters\n"
      "                 ON rasters.raster_id = layers.raster_id;"};

  if (type == Stat::MEAN) {
    executeSql(mean);
    executeSql(mean_index);
    executeSql(mean_view);
  } else if (type == Stat::STDDEV) {
    executeSql(stddev);
    executeSql(stddev_index);
    executeSql(stddev_view);
  } else if (type == Stat::NDVI) {
    executeSql(ndvi);
    executeSql(ndvi_index);
    executeSql(ndvi_view);
  } else if (type == Stat::ZABUD) {
    executeSql(zabud);
    executeSql(zabud_index);
    executeSql(zabud_view);
  }
};

void FeatureRepositorySqlite::executeSql(const std::string &sql) {
  if (db.get() != nullptr) {
    // db->StartTransaction();
    OGRLayer *lyr = db->ExecuteSQL(sql.c_str(), nullptr, "SQLite");
    // db->ReleaseResultSet(lyr);
    // db->FlushCache();
    // db->CommitTransaction();
  }
}

FeatureUniq FeatureRepositorySqlite::getEmptyFeature(Layer &layer) const {
  // Get definition of the layer
  OGRFeatureDefn *defn = layer->GetLayerDefn();

  // Create new empty feature
  FeatureUniq feat{OGRFeature::CreateFeature(defn)};

  return feat;
}

int FeatureRepositorySqlite::createNewLayer(const int rasterId) {
  // Create new layer
  auto layerLyr  = getLayer("layers");
  auto layerFeat = getEmptyFeature(layerLyr);
  layerFeat->SetField("raster_id", rasterId);
  if (layerLyr->CreateFeature(layerFeat.get()) != OGRERR_NONE) {
    throw errors::CreateFeatureError("Cannot add new layer to raster_id=%d", rasterId);
  }

  // Access inserted layer_id
  layerLyr->ResetReading();
  layerLyr->SetNextByIndex(layerLyr->GetFeatureCount() - 1);
  auto insertedFeat     = layerLyr.getNextFeature();
  const auto newLayerId = insertedFeat->GetFID();
  return newLayerId;
}

} // namespace handlers
} //  namespace agroeye
