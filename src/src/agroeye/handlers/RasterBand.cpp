#include "RasterBand.h"

namespace agroeye {
namespace handlers {    
        
RasterBand::RasterBand(GDALRasterBand* rawBand, CallbackFunction func) :
    ptrBand(rawBand), callbackFunc(func) {        
}

RasterBand::RasterBand(const RasterBand& other) : 
    ptrBand(other.ptrBand), callbackFunc(other.callbackFunc) {        
}

RasterBand::RasterBand(RasterBand&& other) :
    ptrBand(other.ptrBand), callbackFunc(other.callbackFunc) {
        other.ptrBand = nullptr;
        other.callbackFunc = nullptr;    
}
    
RasterBand& RasterBand::operator=(const RasterBand& other) {
    ptrBand = other.ptrBand;
    callbackFunc = other.callbackFunc;    
    return *this;
}

RasterBand& RasterBand::operator=(RasterBand&& other) {
    ptrBand = other.ptrBand;
    callbackFunc = other.callbackFunc;    
    
    other.ptrBand = nullptr;
    other.callbackFunc = nullptr;
    
    return *this;    
}

int RasterBand::getWidth() const {
    return static_cast<int>(ptrBand->GetXSize());
}

int RasterBand::getHeight() const {
    return static_cast<int>(ptrBand->GetYSize());
}


RasterData RasterBand::read(size_t x, size_t y, size_t width, size_t height) {
    // Check parameters
    checkPosition(x, y);
    checkExtent(x, y, width, height);
    
    // Create Container for read data
    RasterData rasterData (width, height);
    rasterData.xPos = x;
    rasterData.yPos = y;
    
    // Read data  // Save Data
    inOutRaster(rasterData, GDALRWFlag::GF_Read);
    
    return rasterData;
}

RasterData RasterBand::read(const general::ImageSegment& segment) {
   const auto tl = segment.getTopLeft();
   const auto lr = segment.getLowRight();
   const auto x = tl.x;
   const auto y = tl.y;
   const auto width = lr.x - tl.x;
   const auto height = lr.y - tl.y;
   
   return read(x, y, width, height);
}

RasterData RasterBand::read() {
    const size_t x = 0;
    const size_t y = 0;
    const size_t width = getWidth();
    const size_t height = getHeight();
    return read(x, y, width, height);
}

void RasterBand::save(RasterData& rasterData) {
    // Check Parameters
    checkPosition(rasterData.getXPos(), rasterData.getYPos());
    checkExtent(rasterData.getXPos(), rasterData.getYPos(), rasterData.getWidth(), rasterData.getHeight());
    
    // Save Data
    inOutRaster(rasterData, GDALRWFlag::GF_Write);
    
}

void RasterBand::inOutRaster(RasterData& rasterData, GDALRWFlag inOutType) {
    
    std::string inOutStr; 
    if (inOutType == GDALRWFlag::GF_Read){
       inOutStr = "Reading from ";
    } else {
       inOutStr = "Writing to "; 
    }
    
    
    callbackFunc(0, {CallbackTypes::DEBUG}, inOutStr + "raster started");
    
    GDALRWFlag      eRWFlag     = inOutType;
    int             nXOff       = static_cast<int>(rasterData.getXPos());
    int             nYOff       = static_cast<int>(rasterData.getYPos());
    int             nXSize      = static_cast<int>(rasterData.getWidth());
    int             nYSize      = static_cast<int>(rasterData.getHeight());
    void*           pData       = rasterData.pixelData.data();
    int             nBufXSize   = static_cast<int>(rasterData.getWidth());
    int             nBufYSize   = static_cast<int>(rasterData.getHeight());
    GDALDataType    eBufType    = GDALDataType::GDT_Float64;
    GSpacing        nPixelSpace = 0;
    GSpacing        nLineSpace  = 0;
    CPLErr returnCode = ptrBand->RasterIO(eRWFlag, 
                                          nXOff, nYOff, 
                                          nXSize, nYSize, 
                                          pData, 
                                          nBufXSize, nBufYSize,
                                          eBufType, 
                                          nPixelSpace, nLineSpace);    
    if (returnCode != CPLErr::CE_None) {
       throw errors::ReadWriteError("Error at %s dataset", inOutStr);
    }

    callbackFunc(0, {CallbackTypes::DEBUG}, inOutStr + "raster finished");
}

void RasterBand::checkPosition(size_t x, size_t y) const {
    auto maxX = ptrBand->GetXSize();
    auto maxY = ptrBand->GetYSize();
    
    if (x >= maxX || y >= maxY) {
        throw errors::IncorrectExtent("Pixel X=%d Y=%d is out of band (x_max=%d y_max=%d)", x, y, maxX, maxY);
    }
}

void RasterBand::checkExtent(size_t x, size_t y, size_t width, size_t height) const {
    checkPosition(x, y);
    
    auto maxX = ptrBand->GetXSize();
    auto maxY = ptrBand->GetYSize();
    
    if (x+width > maxX || y+height > maxY) {
        throw errors::IncorrectExtent("The extent is beyond raster");
    }
}

double RasterBand::getNoDataValue() const {
    int pbSucces = 0;
    double value = ptrBand->GetNoDataValue(&pbSucces);
    if (pbSucces == 0)
        value = -999999;
    return value;    
}    

} // namespace handlers
} // namespace agroeye
