#ifndef RASTERDATAGROUP_H
#define RASTERDATAGROUP_H

#include <vector>
#include <unordered_map>

#include "RasterData.h"
#include "Errors.h"

namespace agroeye {
namespace handlers {

/**
 *  @brief Encapsulation of read data from a couple of bands from one RasterDataGroup
 *  Also provides convinient basic operations and quarantees
 *  the same parameters of raster data.
 */    
class RasterData;
class RasterDataGroup {
public:
    RasterDataGroup() {}; /**< Dummy constructor */
    RasterDataGroup(RasterDataGroup&&) = default;
    RasterDataGroup(const RasterDataGroup&) = default;
    RasterDataGroup& operator=(RasterDataGroup&&) = default; 
    RasterDataGroup& operator=(const RasterDataGroup&) = default;
    
    /**
     *  @brief Adds data to the container
     *  @param bandNo Number of read band
     *  @param rasterData Raster data to data. Transfers ownership
     */
    void addData(const int bandNo, RasterData&& rasterData);
    
    /**
     *  @brief Provides access to raster data for given band
     *  
     *  @param  bandNo  Band number
     *  @return Const refernce to raster data
     */
    const RasterData& getBandData(const int bandNo) const;
    
    /**
     *  @brief Gets list of bands
     *  @return List of bands
     */
    std::vector<int> getBandsList() const;
    
    /**
     *  @brief Extracts piece from given data creating copy
     * 
     *  @param rasterData Raster data pixels to be extracted
     *  @param x X of top left corner
     *  @param y Y of top left corner
     *  @param width Width of extracted fragment
     *  @param height Height of extracted fragment 
     *  @param return A new raster data group
     */
    static RasterDataGroup extractPiece(const RasterDataGroup& rasterData,
                                       const size_t x, const size_t y,
                                       const size_t width, const size_t height);
    
    size_t getXPos() const { return xPos;};
    size_t getYPos() const { return yPos;};
    size_t getWidth() const {return width;};
    size_t getHeight() const {return height;};
    
private:
    void checkAttributes(const RasterData& rasterData) const;
    void throwIfBandExists(const int bandNo) const;
    void throwIfBandDoesNotExist(const int bandNo) const;
    void assignMetadataIfFirst(const RasterData& rasterData);
    
    std::unordered_map<int, RasterData> bandsData;
    size_t xPos {0};
    size_t yPos {0};
    size_t width {0};
    size_t height {0};
};

} // namespace handlers
} // namespace agroeye

#endif 
