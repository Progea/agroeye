#ifndef FEATUREREPOSITORY_H
#define FEATUREREPOSITORY_H 

#include "Feature.h"
#include "Callback.h"

namespace agroeye {
namespace handlers {
namespace image_objects {

class FeatureRepository {
public:
  virtual void create(const std::vector<Feature>& features) = 0;
  virtual Feature read(const int64_t id) const = 0;
  virtual std::vector<Feature> readAll() const = 0;
  virtual void update(const std::vector<Feature>& features) = 0;
  virtual void remove(const int64_t  id) = 0;
  virtual void setCallback(general::CallbackFunction callback) = 0;
  virtual ~FeatureRepository() {};
};

}
}
}

#endif
