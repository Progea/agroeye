#ifndef RASTERDATA_H
#define RASTERDATA_H

#include <vector>
#include <algorithm>
#include <Eigen/Dense>
#include "SegmentationParameters.h"
#include "Errors.h"
#include "ContainerMath.h"

namespace agroeye {
namespace handlers {

/**
 * @brief Encapsulation of read data from raster band
 * 
 * By default all values are stored internally as double !
 */  
class RasterBand;
class RasterData {
    friend class RasterBand;
public:
	/**
	 *	@brief Constructor allocating memory
	 *
	 *	Constructor allocating memory of double type with size width * height
	 *	@param	width	Number of columns
	 *	@param	height	Number of rows
         *      @param  x       Column of top left corner
         *      @param  y       Row of top left corner
	 */
    RasterData(const size_t width, const size_t height, const size_t x = 0, const size_t y = 0);
	
	/**
	 *	@brief	Copy constructor
	 *
	 *	Copies the data from one object into another. No resources are shared after
	 *	@param	other	RasterData to be copied
	 */
    RasterData(const RasterData& other);
	
	/**
	 *	@brief	Move constructor
	 *
	 *	After that the source object is cleared and all resource are transferred to new object
	 *	@param	other	Mooved object
	 */
    RasterData(RasterData&& other);
    RasterData& operator=(const RasterData&);
    RasterData& operator=(RasterData&& other);
    virtual ~RasterData();
    
	/**
	 *	@brief Column number of top left corner
	 *	Position of first pixel in 
	 */
    inline size_t getXPos() const {return xPos;}; /**< Returns the column value of top left corner in whole raster units */
    inline size_t getYPos() const {return yPos;};	/**< Returns the row value of top left corner in whole raster units */
    inline size_t getWidth() const {return width;};	/**< Returns the width of read fragment */
    inline size_t getHeight() const {return height;}; /**< Returns the height of read fragment */
    double& operator()(size_t x, size_t y); /**< Returns the reference to pixel of given cooridnates */
    const double& operator()(size_t x, size_t y) const; /**< Returns the const reference to pixel of given cooridnates */
    
    /**
     * @brief Get block of pixels which are
     * 
     * @param x Value of X axis of top left corner
     * @param y Value of Y axis of top left corner
     * @param width Width of extracted pixels
     * @param heighy Height of extracted pixels
     */
    Eigen::MatrixXd getBlock(size_t x, size_t y, size_t width, size_t height) const;
    
    double getMean() const; /**< Calculates the mean value of read data */
    double getStdDev() const;	/**< Calculates the standard deviation value of read data */
	
	
	/**
	 *	@brief	Calculates the value of given statistic attribute
	 *	@param	types	Type of statistic feature
	 *	@return	The computed value
	 */
    double calculate(agroeye::operations::StatisticsTypes types) const;
    
    /**
     *  @brief Extracts fragment of raster data creating new one 
     * 
     *  @param rasterData Original raster Data
     *  @param x X of top left corner relative to original data
     *  @param y Y of top left corner realtive to original data
     *  @param width Width of new fragment
     *  @param height Height of new fragment
     *  @return Newly extracted fragment
     */
    static RasterData extractPiece(const RasterData& rasterData, size_t x, size_t y, size_t width, size_t height);
    
private:
    double getMean(const double noDataValue) const;
    double getStdDev(const double noDataValue) const;
    void checkAndAssign(const size_t x, const size_t y, const size_t width, const size_t height);
    bool checkInsidePosition(const size_t x, const size_t y) const;
    size_t getIndex(const size_t x, const size_t y) const;
    
    size_t xPos;
    size_t yPos;
    size_t width;
    size_t height;
    std::vector<double> pixelData;        
};
    
} // namespace handlers
} // namespace agroeye


#endif
