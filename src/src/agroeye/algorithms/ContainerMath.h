#ifndef CONTAINERMATH_H
#define CONTAINERMATH_H

#include <algorithm>
#include <numeric>
#include <iterator>
#include <type_traits>
#include <cmath>
#include <iostream>

namespace agroeye {
namespace algorithm {
namespace math {
 
/** 
 *  @brief Metafunction computing the mean value.
 *  This function calculates the mean value of elements in given container
 *  pointed by pair of iterators
 * 
 *  @param  first   The first element
 *  @param  last    Next element behid the last
 */ 
template < class Iterator >
double mean(Iterator first, Iterator last) {
    size_t distance = std::distance(first, last);
    double sum = std::accumulate(first, last, *first-*first);    
    return sum/distance;
}

/** 
 *  @brief Metafunction computing the mean value.
 *  This function calculates the mean value of elements in given container
 *  pointed by pair of iterators. From computation of mean value all values
 *  having ignoreValue are excluded
 * 
 *  @param  first       The first element
 *  @param  last        Next element behid the last
 *  @param  ignoreValue The value excluded from computin mean value
 */ 
template < class Iterator >
double mean(Iterator first, Iterator last, typename Iterator::value_type ignoreValue) {
    // Number of elements in container
    size_t distance = std::distance(first, last);
    
    // Sum of all values
    double sum = std::accumulate(first, last, *first-*first);
    
    // How many times ignored values occured
    size_t ignoreValuesOccur = std::count(first, last, ignoreValue);
    
    // Substract from total sum
    sum -= ignoreValuesOccur*ignoreValue;
    
    // New number of elements taken into account
    size_t newCount = distance - ignoreValuesOccur;
    
    return sum/newCount;
}

/// @cond
template < typename T >
struct quadraticdifference_accumulator{
    const T mean;
    T ignoredValue;
    const bool allVals;
    quadraticdifference_accumulator(T m) : 
        mean(m), allVals(true) {};
    quadraticdifference_accumulator(T m, T ignore) :
        mean(m), ignoredValue(ignore), allVals(false) {};
        
    T operator()(T initial, T following) {
        T val = initial;
        if (allVals || following!=ignoredValue)
            val = pow(mean-following, 2) + initial;
        return val;
    }
};
/// @endcond

/** 
 *  @brief Metafunction computing the standard deviation value.
 *  This function calculates the standard deviation value of elements in given container
 *  pointed by pair of iterators.
 * 
 *  @param  first       The first element
 *  @param  last        Next element behid the last
 */ 
template < class Iterator >
double stddev(Iterator first, Iterator last) {    
    quadraticdifference_accumulator<double> qDiff (mean(first, last));    
    double initVal = 0;
    const auto elementsCount = std::distance(first, last);
    const double sum = std::accumulate(first, last, initVal, qDiff);
    
    return pow(sum/elementsCount, 0.5);
}

/** 
 *  @brief Metafunction computing the standard deviation value.
 *  This function calculates the standard deviation value of elements in given container
 *  pointed by pair of iterators. From computation of standard deviation value all values
 *  having ignoreValue are excluded
 * 
 *  @param  first       The first element
 *  @param  last        Next element behid the last
 *  @param  ignoreValue The value excluded from computin mean value
 */
template < class Iterator >
double stddev(Iterator first, Iterator last, typename Iterator::value_type ignoreValue) {
    quadraticdifference_accumulator<double> qDiff (mean(first, last, ignoreValue), ignoreValue);
    double initVal = 0;
    const auto elementsCount = std::distance(first, last) - std::count(first, last, ignoreValue);
    const double sum = std::accumulate(first, last, initVal, qDiff);
    
    return pow(sum/elementsCount, 0.5);
}

    
} // math namespace
} // algorithm namespace
} // agroeye namespace

#endif
