#include "SegmentationChessboard.h"
#include "SegmentationParameters.h"
#include "StatsComputation.h"
#include "StatsParameters.h"
#include "AnalysisParameters.h"
#include "RasterAnalysis.h"

#include <functional>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include <pybind11/eigen.h>
#include <pybind11/functional.h>

namespace py = pybind11;
using namespace agroeye::general;
using namespace agroeye::handlers;
using namespace agroeye::operations::segmentation;

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
//PYBIND11_MAKE_OPAQUE(std::vector<StatisticsTypes>);
PYBIND11_MAKE_OPAQUE(std::vector<CallbackTypes>);

PYBIND11_PLUGIN(agroeye) {
    py::module module ("agroeye", "Agroeye python library");
    
    /** =================================== **/
    /**             OPAQUE STL              **/
    /**             CONTAINERS              **/
    /** =================================== **/
    //py::bind_vector<std::vector<StatisticsTypes>> (module, "StatisticsList");
    py::bind_vector<std::vector<CallbackTypes>> (module, "CallbackList");
    

    /** =================================== **/
    /**            ENUMARTIONS              **/
    /** =================================== **/
    
    /** --- Callback type --- **/
    py::enum_<CallbackTypes> callback_t (module, "Callback_t");
    callback_t
        .value("PROGRESS", CallbackTypes::PROGRESS)
        .value("INFO", CallbackTypes::INFO)
        .value("PROGRESS", CallbackTypes::DEBUG)
        .value("ERROR", CallbackTypes::ERROR)
        .export_values();
        
    /** --- Raster Analysis Parameters --- **/
    py::enum_<agroeye::operations::raster_analysis::Analysis_type> analysis_t (module, "Analysis_type");
    analysis_t
        .value("DIFFERENCE_TO_NEIGHBOUR", agroeye::operations::raster_analysis::Analysis_type::DIFFERENCE_TO_NEIGHBOUR)
        .export_values();
    
    /** --- StatistticType type --- **/
    /*
    py::enum_<StatisticsTypes> statistic_t (module, "Statistic_t");
    statistic_t
        .value("MEAN", StatisticsTypes::MEAN)
        .value("STDDEV", StatisticsTypes::STDDEV)
        .export_values();
    */
        
        
    /** =================================== **/
    /**                RASTER               **/
    /** =================================== **/
    py::class_<Raster> pyRaster (module, "Raster");
    pyRaster
        .def(py::init<const char*>())
        .def_property_readonly("path", &Raster::getPath)
        .def_property_readonly("width", &Raster::getWidth)
        .def_property_readonly("height", &Raster::getHeight);
       
        

    /** =================================== **/
    /**               SQLITE                **/
    /**              DATABASE               **/
    /** =================================== **/
    
    using FeatureRepository = agroeye::handlers::image_objects::FeatureRepository;
    py::class_<FeatureRepository, std::shared_ptr<FeatureRepository>> pyFeatureRpository(module, "FeatureRepository");
    
    
    using FeatureRepositorySqlite = agroeye::handlers::FeatureRepositorySqlite;
    py::class_<FeatureRepositorySqlite, std::shared_ptr<FeatureRepositorySqlite>> pySqliteFeatureRepository(module, "SqliteFeatureRepository", pyFeatureRpository);
    pySqliteFeatureRepository 
         .def(py::init<>());
    
    
    py::class_<Sqlite> pySqlite (module, "Sqlite");
    pySqlite
        .def(py::init<>())
        .def(py::init<const char*, bool>())
        .def("prepareForSegmentation", &Sqlite::prepareForSegmentation)
        .def("openRepo", &Sqlite::openFeatureRepository);
    
    
    /** =================================== **/
    /**             SEGMENTATION            **/
    /**              PARAMETERS             **/
    /** =================================== **/
    py::class_<agroeye::operations::segmentation::Parameters> pySegmentsParameters (module, "SegmentationParameters");
    pySegmentsParameters
        .def(py::init<const size_t>(), py::arg("squareSide") = 256)
        .def(py::init<std::vector<int>, const double>())
        .def_property_readonly("squareSide", &agroeye::operations::segmentation::Parameters::getSquareSide, "Returns square side of initailized object");
    
    /** =================================== **/
    /**             SEGMENTATION            **/
    /**              CHESSBOARD             **/
    /** =================================== **/
   py::class_<Segmentation> pySegmentation (module, "Segmentation");
   
    py::class_<Chessboard> pyChessboard (module, "ChessboardSegmentation", py::base<Segmentation>());
    pyChessboard
        .def(py::init<Raster, agroeye::operations::segmentation::Parameters, std::shared_ptr<FeatureRepositorySqlite>>())
        .def("setCallback", &Segmentation::setCallback)
        .def("start", &Chessboard::start);       
       
    
    py::class_<Quadtree> pyQuadtree (module, "QuadtreeSegmentation", py::base<Segmentation>());
    pyQuadtree
        .def(py::init<Raster, agroeye::operations::segmentation::Parameters, std::shared_ptr<FeatureRepositorySqlite>>())
        .def("setCallback", &Segmentation::setCallback)
        .def("start", &Quadtree::start);       
        
    
    /** =================================== **/
    /**             STATS COMPUTATION       **/
    /**              PARAMETERS             **/
    /** =================================== **/
    py::class_<agroeye::operations::stats::Parameters> pyStatsParameters (module, "StatsParameters");
    pyStatsParameters
        .def(py::init<>())
        .def("addMeanBand", &agroeye::operations::stats::Parameters::addMeanBand)
        .def("addStddevBand", &agroeye::operations::stats::Parameters::addStddevBand)
        .def("setNdvi", &agroeye::operations::stats::Parameters::setNdvi, "Sets bands in order to calc NDVI",
             py::arg("NIR"), py::arg("VIS"))
        .def("setZabud", &agroeye::operations::stats::Parameters::setZabud, "Set bands in order to calc ZABUD coefficient",
            py::arg("blue"), py::arg("green"), py::arg("red"), py::arg("nir"));
    
        
    /** =================================== **/
    /**             STATS                   **/
    /**              COMPUTATION            **/
    /** =================================== **/
    
    py::class_<agroeye::operations::stats::StatsComputation> pyStatsComputation (module, "StatsComputation");
    pyStatsComputation
        .def(py::init<Raster, agroeye::operations::stats::Parameters, std::shared_ptr<FeatureRepository>>())
        .def("setCallback", &agroeye::operations::stats::StatsComputation::setCallback)
        .def("start", &agroeye::operations::stats::StatsComputation::start);       
             
        
    /** =================================== **/
    /**       RASTER ANALYSIS               **/
    /**        PARAMETERS                   **/
    /** =================================== **/
    py::class_<agroeye::operations::raster_analysis::Parameters> pyRasterAnalysisParameters (module, "RasterAnalysisParameters");
    pyRasterAnalysisParameters
        .def(py::init<const agroeye::operations::raster_analysis::Analysis_type, const int, const int, const int>(), py::arg("analysis_type") = agroeye::operations::raster_analysis::Analysis_type::DIFFERENCE_TO_NEIGHBOUR, py::arg("band"), py::arg("rows") = 3, py::arg("cols") = 3);
         
    /** =================================== **/
    /**        RASTER ANALYSIS              **/
    /**              COMPUTATION            **/
    /** =================================== **/
    
    py::class_<agroeye::operations::raster_analysis::BlockStatistics> pyBlockStatistics (module, "BlockStatistics");
    pyBlockStatistics 
        .def(py::init<Raster, const std::string&, agroeye::operations::raster_analysis::Parameters>())
        .def("setCallback", &agroeye::operations::raster_analysis::BlockStatistics::setCallback)
        .def("start", &agroeye::operations::raster_analysis::BlockStatistics::start);       
             
        
    return module.ptr();
}
